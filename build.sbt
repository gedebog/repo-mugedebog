name := """mugedebog"""

version := "1.0-SNAPSHOT"

lazy val root = (project in file(".")).enablePlugins(PlayJava, PlayEbean)

scalaVersion := "2.11.7"

libraryDependencies ++= Seq(
  javaJdbc,
  cache,
  javaWs,
  "org.mindrot" % "jbcrypt" % "0.3m",
  "org.postgresql" % "postgresql" % "9.4-1206-jdbc42",
  "postgresql" % "postgresql" % "9.1-901-1.jdbc4",
  "org.avaje" % "ebean" % "2.7.3",
  "javax.persistence" % "persistence-api" % "1.0.2",
  "com.typesafe.play" %% "play-mailer" % "5.0.0",
  "commons-io" % "commons-io" % "2.4",
  "net.tanesha.recaptcha4j" % "recaptcha4j" % "0.0.7",
  "net.coobird" % "thumbnailator" % "0.4.8",
  "com.cloudinary" % "cloudinary-core" % "1.13.0",
  "com.cloudinary" % "cloudinary-http44" % "1.13.0"
)

libraryDependencies += jdbc
