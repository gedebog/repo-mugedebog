/**
 * Created by tri.jaruto on 9/7/2017.
 */
$(document).ready(function(){
    componentmainvalidate();
    componentcheckedusername();

    $("#upload_foto_profile_div_id").on('click', function() {
        $("#upload_foto_profile_modal_id").modal('show');
        $("#ufpa_filename_img_id").attr("src",'@routes.Assets.versioned("semantic/assets/images/avatar/nan.jpg")')
    });

    $("#ufpa_filename_input_id").change(function(){
        uploadFotoProfile(this);
    });

    $("#masterpermissionsformadmin_add_button_id").on('click', function() {
        getmasterformadmin();
        oncheckboxevent();
        $("#masterpermissionsformadmin_modal_id").modal({
            autofocus : false,
            onApprove : function() {
                componentdetailvalidate();
                return false;
            }
        }).modal('show');
        $("#appmasterformadmin_dropdown_id").html("");
        $("#appmasterformadmin_dropdown_default_id").html("Select Form");
        $("#app_master_permissions_form_admin_save_button_id").val("");
        $("#app_master_permissions_form_admin_save_button_id").val($("#app_master_permissions_form_admin_save_button_id").val() + '@Constans.ACTION_VALUE_NEW');

    });

   // @if(action.equals(Constans.ACTION_VIEW)){
        masterpermissionsformadmindatatable();
    //}

});

function oncheckboxevent(){
    $("#ampfa_isnew_div_toggle_id").checkbox({
        onChecked: function() {
            $("#ampfa_isnew_toggle_id").val("").val($("#ampfa_isnew_toggle_id").val() + "true");
        },
        onUnchecked: function() {
            $("#ampfa_isnew_toggle_id").val("").val($("#ampfa_isnew_toggle_id").val() + "false");
        }
    });
}

function uploadFotoProfile(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();
        reader.onload = function (e) {
            $("#ufpa_filename_img_id").attr('src', e.target.result).width(678).height(622);
        }
        reader.readAsDataURL(input.files[0]);
    }
}

function componentmainvalidate(){
    $("#main_form_id")
        .form({
            fields: {
                username: {
                    identifier  : 'uaa_username_input_id',
                    rules: [
                        {
                            type   : 'empty',
                            prompt : 'Please enter your e-mail'
                        },
                        {
                            type   : 'email',
                            prompt : 'Please enter a valid e-mail'
                        },
                        {
                            type   : 'different[uaa_username_caption_input_id]',
                            prompt : 'Email already exist'
                        }
                    ]
                },
                userpassword: {
                    identifier  : 'uaa_userpassword_id',
                    rules: [
                        {
                            type   : 'empty',
                            prompt : 'Please enter your password'
                        },
                        {
                            type   : 'length[6]',
                            prompt : 'Your password must be at least 6 characters'
                        }
                    ]
                },
                userpasswordconfirm: {
                    identifier  : 'uaa_userpasswordconfirm_id',
                    rules: [
                        {
                            type   : 'empty',
                            prompt : 'Please enter your confirm password'
                        },
                        {
                            type   : 'length[6]',
                            prompt : 'Your password must be at least 6 characters'
                        },
                        {
                            type   : 'match[uaa_userpassword_id]',
                            prompt : 'Please put the same value in both fields'
                        }
                    ]
                },
                usertype: {
                    identifier  : 'uaa_usertype_input_id',
                    rules: [
                        {
                            type   : 'empty',
                            prompt : 'Please select a usertype'
                        }
                    ]
                },
                name: {
                    identifier  : 'upa_name_input_id',
                    rules: [
                        {
                            type   : 'empty',
                            prompt : 'Please enter your name'
                        }
                    ]
                },
                lastname: {
                    identifier  : 'upa_lastname_input_id',
                    rules: [
                        {
                            type   : 'empty',
                            prompt : 'Please enter your lastname'
                        }
                    ]
                },
                birthplace: {
                    identifier  : 'upa_birthplace_input_id',
                    rules: [
                        {
                            type   : 'empty',
                            prompt : 'Please enter your birthplace'
                        }
                    ]
                },
                birthdate: {
                    identifier  : 'upa_birthdate_input_id',
                    rules: [
                        {
                            type   : 'empty',
                            prompt : 'Please enter your birthdate'
                        },
                        {
                            type   : "regExp[/^(0[1-9]|[12][0-9]|3[01])[- /.](0[1-9]|1[012])[- /.](19|20)\\d\\d$/]",
                            prompt : "Please enter a valid dd/mm/yyyy birthdate"
                        }
                    ]
                },
                up_isgender: {
                    identifier  : 'upa_isgender_input_id',
                    rules: [
                        {
                            type   : 'empty',
                            prompt : 'Please select a gender'
                        }
                    ]
                }

            }
        });
}

function componentdetailvalidate(){
    $("#detail_form_id")
        .form({
            fields: {
                username: {
                    identifier  : 'appmasterformadmin_input_id',
                    rules: [
                        {
                            type   : 'empty',
                            prompt : 'Please select a form'
                        }
                    ]
                }
            }
        });
}


function componentcheckedusername() {
    $('#uaa_username_input_id').on("input", function () {
        var uaa_username_name = this.value;
        $.ajax({
            url: "@Configuration.root().getString("server.schemes")@request().host()/ws/administrator/dasboard/form/main/@URLEncDec.URLEncode(AESCrypto.encrypt(appMasterFormAdmin.getAmfa_name()))/@URLEncDec.URLEncode(AESCrypto.encrypt(appMasterFormAdmin.getAmfa_filenamescalahtml()))/@URLEncDec.URLEncode(AESCrypto.encrypt(ConstansTable.TABLE_USER_ACCOUNT_ADMIN))/"+uaa_username_name,
            cache: false,
            success: function(data){
            if(data=="true"){
                $("#uaa_username_caption_input_id").val(uaa_username_name);
            }else{
                $("#uaa_username_caption_input_id").val("");
            }
        }
    });
    } );
}

function masterpermissionsformadmindatatable(){
    $('#table_masterpermissionsformadmin_id').DataTable( {
        "processing": true,
        "serverSide": true,
        "cache": false,
        "ajax": {
            "data": function(){
                var info = $('#table_masterpermissionsformadmin_id').DataTable();
                $('#table_masterpermissionsformadmin_id').DataTable().ajax.url(
                    "@Configuration.root().getString("server.schemes")@request().host()/ws/administrator/dasboard/form/main/detail/list/@URLEncDec.URLEncode(AESCrypto.encrypt(appMasterFormAdmin.getAmfa_name()))/@URLEncDec.URLEncode(AESCrypto.encrypt(appMasterFormAdmin.getAmfa_filenamescalahtml()))/@URLEncDec.URLEncode(AESCrypto.encrypt(ConstansTable.TABLE_APP_MASTER_PERMISSONS_FORM_ADMIN))/@URLEncDec.URLEncode(AESCrypto.encrypt(userAccountAdmin.getUaa_autoindex().toString))/"+info.page.info().page+"/"+info.page.info().length
                );

            }
        },
        "dom": 't<"row"<"ui grid"<"three wide left aligned column"<"field"l>><"five wide center aligned column"<"field"i>><"eight wide right aligned column"<"field"p>>>>',
        "createdRow": function ( row, data, index ) {
            $('td', row).eq(0).addClass('center aligned');
            $('td', row).eq(2).addClass('center aligned');
            $('td', row).eq(3).addClass('center aligned');
            $('td', row).eq(4).addClass('center aligned');
            $('td', row).eq(5).addClass('center aligned');
            $('td', row).eq(6).addClass('center aligned');
            $('td', row).eq(7).addClass('center aligned');
            $('td', row).eq(8).addClass('center aligned');
            $('td', row).eq(9).addClass('center aligned');
            $('td', row).eq(10).addClass('center aligned');
        }
    });
}

function getmasterformadmin(){
    $.ajax({
        type: 'GET',
        url: '@Configuration.root().getString("server.schemes")@request().host()/ws/administrator/dasboard/form/main/detail/@URLEncDec.URLEncode(AESCrypto.encrypt(appMasterFormAdmin.getAmfa_name()))/@URLEncDec.URLEncode(AESCrypto.encrypt(appMasterFormAdmin.getAmfa_filenamescalahtml()))/@URLEncDec.URLEncode(AESCrypto.encrypt(ConstansTable.TABLE_APP_MASTER_FORM_ADMIN))/@URLEncDec.URLEncode(AESCrypto.encrypt(userAccountAdmin.getUaa_autoindex().toString()))',
        success: function(data){
            $("#appmasterformadmin_dropdown_id").html("");
            $.each(data, function(key, value){
                $("#appmasterformadmin_dropdown_id").append($('<div class="item" data-value="'+value.amfa_autoindex+'"></div>').html(value.amfa_name));
            });
        }
    });
}