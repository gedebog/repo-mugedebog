package models.user.app;

import com.avaje.ebean.Model;
import com.avaje.ebean.annotation.DbComment;
import com.fasterxml.jackson.annotation.JsonBackReference;
import models.user.user.UserAccount;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import java.util.Date;


/**
 * Created by tri.jaruto on 21/11/2016.
 */
@DbComment("Table AppMasterPermissionsForm")
@Entity
public class AppMasterPermissionsForm extends Model {

    @Id
    private long ampf_autoindex;

    @ManyToOne
    @JoinColumn(name = "ampf_useraccount", referencedColumnName = "ua_autoindex")
    @JsonBackReference
    private UserAccount ampf_useraccount;

    @ManyToOne
    @JoinColumn(name = "ampf_appmasterform", referencedColumnName = "amf_autoindex")
    @JsonBackReference
    private AppMasterForm ampf_appmasterform;

    private boolean ampf_isnew;
    private boolean ampf_isselect;
    private boolean ampf_isinsert;
    private boolean ampf_isupdate;
    private boolean ampf_isdelete;
    private boolean ampf_isprint;
    private boolean ampf_isexport;
    private boolean ampf_isimport;
    private boolean ampf_isactive;

    private Date ampf_created;
    private String ampf_creater;
    private Date ampf_updated;
    private String ampf_updater;

    public AppMasterPermissionsForm(){}

    public long getAmpf_autoindex() {
        return ampf_autoindex;
    }

    public void setAmpf_autoindex(long ampf_autoindex) {
        this.ampf_autoindex = ampf_autoindex;
    }

    public UserAccount getAmpf_useraccount() {
        return ampf_useraccount;
    }

    public void setAmpf_useraccount(UserAccount ampf_useraccount) {
        this.ampf_useraccount = ampf_useraccount;
    }

    public AppMasterForm getAmpf_appmasterform() {
        return ampf_appmasterform;
    }

    public void setAmpf_appmasterform(AppMasterForm ampf_appmasterform) {
        this.ampf_appmasterform = ampf_appmasterform;
    }

    public boolean isAmpf_isnew() {
        return ampf_isnew;
    }

    public void setAmpf_isnew(boolean ampf_isnew) {
        this.ampf_isnew = ampf_isnew;
    }

    public boolean isAmpf_isselect() {
        return ampf_isselect;
    }

    public void setAmpf_isselect(boolean ampf_isselect) {
        this.ampf_isselect = ampf_isselect;
    }

    public boolean isAmpf_isinsert() {
        return ampf_isinsert;
    }

    public void setAmpf_isinsert(boolean ampf_isinsert) {
        this.ampf_isinsert = ampf_isinsert;
    }

    public boolean isAmpf_isupdate() {
        return ampf_isupdate;
    }

    public void setAmpf_isupdate(boolean ampf_isupdate) {
        this.ampf_isupdate = ampf_isupdate;
    }

    public boolean isAmpf_isdelete() {
        return ampf_isdelete;
    }

    public void setAmpf_isdelete(boolean ampf_isdelete) {
        this.ampf_isdelete = ampf_isdelete;
    }

    public boolean isAmpf_isprint() {
        return ampf_isprint;
    }

    public void setAmpf_isprint(boolean ampf_isprint) {
        this.ampf_isprint = ampf_isprint;
    }

    public boolean isAmpf_isexport() {
        return ampf_isexport;
    }

    public void setAmpf_isexport(boolean ampf_isexport) {
        this.ampf_isexport = ampf_isexport;
    }

    public boolean isAmpf_isimport() {
        return ampf_isimport;
    }

    public void setAmpf_isimport(boolean ampf_isimport) {
        this.ampf_isimport = ampf_isimport;
    }

    public boolean isAmpf_isactive() {
        return ampf_isactive;
    }

    public void setAmpf_isactive(boolean ampf_isactive) {
        this.ampf_isactive = ampf_isactive;
    }

    public Date getAmpf_created() {
        return ampf_created;
    }

    public void setAmpf_created(Date ampf_created) {
        this.ampf_created = ampf_created;
    }

    public String getAmpf_creater() {
        return ampf_creater;
    }

    public void setAmpf_creater(String ampf_creater) {
        this.ampf_creater = ampf_creater;
    }

    public Date getAmpf_updated() {
        return ampf_updated;
    }

    public void setAmpf_updated(Date ampf_updated) {
        this.ampf_updated = ampf_updated;
    }

    public String getAmpf_updater() {
        return ampf_updater;
    }

    public void setAmpf_updater(String ampf_updater) {
        this.ampf_updater = ampf_updater;
    }
}
