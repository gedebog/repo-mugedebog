package models.user.app;

import com.avaje.ebean.Model;
import com.avaje.ebean.annotation.DbComment;
import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

/**
 * Created by tri.jaruto on 21/11/2016.
 */
@DbComment("Table AppMasterStructure")
@Entity
public class AppMasterStructure extends Model {

    @Id
    @JsonManagedReference
    private long ams_autoindex;

    @ManyToOne
    @JoinColumn(name = "ams_appmaster_parent", referencedColumnName = "am_autoindex")
    @JsonBackReference
    private AppMaster ams_appmaster_parent;

    @ManyToOne
    @JoinColumn(name = "ams_appmaster_child", referencedColumnName = "am_autoindex")
    @JsonBackReference
    private AppMaster ams_appmaster_child;

    public AppMasterStructure(){}

    public long getAms_autoindex() {
        return ams_autoindex;
    }

    public void setAms_autoindex(long ams_autoindex) {
        this.ams_autoindex = ams_autoindex;
    }

    public AppMaster getAms_appmaster_parent() {
        return ams_appmaster_parent;
    }

    public void setAmas_appmaster_parent(AppMaster ams_appmaster_parent) {
        this.ams_appmaster_parent = ams_appmaster_parent;
    }

    public AppMaster getAms_appmaster_child() {
        return ams_appmaster_child;
    }

    public void setAms_appmaster_child(AppMaster ams_appmaster_child) {
        this.ams_appmaster_child = ams_appmaster_child;
    }
}
