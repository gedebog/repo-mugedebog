package models.user.app;

import com.avaje.ebean.Model;
import com.avaje.ebean.annotation.DbComment;
import com.fasterxml.jackson.annotation.JsonBackReference;
import play.data.validation.Constraints;

import javax.persistence.*;
import java.util.Date;


/**
 * Created by tri.jaruto on 21/11/2016.
 */
@DbComment("Table AppMasterForm")
@Entity
public class AppMasterForm extends Model {

    @Id
    private long amf_autoindex;

    @OneToOne
    @JoinColumn(name = "amf_appmaster", referencedColumnName = "am_autoindex")
    @Column(unique=true, nullable = false)
    @JsonBackReference
    private AppMaster amf_appmaster;

    private String amf_name;
    @Column(length = 100, unique=true, nullable = false)
    @Constraints.Required
    private String amf_filenamescalahtml;
    private boolean amf_isactive;

    private Date amf_created;
    private String amf_creater;

    private Date amf_updated;
    private String amf_updater;

    public AppMasterForm(){}

    public long getAmf_autoindex() {
        return amf_autoindex;
    }

    public void setAmf_autoindex(long amf_autoindex) {
        this.amf_autoindex = amf_autoindex;
    }

    public AppMaster getAmf_appmaster() {
        return amf_appmaster;
    }

    public void setAmf_appmaster(AppMaster amf_appmaster) {
        this.amf_appmaster = amf_appmaster;
    }

    public String getAmf_name() {
        return amf_name;
    }

    public void setAmf_name(String amf_name) {
        this.amf_name = amf_name;
    }

    public String getAmf_filenamescalahtml() {
        return amf_filenamescalahtml;
    }

    public void setAmf_filenamescalahtml(String amf_filenamescalahtml) {
        this.amf_filenamescalahtml = amf_filenamescalahtml;
    }

    public boolean isAmf_isactive() {
        return amf_isactive;
    }

    public void setAmf_isactive(boolean amf_isactive) {
        this.amf_isactive = amf_isactive;
    }

    public Date getAmf_created() {
        return amf_created;
    }

    public void setAmf_created(Date amf_created) {
        this.amf_created = amf_created;
    }

    public String getAmf_creater() {
        return amf_creater;
    }

    public void setAmf_creater(String amf_creater) {
        this.amf_creater = amf_creater;
    }

    public Date getAmf_updated() {
        return amf_updated;
    }

    public void setAmf_updated(Date amf_updated) {
        this.amf_updated = amf_updated;
    }

    public String getAmf_updater() {
        return amf_updater;
    }

    public void setAmf_updater(String amf_updater) {
        this.amf_updater = amf_updater;
    }
}
