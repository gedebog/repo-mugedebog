package models.user.app;

import com.avaje.ebean.Model;
import com.avaje.ebean.annotation.DbComment;
import com.fasterxml.jackson.annotation.JsonBackReference;
import play.data.validation.Constraints;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;


/**
 * Created by tri.jaruto on 21/11/2016.
 */
@DbComment("Table AppMasterOrganization")
@Entity
public class AppMasterOrganization extends Model {

    @Id
    private long amo_autoindex;

    private String amo_code;
    private String amo_name;
    private boolean amo_isactive;

    private Date amo_created;
    private String amo_creater;

    private Date amo_updated;
    private String amo_updater;

    @OneToMany(mappedBy = "amos_appmasterorganization_parent")
    @JsonBackReference
    private List<AppMasterOrganizationStructure> amo_appmasterorganizationstructure_parent = new ArrayList<AppMasterOrganizationStructure>();

    @OneToMany(mappedBy = "amos_appmasterorganization_child")
    @JsonBackReference
    private List<AppMasterOrganizationStructure> amo_appmasterorganizationstructure_child = new ArrayList<AppMasterOrganizationStructure>();

    public AppMasterOrganization(){}

    public long getAmo_autoindex() {
        return amo_autoindex;
    }

    public void setAmo_autoindex(long amo_autoindex) {
        this.amo_autoindex = amo_autoindex;
    }

    public String getAmo_code() {
        return amo_code;
    }

    public void setAmo_code(String amo_code) {
        this.amo_code = amo_code;
    }

    public String getAmo_name() {
        return amo_name;
    }

    public void setAmo_name(String amo_name) {
        this.amo_name = amo_name;
    }

    public boolean isAmo_isactive() {
        return amo_isactive;
    }

    public void setAmo_isactive(boolean amo_isactive) {
        this.amo_isactive = amo_isactive;
    }

    public Date getAmo_created() {
        return amo_created;
    }

    public void setAmo_created(Date amo_created) {
        this.amo_created = amo_created;
    }

    public String getAmo_creater() {
        return amo_creater;
    }

    public void setAmo_creater(String amo_creater) {
        this.amo_creater = amo_creater;
    }

    public Date getAmo_updated() {
        return amo_updated;
    }

    public void setAmo_updated(Date amo_updated) {
        this.amo_updated = amo_updated;
    }

    public String getAmo_updater() {
        return amo_updater;
    }

    public void setAmo_updater(String amo_updater) {
        this.amo_updater = amo_updater;
    }

    public List<AppMasterOrganizationStructure> getAmo_appmasterorganizationstructure_parent() {
        return amo_appmasterorganizationstructure_parent;
    }

    public void setAmo_appmasterorganizationstructure_parent(List<AppMasterOrganizationStructure> amo_appmasterorganizationstructure_parent) {
        this.amo_appmasterorganizationstructure_parent = amo_appmasterorganizationstructure_parent;
    }

    public List<AppMasterOrganizationStructure> getAmo_appmasterorganizationstructure_child() {
        return amo_appmasterorganizationstructure_child;
    }

    public void setAmo_appmasterorganizationstructure_child(List<AppMasterOrganizationStructure> amo_appmasterorganizationstructure_child) {
        this.amo_appmasterorganizationstructure_child = amo_appmasterorganizationstructure_child;
    }
}
