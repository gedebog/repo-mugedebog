package models.user.app;

import com.avaje.ebean.Model;
import com.avaje.ebean.annotation.DbComment;
import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

/**
 * Created by tri.jaruto on 21/11/2016.
 */
@DbComment("Table AppMasterOrganizationStructure")
@Entity
public class AppMasterOrganizationStructure extends Model {

    @Id
    @JsonManagedReference
    private long amos_autoindex;

    @ManyToOne
    @JoinColumn(name = "amos_appmasterorganization_parent", referencedColumnName = "amo_autoindex")
    @JsonBackReference
    private AppMasterOrganization amos_appmasterorganization_parent;

    @ManyToOne
    @JoinColumn(name = "amos_appmasterorganization_child", referencedColumnName = "amo_autoindex")
    @JsonBackReference
    private AppMasterOrganization amos_appmasterorganization_child;

    public AppMasterOrganizationStructure(){}

    public long getAmos_autoindex() {
        return amos_autoindex;
    }

    public void setAmos_autoindex(long amos_autoindex) {
        this.amos_autoindex = amos_autoindex;
    }

    public AppMasterOrganization getAmos_appmasterorganization_parent() {
        return amos_appmasterorganization_parent;
    }

    public void setAmos_appmasterorganization_parent(AppMasterOrganization amos_appmasterorganization_parent) {
        this.amos_appmasterorganization_parent = amos_appmasterorganization_parent;
    }

    public AppMasterOrganization getAmos_appmasterorganization_child() {
        return amos_appmasterorganization_child;
    }

    public void setAmos_appmasterorganization_child(AppMasterOrganization amos_appmasterorganization_child) {
        this.amos_appmasterorganization_child = amos_appmasterorganization_child;
    }
}
