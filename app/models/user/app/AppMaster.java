package models.user.app;

import com.avaje.ebean.Model;
import com.avaje.ebean.annotation.DbComment;
import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by tri.jaruto on 21/11/2016.
 */
@DbComment("Table AppMaster")
@Entity
public class AppMaster extends Model {

    @Id
    @JsonManagedReference
    private long am_autoindex;
    private String am_name;
    private short am_type;
    private short am_level;
    private boolean am_isgroup;
    private boolean am_isactive;

    private Date am_created;
    private String am_creater;

    private Date am_updated;
    private String am_updater;

    @OneToMany(mappedBy = "ams_appmaster_parent")
    @JsonBackReference
    private List<AppMasterStructure> am_appmasterstructure_parent = new ArrayList<AppMasterStructure>();

    @OneToMany(mappedBy = "ams_appmaster_child")
    @JsonBackReference
    private List<AppMasterStructure> am_appmasterstructure_child = new ArrayList<AppMasterStructure>();

    public AppMaster(){}

    public long getAm_autoindex() {
        return am_autoindex;
    }

    public void setAm_autoindex(long am_autoindex) {
        this.am_autoindex = am_autoindex;
    }

    public String getAm_name() {
        return am_name;
    }

    public void setAm_name(String am_name) {
        this.am_name = am_name;
    }

    public short getAm_type() {
        return am_type;
    }

    public void setAm_type(short am_type) {
        this.am_type = am_type;
    }

    public short getAm_level() {
        return am_level;
    }

    public void setAm_level(short am_level) {
        this.am_level = am_level;
    }

    public boolean isAm_isgroup() {
        return am_isgroup;
    }

    public void setAm_isgroup(boolean am_isgroup) {
        this.am_isgroup = am_isgroup;
    }

    public boolean isAm_isactive() {
        return am_isactive;
    }

    public void setAm_isactive(boolean am_isactive) {
        this.am_isactive = am_isactive;
    }

    public Date getAm_created() {
        return am_created;
    }

    public void setAm_created(Date am_created) {
        this.am_created = am_created;
    }

    public String getAm_creater() {
        return am_creater;
    }

    public void setAm_creater(String am_creater) {
        this.am_creater = am_creater;
    }

    public Date getAm_updated() {
        return am_updated;
    }

    public void setAm_updated(Date am_updated) {
        this.am_updated = am_updated;
    }

    public String getAm_updater() {
        return am_updater;
    }

    public void setAm_updater(String am_updater) {
        this.am_updater = am_updater;
    }

    public List<AppMasterStructure> getAm_appmasterstructure_parent() {
        return am_appmasterstructure_parent;
    }

    public void setAm_appmasterstructure_parent(List<AppMasterStructure> am_appmasterstructure_parent) {
        this.am_appmasterstructure_parent = am_appmasterstructure_parent;
    }

    public List<AppMasterStructure> getAm_appmasterstructure_child() {
        return am_appmasterstructure_child;
    }

    public void setAm_appmasterstructure_child(List<AppMasterStructure> am_appmasterstructure_child) {
        this.am_appmasterstructure_child = am_appmasterstructure_child;
    }
}
