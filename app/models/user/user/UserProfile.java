package models.user.user;

import com.avaje.ebean.Model;
import com.avaje.ebean.annotation.DbComment;
import com.fasterxml.jackson.annotation.JsonBackReference;

import javax.persistence.*;
import java.util.Date;

/**
 * Created by tri.jaruto on 21/11/2016.
 */
@DbComment("Table UserProfile")
@Entity
public class UserProfile extends Model {

    @Id
    private long up_autoindex;

    @OneToOne
    @JoinColumn(name = "up_useraccount", referencedColumnName = "ua_autoindex")
    @Column(unique=true, nullable = false)
    @JsonBackReference
    private UserAccount up_useraccount;

    private String up_name;
    private String up_nickname;

    @Column(columnDefinition = "date")
    private Date up_birthdate;
    private String up_birthplace;
    private boolean up_isgender;

    private Date up_created;
    private String up_creater;

    private Date up_updated;
    private String up_updater;

    public UserProfile(){}

    public long getUp_autoindex() {
        return up_autoindex;
    }

    public void setUp_autoindex(long up_autoindex) {
        this.up_autoindex = up_autoindex;
    }

    public UserAccount getUp_useraccount() {
        return up_useraccount;
    }

    public void setUp_useraccount(UserAccount up_useraccount) {
        this.up_useraccount = up_useraccount;
    }

    public String getUp_name() {
        return up_name;
    }

    public void setUp_name(String up_name) {
        this.up_name = up_name;
    }

    public String getUp_nickname() {
        return up_nickname;
    }

    public void setUp_nickname(String up_nickname) {
        this.up_nickname = up_nickname;
    }

    public Date getUp_birthdate() {
        return up_birthdate;
    }

    public void setUp_birthdate(Date up_birthdate) {
        this.up_birthdate = up_birthdate;
    }

    public String getUp_birthplace() {
        return up_birthplace;
    }

    public void setUp_birthplace(String up_birthplace) {
        this.up_birthplace = up_birthplace;
    }

    public boolean isUp_isgender() {
        return up_isgender;
    }

    public void setUp_isgender(boolean up_isgender) {
        this.up_isgender = up_isgender;
    }

    public Date getUp_created() {
        return up_created;
    }

    public void setUp_created(Date up_created) {
        this.up_created = up_created;
    }

    public String getUp_creater() {
        return up_creater;
    }

    public void setUp_creater(String up_creater) {
        this.up_creater = up_creater;
    }

    public Date getUp_updated() {
        return up_updated;
    }

    public void setUp_updated(Date up_updated) {
        this.up_updated = up_updated;
    }

    public String getUp_updater() {
        return up_updater;
    }

    public void setUp_updater(String up_updater) {
        this.up_updater = up_updater;
    }
}
