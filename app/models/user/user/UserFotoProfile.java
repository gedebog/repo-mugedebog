package models.user.user;

import com.avaje.ebean.Model;
import com.avaje.ebean.annotation.DbComment;
import com.fasterxml.jackson.annotation.JsonBackReference;

import javax.persistence.*;
import java.util.Date;

/**
 * Created by tri.jaruto on 21/11/2016.
 */
@DbComment("Table UserFotoProfile")
@Entity
public class UserFotoProfile extends Model {

    @Id
    private long ufp_autoindex;

    @OneToOne
    @JoinColumn(name = "ufp_useraccount", referencedColumnName = "ua_autoindex")
    @Column(unique=true, nullable = false)
    @JsonBackReference
    private UserAccount ufp_useraccount;

    private String ufp_name;
    private String ufp_status;
    private String ufp_note;
    private String ufp_filename;

    private Date ufp_created;
    private String ufp_creater;

    private Date ufp_updated;
    private String ufp_updater;

    public UserFotoProfile(){}

    public long getUfp_autoindex() {
        return ufp_autoindex;
    }

    public void setUfp_autoindex(long ufp_autoindex) {
        this.ufp_autoindex = ufp_autoindex;
    }

    public UserAccount getUfp_useraccount() {
        return ufp_useraccount;
    }

    public void setUfp_useraccount(UserAccount ufp_useraccount) {
        this.ufp_useraccount = ufp_useraccount;
    }

    public String getUfp_name() {
        return ufp_name;
    }

    public void setUfp_name(String ufp_name) {
        this.ufp_name = ufp_name;
    }

    public String getUfp_status() {
        return ufp_status;
    }

    public void setUfp_status(String ufp_status) {
        this.ufp_status = ufp_status;
    }

    public String getUfp_note() {
        return ufp_note;
    }

    public void setUfp_note(String ufp_note) {
        this.ufp_note = ufp_note;
    }

    public String getUfp_filename() {
        return ufp_filename;
    }

    public void setUfp_filename(String ufp_filename) {
        this.ufp_filename = ufp_filename;
    }

    public Date getUfp_created() {
        return ufp_created;
    }

    public void setUfp_created(Date ufp_created) {
        this.ufp_created = ufp_created;
    }

    public String getUfp_creater() {
        return ufp_creater;
    }

    public void setUfp_creater(String ufp_creater) {
        this.ufp_creater = ufp_creater;
    }

    public Date getUfp_updated() {
        return ufp_updated;
    }

    public void setUfp_updated(Date ufp_updated) {
        this.ufp_updated = ufp_updated;
    }

    public String getUfp_updater() {
        return ufp_updater;
    }

    public void setUfp_updater(String ufp_updater) {
        this.ufp_updater = ufp_updater;
    }
}
