package models.user.user;

import com.avaje.ebean.Model;
import com.avaje.ebean.annotation.DbComment;
import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import play.data.validation.Constraints;
import javax.persistence.*;
import java.util.Date;
import javax.persistence.Entity;

/**
 * Created by tri.jaruto on 5/10/2017.
 */
@DbComment("Table UserAccount")
@Entity
public class UserAccount extends Model {

    @Id
    @JsonManagedReference
    private long ua_autoindex;

    @Column(length = 100, unique=true, nullable = false)
    @Constraints.Required
    private String ua_username;
    @Column(length = 100, nullable = false)
    @Constraints.Required
    private String ua_userpassword;
    private boolean ua_isactive;
    private String ua_activationcode;
    private boolean ua_isforgotpassword;

    private Date ua_created;
    public String ua_creater;

    private Date ua_updated;
    public String ua_updater;

    @OneToOne(mappedBy = "up_useraccount")
    @JsonBackReference
    public UserProfile ua_userprofile = new UserProfile();

    public UserAccount(){}

    public long getUa_autoindex() {
        return ua_autoindex;
    }

    public void setUa_autoindex(long ua_autoindex) {
        this.ua_autoindex = ua_autoindex;
    }

    public String getUa_username() {
        return ua_username;
    }

    public void setUa_username(String ua_username) {
        this.ua_username = ua_username;
    }

    public String getUa_userpassword() {
        return ua_userpassword;
    }

    public void setUa_userpassword(String ua_userpassword) {
        this.ua_userpassword = ua_userpassword;
    }

    public boolean isUa_isactive() {
        return ua_isactive;
    }

    public void setUa_isactive(boolean ua_isactive) {
        this.ua_isactive = ua_isactive;
    }

    public String getUa_activationcode() {
        return ua_activationcode;
    }

    public void setUa_activationcode(String ua_activationcode) {
        this.ua_activationcode = ua_activationcode;
    }

    public boolean isUa_isforgotpassword() {
        return ua_isforgotpassword;
    }

    public void setUa_isforgotpassword(boolean ua_isforgotpassword) {
        this.ua_isforgotpassword = ua_isforgotpassword;
    }

    public Date getUa_created() {
        return ua_created;
    }

    public void setUa_created(Date ua_created) {
        this.ua_created = ua_created;
    }

    public String getUa_creater() {
        return ua_creater;
    }

    public void setUa_creater(String ua_creater) {
        this.ua_creater = ua_creater;
    }

    public Date getUa_updated() {
        return ua_updated;
    }

    public void setUa_updated(Date ua_updated) {
        this.ua_updated = ua_updated;
    }

    public String getUa_updater() {
        return ua_updater;
    }

    public void setUa_updater(String ua_updater) {
        this.ua_updater = ua_updater;
    }

    public UserProfile getUa_userprofile() {
        return ua_userprofile;
    }

    public void setUa_userprofile(UserProfile ua_userprofile) {
        this.ua_userprofile = ua_userprofile;
    }
}
