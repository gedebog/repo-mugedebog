package models.administrator.user;

import com.avaje.ebean.Model;
import com.avaje.ebean.annotation.DbComment;
import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import play.data.validation.Constraints;

import javax.persistence.*;
import java.util.Date;

/**
 * Created by tri.jaruto on 21/11/2016.
 */
@DbComment("Table UserAccountAdmin")
@Entity
public class UserAccountAdmin extends Model {

    @Id
    @JsonManagedReference
    private long uaa_autoindex;

    @Column(length = 100, unique=true, nullable = false)
    @Constraints.Required
    private String uaa_username;
    @Column(length = 100, nullable = false)
    @Constraints.Required
    private String uaa_userpassword;
    private short uaa_usertype;
    private boolean uaa_isactive;
    private String uaa_activationcode;
    private boolean uaa_isforgotpassword;

    private Date uaa_created;
    private String uaa_creater;

    private Date uaa_updated;
    private String uaa_updater;

    @OneToOne(mappedBy = "upa_useraccountadmin")
    @JsonBackReference
    private UserProfileAdmin uaa_userprofileadmin = new UserProfileAdmin();

    public UserAccountAdmin(){}

    public long getUaa_autoindex() {
        return uaa_autoindex;
    }

    public void setUaa_autoindex(long uaa_autoindex) {
        this.uaa_autoindex = uaa_autoindex;
    }

    public String getUaa_username() {
        return uaa_username;
    }

    public void setUaa_username(String uaa_username) {
        this.uaa_username = uaa_username;
    }

    public String getUaa_userpassword() {
        return uaa_userpassword;
    }

    public void setUaa_userpassword(String uaa_userpassword) {
        this.uaa_userpassword = uaa_userpassword;
    }

    public short getUaa_usertype() {
        return uaa_usertype;
    }

    public void setUaa_usertype(short uaa_usertype) {
        this.uaa_usertype = uaa_usertype;
    }

    public boolean isUaa_isactive() {
        return uaa_isactive;
    }

    public void setUaa_isactive(boolean uaa_isactive) {
        this.uaa_isactive = uaa_isactive;
    }

    public String getUaa_activationcode() {
        return uaa_activationcode;
    }

    public void setUaa_activationcode(String uaa_activationcode) {
        this.uaa_activationcode = uaa_activationcode;
    }

    public boolean isUaa_isforgotpassword() {
        return uaa_isforgotpassword;
    }

    public void setUaa_isforgotpassword(boolean uaa_isforgotpassword) {
        this.uaa_isforgotpassword = uaa_isforgotpassword;
    }

    public Date getUaa_created() {
        return uaa_created;
    }

    public void setUaa_created(Date uaa_created) {
        this.uaa_created = uaa_created;
    }

    public String getUaa_creater() {
        return uaa_creater;
    }

    public void setUaa_creater(String uaa_creater) {
        this.uaa_creater = uaa_creater;
    }

    public Date getUaa_updated() {
        return uaa_updated;
    }

    public void setUaa_updated(Date uaa_updated) {
        this.uaa_updated = uaa_updated;
    }

    public String getUaa_updater() {
        return uaa_updater;
    }

    public void setUaa_updater(String uaa_updater) {
        this.uaa_updater = uaa_updater;
    }

    public UserProfileAdmin getUaa_userprofileadmin() {
        return uaa_userprofileadmin;
    }

    public void setUaa_userprofileadmin(UserProfileAdmin uaa_userprofileadmin) {
        this.uaa_userprofileadmin = uaa_userprofileadmin;
    }
}
