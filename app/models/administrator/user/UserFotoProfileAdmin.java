package models.administrator.user;

import com.avaje.ebean.Model;
import com.avaje.ebean.annotation.DbComment;
import com.fasterxml.jackson.annotation.JsonBackReference;
import javax.persistence.*;
import java.util.Date;

/**
 * Created by tri.jaruto on 21/11/2016.
 */
@DbComment("Table UserFotoProfileAdmin")
@Entity
public class UserFotoProfileAdmin extends Model {

    @Id
    private long ufpa_autoindex;

    @OneToOne
    @JoinColumn(name = "ufpa_useraccountadmin", referencedColumnName = "uaa_autoindex")
    @Column(unique=true, nullable = false)
    @JsonBackReference
    private UserAccountAdmin ufpa_useraccountadmin;

    private String ufpa_name;
    private String ufpa_status;
    private String ufpa_note;
    private String ufpa_filename;

    private Date ufpa_created;
    private String ufpa_creater;

    private Date ufpa_updated;
    private String ufpa_updater;


    public UserFotoProfileAdmin(){}

    public long getUfpa_autoindex() {
        return ufpa_autoindex;
    }

    public void setUfpa_autoindex(long ufpa_autoindex) {
        this.ufpa_autoindex = ufpa_autoindex;
    }

    public UserAccountAdmin getUfpa_useraccountadmin() {
        return ufpa_useraccountadmin;
    }

    public void setUfpa_useraccountadmin(UserAccountAdmin ufpa_useraccountadmin) {
        this.ufpa_useraccountadmin = ufpa_useraccountadmin;
    }

    public String getUfpa_name() {
        return ufpa_name;
    }

    public void setUfpa_name(String ufpa_name) {
        this.ufpa_name = ufpa_name;
    }

    public String getUfpa_status() {
        return ufpa_status;
    }

    public void setUfpa_status(String ufpa_status) {
        this.ufpa_status = ufpa_status;
    }

    public String getUfpa_note() {
        return ufpa_note;
    }

    public void setUfpa_note(String ufpa_note) {
        this.ufpa_note = ufpa_note;
    }

    public String getUfpa_filename() {
        return ufpa_filename;
    }

    public void setUfpa_filename(String ufpa_filename) {
        this.ufpa_filename = ufpa_filename;
    }

    public Date getUfpa_created() {
        return ufpa_created;
    }

    public void setUfpa_created(Date ufpa_created) {
        this.ufpa_created = ufpa_created;
    }

    public String getUfpa_creater() {
        return ufpa_creater;
    }

    public void setUfpa_creater(String ufpa_creater) {
        this.ufpa_creater = ufpa_creater;
    }

    public Date getUfpa_updated() {
        return ufpa_updated;
    }

    public void setUfpa_updated(Date ufpa_updated) {
        this.ufpa_updated = ufpa_updated;
    }

    public String getUfpa_updater() {
        return ufpa_updater;
    }

    public void setUfpa_updater(String ufpa_updater) {
        this.ufpa_updater = ufpa_updater;
    }
}
