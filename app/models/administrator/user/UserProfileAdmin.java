package models.administrator.user;

import com.avaje.ebean.Model;
import com.avaje.ebean.annotation.DbComment;
import com.fasterxml.jackson.annotation.JsonBackReference;
import javax.persistence.*;
import java.util.Date;

/**
 * Created by tri.jaruto on 21/11/2016.
 */
@DbComment("Table UserProfileAdmin")
@Entity
public class UserProfileAdmin extends Model {

    @Id
    private long upa_autoindex;

    @OneToOne
    @JoinColumn(name = "upa_useraccountadmin", referencedColumnName = "uaa_autoindex")
    @Column(unique=true, nullable = false)
    @JsonBackReference
    private UserAccountAdmin upa_useraccountadmin;

    private String upa_name;
    private String upa_nickname;

    @Column(columnDefinition = "date")
    private Date upa_birthdate;
    private String upa_birthplace;
    private boolean upa_isgender;

    private Date upa_created;
    private String upa_creater;

    private Date upa_updated;
    private String upa_updater;

    public UserProfileAdmin(){}

    public long getUpa_autoindex() {
        return upa_autoindex;
    }

    public void setUpa_autoindex(long upa_autoindex) {
        this.upa_autoindex = upa_autoindex;
    }

    public UserAccountAdmin getUpa_useraccountadmin() {
        return upa_useraccountadmin;
    }

    public void setUpa_useraccountadmin(UserAccountAdmin upa_useraccountadmin) {
        this.upa_useraccountadmin = upa_useraccountadmin;
    }

    public String getUpa_name() {
        return upa_name;
    }

    public void setUpa_name(String upa_name) {
        this.upa_name = upa_name;
    }

    public String getUpa_nickname() {
        return upa_nickname;
    }

    public void setUpa_nickname(String upa_nickname) {
        this.upa_nickname = upa_nickname;
    }

    public Date getUpa_birthdate() {
        return upa_birthdate;
    }

    public void setUpa_birthdate(Date upa_birthdate) {
        this.upa_birthdate = upa_birthdate;
    }

    public String getUpa_birthplace() {
        return upa_birthplace;
    }

    public void setUpa_birthplace(String upa_birthplace) {
        this.upa_birthplace = upa_birthplace;
    }

    public boolean isUpa_isgender() {
        return upa_isgender;
    }

    public void setUpa_isgender(boolean upa_isgender) {
        this.upa_isgender = upa_isgender;
    }

    public Date getUpa_created() {
        return upa_created;
    }

    public void setUpa_created(Date upa_created) {
        this.upa_created = upa_created;
    }

    public String getUpa_creater() {
        return upa_creater;
    }

    public void setUpa_creater(String upa_creater) {
        this.upa_creater = upa_creater;
    }

    public Date getUpa_updated() {
        return upa_updated;
    }

    public void setUpa_updated(Date upa_updated) {
        this.upa_updated = upa_updated;
    }

    public String getUpa_updater() {
        return upa_updater;
    }

    public void setUpa_updater(String upa_updater) {
        this.upa_updater = upa_updater;
    }
}
