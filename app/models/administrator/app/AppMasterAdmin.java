package models.administrator.app;

import com.avaje.ebean.Model;
import com.avaje.ebean.annotation.DbComment;
import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by tri.jaruto on 21/11/2016.
 */
@DbComment("Table AppMasterAdmin")
@Entity
public class AppMasterAdmin extends Model {

    @Id
    @JsonManagedReference
    private long ama_autoindex;
    private String ama_name;
    private short ama_type;
    private short ama_level;
    private boolean ama_isgroup;
    private boolean ama_isactive;

    private Date ama_created;
    private String ama_creater;

    private Date ama_updated;
    private String ama_updater;

    @OneToMany(mappedBy = "amas_appmasteradmin_parent")
    @JsonBackReference
    private List<AppMasterAdminStructure> ama_appmasteradminstructure_parent = new ArrayList<AppMasterAdminStructure>();

    @OneToMany(mappedBy = "amas_appmasteradmin_child")
    @JsonBackReference
    private List<AppMasterAdminStructure> ama_appmasteradminstructure_child = new ArrayList<AppMasterAdminStructure>();

    public AppMasterAdmin(){}

    public long getAma_autoindex() {
        return ama_autoindex;
    }

    public void setAma_autoindex(long ama_autoindex) {
        this.ama_autoindex = ama_autoindex;
    }

    public String getAma_name() {
        return ama_name;
    }

    public void setAma_name(String ama_name) {
        this.ama_name = ama_name;
    }

    public short getAma_type() {
        return ama_type;
    }

    public void setAma_type(short ama_type) {
        this.ama_type = ama_type;
    }

    public short getAma_level() {
        return ama_level;
    }

    public void setAma_level(short ama_level) {
        this.ama_level = ama_level;
    }

    public boolean isAma_isgroup() {
        return ama_isgroup;
    }

    public void setAma_isgroup(boolean ama_isgroup) {
        this.ama_isgroup = ama_isgroup;
    }

    public boolean isAma_isactive() {
        return ama_isactive;
    }

    public void setAma_isactive(boolean ama_isactive) {
        this.ama_isactive = ama_isactive;
    }

    public Date getAma_created() {
        return ama_created;
    }

    public void setAma_created(Date ama_created) {
        this.ama_created = ama_created;
    }

    public String getAma_creater() {
        return ama_creater;
    }

    public void setAma_creater(String ama_creater) {
        this.ama_creater = ama_creater;
    }

    public Date getAma_updated() {
        return ama_updated;
    }

    public void setAma_updated(Date ama_updated) {
        this.ama_updated = ama_updated;
    }

    public String getAma_updater() {
        return ama_updater;
    }

    public void setAma_updater(String ama_updater) {
        this.ama_updater = ama_updater;
    }

    public List<AppMasterAdminStructure> getAma_appmasteradminstructure_parent() {
        return ama_appmasteradminstructure_parent;
    }

    public void setAma_appmasteradminstructure_parent(List<AppMasterAdminStructure> ama_appmasteradminstructure_parent) {
        this.ama_appmasteradminstructure_parent = ama_appmasteradminstructure_parent;
    }

    public List<AppMasterAdminStructure> getAma_appmasteradminstructure_child() {
        return ama_appmasteradminstructure_child;
    }

    public void setAma_appmasteradminstructure_child(List<AppMasterAdminStructure> ama_appmasteradminstructure_child) {
        this.ama_appmasteradminstructure_child = ama_appmasteradminstructure_child;
    }
}
