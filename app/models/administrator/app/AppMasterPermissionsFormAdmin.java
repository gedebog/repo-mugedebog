package models.administrator.app;

import com.avaje.ebean.Model;
import com.avaje.ebean.annotation.DbComment;
import com.fasterxml.jackson.annotation.JsonBackReference;
import models.administrator.user.UserAccountAdmin;

import javax.persistence.*;
import java.util.Date;


/**
 * Created by tri.jaruto on 21/11/2016.
 */
@DbComment("Table AppMasterPermissionsFormAdmin")
@Entity
public class AppMasterPermissionsFormAdmin extends Model {

    @Id
    private long ampfa_autoindex;

    @ManyToOne
    @JoinColumn(name = "ampfa_useraccountadmin", referencedColumnName = "uaa_autoindex")
    @JsonBackReference
    private UserAccountAdmin ampfa_useraccountadmin;

    @ManyToOne
    @JoinColumn(name = "ampfa_appmasterformadmin", referencedColumnName = "amfa_autoindex")
    @JsonBackReference
    private AppMasterFormAdmin ampfa_appmasterformadmin;

    private boolean ampfa_isnew;
    private boolean ampfa_isselect;
    private boolean ampfa_isinsert;
    private boolean ampfa_isupdate;
    private boolean ampfa_isdelete;
    private boolean ampfa_isprint;
    private boolean ampfa_isexport;
    private boolean ampfa_isimport;
    private boolean ampfa_isactive;

    private Date ampfa_created;
    private String ampfa_creater;

    private Date ampfa_updated;
    private String ampfa_updater;

    public AppMasterPermissionsFormAdmin(){}

    public long getAmpfa_autoindex() {
        return ampfa_autoindex;
    }

    public void setAmpfa_autoindex(long ampfa_autoindex) {
        this.ampfa_autoindex = ampfa_autoindex;
    }

    public UserAccountAdmin getAmpfa_useraccountadmin() {
        return ampfa_useraccountadmin;
    }

    public void setAmpfa_useraccountadmin(UserAccountAdmin ampfa_useraccountadmin) {
        this.ampfa_useraccountadmin = ampfa_useraccountadmin;
    }

    public AppMasterFormAdmin getAmpfa_appmasterformadmin() {
        return ampfa_appmasterformadmin;
    }

    public void setAmpfa_appmasterformadmin(AppMasterFormAdmin ampfa_appmasterformadmin) {
        this.ampfa_appmasterformadmin = ampfa_appmasterformadmin;
    }

    public boolean isAmpfa_isnew() {
        return ampfa_isnew;
    }

    public void setAmpfa_isnew(boolean ampfa_isnew) {
        this.ampfa_isnew = ampfa_isnew;
    }

    public boolean isAmpfa_isselect() {
        return ampfa_isselect;
    }

    public void setAmpfa_isselect(boolean ampfa_isselect) {
        this.ampfa_isselect = ampfa_isselect;
    }

    public boolean isAmpfa_isinsert() {
        return ampfa_isinsert;
    }

    public void setAmpfa_isinsert(boolean ampfa_isinsert) {
        this.ampfa_isinsert = ampfa_isinsert;
    }

    public boolean isAmpfa_isupdate() {
        return ampfa_isupdate;
    }

    public void setAmpfa_isupdate(boolean ampfa_isupdate) {
        this.ampfa_isupdate = ampfa_isupdate;
    }

    public boolean isAmpfa_isdelete() {
        return ampfa_isdelete;
    }

    public void setAmpfa_isdelete(boolean ampfa_isdelete) {
        this.ampfa_isdelete = ampfa_isdelete;
    }

    public boolean isAmpfa_isprint() {
        return ampfa_isprint;
    }

    public void setAmpfa_isprint(boolean ampfa_isprint) {
        this.ampfa_isprint = ampfa_isprint;
    }

    public boolean isAmpfa_isexport() {
        return ampfa_isexport;
    }

    public void setAmpfa_isexport(boolean ampfa_isexport) {
        this.ampfa_isexport = ampfa_isexport;
    }

    public boolean isAmpfa_isimport() {
        return ampfa_isimport;
    }

    public void setAmpfa_isimport(boolean ampfa_isimport) {
        this.ampfa_isimport = ampfa_isimport;
    }

    public boolean isAmpfa_isactive() {
        return ampfa_isactive;
    }

    public void setAmpfa_isactive(boolean ampfa_isactive) {
        this.ampfa_isactive = ampfa_isactive;
    }

    public Date getAmpfa_created() {
        return ampfa_created;
    }

    public void setAmpfa_created(Date ampfa_created) {
        this.ampfa_created = ampfa_created;
    }

    public String getAmpfa_creater() {
        return ampfa_creater;
    }

    public void setAmpfa_creater(String ampfa_creater) {
        this.ampfa_creater = ampfa_creater;
    }

    public Date getAmpfa_updated() {
        return ampfa_updated;
    }

    public void setAmpfa_updated(Date ampfa_updated) {
        this.ampfa_updated = ampfa_updated;
    }

    public String getAmpfa_updater() {
        return ampfa_updater;
    }

    public void setAmpfa_updater(String ampfa_updater) {
        this.ampfa_updater = ampfa_updater;
    }
}
