package models.administrator.app;

import com.avaje.ebean.Model;
import com.avaje.ebean.annotation.DbComment;
import com.fasterxml.jackson.annotation.JsonBackReference;
import play.data.validation.Constraints;

import javax.persistence.*;
import java.util.Date;


/**
 * Created by tri.jaruto on 21/11/2016.
 */
@DbComment("Table AppMasterFormAdmin")
@Entity
public class AppMasterFormAdmin extends Model {

    @Id
    private long amfa_autoindex;

    @OneToOne
    @JoinColumn(name = "amfa_appmasteradmin", referencedColumnName = "ama_autoindex")
    @Column(unique=true, nullable = false)
    @JsonBackReference
    private AppMasterAdmin amfa_appmasteradmin;

    private String amfa_name;
    @Column(length = 100, unique=true, nullable = false)
    @Constraints.Required
    private String amfa_filenamescalahtml;
    private boolean amfa_isactive;

    private Date amfa_created;
    private String amfa_creater;

    private Date amfa_updated;
    private String amfa_updater;

    public AppMasterFormAdmin(){}

    public long getAmfa_autoindex() {
        return amfa_autoindex;
    }

    public void setAmfa_autoindex(long amfa_autoindex) {
        this.amfa_autoindex = amfa_autoindex;
    }

    public AppMasterAdmin getAmfa_appmasteradmin() {
        return amfa_appmasteradmin;
    }

    public void setAmfa_appmasteradmin(AppMasterAdmin amfa_appmasteradmin) {
        this.amfa_appmasteradmin = amfa_appmasteradmin;
    }

    public String getAmfa_name() {
        return amfa_name;
    }

    public void setAmfa_name(String amfa_name) {
        this.amfa_name = amfa_name;
    }

    public String getAmfa_filenamescalahtml() {
        return amfa_filenamescalahtml;
    }

    public void setAmfa_filenamescalahtml(String amfa_filenamescalahtml) {
        this.amfa_filenamescalahtml = amfa_filenamescalahtml;
    }

    public boolean isAmfa_isactive() {
        return amfa_isactive;
    }

    public void setAmfa_isactive(boolean amfa_isactive) {
        this.amfa_isactive = amfa_isactive;
    }

    public Date getAmfa_created() {
        return amfa_created;
    }

    public void setAmfa_created(Date amfa_created) {
        this.amfa_created = amfa_created;
    }

    public String getAmfa_creater() {
        return amfa_creater;
    }

    public void setAmfa_creater(String amfa_creater) {
        this.amfa_creater = amfa_creater;
    }

    public Date getAmfa_updated() {
        return amfa_updated;
    }

    public void setAmfa_updated(Date amfa_updated) {
        this.amfa_updated = amfa_updated;
    }

    public String getAmfa_updater() {
        return amfa_updater;
    }

    public void setAmfa_updater(String amfa_updater) {
        this.amfa_updater = amfa_updater;
    }
}
