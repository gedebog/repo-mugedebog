package models.administrator.app;

import com.avaje.ebean.Model;
import com.avaje.ebean.annotation.DbComment;
import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;

import javax.persistence.*;

/**
 * Created by tri.jaruto on 21/11/2016.
 */
@DbComment("Table AppMasterAdminStructure")
@Entity
public class AppMasterAdminStructure extends Model {

    @Id
    @JsonManagedReference
    private long amas_autoindex;

    @ManyToOne
    @JoinColumn(name = "amas_appmasteradmin_parent", referencedColumnName = "ama_autoindex")
    @JsonBackReference
    private AppMasterAdmin amas_appmasteradmin_parent;

    @ManyToOne
    @JoinColumn(name = "amas_appmasteradmin_child", referencedColumnName = "ama_autoindex")
    @JsonBackReference
    private AppMasterAdmin amas_appmasteradmin_child;

    public AppMasterAdminStructure(){}

    public long getAmas_autoindex() {
        return amas_autoindex;
    }

    public void setAmas_autoindex(long amas_autoindex) {
        this.amas_autoindex = amas_autoindex;
    }

    public AppMasterAdmin getAmas_appmasteradmin_parent() {
        return amas_appmasteradmin_parent;
    }

    public void setAmas_appmasteradmin_parent(AppMasterAdmin amas_appmasteradmin_parent) {
        this.amas_appmasteradmin_parent = amas_appmasteradmin_parent;
    }

    public AppMasterAdmin getAmas_appmasteradmin_child() {
        return amas_appmasteradmin_child;
    }

    public void setAmas_appmasteradmin_child(AppMasterAdmin amas_appmasteradmin_child) {
        this.amas_appmasteradmin_child = amas_appmasteradmin_child;
    }
}
