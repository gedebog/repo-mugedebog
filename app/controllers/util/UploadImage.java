package controllers.util;

import com.cloudinary.utils.ObjectUtils;
import net.coobird.thumbnailator.Thumbnails;
import play.mvc.Http;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by tri.jaruto on 2/9/2017.
 */
public class UploadImage {

    public static String onUploadAdminFotoProfileToCloudinary(Http.MultipartFormData.FilePart<File> picture, String nameFile){
        Map uploadResult = new HashMap();
        if (picture != null) {
            String fileName = picture.getFilename();
            String contentType = picture.getContentType();
            File file = picture.getFile();
            try {
                uploadResult = CloudinaryConfig.getCloudinaryConfig().uploader().upload(file, CloudinaryConfig.getCloudinaryParams(Constans.PATH_CLOUDINARY_ADMIN_PROFILE_FOTO, nameFile));
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        return (String) uploadResult.get("secure_url");
    }

    public static String onUploadAdminFotoProfileToLocal(Http.MultipartFormData.FilePart<File> picture, String nameFile){
        Map uploadResult = new HashMap();
        String fotoProfileName = "";
        if (picture != null) {
            String fileName = picture.getFilename();
            String contentType = picture.getContentType();
            File file = picture.getFile();
            fotoProfileName = nameFile+".png";
            try {
                Thumbnails.of(file).size(678, 622).keepAspectRatio(true).toFile(new File("public/upload/admin/profile/foto", fotoProfileName));
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        return fotoProfileName;
    }


    public static String onUploadUserFotoProfileToCloudinary(Http.MultipartFormData.FilePart<File> picture, String nameFile){
        Map uploadResult = new HashMap();
        if (picture != null) {
            String fileName = picture.getFilename();
            String contentType = picture.getContentType();
            File file = picture.getFile();
            try {
                uploadResult = CloudinaryConfig.getCloudinaryConfig().uploader().upload(file, CloudinaryConfig.getCloudinaryParams(Constans.PATH_CLOUDINARY_USER_PROFILE_FOTO, nameFile));
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        return (String) uploadResult.get("secure_url");
    }


    public static String onUploadUserFotoProfileToLocal(Http.MultipartFormData.FilePart<File> picture, String nameFile){
        Map uploadResult = new HashMap();
        String fotoProfileName = "";
        if (picture != null) {
            String fileName = picture.getFilename();
            String contentType = picture.getContentType();
            File file = picture.getFile();
            fotoProfileName = nameFile+".png";
            try {
                Thumbnails.of(file).size(678, 622).keepAspectRatio(true).toFile(new File("public/upload/user/profile/foto", fotoProfileName));
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        return fotoProfileName;
    }


}
