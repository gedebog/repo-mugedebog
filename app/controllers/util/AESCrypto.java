package controllers.util;

import org.apache.commons.codec.binary.Base64;
import javax.crypto.Cipher;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import java.io.UnsupportedEncodingException;

/**
 * Created by tri.jaruto on 07/12/2016.
 */
public class AESCrypto {

    protected final static String key = "www.gedebog.com/";
    protected final static String init = "/moc.gobedeg.www";

    public static String encrypt(String value){
        if(value!=null){
            try {
                IvParameterSpec iv = new IvParameterSpec(init.getBytes("UTF-8"));
                SecretKeySpec skeySpec = new SecretKeySpec(key.getBytes("UTF-8"), "AES");
                Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5PADDING");
                cipher.init(Cipher.ENCRYPT_MODE, skeySpec, iv);
                byte[] encrypted = cipher.doFinal(value.getBytes());
                return Base64.encodeBase64String(encrypted);
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
        return null;
    }

    public static String decrypt(String value){
        if(value!=null){
            try {
                IvParameterSpec iv = new IvParameterSpec(init.getBytes("UTF-8"));
                SecretKeySpec skeySpec = new SecretKeySpec(key.getBytes("UTF-8"), "AES");
                Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5PADDING");
                cipher.init(Cipher.DECRYPT_MODE, skeySpec, iv);
                byte[] original = cipher.doFinal(Base64.decodeBase64(value));
                return new String(original);
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
        return null;
    }
}
