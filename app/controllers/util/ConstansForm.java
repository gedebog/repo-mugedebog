package controllers.util;

import java.io.Serializable;

/**
 * Created by tri.jaruto on 5/30/2017.
 */
public class ConstansForm implements Serializable {

    //USER
    public static final String APPMASTERFORM_AMF_FILENAMESCALAHTML_ORGANIZATION = "organization";
    public static final String APPMASTERFORM_AMF_FILENAMESCALAHTML_EMPLOYEE = "employee";
    public static final String APPMASTERFORM_AMF_FILENAMESCALAHTML_POSITION = "position";

    public static final String APPMASTERFORM_AMF_FILENAMESCALAHTML_FORMLOCK = "formadminlock";
    public static final String APPMASTERFORM_AMF_FILENAMESCALAHTML_FORMNULL = "formadminnull";

    //ADMINISTRATOR
    public static final String APPMASTERFORMADMIN_AMFA_FILENAMESCALAHTML_ADMINACCOUNT = "adminaccount";
    public static final String APPMASTERFORMADMIN_AMFA_FILENAMESCALAHTML_USERACCOUNT = "useraccount";
    public static final String APPMASTERFORMADMIN_AMFA_FILENAMESCALAHTML_ADMINPERMISSIONS = "adminpermissions";
    public static final String APPMASTERFORMADMIN_AMFA_FILENAMESCALAHTML_USERPERMISSIONS = "userpermissions";

    public static final String APPMASTERFORMADMIN_AMFA_FILENAMESCALAHTML_FORMADMINLOCK = "formadminlock";
    public static final String APPMASTERFORMADMIN_AMFA_FILENAMESCALAHTML_FORMADMINNULL = "formadminnull";


}
