package controllers.util;

import java.io.Serializable;

/**
 * Created by tri.jaruto on 2/3/2017.
 */
public class Constans implements Serializable {

    //ACTION
    public static final String ACTION_VIEW = "view";
    public static final String ACTION_NEW = "new";
    public static final String ACTION_SAVE = "save";
    public static final String ACTION_EDIT = "edit";
    public static final String ACTION_CANCEL = "cancel";
    public static final String ACTION_DELETE = "delete";
    public static final String ACTION_UPLOAD = "upload";
    public static final String ACTION_NEXT = "next";
    public static final String ACTION_PREV = "prev";
    public static final String ACTION_LIST = "list";
    public static final String ACTION_PRINT = "print";

    //BUTTON HTML
    public static final String ACTION_NAME_VIEW = "view_name";
    public static final String ACTION_NAME_NEW = "new_name";
    public static final String ACTION_NAME_SAVE = "save_name";
    public static final String ACTION_NAME_EDIT = "edit_name";
    public static final String ACTION_NAME_CANCEL = "cancel_name";
    public static final String ACTION_NAME_DELETE = "delete_name";
    public static final String ACTION_NAME_UPLOAD = "upload_name";
    public static final String ACTION_NAME_ADD = "add_name";

    public static final String ACTION_VALUE_VIEW = "view_value";
    public static final String ACTION_VALUE_NEW = "new_value";
    public static final String ACTION_VALUE_SAVE = "save_value";
    public static final String ACTION_VALUE_EDIT = "edit_value";
    public static final String ACTION_VALUE_CANCEL = "cancel_value";
    public static final String ACTION_VALUE_DELETE = "delete_value";
    public static final String ACTION_VALUE_UPLOAD = "upload_value";
    public static final String ACTION_VALUE_ADD = "add_value";

    public static final String ACTION_ID_FORM = "form_id";
    public static final String ACTION_ID_FORM_MAIN = "main_form_id";
    public static final String ACTION_ID_FORM_DETAIL = "detail_form_id";

    public static final String ACTION_ID_SAVE = "save_id";
    public static final String ACTION_ID_EDIT = "edit_id";
    public static final String ACTION_ID_NEW = "new_id";
    public static final String ACTION_ID_CANCEL = "cancel_id";
    public static final String ACTION_ID_DELETE = "delete_id";
    public static final String ACTION_ID_UPLOAD = "upload_id";
    public static final String ACTION_ID_PRINT = "print_id";
    public static final String ACTION_ID_EXPORT = "export_id";
    public static final String ACTION_ID_IMPORT = "import_id";
    public static final String ACTION_ID_ADD = "add_id";

    //USER
    public static final short APPMASTER_AM_TYPE_APLIKASI = 0;
    public static final short APPMASTER_AM_TYPE_MENU = 1;
    public static final short APPMASTER_AM_TYPE_FORM = 2;

    public static final short APPMASTER_AMA_LEVEL_1 = 1;
    public static final short APPMASTER_AMA_LEVEL_2 = 2;
    public static final short APPMASTER_AMA_LEVEL_3 = 3;
    public static final short APPMASTER_AMA_LEVEL_4 = 4;

    //ADMINISRATOR
    public static final short USERACCOUNTADMIN_UAA_USERTYPE_ADMINISTRATOR = 0;
    public static final short USERACCOUNTADMIN_UAA_USERTYPE_ADMIN = 1;

    public static final short APPMASTERADMIN_AMA_TYPE_APLIKASI = 0;
    public static final short APPMASTERADMIN_AMA_TYPE_MENU = 1;
    public static final short APPMASTERADMIN_AMA_TYPE_FORM = 2;

    public static final short APPMASTERADMIN_AMA_LEVEL_1 = 1;
    public static final short APPMASTERADMIN_AMA_LEVEL_2 = 2;
    public static final short APPMASTERADMIN_AMA_LEVEL_3 = 3;
    public static final short APPMASTERADMIN_AMA_LEVEL_4 = 4;

    //FOTO PROFILE
    //CLOUDINARY
    //USERNAME : gedebog.it@gmail.com
    //PASSWORD : bukacloudinary
    //ADMIN
    public static final String PROFILE_FOTO_UPLOAD_CLOUDINARY = "cloudinary";
    public static final String PROFILE_FOTO_UPLOAD_LOCALHOST = "localhost";
    public static final String PATH_CLOUDINARY_ADMIN_PROFILE_FOTO = "gedebog/admin/profile/foto/";
    public static final String PATH_CLOUDINARY_USER_PROFILE_FOTO = "gedebog/user/profile/foto/";



}
