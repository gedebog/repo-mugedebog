package controllers.util;

import org.mindrot.jbcrypt.BCrypt;

/**
 * Created by tri.jaruto on 07/12/2016.
 */
public class BCrypto {
    public static String createPassword(String clearString) throws AppException {
        if (clearString == null) {
            throw new AppException("No password defined!");
        }
        return BCrypt.hashpw(clearString, BCrypt.gensalt());
    }

    public static boolean checkPassword(String clearString, String encryptedPassword) {
        if (clearString == null) {
            return false;
        }
        if (encryptedPassword == null) {
            return false;
        }
        return BCrypt.checkpw(clearString, encryptedPassword);
    }
}
