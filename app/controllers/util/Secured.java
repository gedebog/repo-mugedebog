package controllers.util;

import models.user.user.UserAccount;
import models.user.user.UserFotoProfile;
import play.mvc.Http.Context;
import play.mvc.Result;
import play.mvc.Security;
import services.user.user.UserAccountLogic;
import services.user.user.UserFotoProfileLogic;

/**
 * Created by tri.jaruto on 22/11/2016.
 */
public class Secured extends Security.Authenticator {

    /**
     * Used by authentication annotation to determine if user is logged in.
     * @param ctx The context.
     * @return The email address of the logged in user, or null if not logged in.
     */
    @Override
    public String getUsername(Context ctx) {
        return AESCrypto.decrypt(ctx.session().get("ua_username"));
    }

    /**
     * Instruct authenticator to automatically redirect to loginadmin page if unauthorized.
     * @param context The context.
     * @return The loginadmin page.
     */
    @Override
    public Result onUnauthorized(Context context) {
        return redirect(controllers.user.authentication.routes.LoginController.onLogin());
    }

    /**
     * Return the email of the logged in user, or null if no logged in user.
     *
     * @param ctx the context containing the session
     * @return The email of the logged in user, or null if user is not logged in.
     */
    public static String getUa_username(Context ctx) {
        return AESCrypto.decrypt(ctx.session().get("ua_username"));
    }

    public static String getUa_autoindex(Context ctx) {
        return AESCrypto.decrypt(ctx.session().get("ua_autoindex"));
    }

    /**
     * True if there is a logged in user, false otherwise.
     * @param ctx The context.
     * @return True if user is logged in.
     */
    public static boolean isLoggedIn(Context ctx) {
        return (getUa_username(ctx) != null);
    }

    /**
     * Return the UserInfo of the logged in user, or null if no user is logged in.
     * @param ctx The context.
     * @return The UserInfo, or null.
     */
    public static UserAccount getUserInfo(Context ctx) {
        return (isLoggedIn(ctx) ? new UserAccountLogic().onSelectBy_ua_username(getUa_username(ctx)) : null);
    }

    public static UserFotoProfile getUserFotoProfile(Context ctx) {
        return (isLoggedIn(ctx) ? new UserFotoProfileLogic().onSelectBy_ufp_useraccount(getUserInfo(ctx)) : null);
    }


}
