package controllers.util;

import java.io.Serializable;

/**
 * Created by tri.jaruto on 5/30/2017.
 */
public class ConstansTable implements Serializable {

    //ADMINISTRATOR
    public static final String TABLE_USER_ACCOUNT_ADMIN = "user_account_admin";
    public static final String TABLE_USER_PROFILE_ADMIN = "user_profile_admin";
    public static final String TABLE_USER_FOTO_PROFILE_ADMIN = "user_foto_profile_admin";
    public static final String TABLE_APP_MASTER_ADMIN = "app_master_admin";
    public static final String TABLE_APP_MASTER_STRUCTURE_ADMIN = "app_master_structure_admin";
    public static final String TABLE_APP_MASTER_FORM_ADMIN = "app_master_form_admin";
    public static final String TABLE_APP_MASTER_PERMISSONS_FORM_ADMIN = "app_master_permissions_form_admin";


    //USER
    public static final String TABLE_USER_ACCOUNT = "user_account";
    public static final String TABLE_USER_PROFILE = "user_profile";
    public static final String TABLE_USER_FOTO_PROFILE = "user_foto_profile";
    public static final String TABLE_APP_MASTER = "app_master";
    public static final String TABLE_APP_MASTER_STRUCTURE = "app_master_structure";
    public static final String TABLE_APP_MASTER_FORM = "app_master_form";
    public static final String TABLE_APP_MASTER_PERMISSONS_FORM = "app_master_permissions_form";

    public static final String TABLE_APP_MASTER_ORGANIZATION = "app_master_organization";


}
