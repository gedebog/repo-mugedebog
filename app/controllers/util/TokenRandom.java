package controllers.util;

/**
 * Created by tri.jaruto on 2/9/2017.
 */
public class TokenRandom {
    public static String getTokenRandom(){
        return String.format("%06d", getRandomNumberInRange(0, 999999));
    }

    private static int getRandomNumberInRange(int min, int max) {
        return (int)(Math.random() * ((max - min) + 1)) + min;
    }
}
