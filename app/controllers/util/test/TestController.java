package controllers.util.test;

import controllers.util.Secured;
import models.user.user.UserAccount;
import models.user.user.UserFotoProfile;
import play.data.DynamicForm;
import play.data.FormFactory;
import play.mvc.Controller;
import play.mvc.Result;
import services.user.user.IUserAccount;
import services.user.user.UserAccountLogic;
import views.html.test.testform;

import javax.inject.Inject;


/**
 * Created by tri.jaruto on 18/11/2016.
 */
public class TestController extends Controller {

    IUserAccount iUserAccount = new UserAccountLogic();

    UserAccount userAccountLogin = new UserAccount();
    UserFotoProfile userFotoProfileLogin = new UserFotoProfile();
    boolean isLogin = false;

    @Inject
    FormFactory formFactory;

    public Result formtest() {
        userAccountLogin = Secured.getUserInfo(ctx());
        userFotoProfileLogin = Secured.getUserFotoProfile(ctx());
        isLogin = Secured.isLoggedIn(ctx());
        return ok(testform.render("Login", false, userAccountLogin, userFotoProfileLogin));
    }

    public Result action() {
        System.out.println("formtest action");
        //GET DYNAMIC FORM
        DynamicForm requestData = formFactory.form().bindFromRequest();
        System.out.println("requestData.get(\"amfa_name_name\") : "+requestData.get("amfa_name_name"));
        System.out.println("requestData.get(\"amfa_filenamescalahtml_name\") : "+requestData.get("amfa_filenamescalahtml_name"));

        return ok("formtest action");
    }




}
