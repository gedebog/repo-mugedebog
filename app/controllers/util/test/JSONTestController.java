package controllers.util.test;

import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import controllers.administrator.dasboard.form.security.AdminAccountListController;
import controllers.administrator.dasboard.render.CreateMenuAdminHtml;
import controllers.util.*;
import models.administrator.app.AppMasterFormAdmin;
import models.administrator.user.UserAccountAdmin;
import models.administrator.user.UserFotoProfileAdmin;
import play.data.DynamicForm;
import play.data.FormFactory;
import play.libs.Json;
import play.libs.mailer.MailerClient;
import play.mvc.Controller;
import play.mvc.Result;
import play.mvc.Security;
import services.administrator.app.AppMasterFormAdminLogic;
import services.administrator.app.AppMasterPermissionsFormAdminLogic;
import services.administrator.app.IAppMasterFormAdmin;
import services.administrator.app.IAppMasterPermissionsFormAdmin;
import services.administrator.user.IUserAccountAdmin;
import services.administrator.user.UserAccountAdminLogic;
import views.html.administrator.dasboard.form.formadminlock;
import views.html.administrator.dasboard.form.formadminnull;
import views.html.errors.error404admin;

import javax.inject.Inject;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by tri.jaruto on 18/11/2016.
 */
public class JSONTestController extends Controller {

    IAppMasterFormAdmin iAppMasterFormAdmin = new AppMasterFormAdminLogic();
    IAppMasterPermissionsFormAdmin iAppMasterPermissionsFormAdmin = new AppMasterPermissionsFormAdminLogic();
    IUserAccountAdmin iUserAccountAdmin = new UserAccountAdminLogic();

    UserAccountAdmin userAccountAdminLogin = new UserAccountAdmin();
    UserFotoProfileAdmin userFotoProfileAdminLogin = new UserFotoProfileAdmin();
    boolean isLogin = false;

    List<UserAccountAdmin> userAccountAdmins = new ArrayList<UserAccountAdmin>();


    public Result jSonFormAdminList(String page, String length) {
        userAccountAdmins.clear();
        userAccountAdmins = iUserAccountAdmin.onSelectAllBy_Pagination(Integer.parseInt(page),Integer.parseInt(length));

        AppMasterFormAdmin appMasterFormAdmin = iAppMasterFormAdmin.onSelectBy_amfa_filenamescalahtml("adminaccount");

        int rowTotal = iUserAccountAdmin.onRowCount();
        int i = 0;
        ObjectNode result = Json.newObject();
        result.put("draw", i++);
        result.put("recordsTotal", rowTotal);
        result.put("recordsFiltered", rowTotal);
        ArrayNode arrayNode = result.putArray("data");

        if(userAccountAdmins!=null){
            for (UserAccountAdmin userAccountAdmin : userAccountAdmins) {
                ArrayNode arrayData = arrayNode.addArray();
                String viewData = "<a href="+ controllers.administrator.dasboard.form.routes.FormAdminMainController.formAdminMain(
                        URLEncDec.URLEncode(AESCrypto.encrypt(appMasterFormAdmin.getAmfa_name())),
                        URLEncDec.URLEncode(AESCrypto.encrypt(appMasterFormAdmin.getAmfa_filenamescalahtml())),
                        URLEncDec.URLEncode(AESCrypto.encrypt(String.valueOf(userAccountAdmin.getUaa_autoindex()))),
                        URLEncDec.URLEncode(AESCrypto.encrypt(Constans.ACTION_VIEW))
                )+"><i class=\"file text outline icon\"></i></a>";
                arrayData.add(viewData);
                arrayData.add(userAccountAdmin.getUaa_username());
                arrayData.add(userAccountAdmin.getUaa_usertype()==0 ? "Administrator" : "Admin");
                String isActiveIcon = "<i class=\"large red remove icon\"></i>";
                if(userAccountAdmin.isUaa_isactive()){
                    isActiveIcon = "<i class=\"large green checkmark icon\"></i>";
                }
                arrayData.add(isActiveIcon);
                //arrayNode.add(Json.toJson(userAccountAdmin));
            }
        }

        response().setHeader("Access-Control-Allow-Origin", "*");
        //response().setHeader("Content-Encoding", "gzip");
        response().setHeader("Cache-Control", "no-cache");
        return ok(result);
    }

}
