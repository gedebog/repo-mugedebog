package controllers.util;


import com.cloudinary.Cloudinary;
import com.cloudinary.Transformation;
import com.cloudinary.utils.ObjectUtils;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by tri.jaruto on 8/10/2017.
 */
public class CloudinaryConfig {

    public static Cloudinary getCloudinaryConfig(){
        Map config = new HashMap();
        config.put("cloud_name", "gedebog");
        config.put("api_key", "161464137761627");
        config.put("api_secret", "t3ZccPcR9RgU2CoS3SrjjReTWrQ");
        Cloudinary cloudinary = new Cloudinary(config);
        return cloudinary;
    }

    public static Map getCloudinaryParams(String path, String filename){
        Map params = new HashMap();
        params.put("public_id", path+filename);
        params.put("format", "png");
        params.put("transformation", new Transformation().width(678).height(622).crop("limit"));
        return params;
    }

}
