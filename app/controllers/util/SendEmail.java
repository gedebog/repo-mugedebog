package controllers.util;

import models.user.user.UserAccount;
import models.administrator.user.UserAccountAdmin;
import org.apache.commons.io.IOUtils;
import play.Configuration;
import play.api.Play;
import play.libs.mailer.Email;
import play.libs.mailer.MailerClient;
import java.io.IOException;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Map;

import static play.mvc.Controller.request;

/**
 * Created by tri.jaruto on 08/12/2016.
 */
public class SendEmail {

    MailerClient mailerClient;

    public SendEmail(MailerClient mailerClient){
        this.mailerClient = mailerClient;
    }

    //USER
    public void onSendEmailSignUpActivation(UserAccount userAccount){
        try {
            Map<String, String> map = new HashMap<>();
            map.put("$appName", Configuration.root().getString("app.name"));
            map.put("$corpName", Configuration.root().getString("corp.name"));
            map.put("$corpAddress", Configuration.root().getString("corp.address"));
            map.put("$ua_username", userAccount.getUa_username());
            String urlActivating = "/authentication/activation/"+URLEncoder.encode(AESCrypto.encrypt(userAccount.getUa_username()),"UTF-8")+"/"+URLEncoder.encode(userAccount.getUa_activationcode(),"UTF-8");
            map.put("$urlActivating", Configuration.root().getString("server.schemes")+request().host()+urlActivating);
            map.put("$urlLogin", Configuration.root().getString("server.schemes")+request().host()+ controllers.user.authentication.routes.LoginController.onLogin().url());
            map.put("$domainApp", Configuration.root().getString("domain.name"));

            Email email = new Email()
                    .setSubject("Your Account, "+userAccount.getUa_username()+", has been created")
                    .setFrom("Gedebog Team <"+Configuration.root().getString("app.email")+">")
                    .addTo(userAccount.getUa_username())
                    .setBodyHtml(generateHTML(IOUtils.toString(Play.resourceAsStream("public/template/email/signupactivation.html", Play.current()).get(), "UTF-8"), map));
            mailerClient.send(email);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void onSendEmailForgotPassword(UserAccount userAccount){
        try {
            Map<String, String> map = new HashMap<>();
            map.put("$appName", Configuration.root().getString("app.name"));
            map.put("$corpName", Configuration.root().getString("corp.name"));
            map.put("$corpAddress", Configuration.root().getString("corp.address"));
            map.put("$ua_username", userAccount.getUa_username());
            String urlResetAccount = "/authentication/resetpassword/"+URLEncoder.encode(AESCrypto.encrypt(userAccount.getUa_username()),"UTF-8")+"/"+URLEncoder.encode(userAccount.getUa_activationcode(),"UTF-8");
            map.put("$urlResetAccount", Configuration.root().getString("server.schemes")+request().host()+urlResetAccount);
            map.put("$urlLogin", Configuration.root().getString("server.schemes")+request().host()+ controllers.user.authentication.routes.LoginController.onLogin().url());
            map.put("$domainApp", Configuration.root().getString("domain.name"));

            Email email = new Email()
                    .setSubject(Configuration.root().getString("app.name")+" Password Reset")
                    .setFrom("Gedebog Team <"+Configuration.root().getString("app.email")+">")
                    .addTo(userAccount.getUa_username())
                    .setBodyHtml(generateHTML(IOUtils.toString(Play.resourceAsStream("public/template/email/resetpassword.html", Play.current()).get(), "UTF-8"), map));
            mailerClient.send(email);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }



    //ADMINI
    public void onSendEmailResetPasswordAdministratorActivation(UserAccountAdmin userAccountAdmin, String tokenRandom){
        try {
            Map<String, String> map = new HashMap<>();
            map.put("$appName", Configuration.root().getString("app.name"));
            map.put("$corpName", Configuration.root().getString("corp.name"));
            map.put("$corpAddress", Configuration.root().getString("corp.address"));
            map.put("$uaa_username", userAccountAdmin.getUaa_username());
            map.put("$uaa_userpassword", tokenRandom);
            map.put("$urlLogin", Configuration.root().getString("server.schemes")+request().host()+ controllers.administrator.authentication.routes.LoginAdminController.onLogin().url());
            map.put("$domainApp", Configuration.root().getString("domain.name"));

            Email email = new Email()
                    .setSubject("Your Account Administrator, "+userAccountAdmin.getUaa_username()+", has been reset")
                    .setFrom("Gedebog Team <"+Configuration.root().getString("app.email")+">")
                    .addTo(userAccountAdmin.getUaa_username())
                    .setBodyHtml(generateHTML(IOUtils.toString(Play.resourceAsStream("public/template/email/resetpasswordadministrator.html", Play.current()).get(), "UTF-8"), map));
            mailerClient.send(email);

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void onSendEmailCreatePasswordAdminActivation(UserAccountAdmin userAccountAdmin, String uaa_userpassword_name){
        try {
            Map<String, String> map = new HashMap<>();
            map.put("$appName", Configuration.root().getString("app.name"));
            map.put("$corpName", Configuration.root().getString("corp.name"));
            map.put("$corpAddress", Configuration.root().getString("corp.address"));
            map.put("$uaa_username", userAccountAdmin.getUaa_username());
            map.put("$uaa_userpassword", uaa_userpassword_name);
            map.put("$urlLogin", Configuration.root().getString("server.schemes")+request().host()+ controllers.administrator.authentication.routes.LoginAdminController.onLogin().url());
            map.put("$domainApp", Configuration.root().getString("domain.name"));

            Email email = new Email()
                    .setSubject("Your Account Admin, "+userAccountAdmin.getUaa_username()+", has been create")
                    .setFrom("Gedebog Team <"+Configuration.root().getString("app.email")+">")
                    .addTo(userAccountAdmin.getUaa_username())
                    .setBodyHtml(generateHTML(IOUtils.toString(Play.resourceAsStream("public/template/email/createpasswordadmin.html", Play.current()).get(), "UTF-8"), map));
            mailerClient.send(email);

        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    public void onSendEmailUpdatePasswordAdminActivation(UserAccountAdmin userAccountAdmin, String tokenRandom){
        try {
            Map<String, String> map = new HashMap<>();
            map.put("$appName", Configuration.root().getString("app.name"));
            map.put("$corpName", Configuration.root().getString("corp.name"));
            map.put("$corpAddress", Configuration.root().getString("corp.address"));
            map.put("$uaa_username", userAccountAdmin.getUaa_username());
            map.put("$uaa_userpassword", tokenRandom);
            map.put("$urlLogin", Configuration.root().getString("server.schemes")+request().host()+ controllers.administrator.authentication.routes.LoginAdminController.onLogin().url());
            map.put("$domainApp", Configuration.root().getString("domain.name"));

            Email email = new Email()
                    .setSubject("Your Account Admin, "+userAccountAdmin.getUaa_username()+", has been update")
                    .setFrom("Gedebog Team <"+Configuration.root().getString("app.email")+">")
                    .addTo(userAccountAdmin.getUaa_username())
                    .setBodyHtml(generateHTML(IOUtils.toString(Play.resourceAsStream("public/template/email/updatepasswordadmin.html", Play.current()).get(), "UTF-8"), map));
            mailerClient.send(email);

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void onSendEmailCreatePasswordUserActivation(UserAccount userAccount, String ua_userpassword_name){
        try {
            Map<String, String> map = new HashMap<>();
            map.put("$appName", Configuration.root().getString("app.name"));
            map.put("$corpName", Configuration.root().getString("corp.name"));
            map.put("$corpAddress", Configuration.root().getString("corp.address"));
            map.put("$ua_username", userAccount.getUa_username());
            map.put("$ua_userpassword", ua_userpassword_name);
            map.put("$urlLogin", Configuration.root().getString("server.schemes")+request().host()+ controllers.administrator.authentication.routes.LoginAdminController.onLogin().url());
            map.put("$domainApp", Configuration.root().getString("domain.name"));

            Email email = new Email()
                    .setSubject("Your Account, "+userAccount.getUa_username()+", has been create")
                    .setFrom("Gedebog Team <"+Configuration.root().getString("app.email")+">")
                    .addTo(userAccount.getUa_username())
                    .setBodyHtml(generateHTML(IOUtils.toString(Play.resourceAsStream("public/template/email/createpassworduser.html", Play.current()).get(), "UTF-8"), map));
            mailerClient.send(email);

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void onSendEmailUpdatePasswordUserActivation(UserAccount userAccount, String tokenRandom){
        try {
            Map<String, String> map = new HashMap<>();
            map.put("$appName", Configuration.root().getString("app.name"));
            map.put("$corpName", Configuration.root().getString("corp.name"));
            map.put("$corpAddress", Configuration.root().getString("corp.address"));
            map.put("$ua_username", userAccount.getUa_username());
            map.put("$ua_userpassword", tokenRandom);
            map.put("$urlLogin", Configuration.root().getString("server.schemes")+request().host()+ controllers.administrator.authentication.routes.LoginAdminController.onLogin().url());
            map.put("$domainApp", Configuration.root().getString("domain.name"));

            Email email = new Email()
                    .setSubject("Your Account, "+userAccount.getUa_username()+", has been update")
                    .setFrom("Gedebog Team <"+Configuration.root().getString("app.email")+">")
                    .addTo(userAccount.getUa_username())
                    .setBodyHtml(generateHTML(IOUtils.toString(Play.resourceAsStream("public/template/email/updatepassworduser.html", Play.current()).get(), "UTF-8"), map));
            mailerClient.send(email);

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private String generateHTML(String content, Map<String, String> param){
        for (Map.Entry<String, String> entry : param.entrySet()){
            content = content.replace(entry.getKey(), entry.getValue());
        }
        return content;
    }

}
