package controllers.util;

import controllers.administrator.authentication.LoginAdminController;
import models.administrator.user.UserAccountAdmin;
import models.administrator.user.UserFotoProfileAdmin;
import play.mvc.Http.Context;
import play.mvc.Result;
import play.mvc.Security;
import services.administrator.user.UserAccountAdminLogic;
import services.administrator.user.UserFotoProfileAdminLogic;

/**
 * Created by tri.jaruto on 22/11/2016.
 */
public class SecuredAdmin extends Security.Authenticator {

    /**
     * Used by authentication annotation to determine if user is logged in.
     * @param ctx The context.
     * @return The email address of the logged in user, or null if not logged in.
     */
    @Override
    public String getUsername(Context ctx) {
        return AESCrypto.decrypt(ctx.session().get("uaa_username"));
    }

    /**
     * Instruct authenticator to automatically redirect to loginadmin page if unauthorized.
     * @param context The context.
     * @return The loginadmin page.
     */
    @Override
    public Result onUnauthorized(Context context) {
        return redirect(controllers.administrator.authentication.routes.LoginAdminController.onLogin());
    }

    /**
     * Return the email of the logged in user, or null if no logged in user.
     *
     * @param ctx the context containing the session
     * @return The email of the logged in user, or null if user is not logged in.
     */
    public static String getUaa_username(Context ctx) {
        return AESCrypto.decrypt(ctx.session().get("uaa_username"));
    }

    public static String getUaa_autoindex(Context ctx) {
        return AESCrypto.decrypt(ctx.session().get("uaa_autoindex"));
    }

    /**
     * True if there is a logged in user, false otherwise.
     * @param ctx The context.
     * @return True if user is logged in.
     */
    public static boolean isLoggedIn(Context ctx) {
        return (getUaa_username(ctx) != null);
    }

    /**
     * Return the UserInfo of the logged in user, or null if no user is logged in.
     * @param ctx The context.
     * @return The UserInfo, or null.
     */
    public static UserAccountAdmin getUserInfo(Context ctx) {
        return (isLoggedIn(ctx) ? new UserAccountAdminLogic().onSelectBy_uaa_username(getUaa_username(ctx)) : null);
    }

    public static UserFotoProfileAdmin getUserFotoProfileAdmin(Context ctx) {
        return (isLoggedIn(ctx) ? new UserFotoProfileAdminLogic().onSelectBy_ufpa_useraccountadmin(getUserInfo(ctx)) : null);
    }

}
