package controllers.ws.json.administrator;

import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import controllers.administrator.dasboard.form.security.JSONAdminAccountListController;
import controllers.administrator.dasboard.form.security.JSONUserAccountListController;
import controllers.util.*;
import models.administrator.app.AppMasterFormAdmin;
import models.administrator.app.AppMasterPermissionsFormAdmin;
import models.administrator.user.UserAccountAdmin;
import models.administrator.user.UserFotoProfileAdmin;
import play.libs.Json;
import play.mvc.Controller;
import play.mvc.Result;
import services.administrator.app.AppMasterFormAdminLogic;
import services.administrator.app.AppMasterPermissionsFormAdminLogic;
import services.administrator.app.IAppMasterFormAdmin;
import services.administrator.app.IAppMasterPermissionsFormAdmin;
import services.administrator.user.IUserAccountAdmin;
import services.administrator.user.UserAccountAdminLogic;
import views.html.errors.error404admin;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by tri.jaruto on 9/4/2017.
 */
public class UserAccountAdminWSController extends Controller {

    IAppMasterFormAdmin iAppMasterFormAdmin = new AppMasterFormAdminLogic();
    IAppMasterPermissionsFormAdmin iAppMasterPermissionsFormAdmin = new AppMasterPermissionsFormAdminLogic();
    IUserAccountAdmin iUserAccountAdmin = new UserAccountAdminLogic();
    List<UserAccountAdmin> userAccountAdmins = new ArrayList<UserAccountAdmin>();

    UserAccountAdmin userAccountAdminLogin = new UserAccountAdmin();
    UserFotoProfileAdmin userFotoProfileAdminLogin = new UserFotoProfileAdmin();
    boolean isLogin = false;

    public Result jSONUserAccountAdminListPage(String amfa_name, String amfa_filenamescalahtml, String page, String length){

        userAccountAdminLogin = SecuredAdmin.getUserInfo(ctx());
        amfa_name = AESCrypto.decrypt(amfa_name);
        amfa_filenamescalahtml = AESCrypto.decrypt(amfa_filenamescalahtml);

        AppMasterFormAdmin appMasterFormAdmin = iAppMasterFormAdmin.onSelectBy_amfa_filenamescalahtml(amfa_filenamescalahtml);

        userAccountAdmins.clear();
        userAccountAdmins = iUserAccountAdmin.onSelectAllBy_Pagination(Integer.parseInt(page),Integer.parseInt(length));
        AppMasterPermissionsFormAdmin appMasterPermissionsFormAdmin = iAppMasterPermissionsFormAdmin.onSelectBy_ampfa_useraccountadmin_and_ampfa_appmasterformadmin(userAccountAdminLogin, appMasterFormAdmin);

        int rowTotal = iUserAccountAdmin.onRowCount();
        int i = 0;
        ObjectNode result = Json.newObject();
        result.put("draw", i++);
        result.put("recordsTotal", rowTotal);
        result.put("recordsFiltered", rowTotal);
        ArrayNode arrayNode = result.putArray("data");

        if(userAccountAdmins!=null){
            for (UserAccountAdmin userAccountAdmin : userAccountAdmins) {
                ArrayNode arrayData = arrayNode.addArray();
                String viewData = "";
                if(appMasterPermissionsFormAdmin.isAmpfa_isselect()){
                    viewData = "<a href="+ controllers.administrator.dasboard.form.routes.FormAdminMainController.formAdminMain(
                            URLEncDec.URLEncode(AESCrypto.encrypt(appMasterFormAdmin.getAmfa_name())),
                            URLEncDec.URLEncode(AESCrypto.encrypt(appMasterFormAdmin.getAmfa_filenamescalahtml())),
                            URLEncDec.URLEncode(AESCrypto.encrypt(String.valueOf(userAccountAdmin.getUaa_autoindex()))),
                            URLEncDec.URLEncode(AESCrypto.encrypt(Constans.ACTION_VIEW))
                    )+"><i class=\"file text outline icon\"></i></a>";
                }else{
                    viewData = "<a><i class=\"lock icon\"></i></a>";
                }
                arrayData.add(viewData);
                arrayData.add(userAccountAdmin.getUaa_username());
                arrayData.add(userAccountAdmin.getUaa_usertype()==0 ? "Administrator" : "Admin");
                String isActiveIcon = "<i class=\"large red remove icon\"></i>";
                if(userAccountAdmin.isUaa_isactive()){
                    isActiveIcon = "<i class=\"large green checkmark icon\"></i>";
                }
                arrayData.add(isActiveIcon);
                //arrayNode.add(Json.toJson(userAccountAdmin));
            }
        }

        response().setHeader("Access-Control-Allow-Origin", "*");
        response().setHeader("Cache-Control", "no-cache");
        return ok(result);
    }


    //RESULT NULL
    public Result getResultNull(String amfa_name, String amfa_filenamescalahtml){
        userAccountAdminLogin = SecuredAdmin.getUserInfo(ctx());
        userFotoProfileAdminLogin = SecuredAdmin.getUserFotoProfileAdmin(ctx());
        isLogin = SecuredAdmin.isLoggedIn(ctx());
        return ok(error404admin.render("Error 404", "404", isLogin, userAccountAdminLogin, userFotoProfileAdminLogin));
    }

}
