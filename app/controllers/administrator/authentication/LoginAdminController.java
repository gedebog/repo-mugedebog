package controllers.administrator.authentication;

import controllers.util.AESCrypto;
import controllers.util.SecuredAdmin;
import models.administrator.user.UserAccountAdmin;
import models.administrator.user.UserFotoProfileAdmin;
import play.data.DynamicForm;
import play.data.FormFactory;
import play.mvc.Controller;
import play.mvc.Result;
import services.administrator.user.IUserAccountAdmin;
import services.administrator.user.UserAccountAdminLogic;
import views.html.administrator.authentication.loginadmin;

import javax.inject.Inject;

/**
 * Created by tri.jaruto on 16/11/2016.
 */
public class LoginAdminController extends Controller {

    @Inject
    FormFactory formFactory;

    IUserAccountAdmin iUserAccountAdmin = new UserAccountAdminLogic();

    UserAccountAdmin userAccountAdminLogin = new UserAccountAdmin();
    UserFotoProfileAdmin userFotoProfileAdminLogin = new UserFotoProfileAdmin();
    boolean isLogin = false;


    public Result login(){
        userAccountAdminLogin = SecuredAdmin.getUserInfo(ctx());
        userFotoProfileAdminLogin = SecuredAdmin.getUserFotoProfileAdmin(ctx());
        isLogin = SecuredAdmin.isLoggedIn(ctx());
        if(isLogin){
            return redirect(controllers.administrator.dasboard.routes.DasboardAdminController.dasboardAdmin());
        }
        return ok(loginadmin.render("Login Admin", isLogin, userAccountAdminLogin, userFotoProfileAdminLogin));
    }

    public Result onLogin(){
        userAccountAdminLogin = SecuredAdmin.getUserInfo(ctx());
        userFotoProfileAdminLogin = SecuredAdmin.getUserFotoProfileAdmin(ctx());
        isLogin = SecuredAdmin.isLoggedIn(ctx());
        //GET DYNAMIC FORM
        DynamicForm requestData = formFactory.form().bindFromRequest();
        if (!iUserAccountAdmin.onValidateBy_uaa_username_and_uaa_password(requestData.get("uaa_username_name"), requestData.get("uaa_userpassword_name"))) {
            flash("error", "Login credentials not valid.");
            return badRequest(loginadmin.render("Login", isLogin, userAccountAdminLogin, userFotoProfileAdminLogin));
        }else{
            session().clear();
            UserAccountAdmin userAccountAdmin = iUserAccountAdmin.onSelectBy_uaa_username(requestData.get("uaa_username_name"));
            session("uaa_autoindex", AESCrypto.encrypt(Long.toString(userAccountAdmin.getUaa_autoindex())));
            session("uaa_username", AESCrypto.encrypt(userAccountAdmin.getUaa_username()));
            return redirect(controllers.administrator.dasboard.routes.DasboardAdminController.dasboardAdmin());
        }
    }


}
