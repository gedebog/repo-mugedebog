package controllers.administrator.authentication;

import controllers.util.SecuredAdmin;
import play.mvc.Controller;
import play.mvc.Result;
import play.mvc.Security;

/**
 * Created by tri.jaruto on 22/11/2016.
 */
public class LogoutAdminController extends Controller {

    @Security.Authenticated(SecuredAdmin.class)
    public Result logout() {
        session().clear();
        return redirect(routes.LoginAdminController.login());
    }

}
