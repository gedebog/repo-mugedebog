package controllers.administrator.authentication;

import controllers.util.*;
import models.administrator.user.UserAccountAdmin;
import models.administrator.user.UserFotoProfileAdmin;
import models.administrator.user.UserProfileAdmin;
import play.Configuration;
import play.libs.mailer.MailerClient;
import play.mvc.Controller;
import play.mvc.Result;
import services.administrator.user.IUserAccountAdmin;
import services.administrator.user.IUserProfileAdmin;
import services.administrator.user.UserAccountAdminLogic;
import services.administrator.user.UserProfileAdminLogic;
import views.html.administrator.authentication.passwordreset;
import javax.inject.Inject;
import java.util.Date;

import static controllers.util.TokenRandom.getTokenRandom;

/**
 * Created by tri.jaruto on 22/11/2016.
 */
public class ResetPasswordController extends Controller {

    IUserAccountAdmin iUserAccountAdmin = new UserAccountAdminLogic();
    IUserProfileAdmin iUserProfileAdmin = new UserProfileAdminLogic();

    UserAccountAdmin userAccountAdminLogin = new UserAccountAdmin();
    UserFotoProfileAdmin userFotoProfileAdminLogin = new UserFotoProfileAdmin();
    boolean isLogin = false;

    @Inject
    MailerClient mailerClient;

    public Result resetPassword() {
        userAccountAdminLogin = SecuredAdmin.getUserInfo(ctx());
        userFotoProfileAdminLogin = SecuredAdmin.getUserFotoProfileAdmin(ctx());
        isLogin = SecuredAdmin.isLoggedIn(ctx());

        String reset = "no";
        if(Configuration.root().getString("app.default.account.administrator.reset").equals("yes")){
            reset = "yes";
        }
        return ok(passwordreset.render("Reset Password", reset, isLogin, userAccountAdminLogin, userFotoProfileAdminLogin));
    }

    public Result onResetPassword(){
        userAccountAdminLogin = SecuredAdmin.getUserInfo(ctx());
        userFotoProfileAdminLogin = SecuredAdmin.getUserFotoProfileAdmin(ctx());
        isLogin = SecuredAdmin.isLoggedIn(ctx());

        String reset = "no";
        String uaa_username = Configuration.root().getString("app.default.account.administrator.username");
        String tokenRandom = getTokenRandom();
        try {
            if(Configuration.root().getString("app.default.account.administrator.reset").equals("yes")){
                reset = "succes";
                if (iUserAccountAdmin.onCheckedBy_uaa_username(uaa_username)) {
                    //UPDATE USERACCOUNT ADMINISTRATOR JIKA SUDAH ADA
                    UserAccountAdmin userAccountAdmin = iUserAccountAdmin.onSelectBy_uaa_username(uaa_username);
                    userAccountAdmin.setUaa_userpassword(BCrypto.createPassword(tokenRandom));
                    userAccountAdmin.setUaa_usertype((short)0); //0 = administrator
                    userAccountAdmin.setUaa_activationcode(BCrypto.createPassword(uaa_username));
                    userAccountAdmin.setUaa_isforgotpassword(false);
                    userAccountAdmin.setUaa_updated(new Date());
                    userAccountAdmin = iUserAccountAdmin.onUpdate_Return_UserAccountAdmin(userAccountAdmin);
                    //UPDATE USERPROFILEADMIN ADMINISTRATOR JIKA SUDAH ADA
                    UserProfileAdmin userProfileAdmin = userAccountAdmin.getUaa_userprofileadmin();
                    userProfileAdmin.setUpa_useraccountadmin(userAccountAdmin);
                    userProfileAdmin.setUpa_updated(new Date());
                    userProfileAdmin.setUpa_updater(userAccountAdmin.getUaa_username());
                    iUserProfileAdmin.onUpdate(userProfileAdmin);

                    //SEND EMAIL
                    SendEmail sendEmail = new SendEmail(mailerClient);
                    sendEmail.onSendEmailResetPasswordAdministratorActivation(userAccountAdmin, tokenRandom);
                }else{
                    //INSERT USERACCOUNT ADMINISTRATOR JIKA BELUM ADA
                    UserAccountAdmin userAccountAdmin = new UserAccountAdmin();
                    userAccountAdmin.setUaa_username(uaa_username);
                    userAccountAdmin.setUaa_userpassword(BCrypto.createPassword(tokenRandom));
                    userAccountAdmin.setUaa_usertype((short)0); //0 = administrator
                    userAccountAdmin.setUaa_isactive(true);
                    userAccountAdmin.setUaa_activationcode(BCrypto.createPassword(uaa_username));
                    userAccountAdmin.setUaa_isforgotpassword(false);
                    userAccountAdmin.setUaa_created(new Date());
                    userAccountAdmin.setUaa_updated(new Date());
                    userAccountAdmin = iUserAccountAdmin.onSave_Return_UserAccountAdmin(userAccountAdmin);

                    userAccountAdmin.setUaa_creater(userAccountAdmin.getUaa_username());
                    userAccountAdmin.setUaa_updater(userAccountAdmin.getUaa_username());
                    iUserAccountAdmin.onUpdate(userAccountAdmin);

                    //INSERT USERPROFILEADMIN ADMINISTRATOR JIKA BELUM ADA
                    UserProfileAdmin userProfileAdmin = new UserProfileAdmin();
                    userProfileAdmin.setUpa_useraccountadmin(userAccountAdmin);
                    userProfileAdmin.setUpa_created(new Date());
                    userProfileAdmin.setUpa_creater(userAccountAdmin.getUaa_username());
                    userProfileAdmin.setUpa_updated(new Date());
                    userProfileAdmin.setUpa_updater(userAccountAdmin.getUaa_username());
                    iUserProfileAdmin.onSave(userProfileAdmin);

                    //SEND EMAIL
                    SendEmail sendEmail = new SendEmail(mailerClient);
                    sendEmail.onSendEmailResetPasswordAdministratorActivation(userAccountAdmin, tokenRandom);
                }
            }

        } catch (AppException e) {
            e.printStackTrace();
        }

        return ok(passwordreset.render("Reset Password", reset, isLogin, userAccountAdminLogin, userFotoProfileAdminLogin));
    }



}
