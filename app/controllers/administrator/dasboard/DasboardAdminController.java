package controllers.administrator.dasboard;

import controllers.administrator.dasboard.render.CreateMenuAdminHtml;
import controllers.util.SecuredAdmin;
import models.administrator.user.UserAccountAdmin;
import models.administrator.user.UserFotoProfileAdmin;
import play.libs.mailer.MailerClient;
import play.mvc.Controller;
import play.mvc.Result;
import play.mvc.Security;
import views.html.administrator.dasboard.dasboardadmin;

import javax.inject.Inject;


/**
 * Created by tri.jaruto on 18/11/2016.
 */
public class DasboardAdminController extends Controller {

    UserAccountAdmin userAccountAdminLogin = new UserAccountAdmin();
    UserFotoProfileAdmin userFotoProfileAdminLogin = new UserFotoProfileAdmin();
    boolean isLogin = false;

    @Inject
    MailerClient mailerClient;

    @Security.Authenticated(SecuredAdmin.class)
    public Result dasboardAdmin(){
        userAccountAdminLogin = SecuredAdmin.getUserInfo(ctx());
        userFotoProfileAdminLogin = SecuredAdmin.getUserFotoProfileAdmin(ctx());
        isLogin = SecuredAdmin.isLoggedIn(ctx());
        return ok(dasboardadmin.render("Dasboard Admin", isLogin, userAccountAdminLogin, userFotoProfileAdminLogin, new CreateMenuAdminHtml().getCreateMenuAdminRenderHtml(userAccountAdminLogin)));
    }

}
