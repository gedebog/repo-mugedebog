package controllers.administrator.dasboard.profile;

import controllers.administrator.dasboard.render.CreateMenuAdminHtml;
import controllers.util.SecuredAdmin;
import models.administrator.user.UserAccountAdmin;
import models.administrator.user.UserFotoProfileAdmin;
import models.administrator.user.UserProfileAdmin;
import play.data.DynamicForm;
import play.data.FormFactory;
import play.mvc.Controller;
import play.mvc.Result;
import play.mvc.Security;
import services.administrator.user.IUserProfileAdmin;
import services.administrator.user.UserProfileAdminLogic;
import views.html.administrator.dasboard.profile.profileadmin;

import javax.inject.Inject;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import static controllers.util.Constans.ACTION_EDIT;
import static controllers.util.Constans.ACTION_VIEW;

/**
 * Created by tri.jaruto on 13/12/2016.
 */
public class ProfileAdminController extends Controller {

    IUserProfileAdmin iUserProfileAdmin = new UserProfileAdminLogic();

    UserAccountAdmin userAccountAdminLogin = new UserAccountAdmin();
    UserFotoProfileAdmin userFotoProfileAdminLogin = new UserFotoProfileAdmin();
    boolean isLogin = false;

    SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");

    @Inject
    FormFactory formFactory;

    @Security.Authenticated(SecuredAdmin.class)
    public Result profileAdmin(){
        userAccountAdminLogin = SecuredAdmin.getUserInfo(ctx());
        userFotoProfileAdminLogin = SecuredAdmin.getUserFotoProfileAdmin(ctx());
        isLogin = SecuredAdmin.isLoggedIn(ctx());
        return ok(profileadmin.render("Profile Admin", isLogin, userAccountAdminLogin, userFotoProfileAdminLogin, new CreateMenuAdminHtml().getCreateMenuAdminRenderHtml(userAccountAdminLogin), ACTION_VIEW));
    }

    public Result onAction(){
        userAccountAdminLogin = SecuredAdmin.getUserInfo(ctx());
        userFotoProfileAdminLogin = SecuredAdmin.getUserFotoProfileAdmin(ctx());
        isLogin = SecuredAdmin.isLoggedIn(ctx());
        //GET DYNAMIC FORM
        DynamicForm requestData = formFactory.form().bindFromRequest();
        if(requestData.get("profile_save_button_name") != null){
            onUpdateAdmin(requestData);
            return ok(profileadmin.render("Profile", isLogin, userAccountAdminLogin, userFotoProfileAdminLogin, new CreateMenuAdminHtml().getCreateMenuAdminRenderHtml(userAccountAdminLogin), ACTION_VIEW));
        }else if(requestData.get("profile_edit_button_name") != null){
            return ok(profileadmin.render("Profile", isLogin, userAccountAdminLogin, userFotoProfileAdminLogin, new CreateMenuAdminHtml().getCreateMenuAdminRenderHtml(userAccountAdminLogin), ACTION_EDIT));
        }else if(requestData.get("profile_cancel_button_name") != null){
            return ok(profileadmin.render("Profile", isLogin, userAccountAdminLogin, userFotoProfileAdminLogin, new CreateMenuAdminHtml().getCreateMenuAdminRenderHtml(userAccountAdminLogin), ACTION_VIEW));
        }
        return ok(profileadmin.render("Profile", isLogin, userAccountAdminLogin, userFotoProfileAdminLogin, new CreateMenuAdminHtml().getCreateMenuAdminRenderHtml(userAccountAdminLogin), ACTION_VIEW));
    }

    public UserAccountAdmin onUpdateAdmin(DynamicForm requestData){
        try {
            UserProfileAdmin userProfileAdmin = new UserProfileAdmin();
            userProfileAdmin.setUpa_autoindex(userAccountAdminLogin.getUaa_userprofileadmin().getUpa_autoindex());
            userProfileAdmin.setUpa_name(requestData.get("upa_name_name"));
            userProfileAdmin.setUpa_nickname(requestData.get("upa_nickname_name"));
            userProfileAdmin.setUpa_birthplace(requestData.get("upa_birthplace_name"));
            userProfileAdmin.setUpa_birthdate(dateFormat.parse(requestData.get("upa_birthdate_name")));
            userProfileAdmin.setUpa_isgender(Boolean.parseBoolean(requestData.get("upa_isgender_name")));
            userProfileAdmin.setUpa_created(new Date());
            userProfileAdmin.setUpa_creater(userAccountAdminLogin.getUaa_username());
            userProfileAdmin.setUpa_updated(new Date());
            userProfileAdmin.setUpa_updater(userAccountAdminLogin.getUaa_username());
            iUserProfileAdmin.onUpdate(userProfileAdmin);
            userAccountAdminLogin.setUaa_userprofileadmin(userProfileAdmin);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return userAccountAdminLogin;
    }

}
