package controllers.administrator.dasboard.form;

import controllers.administrator.dasboard.form.security.JSONAdminAccountMainDetailListController;
import controllers.administrator.dasboard.form.security.JSONUserAccountMainDetailListController;
import controllers.util.AESCrypto;
import controllers.util.ConstansForm;
import controllers.util.ConstansTable;
import controllers.util.SecuredAdmin;
import models.administrator.app.AppMasterFormAdmin;
import models.administrator.user.UserAccountAdmin;
import models.administrator.user.UserFotoProfileAdmin;
import play.mvc.Controller;
import play.mvc.Result;
import services.administrator.app.AppMasterFormAdminLogic;
import services.administrator.app.IAppMasterFormAdmin;
import views.html.errors.error404admin;

/**
 * Created by tri.jaruto on 6/16/2017.
 */
public class JSONFormAdminMainDetailListController extends Controller {

    IAppMasterFormAdmin iAppMasterFormAdmin = new AppMasterFormAdminLogic();

    UserAccountAdmin userAccountAdminLogin = new UserAccountAdmin();
    UserFotoProfileAdmin userFotoProfileAdminLogin = new UserFotoProfileAdmin();
    boolean isLogin = false;


    public Result jSonFormAdminMainDetailListPage(String amfa_name, String amfa_filenamescalahtml, String tablename, String autoindex, String page, String length) {
        userAccountAdminLogin = SecuredAdmin.getUserInfo(ctx());
        amfa_name = AESCrypto.decrypt(amfa_name);
        amfa_filenamescalahtml = AESCrypto.decrypt(amfa_filenamescalahtml);
        tablename = AESCrypto.decrypt(tablename);
        autoindex = AESCrypto.decrypt(autoindex);
        return createJSONFormAdminDetailListPage(userAccountAdminLogin, amfa_name, amfa_filenamescalahtml, tablename, autoindex, page, length);
    }

    //CREATE JSON FORM ADMIN DETAIL LIST PAGE
    public Result createJSONFormAdminDetailListPage(UserAccountAdmin userAccountAdminLogin, String amfa_name, String amfa_filenamescalahtml, String tablename, String autoindex, String page, String length) {
        AppMasterFormAdmin appMasterFormAdmin = iAppMasterFormAdmin.onSelectBy_amfa_filenamescalahtml(amfa_filenamescalahtml);
        if(appMasterFormAdmin!=null){
            if(amfa_filenamescalahtml.equals(ConstansForm.APPMASTERFORMADMIN_AMFA_FILENAMESCALAHTML_ADMINACCOUNT)){
                if(tablename.equals(ConstansTable.TABLE_APP_MASTER_PERMISSONS_FORM_ADMIN)){
                    return new JSONAdminAccountMainDetailListController().jSONAppMasterPermissionsFormAdminListPage(userAccountAdminLogin, appMasterFormAdmin, autoindex, page, length);
                }
            }else if(amfa_filenamescalahtml.equals(ConstansForm.APPMASTERFORMADMIN_AMFA_FILENAMESCALAHTML_USERACCOUNT)){
                if(tablename.equals(ConstansTable.TABLE_APP_MASTER_PERMISSONS_FORM)){
                    return new JSONUserAccountMainDetailListController().jSONAppMasterPermissionsFormUserListPage(userAccountAdminLogin, appMasterFormAdmin, autoindex, page, length);
                }
            }
        }
        return getResultNull(amfa_name, amfa_filenamescalahtml);
    }

    //RESULT NULL
    public Result getResultNull(String amfa_name, String amfa_filenamescalahtml){
        userAccountAdminLogin = SecuredAdmin.getUserInfo(ctx());
        userFotoProfileAdminLogin = SecuredAdmin.getUserFotoProfileAdmin(ctx());
        isLogin = SecuredAdmin.isLoggedIn(ctx());
        return ok(error404admin.render("Error 404", "404", isLogin, userAccountAdminLogin, userFotoProfileAdminLogin));
    }
}
