package controllers.administrator.dasboard.form;

import controllers.administrator.dasboard.form.security.JSONAdminAccountMainDetailController;
import controllers.administrator.dasboard.form.security.JSONUserAccountMainDetailController;
import controllers.util.AESCrypto;
import controllers.util.ConstansForm;
import controllers.util.ConstansTable;
import controllers.util.SecuredAdmin;
import models.administrator.app.AppMasterFormAdmin;
import models.administrator.user.UserAccountAdmin;
import models.administrator.user.UserFotoProfileAdmin;
import play.mvc.Controller;
import play.mvc.Result;
import services.administrator.app.AppMasterFormAdminLogic;
import services.administrator.app.IAppMasterFormAdmin;
import views.html.errors.error404admin;

/**
 * Created by tri.jaruto on 6/16/2017.
 */
public class JSONFormAdminMainDetailController extends Controller {

    IAppMasterFormAdmin iAppMasterFormAdmin = new AppMasterFormAdminLogic();

    UserAccountAdmin userAccountAdminLogin = new UserAccountAdmin();
    UserFotoProfileAdmin userFotoProfileAdminLogin = new UserFotoProfileAdmin();
    boolean isLogin = false;

    public Result jSonFormAdminMainDetail(String amfa_name, String amfa_filenamescalahtml, String tablename, String autoindex) {
        userAccountAdminLogin = SecuredAdmin.getUserInfo(ctx());
        amfa_name = AESCrypto.decrypt(amfa_name);
        amfa_filenamescalahtml = AESCrypto.decrypt(amfa_filenamescalahtml);
        autoindex = AESCrypto.decrypt(autoindex);
        tablename = AESCrypto.decrypt(tablename);
        return createJSONFormAdminDetail(userAccountAdminLogin, amfa_name, amfa_filenamescalahtml, tablename, autoindex);
    }

    //CREATE JSON FORM ADMIN DETAIL
    public Result createJSONFormAdminDetail(UserAccountAdmin userAccountAdminLogin, String amfa_name, String amfa_filenamescalahtml, String tablename, String autoindex) {
        AppMasterFormAdmin appMasterFormAdmin = iAppMasterFormAdmin.onSelectBy_amfa_filenamescalahtml(amfa_filenamescalahtml);
        if(appMasterFormAdmin!=null){
            if(amfa_filenamescalahtml.equals(ConstansForm.APPMASTERFORMADMIN_AMFA_FILENAMESCALAHTML_ADMINACCOUNT)){
                if(tablename.equals(ConstansTable.TABLE_APP_MASTER_FORM_ADMIN)){
                    return new JSONAdminAccountMainDetailController().jSONAppMasterFormAdminList(autoindex);
                }
            }else if(amfa_filenamescalahtml.equals(ConstansForm.APPMASTERFORMADMIN_AMFA_FILENAMESCALAHTML_USERACCOUNT)){
                if(tablename.equals(ConstansTable.TABLE_APP_MASTER_FORM)){
                    return new JSONUserAccountMainDetailController().jSONAppMasterFormList(autoindex);
                }
            }
        }
        return getResultNull(amfa_name, amfa_filenamescalahtml);
    }

    //RESULT NULL
    public Result getResultNull(String amfa_name, String amfa_filenamescalahtml){
        userAccountAdminLogin = SecuredAdmin.getUserInfo(ctx());
        userFotoProfileAdminLogin = SecuredAdmin.getUserFotoProfileAdmin(ctx());
        isLogin = SecuredAdmin.isLoggedIn(ctx());
        return ok(error404admin.render("Error 404", "404", isLogin, userAccountAdminLogin, userFotoProfileAdminLogin));
    }
}
