package controllers.administrator.dasboard.form;

import controllers.administrator.dasboard.form.security.AdminAccountMainController;
import controllers.administrator.dasboard.form.security.UserAccountMainController;
import controllers.administrator.dasboard.render.CreateMenuAdminHtml;
import controllers.util.*;
import models.administrator.app.AppMasterFormAdmin;
import models.administrator.user.UserAccountAdmin;
import models.administrator.user.UserFotoProfileAdmin;
import play.data.DynamicForm;
import play.data.FormFactory;
import play.libs.mailer.MailerClient;
import play.mvc.Controller;
import play.mvc.Http;
import play.mvc.Result;
import play.mvc.Security;
import services.administrator.app.AppMasterFormAdminLogic;
import services.administrator.app.IAppMasterFormAdmin;
import views.html.administrator.dasboard.form.formadminlock;
import views.html.administrator.dasboard.form.formadminnull;
import views.html.errors.error404admin;

import javax.inject.Inject;
import java.io.File;

/**
 * Created by tri.jaruto on 18/11/2016.
 */
public class FormAdminMainController extends Controller {

    IAppMasterFormAdmin iAppMasterFormAdmin = new AppMasterFormAdminLogic();

    UserAccountAdmin userAccountAdminLogin = new UserAccountAdmin();
    UserFotoProfileAdmin userFotoProfileAdminLogin = new UserFotoProfileAdmin();
    boolean isLogin = false;

    UserAccountAdmin userAccountAdmin = new UserAccountAdmin();

    @Inject
    MailerClient mailerClient;

    @Inject
    FormFactory formFactory;

    //GET
    @Security.Authenticated(SecuredAdmin.class)
    public Result formAdminMain(String amfa_name, String amfa_filenamescalahtml, String autoindex, String action){
        userAccountAdminLogin = SecuredAdmin.getUserInfo(ctx());
        userFotoProfileAdminLogin = SecuredAdmin.getUserFotoProfileAdmin(ctx());
        isLogin = SecuredAdmin.isLoggedIn(ctx());
        Http.MultipartFormData<File> requestbody = request().body().asMultipartFormData();
        DynamicForm requestData = formFactory.form().bindFromRequest();
        amfa_name = AESCrypto.decrypt(URLEncDec.URLDecode(amfa_name));
        amfa_filenamescalahtml = AESCrypto.decrypt(URLEncDec.URLDecode(amfa_filenamescalahtml));
        autoindex = AESCrypto.decrypt(URLEncDec.URLDecode(autoindex));
        action = AESCrypto.decrypt(URLEncDec.URLDecode(action));
        return createFormAdminMainGET(isLogin, userAccountAdminLogin, userFotoProfileAdminLogin, amfa_name, amfa_filenamescalahtml, autoindex, action, requestData, requestbody, mailerClient);
    }

    public Result createFormAdminMainGET(boolean isLogin, UserAccountAdmin userAccountAdminLogin, UserFotoProfileAdmin userFotoProfileAdminLogin, String amfa_name, String amfa_filenamescalahtml, String autoindex, String action, DynamicForm requestData, Http.MultipartFormData<File> requestbody, MailerClient mailerClient){
        AppMasterFormAdmin appMasterFormAdmin = iAppMasterFormAdmin.onSelectBy_amfa_filenamescalahtml(amfa_filenamescalahtml);
        if(appMasterFormAdmin!=null){
            if(amfa_filenamescalahtml.equals(ConstansForm.APPMASTERFORMADMIN_AMFA_FILENAMESCALAHTML_ADMINACCOUNT)){
                return new AdminAccountMainController().userAccountAdminController(isLogin, userAccountAdminLogin, userFotoProfileAdminLogin, appMasterFormAdmin, autoindex, action, requestData, requestbody, mailerClient);
            }else if(amfa_filenamescalahtml.equals(ConstansForm.APPMASTERFORMADMIN_AMFA_FILENAMESCALAHTML_USERACCOUNT)){
                return new UserAccountMainController().userAccountController(isLogin, userAccountAdminLogin, userFotoProfileAdminLogin, appMasterFormAdmin, autoindex, action, requestData, requestbody, mailerClient);
            }
        }
        return getResultLockAndNull(isLogin, userAccountAdminLogin, userFotoProfileAdminLogin, amfa_name, amfa_filenamescalahtml);
    }


    //POST
    public Result onAction(){
        userAccountAdminLogin = SecuredAdmin.getUserInfo(ctx());
        userFotoProfileAdminLogin = SecuredAdmin.getUserFotoProfileAdmin(ctx());
        isLogin = SecuredAdmin.isLoggedIn(ctx());
        Http.MultipartFormData<File> requestbody = request().body().asMultipartFormData();
        DynamicForm requestData = formFactory.form().bindFromRequest();
        String amfa_name = AESCrypto.decrypt(URLEncDec.URLDecode(requestData.get("amfa_name_name")));
        String amfa_filenamescalahtml = AESCrypto.decrypt(URLEncDec.URLDecode(requestData.get("amfa_filenamescalahtml_name")));
        String tablename_input_name = AESCrypto.decrypt(URLEncDec.URLDecode(requestData.get("tablename_input_name")));
        String autoindex_input_name = AESCrypto.decrypt(URLEncDec.URLDecode(requestData.get("autoindex_input_name")));
        return createFormAdminMainPOST(isLogin, userAccountAdminLogin, userFotoProfileAdminLogin, amfa_name, amfa_filenamescalahtml, tablename_input_name, autoindex_input_name, Constans.ACTION_SAVE, requestData, requestbody, mailerClient);
    }

    public Result onUpload(){
        userAccountAdminLogin = SecuredAdmin.getUserInfo(ctx());
        isLogin = SecuredAdmin.isLoggedIn(ctx());
        Http.MultipartFormData<File> requestbody = request().body().asMultipartFormData();
        DynamicForm requestData = formFactory.form().bindFromRequest();
        String amfa_name = AESCrypto.decrypt(URLEncDec.URLDecode(requestData.get("amfa_name_name")));
        String amfa_filenamescalahtml = AESCrypto.decrypt(URLEncDec.URLDecode(requestData.get("amfa_filenamescalahtml_name")));
        String tablename_input_name = AESCrypto.decrypt(URLEncDec.URLDecode(requestData.get("tablename_input_name")));
        String autoindex_input_name = AESCrypto.decrypt(URLEncDec.URLDecode(requestData.get("autoindex_input_name")));
        return createFormAdminMainPOST(isLogin, userAccountAdminLogin, userFotoProfileAdminLogin, amfa_name, amfa_filenamescalahtml, tablename_input_name, autoindex_input_name, Constans.ACTION_UPLOAD, requestData, requestbody, mailerClient);
    }

    public Result createFormAdminMainPOST(boolean isLogin, UserAccountAdmin userAccountAdminLogin, UserFotoProfileAdmin userFotoProfileAdminLogin, String amfa_name, String amfa_filenamescalahtml, String tablename_input_name, String autoindex_input_name, String action, DynamicForm requestData, Http.MultipartFormData<File> requestbody, MailerClient mailerClient){
        AppMasterFormAdmin appMasterFormAdmin = iAppMasterFormAdmin.onSelectBy_amfa_filenamescalahtml(amfa_filenamescalahtml);
        if(appMasterFormAdmin!=null){
            if(amfa_filenamescalahtml.equals(ConstansForm.APPMASTERFORMADMIN_AMFA_FILENAMESCALAHTML_ADMINACCOUNT)){
                if(tablename_input_name.equals(ConstansTable.TABLE_USER_ACCOUNT_ADMIN)){
                    return new AdminAccountMainController().userAccountAdminController(isLogin, userAccountAdminLogin, userFotoProfileAdminLogin, appMasterFormAdmin, autoindex_input_name, action, requestData, requestbody, mailerClient);
                }
            }else if(amfa_filenamescalahtml.equals(ConstansForm.APPMASTERFORMADMIN_AMFA_FILENAMESCALAHTML_USERACCOUNT)){
                if(tablename_input_name.equals(ConstansTable.TABLE_USER_ACCOUNT)){
                    return new UserAccountMainController().userAccountController(isLogin, userAccountAdminLogin, userFotoProfileAdminLogin, appMasterFormAdmin, autoindex_input_name, action, requestData, requestbody, mailerClient);
                }
            }
        }
        return getResultLockAndNull(isLogin, userAccountAdminLogin, userFotoProfileAdminLogin, amfa_name, amfa_filenamescalahtml);
    }

    //RESULT LOCK AND NULL
    public Result getResultLockAndNull(boolean isLogin, UserAccountAdmin userAccountAdminLogin, UserFotoProfileAdmin userFotoProfileAdminLogin, String amfa_name, String amfa_filenamescalahtml){
        if(amfa_filenamescalahtml.equals(ConstansForm.APPMASTERFORMADMIN_AMFA_FILENAMESCALAHTML_FORMADMINLOCK)){
            return ok(formadminlock.render("Form Admin Lock", isLogin, userAccountAdminLogin, userFotoProfileAdminLogin, new CreateMenuAdminHtml().getCreateMenuAdminRenderHtml(userAccountAdminLogin)));
        }else if(amfa_filenamescalahtml.equals(ConstansForm.APPMASTERFORMADMIN_AMFA_FILENAMESCALAHTML_FORMADMINNULL)){
            return ok(formadminnull.render("Form Admin Null", isLogin, userAccountAdminLogin, userFotoProfileAdminLogin, new CreateMenuAdminHtml().getCreateMenuAdminRenderHtml(userAccountAdminLogin)));
        }else{
            return ok(error404admin.render("Error 404", "404", isLogin, userAccountAdminLogin, userFotoProfileAdminLogin));
        }
    }

}
