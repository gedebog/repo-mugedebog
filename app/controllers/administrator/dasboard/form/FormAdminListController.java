package controllers.administrator.dasboard.form;

import controllers.administrator.dasboard.form.security.*;
import controllers.administrator.dasboard.render.CreateMenuAdminHtml;
import controllers.util.*;
import models.administrator.app.AppMasterFormAdmin;
import models.administrator.user.UserAccountAdmin;
import models.administrator.user.UserFotoProfileAdmin;
import play.data.FormFactory;
import play.libs.mailer.MailerClient;
import play.mvc.Controller;
import play.mvc.Result;
import play.mvc.Security;
import services.administrator.app.AppMasterFormAdminLogic;
import services.administrator.app.IAppMasterFormAdmin;
import views.html.errors.error404admin;
import views.html.administrator.dasboard.form.formadminlock;
import views.html.administrator.dasboard.form.formadminnull;

import javax.inject.Inject;

/**
 * Created by tri.jaruto on 18/11/2016.
 */
public class FormAdminListController extends Controller {

    IAppMasterFormAdmin iAppMasterFormAdmin = new AppMasterFormAdminLogic();

    UserAccountAdmin userAccountAdminLogin = new UserAccountAdmin();
    UserFotoProfileAdmin userFotoProfileAdminLogin = new UserFotoProfileAdmin();
    boolean isLogin = false;

    @Inject
    MailerClient mailerClient;

    @Inject
    FormFactory formFactory;

    @Security.Authenticated(SecuredAdmin.class)
    public Result formAdminList(String amfa_name, String amfa_filenamescalahtml) {
        userAccountAdminLogin = SecuredAdmin.getUserInfo(ctx());
        userFotoProfileAdminLogin = SecuredAdmin.getUserFotoProfileAdmin(ctx());
        isLogin = SecuredAdmin.isLoggedIn(ctx());
        amfa_name = AESCrypto.decrypt(URLEncDec.URLDecode(amfa_name));
        amfa_filenamescalahtml = AESCrypto.decrypt(URLEncDec.URLDecode(amfa_filenamescalahtml));
        return createFormAdminList(isLogin, userAccountAdminLogin, userFotoProfileAdminLogin, amfa_name, amfa_filenamescalahtml);
    }

    public Result createFormAdminList(boolean isLogin, UserAccountAdmin userAccountAdminLogin, UserFotoProfileAdmin userFotoProfileAdminLogin, String amfa_name, String amfa_filenamescalahtml){
        AppMasterFormAdmin appMasterFormAdmin = iAppMasterFormAdmin.onSelectBy_amfa_filenamescalahtml(amfa_filenamescalahtml);
        if(appMasterFormAdmin!=null){
            if(amfa_filenamescalahtml.equals(ConstansForm.APPMASTERFORMADMIN_AMFA_FILENAMESCALAHTML_ADMINACCOUNT)){
                return new AdminAccountListController().userAccountAdminListController(isLogin, userAccountAdminLogin, userFotoProfileAdminLogin, appMasterFormAdmin);
            }else if(amfa_filenamescalahtml.equals(ConstansForm.APPMASTERFORMADMIN_AMFA_FILENAMESCALAHTML_USERACCOUNT)){
                return new UserAccountListController().userAccountListController(isLogin, userAccountAdminLogin, userFotoProfileAdminLogin, appMasterFormAdmin);
            }
        }
        return getResultLockAndNull(isLogin, userAccountAdminLogin, userFotoProfileAdminLogin, amfa_name, amfa_filenamescalahtml);
    }


    //RESULT LOCK AND NULL
    public Result getResultLockAndNull(boolean isLogin, UserAccountAdmin userAccountAdminLogin,  UserFotoProfileAdmin userFotoProfileAdminLogin, String amfa_name, String amfa_filenamescalahtml){
        if(amfa_filenamescalahtml.equals(ConstansForm.APPMASTERFORMADMIN_AMFA_FILENAMESCALAHTML_FORMADMINLOCK)){
            return ok(formadminlock.render(amfa_name+" Lock", isLogin, userAccountAdminLogin, userFotoProfileAdminLogin, new CreateMenuAdminHtml().getCreateMenuAdminRenderHtml(userAccountAdminLogin)));
        }else if(amfa_filenamescalahtml.equals(ConstansForm.APPMASTERFORMADMIN_AMFA_FILENAMESCALAHTML_FORMADMINNULL)){
            return ok(formadminnull.render(amfa_name+" Null", isLogin, userAccountAdminLogin, userFotoProfileAdminLogin, new CreateMenuAdminHtml().getCreateMenuAdminRenderHtml(userAccountAdminLogin)));
        }else{
            return ok(error404admin.render("Error 404", "404", isLogin, userAccountAdminLogin, userFotoProfileAdminLogin));
        }
    }


}
