package controllers.administrator.dasboard.form.security;

import controllers.administrator.dasboard.render.CreateMenuAdminHtml;
import controllers.administrator.dasboard.render.CreateMenuFormAdminMainHtml;
import controllers.util.*;
import models.administrator.app.AppMasterFormAdmin;
import models.administrator.app.AppMasterPermissionsFormAdmin;
import models.administrator.user.UserAccountAdmin;
import models.administrator.user.UserFotoProfileAdmin;
import models.user.user.UserAccount;
import models.user.user.UserFotoProfile;
import models.user.user.UserProfile;
import play.Configuration;
import play.data.DynamicForm;
import play.libs.mailer.MailerClient;
import play.mvc.Controller;
import play.mvc.Http;
import play.mvc.Result;
import play.mvc.Security;
import services.administrator.app.AppMasterPermissionsFormAdminLogic;
import services.administrator.app.IAppMasterPermissionsFormAdmin;
import services.user.user.*;
import views.html.administrator.dasboard.form.security.useraccountmain;

import java.io.File;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by tri.jaruto on 5/31/2017.
 */
public class UserAccountMainController extends Controller {

    IAppMasterPermissionsFormAdmin iAppMasterPermissionsFormAdmin = new AppMasterPermissionsFormAdminLogic();
    IUserAccount iUserAccount = new UserAccountLogic();
    IUserProfile iUserProfile = new UserProfileLogic();
    IUserFotoProfile iUserFotoProfile = new UserFotoProfileLogic();

    UserAccount userAccount = new UserAccount();
    UserFotoProfile userFotoProfile = new UserFotoProfile();

    AppMasterPermissionsFormAdmin appMasterPermissionsFormAdmin = new AppMasterPermissionsFormAdmin();

    SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");

    Result result;

    @Security.Authenticated(SecuredAdmin.class)
    public Result userAccountController(boolean isLogin, UserAccountAdmin userAccountAdminLogin, UserFotoProfileAdmin userFotoProfileAdminLogin, AppMasterFormAdmin appMasterFormAdmin, String autoindex, String action, DynamicForm requestData, Http.MultipartFormData<File> requestbody, MailerClient mailerClient){
        appMasterPermissionsFormAdmin = iAppMasterPermissionsFormAdmin.onSelectBy_ampfa_useraccountadmin_and_ampfa_appmasterformadmin(userAccountAdminLogin, appMasterFormAdmin);
        if(action.equals(Constans.ACTION_VIEW)){
            result = onActionView(isLogin, userAccountAdminLogin, userFotoProfileAdminLogin, appMasterFormAdmin, appMasterPermissionsFormAdmin, autoindex, action, requestData, requestbody, mailerClient);
        }else if(action.equals(Constans.ACTION_NEW)){
            result = onActionNew(isLogin, userAccountAdminLogin, userFotoProfileAdminLogin, appMasterFormAdmin, appMasterPermissionsFormAdmin, autoindex, action, requestData, requestbody, mailerClient);
        }else if(action.equals(Constans.ACTION_SAVE)){
            result = onActionSave(isLogin, userAccountAdminLogin, userFotoProfileAdminLogin, appMasterFormAdmin, appMasterPermissionsFormAdmin, autoindex, action, requestData, requestbody, mailerClient);
        }else if(action.equals(Constans.ACTION_EDIT)){
            result = onActionEdit(isLogin, userAccountAdminLogin, userFotoProfileAdminLogin, appMasterFormAdmin, appMasterPermissionsFormAdmin, autoindex, action, requestData, requestbody, mailerClient);
        }else if(action.equals(Constans.ACTION_DELETE)){
            result = onActionDelete(isLogin, userAccountAdminLogin, userFotoProfileAdminLogin, appMasterFormAdmin, appMasterPermissionsFormAdmin, autoindex, action, requestData, requestbody, mailerClient);
        }else if(action.equals(Constans.ACTION_UPLOAD)){
            result = onActionUpload(isLogin, userAccountAdminLogin, userFotoProfileAdminLogin, appMasterFormAdmin, appMasterPermissionsFormAdmin, autoindex, action, requestData, requestbody, mailerClient);
        }
        return result;
    }

    //ACTION
    public Result onActionView(boolean isLogin, UserAccountAdmin userAccountAdminLogin, UserFotoProfileAdmin userFotoProfileAdminLogin, AppMasterFormAdmin appMasterFormAdmin, AppMasterPermissionsFormAdmin appMasterPermissionsFormAdmin, String autoindex, String action, DynamicForm requestData, Http.MultipartFormData<File> requestbody, MailerClient mailerClient){
        if(!autoindex.equals("0")) {
            userAccount = iUserAccount.onSelectBy_ua_autoindex(Long.valueOf(autoindex));
            userFotoProfile = iUserFotoProfile.onSelectBy_ufp_useraccount(userAccount);
        }
        return ok(useraccountmain.render(appMasterFormAdmin.getAmfa_name(), isLogin, userAccountAdminLogin, userFotoProfileAdminLogin, userAccount, userFotoProfile, appMasterFormAdmin, appMasterPermissionsFormAdmin, new CreateMenuAdminHtml().getCreateMenuAdminRenderHtml(userAccountAdminLogin), new CreateMenuFormAdminMainHtml().getCreateMenuFormAdminMainRenderHtml(autoindex, appMasterFormAdmin, appMasterPermissionsFormAdmin, action), action));
    }

    public Result onActionNew(boolean isLogin, UserAccountAdmin userAccountAdminLogin, UserFotoProfileAdmin userFotoProfileAdminLogin, AppMasterFormAdmin appMasterFormAdmin, AppMasterPermissionsFormAdmin appMasterPermissionsFormAdmin, String autoindex, String action, DynamicForm requestData, Http.MultipartFormData<File> requestbody, MailerClient mailerClient){
        appMasterPermissionsFormAdmin = iAppMasterPermissionsFormAdmin.onSelectBy_ampfa_useraccountadmin_and_ampfa_appmasterformadmin(userAccountAdminLogin, appMasterFormAdmin);
        userFotoProfile = null;
        return ok(useraccountmain.render(appMasterFormAdmin.getAmfa_name(), isLogin, userAccountAdminLogin, userFotoProfileAdminLogin, userAccount, userFotoProfile, appMasterFormAdmin, appMasterPermissionsFormAdmin, new CreateMenuAdminHtml().getCreateMenuAdminRenderHtml(userAccountAdminLogin), new CreateMenuFormAdminMainHtml().getCreateMenuFormAdminMainRenderHtml(autoindex, appMasterFormAdmin, appMasterPermissionsFormAdmin, action), action));
    }

    public Result onActionSave(boolean isLogin, UserAccountAdmin userAccountAdminLogin, UserFotoProfileAdmin userFotoProfileAdminLogin, AppMasterFormAdmin appMasterFormAdmin, AppMasterPermissionsFormAdmin appMasterPermissionsFormAdmin, String autoindex, String action, DynamicForm requestData, Http.MultipartFormData<File> requestbody, MailerClient mailerClient){
        appMasterPermissionsFormAdmin = iAppMasterPermissionsFormAdmin.onSelectBy_ampfa_useraccountadmin_and_ampfa_appmasterformadmin(userAccountAdminLogin, appMasterFormAdmin);
        if(autoindex.equals("0")) {
            userFotoProfile = null;
            String ua_username_name = requestData.get("ua_username_name");
            String ua_userpassword_name = requestData.get("ua_userpassword_name");
            //INSERT
            try {
                userAccount = new UserAccount();
                userAccount.setUa_username(ua_username_name);
                userAccount.setUa_userpassword(BCrypto.createPassword(ua_userpassword_name));
                userAccount.setUa_isactive(requestData.get("ua_isactive_name")==null ? false : true);
                userAccount.setUa_activationcode(BCrypto.createPassword(ua_username_name));
                userAccount.setUa_isforgotpassword(false);
                userAccount.setUa_created(new Date());
                userAccount.setUa_creater(userAccountAdminLogin.getUaa_username());
                userAccount.setUa_updated(new Date());
                userAccount.setUa_updater(userAccountAdminLogin.getUaa_username());
                iUserAccount.onSave(userAccount);

                UserProfile userProfile = new UserProfile();
                userProfile.setUp_useraccount(userAccount);
                userProfile.setUp_name(requestData.get("up_name_name"));
                userProfile.setUp_nickname(requestData.get("up_nickname_name"));
                userProfile.setUp_birthplace(requestData.get("up_birthplace_name"));
                userProfile.setUp_birthdate(dateFormat.parse(requestData.get("up_birthdate_name")));
                userProfile.setUp_isgender(Boolean.parseBoolean(requestData.get("up_isgender_name")));
                userProfile.setUp_created(new Date());
                userProfile.setUp_creater(userAccountAdminLogin.getUaa_username());
                userProfile.setUp_updated(new Date());
                userProfile.setUp_updater(userAccountAdminLogin.getUaa_username());
                iUserProfile.onSave(userProfile);
                userAccount.setUa_userprofile(userProfile);

                //SEND EMAIL
                SendEmail sendEmail = new SendEmail(mailerClient);
                sendEmail.onSendEmailCreatePasswordUserActivation(userAccount, ua_userpassword_name);

            } catch (AppException e) {
                e.printStackTrace();
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }else{
            userAccount = iUserAccount.onSelectBy_ua_autoindex(Long.valueOf(autoindex));
            userFotoProfile = iUserFotoProfile.onSelectBy_ufp_useraccount(userAccount);
            String ua_username_name = requestData.get("ua_username_name");
            String ua_userpassword_name = requestData.get("ua_userpassword_name");
            //UPDATE
            try {
                userAccount.setUa_username(ua_username_name);
                userAccount.setUa_userpassword(BCrypto.createPassword(ua_userpassword_name));
                userAccount.setUa_isactive(requestData.get("ua_isactive_name")==null ? false : true);
                userAccount.setUa_activationcode(BCrypto.createPassword(ua_username_name));
                userAccount.setUa_isforgotpassword(false);
                userAccount.setUa_updated(new Date());
                userAccount.setUa_updater(userAccountAdminLogin.getUaa_username());
                iUserAccount.onUpdate(userAccount);

                UserProfile userProfile = userAccount.getUa_userprofile();
                userProfile.setUp_name(requestData.get("up_name_name"));
                userProfile.setUp_nickname(requestData.get("up_nickname_name"));
                userProfile.setUp_birthplace(requestData.get("up_birthplace_name"));
                userProfile.setUp_birthdate(dateFormat.parse(requestData.get("up_birthdate_name")));
                userProfile.setUp_isgender(Boolean.parseBoolean(requestData.get("up_isgender_name")));
                userProfile.setUp_updated(new Date());
                userProfile.setUp_updater(userAccountAdminLogin.getUaa_username());
                iUserProfile.onUpdate(userProfile);
                userAccount.setUa_userprofile(userProfile);

                //SEND EMAIL
                SendEmail sendEmail = new SendEmail(mailerClient);
                sendEmail.onSendEmailUpdatePasswordUserActivation(userAccount, ua_userpassword_name);

            } catch (AppException e) {
                e.printStackTrace();
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }
        return ok(useraccountmain.render(appMasterFormAdmin.getAmfa_name(), isLogin, userAccountAdminLogin, userFotoProfileAdminLogin, userAccount, userFotoProfile, appMasterFormAdmin, appMasterPermissionsFormAdmin, new CreateMenuAdminHtml().getCreateMenuAdminRenderHtml(userAccountAdminLogin), new CreateMenuFormAdminMainHtml().getCreateMenuFormAdminMainRenderHtml(autoindex, appMasterFormAdmin, appMasterPermissionsFormAdmin, Constans.ACTION_VIEW), Constans.ACTION_VIEW));
    }

    public Result onActionEdit(boolean isLogin, UserAccountAdmin userAccountAdminLogin, UserFotoProfileAdmin userFotoProfileAdminLogin, AppMasterFormAdmin appMasterFormAdmin, AppMasterPermissionsFormAdmin appMasterPermissionsFormAdmin, String autoindex, String action, DynamicForm requestData, Http.MultipartFormData<File> requestbody, MailerClient mailerClient){
        appMasterPermissionsFormAdmin = iAppMasterPermissionsFormAdmin.onSelectBy_ampfa_useraccountadmin_and_ampfa_appmasterformadmin(userAccountAdminLogin, appMasterFormAdmin);
        if(!autoindex.equals("0")) {
            userAccount = iUserAccount.onSelectBy_ua_autoindex(Long.valueOf(autoindex));
            userFotoProfile = iUserFotoProfile.onSelectBy_ufp_useraccount(userAccount);
            userAccount.setUa_userpassword(null);
        }
        return ok(useraccountmain.render(appMasterFormAdmin.getAmfa_name(), isLogin, userAccountAdminLogin, userFotoProfileAdminLogin, userAccount, userFotoProfile, appMasterFormAdmin, appMasterPermissionsFormAdmin, new CreateMenuAdminHtml().getCreateMenuAdminRenderHtml(userAccountAdminLogin), new CreateMenuFormAdminMainHtml().getCreateMenuFormAdminMainRenderHtml(autoindex, appMasterFormAdmin, appMasterPermissionsFormAdmin, action), action));
    }

    public Result onActionDelete(boolean isLogin, UserAccountAdmin userAccountAdminLogin, UserFotoProfileAdmin userFotoProfileAdminLogin, AppMasterFormAdmin appMasterFormAdmin, AppMasterPermissionsFormAdmin appMasterPermissionsFormAdmin, String autoindex, String action, DynamicForm requestData, Http.MultipartFormData<File> requestbody, MailerClient mailerClient){
        userAccountAdminLogin = SecuredAdmin.getUserInfo(ctx());
        appMasterPermissionsFormAdmin = iAppMasterPermissionsFormAdmin.onSelectBy_ampfa_useraccountadmin_and_ampfa_appmasterformadmin(userAccountAdminLogin, appMasterFormAdmin);
        return ok(useraccountmain.render(appMasterFormAdmin.getAmfa_name(), isLogin, userAccountAdminLogin, userFotoProfileAdminLogin, userAccount, userFotoProfile, appMasterFormAdmin, appMasterPermissionsFormAdmin, new CreateMenuAdminHtml().getCreateMenuAdminRenderHtml(userAccountAdminLogin), new CreateMenuFormAdminMainHtml().getCreateMenuFormAdminMainRenderHtml(autoindex, appMasterFormAdmin, appMasterPermissionsFormAdmin, action), action));
    }

    public Result onActionUpload(boolean isLogin, UserAccountAdmin userAccountAdminLogin, UserFotoProfileAdmin userFotoProfileAdminLogin, AppMasterFormAdmin appMasterFormAdmin, AppMasterPermissionsFormAdmin appMasterPermissionsFormAdmin, String autoindex, String action, DynamicForm requestData, Http.MultipartFormData<File> requestbody, MailerClient mailerClient){
        appMasterPermissionsFormAdmin = iAppMasterPermissionsFormAdmin.onSelectBy_ampfa_useraccountadmin_and_ampfa_appmasterformadmin(userAccountAdminLogin, appMasterFormAdmin);
        Http.MultipartFormData.FilePart<File> picture = requestbody.getFile("ufp_filename_name");

        System.out.println("picture : "+picture.getFilename());
        if(!autoindex.equals("0")) {
            userAccount = iUserAccount.onSelectBy_ua_autoindex(Long.valueOf(autoindex));
            userFotoProfile = iUserFotoProfile.onSelectBy_ufp_useraccount(userAccount);

            if(userFotoProfile==null){
                userFotoProfile = new UserFotoProfile();
                userFotoProfile.setUfp_useraccount(userAccount);
                userFotoProfile.setUfp_name(userAccount.getUa_username());
                if(Configuration.root().getString("app.upload.file.type").equals(Constans.PROFILE_FOTO_UPLOAD_CLOUDINARY)){
                    userFotoProfile.setUfp_status(Constans.PROFILE_FOTO_UPLOAD_CLOUDINARY);
                    userFotoProfile.setUfp_note("");
                    userFotoProfile.setUfp_filename(UploadImage.onUploadUserFotoProfileToCloudinary(picture, userAccount.getUa_username()));
                }else {
                    userFotoProfile.setUfp_status(Constans.PROFILE_FOTO_UPLOAD_LOCALHOST);
                    userFotoProfile.setUfp_note("");
                    userFotoProfile.setUfp_filename(UploadImage.onUploadUserFotoProfileToLocal(picture, userAccount.getUa_username()));
                }
                userFotoProfile.setUfp_created(new Date());
                System.out.println("userAccountAdminLogin.getUaa_username() : "+userAccountAdminLogin.getUaa_username());
                userFotoProfile.setUfp_creater(userAccountAdminLogin.getUaa_username());
                userFotoProfile.setUfp_updated(new Date());
                userFotoProfile.setUfp_updater(userAccountAdminLogin.getUaa_username());
                iUserFotoProfile.onSave(userFotoProfile);
            }else{
                userFotoProfile.setUfp_useraccount(userAccount);
                userFotoProfile.setUfp_name(userAccount.getUa_username());
                if(Configuration.root().getString("app.upload.file.type").equals(Constans.PROFILE_FOTO_UPLOAD_CLOUDINARY)){
                    userFotoProfile.setUfp_status(Constans.PROFILE_FOTO_UPLOAD_CLOUDINARY);
                    userFotoProfile.setUfp_note("");
                    userFotoProfile.setUfp_filename(UploadImage.onUploadUserFotoProfileToLocal(picture, userAccount.getUa_username()));
                }else {
                    userFotoProfile.setUfp_status(Constans.PROFILE_FOTO_UPLOAD_LOCALHOST);
                    userFotoProfile.setUfp_note("");
                    userFotoProfile.setUfp_filename(UploadImage.onUploadUserFotoProfileToLocal(picture, userAccount.getUa_username()));
                }
                userFotoProfile.setUfp_updated(new Date());
                userFotoProfile.setUfp_updater(userAccountAdminLogin.getUaa_username());
                iUserFotoProfile.onUpdate(userFotoProfile);
            }

        }
        return ok(useraccountmain.render(appMasterFormAdmin.getAmfa_name(), isLogin, userAccountAdminLogin, userFotoProfileAdminLogin, userAccount, userFotoProfile, appMasterFormAdmin, appMasterPermissionsFormAdmin, new CreateMenuAdminHtml().getCreateMenuAdminRenderHtml(userAccountAdminLogin), new CreateMenuFormAdminMainHtml().getCreateMenuFormAdminMainRenderHtml(autoindex, appMasterFormAdmin, appMasterPermissionsFormAdmin, Constans.ACTION_VIEW), Constans.ACTION_VIEW));
    }


}
