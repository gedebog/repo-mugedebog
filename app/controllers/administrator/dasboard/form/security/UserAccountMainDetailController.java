package controllers.administrator.dasboard.form.security;

import controllers.administrator.dasboard.render.CreateMenuAdminHtml;
import controllers.administrator.dasboard.render.CreateMenuFormAdminMainHtml;
import controllers.util.AESCrypto;
import controllers.util.Constans;
import controllers.util.SecuredAdmin;
import controllers.util.URLEncDec;
import models.administrator.app.AppMasterFormAdmin;
import models.administrator.app.AppMasterPermissionsFormAdmin;
import models.administrator.user.UserAccountAdmin;
import models.administrator.user.UserFotoProfileAdmin;
import models.user.app.AppMasterForm;
import models.user.app.AppMasterPermissionsForm;
import models.user.user.UserAccount;
import models.user.user.UserFotoProfile;
import play.data.DynamicForm;
import play.libs.mailer.MailerClient;
import play.mvc.Controller;
import play.mvc.Http;
import play.mvc.Result;
import play.mvc.Security;
import services.administrator.app.AppMasterPermissionsFormAdminLogic;
import services.administrator.app.IAppMasterPermissionsFormAdmin;
import services.user.app.AppMasterFormLogic;
import services.user.app.AppMasterPermissionsFormLogic;
import services.user.app.IAppMasterForm;
import services.user.app.IAppMasterPermissionsForm;
import services.user.user.IUserAccount;
import services.user.user.IUserFotoProfile;
import services.user.user.UserAccountLogic;
import services.user.user.UserFotoProfileLogic;
import views.html.administrator.dasboard.form.security.useraccountmain;

import java.io.File;
import java.util.Date;

/**
 * Created by tri.jaruto on 5/31/2017.
 */
public class UserAccountMainDetailController extends Controller {

    IAppMasterPermissionsFormAdmin iAppMasterPermissionsFormAdmin = new AppMasterPermissionsFormAdminLogic();
    IAppMasterPermissionsForm iAppMasterPermissionsForm = new AppMasterPermissionsFormLogic();

    IUserAccount iUserAccount = new UserAccountLogic();
    IUserFotoProfile iUserFotoProfile = new UserFotoProfileLogic();
    IAppMasterForm iAppMasterForm = new AppMasterFormLogic();

    UserAccount userAccount = new UserAccount();
    UserFotoProfile userFotoProfile = new UserFotoProfile();
    AppMasterForm selectedAppMasterForm = new AppMasterForm();

    AppMasterPermissionsFormAdmin appMasterPermissionsFormAdmin = new AppMasterPermissionsFormAdmin();

    Result result;

    @Security.Authenticated(SecuredAdmin.class)
    public Result appMasterPermissionsFormDetailController(boolean isLogin, UserAccountAdmin userAccountAdminLogin, UserFotoProfileAdmin userFotoProfileAdminLogin, AppMasterFormAdmin appMasterFormAdmin, String autoindex, String action, DynamicForm requestData, Http.MultipartFormData<File> requestbody, MailerClient mailerClient){
        appMasterPermissionsFormAdmin = iAppMasterPermissionsFormAdmin.onSelectBy_ampfa_useraccountadmin_and_ampfa_appmasterformadmin(userAccountAdminLogin, appMasterFormAdmin);
        if(action.equals(Constans.ACTION_SAVE)){
            result = onActionSave(isLogin, userAccountAdminLogin, userFotoProfileAdminLogin, appMasterFormAdmin, appMasterPermissionsFormAdmin, autoindex, action, requestData, requestbody, mailerClient);
        }
        return result;
    }

    public Result onActionSave(boolean isLogin, UserAccountAdmin userAccountAdminLogin, UserFotoProfileAdmin userFotoProfileAdminLogin, AppMasterFormAdmin appMasterFormAdmin, AppMasterPermissionsFormAdmin appMasterPermissionsFormAdmin, String autoindex, String action, DynamicForm requestData, Http.MultipartFormData<File> requestbody, MailerClient mailerClient){
        String save_value = requestData.get("action_input_name");
        if(save_value.equals(Constans.ACTION_VALUE_NEW)) {
            userAccount = iUserAccount.onSelectBy_ua_autoindex(Long.valueOf(autoindex));
            userFotoProfile = iUserFotoProfile.onSelectBy_ufp_useraccount(userAccount);
            selectedAppMasterForm = iAppMasterForm.onSelectBy_amf_autoindex(Long.valueOf(requestData.get("appmasterform_input_name")));

            AppMasterPermissionsForm selecteAppMasterPermissionsForm = new AppMasterPermissionsForm();
            selecteAppMasterPermissionsForm.setAmpf_useraccount(userAccount);
            selecteAppMasterPermissionsForm.setAmpf_appmasterform(selectedAppMasterForm);
            selecteAppMasterPermissionsForm.setAmpf_isnew(requestData.get("ampf_isnew_name")==null ? false : true);
            selecteAppMasterPermissionsForm.setAmpf_isselect(requestData.get("ampf_isselect_name")==null ? false : true);
            selecteAppMasterPermissionsForm.setAmpf_isinsert(requestData.get("ampf_isinsert_name")==null ? false : true);
            selecteAppMasterPermissionsForm.setAmpf_isupdate(requestData.get("ampf_isupdate_name")==null ? false : true);
            selecteAppMasterPermissionsForm.setAmpf_isdelete(requestData.get("ampf_isdelete_name")==null ? false : true);
            selecteAppMasterPermissionsForm.setAmpf_isprint(requestData.get("ampf_isprint_name")==null ? false : true);
            selecteAppMasterPermissionsForm.setAmpf_isexport(requestData.get("ampf_isexport_name")==null ? false : true);
            selecteAppMasterPermissionsForm.setAmpf_isimport(requestData.get("ampf_isimport_name")==null ? false : true);
            selecteAppMasterPermissionsForm.setAmpf_isactive(requestData.get("ampf_isactive_name")==null ? false : true);
            selecteAppMasterPermissionsForm.setAmpf_created(new Date());
            selecteAppMasterPermissionsForm.setAmpf_creater(userAccountAdminLogin.getUaa_username());
            selecteAppMasterPermissionsForm.setAmpf_updated(new Date());
            selecteAppMasterPermissionsForm.setAmpf_updater(userAccountAdminLogin.getUaa_username());
            iAppMasterPermissionsForm.onSave(selecteAppMasterPermissionsForm);

            //INSERT
           // System.out.println("appMasterPermissionsFormDetailController onActionSave : INSERT");
        }else{
            userAccount = iUserAccount.onSelectBy_ua_autoindex(Long.valueOf(autoindex));
            userFotoProfile = iUserFotoProfile.onSelectBy_ufp_useraccount(userAccount);
            selectedAppMasterForm = iAppMasterForm.onSelectBy_amf_autoindex(Long.valueOf(requestData.get("appmasterform_input_name")));

            AppMasterPermissionsForm selecteAppMasterPermissionsForm = iAppMasterPermissionsForm.onSelectBy_ampf_autoindex(Long.valueOf(AESCrypto.decrypt(URLEncDec.URLDecode(requestData.get("ampf_autoindex_input_name")))));
            selecteAppMasterPermissionsForm.setAmpf_useraccount(userAccount);
            selecteAppMasterPermissionsForm.setAmpf_appmasterform(selectedAppMasterForm);
            selecteAppMasterPermissionsForm.setAmpf_isnew(requestData.get("ampf_isnew_name")==null ? false : true);
            selecteAppMasterPermissionsForm.setAmpf_isselect(requestData.get("ampf_isselect_name")==null ? false : true);
            selecteAppMasterPermissionsForm.setAmpf_isinsert(requestData.get("ampf_isinsert_name")==null ? false : true);
            selecteAppMasterPermissionsForm.setAmpf_isupdate(requestData.get("ampf_isupdate_name")==null ? false : true);
            selecteAppMasterPermissionsForm.setAmpf_isdelete(requestData.get("ampf_isdelete_name")==null ? false : true);
            selecteAppMasterPermissionsForm.setAmpf_isprint(requestData.get("ampf_isprint_name")==null ? false : true);
            selecteAppMasterPermissionsForm.setAmpf_isexport(requestData.get("ampf_isexport_name")==null ? false : true);
            selecteAppMasterPermissionsForm.setAmpf_isimport(requestData.get("ampf_isimport_name")==null ? false : true);
            selecteAppMasterPermissionsForm.setAmpf_isactive(requestData.get("ampf_isactive_name")==null ? false : true);
            selecteAppMasterPermissionsForm.setAmpf_updated(new Date());
            selecteAppMasterPermissionsForm.setAmpf_updater(userAccountAdminLogin.getUaa_username());
            iAppMasterPermissionsForm.onUpdate(selecteAppMasterPermissionsForm);

            //UPDATE
            //System.out.println("appMasterPermissionsFormDetailController onActionSave : UPDATE");
        }
        return ok(useraccountmain.render(appMasterFormAdmin.getAmfa_name(), isLogin, userAccountAdminLogin, userFotoProfileAdminLogin, userAccount, userFotoProfile, appMasterFormAdmin, appMasterPermissionsFormAdmin, new CreateMenuAdminHtml().getCreateMenuAdminRenderHtml(userAccountAdminLogin), new CreateMenuFormAdminMainHtml().getCreateMenuFormAdminMainRenderHtml(autoindex, appMasterFormAdmin, appMasterPermissionsFormAdmin, Constans.ACTION_VIEW), Constans.ACTION_VIEW));
    }

}
