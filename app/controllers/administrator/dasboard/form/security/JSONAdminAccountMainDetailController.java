package controllers.administrator.dasboard.form.security;

import models.administrator.app.AppMasterFormAdmin;
import play.libs.Json;
import play.mvc.Controller;
import play.mvc.Result;
import services.administrator.app.AppMasterFormAdminLogic;
import services.administrator.app.IAppMasterFormAdmin;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by tri.jaruto on 5/31/2017.
 */
public class JSONAdminAccountMainDetailController extends Controller {

    IAppMasterFormAdmin iAppMasterFormAdmin = new AppMasterFormAdminLogic();
    List<AppMasterFormAdmin> appMasterFormAdmins = new ArrayList<AppMasterFormAdmin>();

    public Result jSONAppMasterFormAdminList(String autoindex){
        appMasterFormAdmins.clear();
        appMasterFormAdmins = iAppMasterFormAdmin.onSelectAllBy_amfa_autoindex_notin_ampfa_appmasterformadmin_by_ampfa_useraccountadmin(Long.valueOf(autoindex));
        response().setHeader("Access-Control-Allow-Origin", "*");
        response().setHeader("Cache-Control", "no-cache");
        if(appMasterFormAdmins!=null){
            return ok(Json.toJson(appMasterFormAdmins));
        }
        return ok("Empty Data");
    }

}
