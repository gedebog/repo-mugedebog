package controllers.administrator.dasboard.form.security;

import play.mvc.Controller;
import play.mvc.Result;
import services.administrator.user.*;
import views.html.administrator.dasboard.form.security.adminaccountmain;

/**
 * Created by tri.jaruto on 5/31/2017.
 */
public class JSONAdminAccountMainController extends Controller {

    IUserAccountAdmin iUserAccountAdmin = new UserAccountAdminLogic();

    public Result jSONUserAccountAdmin_CheckedBy_uaa_username(String parameter){
        boolean isChecked = iUserAccountAdmin.onCheckedBy_uaa_username(parameter);
        response().setHeader("Access-Control-Allow-Origin", "*");
        response().setHeader("Cache-Control", "no-cache");
        return ok(String.valueOf(isChecked));
    }

}
