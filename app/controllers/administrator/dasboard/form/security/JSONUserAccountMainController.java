package controllers.administrator.dasboard.form.security;

import play.mvc.Controller;
import play.mvc.Result;
import services.user.user.IUserAccount;
import services.user.user.UserAccountLogic;

/**
 * Created by tri.jaruto on 5/31/2017.
 */
public class JSONUserAccountMainController extends Controller {

    IUserAccount iUserAccount = new UserAccountLogic();

    public Result jSONUserAccount_CheckedBy_ua_username(String parameter){
        boolean isChecked = iUserAccount.onCheckedBy_ua_username(parameter);
        response().setHeader("Access-Control-Allow-Origin", "*");
        response().setHeader("Cache-Control", "no-cache");
        return ok(String.valueOf(isChecked));
    }

}
