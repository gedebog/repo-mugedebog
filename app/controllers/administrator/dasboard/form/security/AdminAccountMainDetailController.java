package controllers.administrator.dasboard.form.security;

import controllers.administrator.dasboard.render.CreateMenuAdminHtml;
import controllers.administrator.dasboard.render.CreateMenuFormAdminMainHtml;
import controllers.util.*;
import models.administrator.app.AppMasterFormAdmin;
import models.administrator.app.AppMasterPermissionsFormAdmin;
import models.administrator.user.UserAccountAdmin;
import models.administrator.user.UserFotoProfileAdmin;
import play.data.DynamicForm;
import play.libs.mailer.MailerClient;
import play.mvc.Controller;
import play.mvc.Http;
import play.mvc.Result;
import play.mvc.Security;
import services.administrator.app.AppMasterFormAdminLogic;
import services.administrator.app.AppMasterPermissionsFormAdminLogic;
import services.administrator.app.IAppMasterFormAdmin;
import services.administrator.app.IAppMasterPermissionsFormAdmin;
import services.administrator.user.*;
import views.html.administrator.dasboard.form.security.adminaccountmain;

import java.io.File;
import java.util.Date;

/**
 * Created by tri.jaruto on 5/31/2017.
 */
public class AdminAccountMainDetailController extends Controller {

    IAppMasterPermissionsFormAdmin iAppMasterPermissionsFormAdmin = new AppMasterPermissionsFormAdminLogic();
    IUserAccountAdmin iUserAccountAdmin = new UserAccountAdminLogic();
    IAppMasterFormAdmin iAppMasterFormAdmin = new AppMasterFormAdminLogic();
    IUserFotoProfileAdmin iUserFotoProfileAdmin = new UserFotoProfileAdminLogic();

    UserAccountAdmin userAccountAdmin = new UserAccountAdmin();
    UserFotoProfileAdmin userFotoProfileAdmin = new UserFotoProfileAdmin();
    AppMasterFormAdmin selectedAppMasterFormAdmin = new AppMasterFormAdmin();

    AppMasterPermissionsFormAdmin appMasterPermissionsFormAdmin = new AppMasterPermissionsFormAdmin();

    Result result;

    @Security.Authenticated(SecuredAdmin.class)
    public Result appMasterPermissionsFormAdminDetailController(boolean isLogin, UserAccountAdmin userAccountAdminLogin, UserFotoProfileAdmin userFotoProfileAdminLogin, AppMasterFormAdmin appMasterFormAdmin, String autoindex, String action, DynamicForm requestData, Http.MultipartFormData<File> requestbody, MailerClient mailerClient){
        appMasterPermissionsFormAdmin = iAppMasterPermissionsFormAdmin.onSelectBy_ampfa_useraccountadmin_and_ampfa_appmasterformadmin(userAccountAdminLogin, appMasterFormAdmin);
        if(action.equals(Constans.ACTION_SAVE)){
            result = onActionSave(isLogin, userAccountAdminLogin, userFotoProfileAdminLogin, appMasterFormAdmin, appMasterPermissionsFormAdmin, autoindex, action, requestData, requestbody, mailerClient);
        }
        return result;
    }

    public Result onActionSave(boolean isLogin, UserAccountAdmin userAccountAdminLogin, UserFotoProfileAdmin userFotoProfileAdminLogin, AppMasterFormAdmin appMasterFormAdmin, AppMasterPermissionsFormAdmin appMasterPermissionsFormAdmin, String autoindex, String action, DynamicForm requestData, Http.MultipartFormData<File> requestbody, MailerClient mailerClient){
        String save_value = requestData.get("action_input_name");
        if(save_value.equals(Constans.ACTION_VALUE_NEW)) {
            userAccountAdmin = iUserAccountAdmin.onSelectBy_uaa_autoindex(Long.valueOf(autoindex));
            userFotoProfileAdmin = iUserFotoProfileAdmin.onSelectBy_ufpa_useraccountadmin(userAccountAdmin);
            selectedAppMasterFormAdmin = iAppMasterFormAdmin.onSelectBy_amfa_autoindex(Long.valueOf(requestData.get("appmasterformadmin_input_name")));

            AppMasterPermissionsFormAdmin selecteAppMasterPermissionsFormAdmin = new AppMasterPermissionsFormAdmin();
            selecteAppMasterPermissionsFormAdmin.setAmpfa_useraccountadmin(userAccountAdmin);
            selecteAppMasterPermissionsFormAdmin.setAmpfa_appmasterformadmin(selectedAppMasterFormAdmin);
            selecteAppMasterPermissionsFormAdmin.setAmpfa_isnew(requestData.get("ampfa_isnew_name")==null ? false : true);
            selecteAppMasterPermissionsFormAdmin.setAmpfa_isselect(requestData.get("ampfa_isselect_name")==null ? false : true);
            selecteAppMasterPermissionsFormAdmin.setAmpfa_isinsert(requestData.get("ampfa_isinsert_name")==null ? false : true);
            selecteAppMasterPermissionsFormAdmin.setAmpfa_isupdate(requestData.get("ampfa_isupdate_name")==null ? false : true);
            selecteAppMasterPermissionsFormAdmin.setAmpfa_isdelete(requestData.get("ampfa_isdelete_name")==null ? false : true);
            selecteAppMasterPermissionsFormAdmin.setAmpfa_isprint(requestData.get("ampfa_isprint_name")==null ? false : true);
            selecteAppMasterPermissionsFormAdmin.setAmpfa_isexport(requestData.get("ampfa_isexport_name")==null ? false : true);
            selecteAppMasterPermissionsFormAdmin.setAmpfa_isimport(requestData.get("ampfa_isimport_name")==null ? false : true);
            selecteAppMasterPermissionsFormAdmin.setAmpfa_isactive(requestData.get("ampfa_isactive_name")==null ? false : true);
            selecteAppMasterPermissionsFormAdmin.setAmpfa_created(new Date());
            selecteAppMasterPermissionsFormAdmin.setAmpfa_creater(userAccountAdminLogin.getUaa_username());
            selecteAppMasterPermissionsFormAdmin.setAmpfa_updated(new Date());
            selecteAppMasterPermissionsFormAdmin.setAmpfa_updater(userAccountAdminLogin.getUaa_username());
            iAppMasterPermissionsFormAdmin.onSave(selecteAppMasterPermissionsFormAdmin);

            //INSERT
            //System.out.println("appMasterPermissionsFormAdminDetailController onActionSave : INSERT");
        }else{
            userAccountAdmin = iUserAccountAdmin.onSelectBy_uaa_autoindex(Long.valueOf(autoindex));
            userFotoProfileAdmin = iUserFotoProfileAdmin.onSelectBy_ufpa_useraccountadmin(userAccountAdmin);
            selectedAppMasterFormAdmin = iAppMasterFormAdmin.onSelectBy_amfa_autoindex(Long.valueOf(requestData.get("appmasterformadmin_input_name")));

            AppMasterPermissionsFormAdmin selecteAppMasterPermissionsFormAdmin = iAppMasterPermissionsFormAdmin.onSelectBy_ampfa_autoindex(Long.valueOf(AESCrypto.decrypt(URLEncDec.URLDecode(requestData.get("ampfa_autoindex_input_name")))));
            selecteAppMasterPermissionsFormAdmin.setAmpfa_useraccountadmin(userAccountAdmin);
            selecteAppMasterPermissionsFormAdmin.setAmpfa_appmasterformadmin(selectedAppMasterFormAdmin);
            selecteAppMasterPermissionsFormAdmin.setAmpfa_isnew(requestData.get("ampfa_isnew_name")==null ? false : true);
            selecteAppMasterPermissionsFormAdmin.setAmpfa_isselect(requestData.get("ampfa_isselect_name")==null ? false : true);
            selecteAppMasterPermissionsFormAdmin.setAmpfa_isinsert(requestData.get("ampfa_isinsert_name")==null ? false : true);
            selecteAppMasterPermissionsFormAdmin.setAmpfa_isupdate(requestData.get("ampfa_isupdate_name")==null ? false : true);
            selecteAppMasterPermissionsFormAdmin.setAmpfa_isdelete(requestData.get("ampfa_isdelete_name")==null ? false : true);
            selecteAppMasterPermissionsFormAdmin.setAmpfa_isprint(requestData.get("ampfa_isprint_name")==null ? false : true);
            selecteAppMasterPermissionsFormAdmin.setAmpfa_isexport(requestData.get("ampfa_isexport_name")==null ? false : true);
            selecteAppMasterPermissionsFormAdmin.setAmpfa_isimport(requestData.get("ampfa_isimport_name")==null ? false : true);
            selecteAppMasterPermissionsFormAdmin.setAmpfa_isactive(requestData.get("ampfa_isactive_name")==null ? false : true);
            selecteAppMasterPermissionsFormAdmin.setAmpfa_updated(new Date());
            selecteAppMasterPermissionsFormAdmin.setAmpfa_updater(userAccountAdminLogin.getUaa_username());
            iAppMasterPermissionsFormAdmin.onUpdate(selecteAppMasterPermissionsFormAdmin);

            //UPDATE
            //System.out.println("appMasterPermissionsFormAdminDetailController onActionSave : UPDATE");
        }
        return ok(adminaccountmain.render(appMasterFormAdmin.getAmfa_name(), isLogin, userAccountAdminLogin, userFotoProfileAdminLogin, userAccountAdmin, userFotoProfileAdmin, appMasterFormAdmin, appMasterPermissionsFormAdmin, new CreateMenuAdminHtml().getCreateMenuAdminRenderHtml(userAccountAdminLogin), new CreateMenuFormAdminMainHtml().getCreateMenuFormAdminMainRenderHtml(autoindex, appMasterFormAdmin, appMasterPermissionsFormAdmin, Constans.ACTION_VIEW), Constans.ACTION_VIEW));
    }

}
