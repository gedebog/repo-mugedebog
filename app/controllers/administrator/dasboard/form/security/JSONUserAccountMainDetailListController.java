package controllers.administrator.dasboard.form.security;

import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import controllers.util.AESCrypto;
import controllers.util.Constans;
import controllers.util.ConstansTable;
import controllers.util.URLEncDec;
import models.administrator.app.AppMasterFormAdmin;
import models.administrator.app.AppMasterPermissionsFormAdmin;
import models.administrator.user.UserAccountAdmin;
import models.user.app.AppMasterPermissionsForm;
import models.user.user.UserAccount;
import play.Configuration;
import play.libs.Json;
import play.mvc.Controller;
import play.mvc.Result;
import services.administrator.app.AppMasterPermissionsFormAdminLogic;
import services.administrator.app.IAppMasterPermissionsFormAdmin;
import services.administrator.user.IUserAccountAdmin;
import services.administrator.user.UserAccountAdminLogic;
import services.user.app.AppMasterPermissionsFormLogic;
import services.user.app.IAppMasterPermissionsForm;
import services.user.user.IUserAccount;
import services.user.user.UserAccountLogic;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by tri.jaruto on 5/31/2017.
 */
public class JSONUserAccountMainDetailListController extends Controller {

    IAppMasterPermissionsFormAdmin iAppMasterPermissionsFormAdmin = new AppMasterPermissionsFormAdminLogic();
    IAppMasterPermissionsForm iAppMasterPermissionsForm = new AppMasterPermissionsFormLogic();
    IUserAccount iUserAccount = new UserAccountLogic();

    UserAccount userAccount = new UserAccount();
    List<AppMasterPermissionsForm> appMasterPermissionsForms = new ArrayList<AppMasterPermissionsForm>();

    public Result jSONAppMasterPermissionsFormUserListPage(UserAccountAdmin userAccountAdminLogin, AppMasterFormAdmin appMasterFormAdmin, String autoindex, String page, String length){
        userAccount = iUserAccount.onSelectBy_ua_autoindex(Long.valueOf(autoindex));

        appMasterPermissionsForms.clear();
        appMasterPermissionsForms = iAppMasterPermissionsForm.onSelectAllInnerAppMasterFormBy_ampf_useraccount_and_pagination(userAccount, Integer.parseInt(page),Integer.parseInt(length));
        AppMasterPermissionsFormAdmin appMasterPermissionsFormAdminLogin = iAppMasterPermissionsFormAdmin.onSelectBy_ampfa_useraccountadmin_and_ampfa_appmasterformadmin(userAccountAdminLogin, appMasterFormAdmin);

        int rowTotal = iAppMasterPermissionsForm.onRowCountBy_ampf_useraccount(userAccount);
        int i = 0;
        ObjectNode result = Json.newObject();
        result.put("draw", i++);
        result.put("recordsTotal", rowTotal);
        result.put("recordsFiltered", rowTotal);
        ArrayNode arrayNode = result.putArray("data");

        if(appMasterPermissionsForms!=null){
            for (AppMasterPermissionsForm appMasterPermissionsForm : appMasterPermissionsForms) {
                ArrayNode arrayData = arrayNode.addArray();
                String updateData = "";
                String getmasterform = "";
                if(appMasterPermissionsFormAdminLogin.isAmpfa_isselect()){
                    getmasterform = "function getmasterform_"+appMasterPermissionsForm.getAmpf_appmasterform().getAmf_filenamescalahtml()+"(){" +
                            "                $.ajax({\n" +
                            "                    type: 'GET',\n" +
                            "                    url: '"+Configuration.root().getString("server.schemes")+request().host()+"/ws/administrator/dasboard/form/main/detail/"+URLEncDec.URLEncode(AESCrypto.encrypt(appMasterFormAdmin.getAmfa_name()))+"/"+URLEncDec.URLEncode(AESCrypto.encrypt(appMasterFormAdmin.getAmfa_filenamescalahtml()))+"/"+URLEncDec.URLEncode(AESCrypto.encrypt(ConstansTable.TABLE_APP_MASTER_FORM))+"/"+URLEncDec.URLEncode(AESCrypto.encrypt(String.valueOf(userAccount.getUa_autoindex())))+"',"+
                            "                    success: function(data){\n" +
                            "                        $.each(data, function(key, value){\n" +
                            "                            $(\"#appmasterform_dropdown_id\").append($('<div class=\"item\" data-value=\"'+value.amf_autoindex+'\"></div>').html(value.amf_name));\n" +
                            "                        });\n" +
                            "                    }\n" +
                            "                });\n" +
                            "            } ";

                    updateData = "<script> "+
                                "$(document).ready(function(){ " +
                                    "$(\"#"+ Constans.ACTION_ID_ADD+"_"+appMasterPermissionsForm.getAmpf_appmasterform().getAmf_filenamescalahtml()+"\").on('click', function() {" +
                                        "$(\"#ampf_autoindex_input_id\").val(\"\").val($(\"#ampf_autoindex_input_id\").val()+'"+ URLEncDec.URLEncode(AESCrypto.encrypt(String.valueOf(appMasterPermissionsForm.getAmpf_autoindex()))) +"');" +
                                        "$(\"#appmasterform_input_id\").val(\"\").val($(\"#appmasterform_input_id\").val()+'"+ String.valueOf(appMasterPermissionsForm.getAmpf_appmasterform().getAmf_autoindex()) +"');" +
                                        "$(\"#appmasterform_dropdown_default_id\").html(\"\").html(\""+appMasterPermissionsForm.getAmpf_appmasterform().getAmf_name()+"\");" +
                                        "$(\"#appmasterform_dropdown_id\").html(\"\").append($('<div class=\"item active selected\" data-value=\""+String.valueOf(appMasterPermissionsForm.getAmpf_appmasterform().getAmf_autoindex())+"\"></div>').html('"+appMasterPermissionsForm.getAmpf_appmasterform().getAmf_name()+"'));\n" +
                                        "$(\"#action_input_id\").val(\"\").val($(\"#action_input_id\").val() + \""+Constans.ACTION_VALUE_EDIT+"\");" +
                                        "getmasterform_"+appMasterPermissionsForm.getAmpf_appmasterform().getAmf_filenamescalahtml()+"();" +
                                        getAppMasterPermissionsFormList(appMasterPermissionsForm) +
                                        getPermissionsFormAdminLogin(appMasterPermissionsFormAdminLogin) +
                                        "$(\"#masterpermissionsform_modal_id\").modal({autofocus: false,}).modal('show');"+
                                    "});" +
                                "});" +
                                "" +
                                getmasterform +
                                " </script>" +
                                "" +
                                "<a href=\"#!\" id=\""+Constans.ACTION_ID_ADD+"_"+appMasterPermissionsForm.getAmpf_appmasterform().getAmf_filenamescalahtml()+"\">" +
                                "<i class=\"file text outline icon\"></i>" +
                                "</a>";
                }else{
                    updateData = "<a><i class=\"lock icon\"></i></a>";
                }
                arrayData.add(updateData);
                arrayData.add(appMasterPermissionsForm.getAmpf_appmasterform().getAmf_name());
                arrayData.add(appMasterPermissionsForm.isAmpf_isnew() ? "<i class=\"large green checkmark icon\"></i>" : "<i class=\"large red remove icon\"></i>");
                arrayData.add(appMasterPermissionsForm.isAmpf_isselect() ? "<i class=\"large green checkmark icon\"></i>" : "<i class=\"large red remove icon\"></i>");
                arrayData.add(appMasterPermissionsForm.isAmpf_isinsert() ? "<i class=\"large green checkmark icon\"></i>" : "<i class=\"large red remove icon\"></i>");
                arrayData.add(appMasterPermissionsForm.isAmpf_isupdate() ? "<i class=\"large green checkmark icon\"></i>" : "<i class=\"large red remove icon\"></i>");
                arrayData.add(appMasterPermissionsForm.isAmpf_isdelete() ? "<i class=\"large green checkmark icon\"></i>" : "<i class=\"large red remove icon\"></i>");
                arrayData.add(appMasterPermissionsForm.isAmpf_isprint() ? "<i class=\"large green checkmark icon\"></i>" : "<i class=\"large red remove icon\"></i>");
                arrayData.add(appMasterPermissionsForm.isAmpf_isexport() ? "<i class=\"large green checkmark icon\"></i>" : "<i class=\"large red remove icon\"></i>");
                arrayData.add(appMasterPermissionsForm.isAmpf_isimport() ? "<i class=\"large green checkmark icon\"></i>" : "<i class=\"large red remove icon\"></i>");
                arrayData.add(appMasterPermissionsForm.isAmpf_isactive() ? "<i class=\"large green checkmark icon\"></i>" : "<i class=\"large red remove icon\"></i>");
            }
        }

        response().setHeader("Access-Control-Allow-Origin", "*");
        response().setHeader("Cache-Control", "no-cache");
        return ok(result);
    }


    public String getAppMasterPermissionsFormList(AppMasterPermissionsForm appMasterPermissionsForm){
        String permissionsCheckedData = "";
        permissionsCheckedData = permissionsCheckedData + (appMasterPermissionsForm.isAmpf_isnew() ? "$(\"#ampf_isnew_div_toggle_id\").checkbox('check');" + "oncheckboxevent();" : "$(\"#ampf_isnew_div_toggle_id\").checkbox('uncheck');" + "oncheckboxevent();");
        permissionsCheckedData = permissionsCheckedData + (appMasterPermissionsForm.isAmpf_isselect() ? "$(\"#ampf_isselect_div_toggle_id\").checkbox('check');" + "oncheckboxevent();" : "$(\"#ampf_isselect_div_toggle_id\").checkbox('uncheck');" + "oncheckboxevent();");
        permissionsCheckedData = permissionsCheckedData + (appMasterPermissionsForm.isAmpf_isinsert() ? "$(\"#ampf_isinsert_div_toggle_id\").checkbox('check');" + "oncheckboxevent();" : "$(\"#ampf_isinsert_div_toggle_id\").checkbox('uncheck');" + "oncheckboxevent();");
        permissionsCheckedData = permissionsCheckedData + (appMasterPermissionsForm.isAmpf_isupdate() ? "$(\"#ampf_isupdate_div_toggle_id\").checkbox('check');" + "oncheckboxevent();" : "$(\"#ampf_isupdate_div_toggle_id\").checkbox('uncheck');" + "oncheckboxevent();");
        permissionsCheckedData = permissionsCheckedData + (appMasterPermissionsForm.isAmpf_isdelete() ? "$(\"#ampf_isdelete_div_toggle_id\").checkbox('check');" + "oncheckboxevent();" : "$(\"#ampf_isdelete_div_toggle_id\").checkbox('uncheck');" + "oncheckboxevent();");
        permissionsCheckedData = permissionsCheckedData + (appMasterPermissionsForm.isAmpf_isprint() ? "$(\"#ampf_isprint_div_toggle_id\").checkbox('check');" + "oncheckboxevent();" : "$(\"#ampf_isprint_div_toggle_id\").checkbox('uncheck');" + "oncheckboxevent();");
        permissionsCheckedData = permissionsCheckedData + (appMasterPermissionsForm.isAmpf_isexport() ? "$(\"#ampf_isexport_div_toggle_id\").checkbox('check');" + "oncheckboxevent();" : "$(\"#ampf_isexport_div_toggle_id\").checkbox('uncheck');" + "oncheckboxevent();");
        permissionsCheckedData = permissionsCheckedData + (appMasterPermissionsForm.isAmpf_isimport() ? "$(\"#ampf_isimport_div_toggle_id\").checkbox('check');" + "oncheckboxevent();" : "$(\"#ampf_isimport_div_toggle_id\").checkbox('uncheck');" + "oncheckboxevent();");
        permissionsCheckedData = permissionsCheckedData + (appMasterPermissionsForm.isAmpf_isactive() ? "$(\"#ampf_isactive_div_toggle_id\").checkbox('check');" + "oncheckboxevent();" : "$(\"#ampf_isactive_div_toggle_id\").checkbox('uncheck');" + "oncheckboxevent();");
        return permissionsCheckedData;
    }

    public String getPermissionsFormAdminLogin(AppMasterPermissionsFormAdmin appMasterPermissionsFormAdminLogin){
        String permissionsFormLogin = "";
        if(appMasterPermissionsFormAdminLogin.isAmpfa_isupdate()){
            permissionsFormLogin = permissionsFormLogin + "$(\"#app_master_permissions_form_save_div_button_id\").html(\"\").append($('<button id=\"app_master_permissions_form_save_button_id\" name=\"save_name\" class=\"ui green ok button\" type=\"submit\"></button>').html(\"Save\"));" +
                                                          "$(\"#app_master_permissions_form_save_button_id\").on('click', function() {componentdetailvalidate(); $(\"#detail_form_id\").submit();});";
        }
        return permissionsFormLogin;
    }


}
