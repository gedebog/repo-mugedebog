package controllers.administrator.dasboard.form.security;

import controllers.administrator.dasboard.render.CreateMenuAdminHtml;
import controllers.administrator.dasboard.render.CreateMenuFormAdminMainHtml;
import controllers.util.*;
import models.administrator.app.AppMasterFormAdmin;
import models.administrator.app.AppMasterPermissionsFormAdmin;
import models.administrator.user.UserAccountAdmin;
import models.administrator.user.UserFotoProfileAdmin;
import models.administrator.user.UserProfileAdmin;
import play.Configuration;
import play.data.DynamicForm;
import play.libs.mailer.MailerClient;
import play.mvc.Controller;
import play.mvc.Http;
import play.mvc.Result;
import play.mvc.Security;
import services.administrator.app.AppMasterPermissionsFormAdminLogic;
import services.administrator.app.IAppMasterPermissionsFormAdmin;
import services.administrator.user.*;
import views.html.administrator.dasboard.form.security.adminaccountmain;

import java.io.File;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by tri.jaruto on 5/31/2017.
 */
public class AdminAccountMainController extends Controller {

    IAppMasterPermissionsFormAdmin iAppMasterPermissionsFormAdmin = new AppMasterPermissionsFormAdminLogic();
    IUserAccountAdmin iUserAccountAdmin = new UserAccountAdminLogic();
    IUserProfileAdmin iUserProfileAdmin = new UserProfileAdminLogic();
    IUserFotoProfileAdmin iUserFotoProfileAdmin = new UserFotoProfileAdminLogic();

    UserAccountAdmin userAccountAdmin = new UserAccountAdmin();
    UserFotoProfileAdmin userFotoProfileAdmin = new UserFotoProfileAdmin();

    AppMasterPermissionsFormAdmin appMasterPermissionsFormAdmin = new AppMasterPermissionsFormAdmin();

    SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");

    Result result;

    @Security.Authenticated(SecuredAdmin.class)
    public Result userAccountAdminController(boolean isLogin, UserAccountAdmin userAccountAdminLogin, UserFotoProfileAdmin userFotoProfileAdminLogin, AppMasterFormAdmin appMasterFormAdmin, String autoindex, String action, DynamicForm requestData, Http.MultipartFormData<File> requestbody, MailerClient mailerClient){
        appMasterPermissionsFormAdmin = iAppMasterPermissionsFormAdmin.onSelectBy_ampfa_useraccountadmin_and_ampfa_appmasterformadmin(userAccountAdminLogin, appMasterFormAdmin);
        if(action.equals(Constans.ACTION_VIEW)){
            result = onActionView(isLogin, userAccountAdminLogin, userFotoProfileAdminLogin, appMasterFormAdmin, appMasterPermissionsFormAdmin, autoindex, action, requestData, requestbody, mailerClient);
        }else if(action.equals(Constans.ACTION_NEW)){
            result = onActionNew(isLogin, userAccountAdminLogin, userFotoProfileAdminLogin, appMasterFormAdmin, appMasterPermissionsFormAdmin, autoindex, action, requestData, requestbody, mailerClient);
        }else if(action.equals(Constans.ACTION_SAVE)){
            result = onActionSave(isLogin, userAccountAdminLogin, userFotoProfileAdminLogin, appMasterFormAdmin, appMasterPermissionsFormAdmin, autoindex, action, requestData, requestbody, mailerClient);
        }else if(action.equals(Constans.ACTION_EDIT)){
            result = onActionEdit(isLogin, userAccountAdminLogin, userFotoProfileAdminLogin, appMasterFormAdmin, appMasterPermissionsFormAdmin, autoindex, action, requestData, requestbody, mailerClient);
        }else if(action.equals(Constans.ACTION_DELETE)){
            result = onActionDelete(isLogin, userAccountAdminLogin, userFotoProfileAdminLogin, appMasterFormAdmin, appMasterPermissionsFormAdmin, autoindex, action, requestData, requestbody, mailerClient);
        }else if(action.equals(Constans.ACTION_UPLOAD)){
            result = onActionUpload(isLogin, userAccountAdminLogin, userFotoProfileAdminLogin, appMasterFormAdmin, appMasterPermissionsFormAdmin, autoindex, action, requestData, requestbody, mailerClient);
        }
        return result;
    }

    //ACTION
    public Result onActionView(boolean isLogin, UserAccountAdmin userAccountAdminLogin, UserFotoProfileAdmin userFotoProfileAdminLogin, AppMasterFormAdmin appMasterFormAdmin, AppMasterPermissionsFormAdmin appMasterPermissionsFormAdmin, String autoindex, String action, DynamicForm requestData, Http.MultipartFormData<File> requestbody, MailerClient mailerClient){
        if(!autoindex.equals("0")) {
            userAccountAdmin = iUserAccountAdmin.onSelectBy_uaa_autoindex(Long.valueOf(autoindex));
            userFotoProfileAdmin = iUserFotoProfileAdmin.onSelectBy_ufpa_useraccountadmin(userAccountAdmin);
        }
        return ok(adminaccountmain.render(appMasterFormAdmin.getAmfa_name(), isLogin, userAccountAdminLogin, userFotoProfileAdminLogin, userAccountAdmin, userFotoProfileAdmin, appMasterFormAdmin, appMasterPermissionsFormAdmin, new CreateMenuAdminHtml().getCreateMenuAdminRenderHtml(userAccountAdminLogin), new CreateMenuFormAdminMainHtml().getCreateMenuFormAdminMainRenderHtml(autoindex, appMasterFormAdmin, appMasterPermissionsFormAdmin, action), action));
    }

    public Result onActionNew(boolean isLogin, UserAccountAdmin userAccountAdminLogin, UserFotoProfileAdmin userFotoProfileAdminLogin, AppMasterFormAdmin appMasterFormAdmin, AppMasterPermissionsFormAdmin appMasterPermissionsFormAdmin, String autoindex, String action, DynamicForm requestData, Http.MultipartFormData<File> requestbody, MailerClient mailerClient){
        appMasterPermissionsFormAdmin = iAppMasterPermissionsFormAdmin.onSelectBy_ampfa_useraccountadmin_and_ampfa_appmasterformadmin(userAccountAdminLogin, appMasterFormAdmin);
        userFotoProfileAdmin = null;
        return ok(adminaccountmain.render(appMasterFormAdmin.getAmfa_name(), isLogin, userAccountAdminLogin, userFotoProfileAdminLogin, userAccountAdmin, userFotoProfileAdmin, appMasterFormAdmin, appMasterPermissionsFormAdmin, new CreateMenuAdminHtml().getCreateMenuAdminRenderHtml(userAccountAdminLogin), new CreateMenuFormAdminMainHtml().getCreateMenuFormAdminMainRenderHtml(autoindex, appMasterFormAdmin, appMasterPermissionsFormAdmin, action), action));
    }

    public Result onActionSave(boolean isLogin, UserAccountAdmin userAccountAdminLogin, UserFotoProfileAdmin userFotoProfileAdminLogin, AppMasterFormAdmin appMasterFormAdmin, AppMasterPermissionsFormAdmin appMasterPermissionsFormAdmin, String autoindex, String action, DynamicForm requestData, Http.MultipartFormData<File> requestbody, MailerClient mailerClient){
        appMasterPermissionsFormAdmin = iAppMasterPermissionsFormAdmin.onSelectBy_ampfa_useraccountadmin_and_ampfa_appmasterformadmin(userAccountAdminLogin, appMasterFormAdmin);
        if(autoindex.equals("0")) {
            userFotoProfileAdmin = null;
            String uaa_username_name = requestData.get("uaa_username_name");
            String uaa_userpassword_name = requestData.get("uaa_userpassword_name");
            //INSERT
            try {
                userAccountAdmin = new UserAccountAdmin();
                userAccountAdmin.setUaa_username(uaa_username_name);
                userAccountAdmin.setUaa_userpassword(BCrypto.createPassword(uaa_userpassword_name));
                userAccountAdmin.setUaa_usertype(Short.valueOf(requestData.get("uaa_usertype_name")).shortValue());
                userAccountAdmin.setUaa_isactive(requestData.get("uaa_isactive_name")==null ? false : true);
                userAccountAdmin.setUaa_activationcode(BCrypto.createPassword(uaa_username_name));
                userAccountAdmin.setUaa_isforgotpassword(false);
                userAccountAdmin.setUaa_created(new Date());
                userAccountAdmin.setUaa_creater(userAccountAdminLogin.getUaa_username());
                userAccountAdmin.setUaa_updated(new Date());
                userAccountAdmin.setUaa_updater(userAccountAdminLogin.getUaa_username());
                iUserAccountAdmin.onSave(userAccountAdmin);

                UserProfileAdmin userProfileAdmin = new UserProfileAdmin();
                userProfileAdmin.setUpa_useraccountadmin(userAccountAdmin);
                userProfileAdmin.setUpa_name(requestData.get("upa_name_name"));
                userProfileAdmin.setUpa_nickname(requestData.get("upa_nickname_name"));
                userProfileAdmin.setUpa_birthplace(requestData.get("upa_birthplace_name"));
                userProfileAdmin.setUpa_birthdate(dateFormat.parse(requestData.get("upa_birthdate_name")));
                userProfileAdmin.setUpa_isgender(Boolean.parseBoolean(requestData.get("upa_isgender_name")));
                userProfileAdmin.setUpa_created(new Date());
                userProfileAdmin.setUpa_creater(userAccountAdminLogin.getUaa_username());
                userProfileAdmin.setUpa_updated(new Date());
                userProfileAdmin.setUpa_updater(userAccountAdminLogin.getUaa_username());
                iUserProfileAdmin.onSave(userProfileAdmin);
                userAccountAdmin.setUaa_userprofileadmin(userProfileAdmin);

                //SEND EMAIL
                SendEmail sendEmail = new SendEmail(mailerClient);
                sendEmail.onSendEmailCreatePasswordAdminActivation(userAccountAdmin, uaa_userpassword_name);

            } catch (AppException e) {
                e.printStackTrace();
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }else{
            userAccountAdmin = iUserAccountAdmin.onSelectBy_uaa_autoindex(Long.valueOf(autoindex));
            userFotoProfileAdmin = iUserFotoProfileAdmin.onSelectBy_ufpa_useraccountadmin(userAccountAdmin);
            String uaa_username_name = requestData.get("uaa_username_name");
            String uaa_userpassword_name = requestData.get("uaa_userpassword_name");
            //UPDATE
            try {
                userAccountAdmin.setUaa_username(uaa_username_name);
                userAccountAdmin.setUaa_userpassword(BCrypto.createPassword(uaa_userpassword_name));
                userAccountAdmin.setUaa_usertype(Short.valueOf(requestData.get("uaa_usertype_name")).shortValue());
                userAccountAdmin.setUaa_isactive(requestData.get("uaa_isactive_name")==null ? false : true);
                userAccountAdmin.setUaa_activationcode(BCrypto.createPassword(uaa_username_name));
                userAccountAdmin.setUaa_isforgotpassword(false);
                userAccountAdmin.setUaa_updated(new Date());
                userAccountAdmin.setUaa_updater(userAccountAdminLogin.getUaa_username());
                iUserAccountAdmin.onUpdate(userAccountAdmin);

                UserProfileAdmin userProfileAdmin = userAccountAdmin.getUaa_userprofileadmin();
                userProfileAdmin.setUpa_name(requestData.get("upa_name_name"));
                userProfileAdmin.setUpa_nickname(requestData.get("upa_nickname_name"));
                userProfileAdmin.setUpa_birthplace(requestData.get("upa_birthplace_name"));
                userProfileAdmin.setUpa_birthdate(dateFormat.parse(requestData.get("upa_birthdate_name")));
                userProfileAdmin.setUpa_isgender(Boolean.parseBoolean(requestData.get("upa_isgender_name")));
                userProfileAdmin.setUpa_updated(new Date());
                userProfileAdmin.setUpa_updater(userAccountAdminLogin.getUaa_username());
                iUserProfileAdmin.onUpdate(userProfileAdmin);
                userAccountAdmin.setUaa_userprofileadmin(userProfileAdmin);

                //SEND EMAIL
                SendEmail sendEmail = new SendEmail(mailerClient);
                sendEmail.onSendEmailUpdatePasswordAdminActivation(userAccountAdmin, uaa_userpassword_name);

            } catch (AppException e) {
                e.printStackTrace();
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }
        return ok(adminaccountmain.render(appMasterFormAdmin.getAmfa_name(), isLogin, userAccountAdminLogin, userFotoProfileAdminLogin, userAccountAdmin, userFotoProfileAdmin, appMasterFormAdmin, appMasterPermissionsFormAdmin, new CreateMenuAdminHtml().getCreateMenuAdminRenderHtml(userAccountAdminLogin), new CreateMenuFormAdminMainHtml().getCreateMenuFormAdminMainRenderHtml(autoindex, appMasterFormAdmin, appMasterPermissionsFormAdmin, Constans.ACTION_VIEW), Constans.ACTION_VIEW));
    }

    public Result onActionEdit(boolean isLogin, UserAccountAdmin userAccountAdminLogin, UserFotoProfileAdmin userFotoProfileAdminLogin, AppMasterFormAdmin appMasterFormAdmin, AppMasterPermissionsFormAdmin appMasterPermissionsFormAdmin, String autoindex, String action, DynamicForm requestData, Http.MultipartFormData<File> requestbody, MailerClient mailerClient){
        appMasterPermissionsFormAdmin = iAppMasterPermissionsFormAdmin.onSelectBy_ampfa_useraccountadmin_and_ampfa_appmasterformadmin(userAccountAdminLogin, appMasterFormAdmin);
        if(!autoindex.equals("0")) {
            userAccountAdmin = iUserAccountAdmin.onSelectBy_uaa_autoindex(Long.valueOf(autoindex));
            userFotoProfileAdmin = iUserFotoProfileAdmin.onSelectBy_ufpa_useraccountadmin(userAccountAdmin);
            userAccountAdmin.setUaa_userpassword(null);
        }
        return ok(adminaccountmain.render(appMasterFormAdmin.getAmfa_name(), isLogin, userAccountAdminLogin, userFotoProfileAdminLogin, userAccountAdmin, userFotoProfileAdmin, appMasterFormAdmin, appMasterPermissionsFormAdmin, new CreateMenuAdminHtml().getCreateMenuAdminRenderHtml(userAccountAdminLogin), new CreateMenuFormAdminMainHtml().getCreateMenuFormAdminMainRenderHtml(autoindex, appMasterFormAdmin, appMasterPermissionsFormAdmin, action), action));
    }

    public Result onActionDelete(boolean isLogin, UserAccountAdmin userAccountAdminLogin, UserFotoProfileAdmin userFotoProfileAdminLogin, AppMasterFormAdmin appMasterFormAdmin, AppMasterPermissionsFormAdmin appMasterPermissionsFormAdmin, String autoindex, String action, DynamicForm requestData, Http.MultipartFormData<File> requestbody, MailerClient mailerClient){
        userAccountAdminLogin = SecuredAdmin.getUserInfo(ctx());
        appMasterPermissionsFormAdmin = iAppMasterPermissionsFormAdmin.onSelectBy_ampfa_useraccountadmin_and_ampfa_appmasterformadmin(userAccountAdminLogin, appMasterFormAdmin);
        return ok(adminaccountmain.render(appMasterFormAdmin.getAmfa_name(), isLogin, userAccountAdminLogin, userFotoProfileAdminLogin, userAccountAdmin, userFotoProfileAdmin, appMasterFormAdmin, appMasterPermissionsFormAdmin, new CreateMenuAdminHtml().getCreateMenuAdminRenderHtml(userAccountAdminLogin), new CreateMenuFormAdminMainHtml().getCreateMenuFormAdminMainRenderHtml(autoindex, appMasterFormAdmin, appMasterPermissionsFormAdmin, action), action));
    }

    public Result onActionUpload(boolean isLogin, UserAccountAdmin userAccountAdminLogin, UserFotoProfileAdmin userFotoProfileAdminLogin, AppMasterFormAdmin appMasterFormAdmin, AppMasterPermissionsFormAdmin appMasterPermissionsFormAdmin, String autoindex, String action, DynamicForm requestData, Http.MultipartFormData<File> requestbody, MailerClient mailerClient){
        appMasterPermissionsFormAdmin = iAppMasterPermissionsFormAdmin.onSelectBy_ampfa_useraccountadmin_and_ampfa_appmasterformadmin(userAccountAdminLogin, appMasterFormAdmin);
        Http.MultipartFormData.FilePart<File> picture = requestbody.getFile("ufpa_filename_name");

        if(!autoindex.equals("0")) {
            userAccountAdmin = iUserAccountAdmin.onSelectBy_uaa_autoindex(Long.valueOf(autoindex));
            userFotoProfileAdmin = iUserFotoProfileAdmin.onSelectBy_ufpa_useraccountadmin(userAccountAdmin);
            if(userFotoProfileAdmin==null){
                userFotoProfileAdmin = new UserFotoProfileAdmin();
                userFotoProfileAdmin.setUfpa_useraccountadmin(userAccountAdmin);
                userFotoProfileAdmin.setUfpa_name(userAccountAdmin.getUaa_username());
                if(Configuration.root().getString("app.upload.file.type").equals(Constans.PROFILE_FOTO_UPLOAD_CLOUDINARY)){
                    userFotoProfileAdmin.setUfpa_status(Constans.PROFILE_FOTO_UPLOAD_CLOUDINARY);
                    userFotoProfileAdmin.setUfpa_note("");
                    userFotoProfileAdmin.setUfpa_filename(UploadImage.onUploadAdminFotoProfileToCloudinary(picture, userAccountAdmin.getUaa_username()));
                }else {
                    userFotoProfileAdmin.setUfpa_status(Constans.PROFILE_FOTO_UPLOAD_LOCALHOST);
                    userFotoProfileAdmin.setUfpa_note("");
                    userFotoProfileAdmin.setUfpa_filename(UploadImage.onUploadAdminFotoProfileToLocal(picture, userAccountAdmin.getUaa_username()));
                }
                userFotoProfileAdmin.setUfpa_created(new Date());
                userFotoProfileAdmin.setUfpa_creater(userAccountAdminLogin.getUaa_username());
                userFotoProfileAdmin.setUfpa_updated(new Date());
                userFotoProfileAdmin.setUfpa_updater(userAccountAdminLogin.getUaa_username());
                iUserFotoProfileAdmin.onSave(userFotoProfileAdmin);
            }else{
                userFotoProfileAdmin.setUfpa_useraccountadmin(userAccountAdmin);
                userFotoProfileAdmin.setUfpa_name(userAccountAdmin.getUaa_username());
                if(Configuration.root().getString("app.upload.file.type").equals(Constans.PROFILE_FOTO_UPLOAD_CLOUDINARY)){
                    userFotoProfileAdmin.setUfpa_status(Constans.PROFILE_FOTO_UPLOAD_CLOUDINARY);
                    userFotoProfileAdmin.setUfpa_note("");
                    userFotoProfileAdmin.setUfpa_filename(UploadImage.onUploadAdminFotoProfileToCloudinary(picture, userAccountAdmin.getUaa_username()));
                }else {
                    userFotoProfileAdmin.setUfpa_status(Constans.PROFILE_FOTO_UPLOAD_LOCALHOST);
                    userFotoProfileAdmin.setUfpa_note("");
                    userFotoProfileAdmin.setUfpa_filename(UploadImage.onUploadAdminFotoProfileToLocal(picture, userAccountAdmin.getUaa_username()));
                }
                userFotoProfileAdmin.setUfpa_updated(new Date());
                userFotoProfileAdmin.setUfpa_updater(userAccountAdminLogin.getUaa_username());
                iUserFotoProfileAdmin.onUpdate(userFotoProfileAdmin);
            }

        }
        return ok(adminaccountmain.render(appMasterFormAdmin.getAmfa_name(), isLogin, userAccountAdminLogin, userFotoProfileAdminLogin, userAccountAdmin, userFotoProfileAdmin, appMasterFormAdmin, appMasterPermissionsFormAdmin, new CreateMenuAdminHtml().getCreateMenuAdminRenderHtml(userAccountAdminLogin), new CreateMenuFormAdminMainHtml().getCreateMenuFormAdminMainRenderHtml(autoindex, appMasterFormAdmin, appMasterPermissionsFormAdmin, Constans.ACTION_VIEW), Constans.ACTION_VIEW));
    }


}
