package controllers.administrator.dasboard.form.security;

import models.user.app.AppMasterForm;
import play.libs.Json;
import play.mvc.Controller;
import play.mvc.Result;
import services.user.app.AppMasterFormLogic;
import services.user.app.IAppMasterForm;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by tri.jaruto on 5/31/2017.
 */
public class JSONUserAccountMainDetailController extends Controller {

    IAppMasterForm iAppMasterForm = new AppMasterFormLogic();
    List<AppMasterForm> appMasterForms = new ArrayList<AppMasterForm>();

    public Result jSONAppMasterFormList(String autoindex){
        appMasterForms.clear();
        appMasterForms = iAppMasterForm.onSelectAllBy_amf_autoindex_notin_ampf_appmasterform_by_ampf_useraccount(Long.valueOf(autoindex));
        response().setHeader("Access-Control-Allow-Origin", "*");
        response().setHeader("Cache-Control", "no-cache");
        if(appMasterForms!=null){
            return ok(Json.toJson(appMasterForms));
        }
        return ok("Empty Data");
    }

}
