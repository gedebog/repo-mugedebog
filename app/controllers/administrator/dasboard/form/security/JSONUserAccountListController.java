package controllers.administrator.dasboard.form.security;

import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import controllers.util.AESCrypto;
import controllers.util.Constans;
import controllers.util.URLEncDec;
import models.administrator.app.AppMasterFormAdmin;
import models.administrator.app.AppMasterPermissionsFormAdmin;
import models.administrator.user.UserAccountAdmin;
import models.user.user.UserAccount;
import play.libs.Json;
import play.mvc.Controller;
import play.mvc.Result;
import services.administrator.app.AppMasterPermissionsFormAdminLogic;
import services.administrator.app.IAppMasterPermissionsFormAdmin;
import services.user.user.IUserAccount;
import services.user.user.UserAccountLogic;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by tri.jaruto on 5/31/2017.
 */
public class JSONUserAccountListController extends Controller {

    IAppMasterPermissionsFormAdmin iAppMasterPermissionsFormAdmin = new AppMasterPermissionsFormAdminLogic();
    IUserAccount iUserAccount = new UserAccountLogic();

    List<UserAccount> userAccounts = new ArrayList<UserAccount>();

    public Result jSONUserAccountListPage(UserAccountAdmin userAccountAdminLogin, AppMasterFormAdmin appMasterFormAdmin, String page, String length){
        userAccounts.clear();
        userAccounts = iUserAccount.onSelectAllBy_Pagination(Integer.parseInt(page),Integer.parseInt(length));
        AppMasterPermissionsFormAdmin appMasterPermissionsFormAdmin = iAppMasterPermissionsFormAdmin.onSelectBy_ampfa_useraccountadmin_and_ampfa_appmasterformadmin(userAccountAdminLogin, appMasterFormAdmin);

        int rowTotal = iUserAccount.onRowCount();
        int i = 0;
        ObjectNode result = Json.newObject();
        result.put("draw", i++);
        result.put("recordsTotal", rowTotal);
        result.put("recordsFiltered", rowTotal);
        ArrayNode arrayNode = result.putArray("data");

        if(userAccounts!=null){
            for (UserAccount userAccount : userAccounts) {
                ArrayNode arrayData = arrayNode.addArray();
                String viewData = "";
                if(appMasterPermissionsFormAdmin.isAmpfa_isselect()){
                    viewData = "<a href="+ controllers.administrator.dasboard.form.routes.FormAdminMainController.formAdminMain(
                            URLEncDec.URLEncode(AESCrypto.encrypt(appMasterFormAdmin.getAmfa_name())),
                            URLEncDec.URLEncode(AESCrypto.encrypt(appMasterFormAdmin.getAmfa_filenamescalahtml())),
                            URLEncDec.URLEncode(AESCrypto.encrypt(String.valueOf(userAccount.getUa_autoindex()))),
                            URLEncDec.URLEncode(AESCrypto.encrypt(Constans.ACTION_VIEW))
                    )+"><i class=\"file text outline icon\"></i></a>";
                }else{
                    viewData = "<a href=\"#\"><i class=\"lock icon\"></i></a>";
                }
                arrayData.add(viewData);
                arrayData.add(userAccount.getUa_username());
                String isActiveIcon = "<i class=\"large red remove icon\"></i>";
                if(userAccount.isUa_isactive()){
                    isActiveIcon = "<i class=\"large green checkmark icon\"></i>";
                }
                arrayData.add(isActiveIcon);
                //arrayNode.add(Json.toJson(userAccountAdmin));
            }
        }

        response().setHeader("Access-Control-Allow-Origin", "*");
        response().setHeader("Cache-Control", "no-cache");
        return ok(result);
    }


    public Result jSONUserAccountListFilterPage(AppMasterFormAdmin appMasterFormAdmin, String filter, String page, String length){
        userAccounts.clear();
        userAccounts = iUserAccount.onSelectAllBy_Filter_and_Pagination(filter, Integer.parseInt(page),Integer.parseInt(length));

        int rowTotal = iUserAccount.onRowCountByFilter(filter);
        int i = 0;
        ObjectNode result = Json.newObject();
        result.put("draw", i++);
        result.put("recordsTotal", rowTotal);
        result.put("recordsFiltered", rowTotal);
        ArrayNode arrayNode = result.putArray("data");

        if(userAccounts!=null){
            for (UserAccount userAccount : userAccounts) {
                ArrayNode arrayData = arrayNode.addArray();
                String viewData = "<a href="+ controllers.administrator.dasboard.form.routes.FormAdminMainController.formAdminMain(
                        URLEncDec.URLEncode(AESCrypto.encrypt(appMasterFormAdmin.getAmfa_name())),
                        URLEncDec.URLEncode(AESCrypto.encrypt(appMasterFormAdmin.getAmfa_filenamescalahtml())),
                        URLEncDec.URLEncode(AESCrypto.encrypt(String.valueOf(userAccount.getUa_autoindex()))),
                        URLEncDec.URLEncode(AESCrypto.encrypt(Constans.ACTION_VIEW))
                )+"><i class=\"file text outline icon\"></i></a>";
                arrayData.add(viewData);
                arrayData.add(userAccount.getUa_username());
                String isActiveIcon = "<i class=\"large red remove icon\"></i>";
                if(userAccount.isUa_isactive()){
                    isActiveIcon = "<i class=\"large green checkmark icon\"></i>";
                }
                arrayData.add(isActiveIcon);
            }
        }

        response().setHeader("Access-Control-Allow-Origin", "*");
        response().setHeader("Cache-Control", "no-cache");
        return ok(result);
    }


}
