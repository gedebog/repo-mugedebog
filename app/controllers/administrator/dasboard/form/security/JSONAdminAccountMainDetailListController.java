package controllers.administrator.dasboard.form.security;

import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import controllers.util.AESCrypto;
import controllers.util.Constans;
import controllers.util.ConstansTable;
import controllers.util.URLEncDec;
import models.administrator.app.AppMasterFormAdmin;
import models.administrator.app.AppMasterPermissionsFormAdmin;
import models.administrator.user.UserAccountAdmin;
import play.Configuration;
import play.libs.Json;
import play.mvc.Controller;
import play.mvc.Result;
import services.administrator.app.AppMasterPermissionsFormAdminLogic;
import services.administrator.app.IAppMasterPermissionsFormAdmin;
import services.administrator.user.IUserAccountAdmin;
import services.administrator.user.UserAccountAdminLogic;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by tri.jaruto on 5/31/2017.
 */
public class JSONAdminAccountMainDetailListController extends Controller {

    IAppMasterPermissionsFormAdmin iAppMasterPermissionsFormAdmin = new AppMasterPermissionsFormAdminLogic();
    IUserAccountAdmin iUserAccountAdmin = new UserAccountAdminLogic();

    UserAccountAdmin userAccountAdmin = new UserAccountAdmin();
    List<AppMasterPermissionsFormAdmin> appMasterPermissionsFormAdmins = new ArrayList<AppMasterPermissionsFormAdmin>();

    public Result jSONAppMasterPermissionsFormAdminListPage(UserAccountAdmin userAccountAdminLogin, AppMasterFormAdmin appMasterFormAdmin, String autoindex, String page, String length){
        userAccountAdmin = iUserAccountAdmin.onSelectBy_uaa_autoindex(Long.valueOf(autoindex));

        appMasterPermissionsFormAdmins.clear();
        appMasterPermissionsFormAdmins = iAppMasterPermissionsFormAdmin.onSelectAllInnerAppMasterFormAdminBy_ampfa_useraccountadmin_and_pagination(userAccountAdmin, Integer.parseInt(page),Integer.parseInt(length));
        AppMasterPermissionsFormAdmin appMasterPermissionsFormAdminLogin = iAppMasterPermissionsFormAdmin.onSelectBy_ampfa_useraccountadmin_and_ampfa_appmasterformadmin(userAccountAdminLogin, appMasterFormAdmin);

        int rowTotal = iAppMasterPermissionsFormAdmin.onRowCountBy_ampfa_useraccountadmin(userAccountAdmin);
        int i = 0;
        ObjectNode result = Json.newObject();
        result.put("draw", i++);
        result.put("recordsTotal", rowTotal);
        result.put("recordsFiltered", rowTotal);
        ArrayNode arrayNode = result.putArray("data");

        if(appMasterPermissionsFormAdmins!=null){
            for (AppMasterPermissionsFormAdmin appMasterPermissionsFormAdmin : appMasterPermissionsFormAdmins) {
                ArrayNode arrayData = arrayNode.addArray();
                String updateData = "";
                String getmasterformadmin = "";
                if(appMasterPermissionsFormAdminLogin.isAmpfa_isselect()){
                    getmasterformadmin = "function getmasterformadmin_"+appMasterPermissionsFormAdmin.getAmpfa_appmasterformadmin().getAmfa_filenamescalahtml()+"(){" +
                            "                $.ajax({\n" +
                            "                    type: 'GET',\n" +
                            "                    url: '"+Configuration.root().getString("server.schemes")+request().host()+"/ws/administrator/dasboard/form/main/detail/"+URLEncDec.URLEncode(AESCrypto.encrypt(appMasterFormAdmin.getAmfa_name()))+"/"+URLEncDec.URLEncode(AESCrypto.encrypt(appMasterFormAdmin.getAmfa_filenamescalahtml()))+"/"+URLEncDec.URLEncode(AESCrypto.encrypt(ConstansTable.TABLE_APP_MASTER_FORM_ADMIN))+"/"+URLEncDec.URLEncode(AESCrypto.encrypt(String.valueOf(userAccountAdmin.getUaa_autoindex())))+"',"+
                            "                    success: function(data){\n" +
                            "                        $.each(data, function(key, value){\n" +
                            "                            $(\"#appmasterformadmin_dropdown_id\").append($('<div class=\"item\" data-value=\"'+value.amfa_autoindex+'\"></div>').html(value.amfa_name));\n" +
                            "                        });\n" +
                            "                    }\n" +
                            "                });\n" +
                            "            } ";

                    updateData = "<script> "+
                                "$(document).ready(function(){ " +
                                    "$(\"#"+ Constans.ACTION_ID_ADD+"_"+appMasterPermissionsFormAdmin.getAmpfa_appmasterformadmin().getAmfa_filenamescalahtml()+"\").on('click', function() {" +
                                        "$(\"#ampfa_autoindex_input_id\").val(\"\").val($(\"#ampfa_autoindex_input_id\").val()+'"+ URLEncDec.URLEncode(AESCrypto.encrypt(String.valueOf(appMasterPermissionsFormAdmin.getAmpfa_autoindex()))) +"');" +
                                        "$(\"#appmasterformadmin_input_id\").val(\"\").val($(\"#appmasterformadmin_input_id\").val()+'"+ String.valueOf(appMasterPermissionsFormAdmin.getAmpfa_appmasterformadmin().getAmfa_autoindex()) +"');" +
                                        "$(\"#appmasterformadmin_dropdown_default_id\").html(\"\").html(\""+appMasterPermissionsFormAdmin.getAmpfa_appmasterformadmin().getAmfa_name()+"\");" +
                                        "$(\"#appmasterformadmin_dropdown_id\").html(\"\").append($('<div class=\"item active selected\" data-value=\""+String.valueOf(appMasterPermissionsFormAdmin.getAmpfa_appmasterformadmin().getAmfa_autoindex())+"\"></div>').html('"+appMasterPermissionsFormAdmin.getAmpfa_appmasterformadmin().getAmfa_name()+"'));\n" +
                                        "$(\"#action_input_id\").val(\"\").val($(\"#action_input_id\").val() + \""+Constans.ACTION_VALUE_EDIT+"\");" +
                                        "getmasterformadmin_"+appMasterPermissionsFormAdmin.getAmpfa_appmasterformadmin().getAmfa_filenamescalahtml()+"();" +
                                        getAppMasterPermissionsFormAdminList(appMasterPermissionsFormAdmin) +
                                        getPermissionsFormAdminLogin(appMasterPermissionsFormAdminLogin) +
                                        "$(\"#masterpermissionsformadmin_modal_id\").modal({autofocus: false,}).modal('show');"+
                                    "});" +
                                "});" +
                                "" +
                                getmasterformadmin +
                                " </script>" +
                                "" +
                                "<a href=\"#!\" id=\""+Constans.ACTION_ID_ADD+"_"+appMasterPermissionsFormAdmin.getAmpfa_appmasterformadmin().getAmfa_filenamescalahtml()+"\">" +
                                "<i class=\"file text outline icon\"></i>" +
                                "</a>";
                }else{
                    updateData = "<a><i class=\"lock icon\"></i></a>";
                }
                arrayData.add(updateData);
                arrayData.add(appMasterPermissionsFormAdmin.getAmpfa_appmasterformadmin().getAmfa_name());
                arrayData.add(appMasterPermissionsFormAdmin.isAmpfa_isnew() ? "<i class=\"large green checkmark icon\"></i>" : "<i class=\"large red remove icon\"></i>");
                arrayData.add(appMasterPermissionsFormAdmin.isAmpfa_isselect() ? "<i class=\"large green checkmark icon\"></i>" : "<i class=\"large red remove icon\"></i>");
                arrayData.add(appMasterPermissionsFormAdmin.isAmpfa_isinsert() ? "<i class=\"large green checkmark icon\"></i>" : "<i class=\"large red remove icon\"></i>");
                arrayData.add(appMasterPermissionsFormAdmin.isAmpfa_isupdate() ? "<i class=\"large green checkmark icon\"></i>" : "<i class=\"large red remove icon\"></i>");
                arrayData.add(appMasterPermissionsFormAdmin.isAmpfa_isdelete() ? "<i class=\"large green checkmark icon\"></i>" : "<i class=\"large red remove icon\"></i>");
                arrayData.add(appMasterPermissionsFormAdmin.isAmpfa_isprint() ? "<i class=\"large green checkmark icon\"></i>" : "<i class=\"large red remove icon\"></i>");
                arrayData.add(appMasterPermissionsFormAdmin.isAmpfa_isexport() ? "<i class=\"large green checkmark icon\"></i>" : "<i class=\"large red remove icon\"></i>");
                arrayData.add(appMasterPermissionsFormAdmin.isAmpfa_isimport() ? "<i class=\"large green checkmark icon\"></i>" : "<i class=\"large red remove icon\"></i>");
                arrayData.add(appMasterPermissionsFormAdmin.isAmpfa_isactive() ? "<i class=\"large green checkmark icon\"></i>" : "<i class=\"large red remove icon\"></i>");
            }
        }

        response().setHeader("Access-Control-Allow-Origin", "*");
        response().setHeader("Cache-Control", "no-cache");
        return ok(result);
    }


    public String getAppMasterPermissionsFormAdminList(AppMasterPermissionsFormAdmin appMasterPermissionsFormAdmin){
        String permissionsCheckedData = "";
        permissionsCheckedData = permissionsCheckedData + (appMasterPermissionsFormAdmin.isAmpfa_isnew() ? "$(\"#ampfa_isnew_div_toggle_id\").checkbox('check');" + "oncheckboxevent();" : "$(\"#ampfa_isnew_div_toggle_id\").checkbox('uncheck');" + "oncheckboxevent();");
        permissionsCheckedData = permissionsCheckedData + (appMasterPermissionsFormAdmin.isAmpfa_isselect() ? "$(\"#ampfa_isselect_div_toggle_id\").checkbox('check');" + "oncheckboxevent();" : "$(\"#ampfa_isselect_div_toggle_id\").checkbox('uncheck');" + "oncheckboxevent();");
        permissionsCheckedData = permissionsCheckedData + (appMasterPermissionsFormAdmin.isAmpfa_isinsert() ? "$(\"#ampfa_isinsert_div_toggle_id\").checkbox('check');" + "oncheckboxevent();" : "$(\"#ampfa_isinsert_div_toggle_id\").checkbox('uncheck');" + "oncheckboxevent();");
        permissionsCheckedData = permissionsCheckedData + (appMasterPermissionsFormAdmin.isAmpfa_isupdate() ? "$(\"#ampfa_isupdate_div_toggle_id\").checkbox('check');" + "oncheckboxevent();" : "$(\"#ampfa_isupdate_div_toggle_id\").checkbox('uncheck');" + "oncheckboxevent();");
        permissionsCheckedData = permissionsCheckedData + (appMasterPermissionsFormAdmin.isAmpfa_isdelete() ? "$(\"#ampfa_isdelete_div_toggle_id\").checkbox('check');" + "oncheckboxevent();" : "$(\"#ampfa_isdelete_div_toggle_id\").checkbox('uncheck');" + "oncheckboxevent();");
        permissionsCheckedData = permissionsCheckedData + (appMasterPermissionsFormAdmin.isAmpfa_isprint() ? "$(\"#ampfa_isprint_div_toggle_id\").checkbox('check');" + "oncheckboxevent();" : "$(\"#ampfa_isprint_div_toggle_id\").checkbox('uncheck');" + "oncheckboxevent();");
        permissionsCheckedData = permissionsCheckedData + (appMasterPermissionsFormAdmin.isAmpfa_isexport() ? "$(\"#ampfa_isexport_div_toggle_id\").checkbox('check');" + "oncheckboxevent();" : "$(\"#ampfa_isexport_div_toggle_id\").checkbox('uncheck');" + "oncheckboxevent();");
        permissionsCheckedData = permissionsCheckedData + (appMasterPermissionsFormAdmin.isAmpfa_isimport() ? "$(\"#ampfa_isimport_div_toggle_id\").checkbox('check');" + "oncheckboxevent();" : "$(\"#ampfa_isimport_div_toggle_id\").checkbox('uncheck');" + "oncheckboxevent();");
        permissionsCheckedData = permissionsCheckedData + (appMasterPermissionsFormAdmin.isAmpfa_isactive() ? "$(\"#ampfa_isactive_div_toggle_id\").checkbox('check');" + "oncheckboxevent();" : "$(\"#ampfa_isactive_div_toggle_id\").checkbox('uncheck');" + "oncheckboxevent();");
        return permissionsCheckedData;
    }

    public String getPermissionsFormAdminLogin(AppMasterPermissionsFormAdmin appMasterPermissionsFormAdmin){
        String permissionsFormAdminLogin = "";
        if(appMasterPermissionsFormAdmin.isAmpfa_isupdate()){
            permissionsFormAdminLogin = permissionsFormAdminLogin + "$(\"#app_master_permissions_form_admin_save_div_button_id\").html(\"\").append($('<button id=\"app_master_permissions_form_admin_save_button_id\" name=\"save_name\" class=\"ui green ok button\" type=\"submit\"></button>').html(\"Save\"));" +
                                                                    "$(\"#app_master_permissions_form_admin_save_button_id\").on('click', function() {componentdetailvalidate(); $(\"#detail_form_id\").submit();});";
        }
        return permissionsFormAdminLogin;
    }


}
