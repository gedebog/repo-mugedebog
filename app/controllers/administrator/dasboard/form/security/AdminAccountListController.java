package controllers.administrator.dasboard.form.security;


import controllers.administrator.dasboard.render.CreateMenuAdminHtml;
import controllers.administrator.dasboard.render.CreateMenuFormAdminListHtml;
import controllers.util.SecuredAdmin;
import models.administrator.app.AppMasterFormAdmin;
import models.administrator.app.AppMasterPermissionsFormAdmin;
import models.administrator.user.UserAccountAdmin;
import models.administrator.user.UserFotoProfileAdmin;
import play.data.FormFactory;
import play.mvc.Controller;
import play.mvc.Result;
import play.mvc.Security;
import services.administrator.app.AppMasterPermissionsFormAdminLogic;
import services.administrator.app.IAppMasterPermissionsFormAdmin;
import services.administrator.user.IUserAccountAdmin;
import services.administrator.user.UserAccountAdminLogic;
import views.html.administrator.dasboard.form.security.adminaccountlist;

import javax.inject.Inject;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by tri.jaruto on 5/31/2017.
 */
public class AdminAccountListController extends Controller {

    IAppMasterPermissionsFormAdmin iAppMasterPermissionsFormAdmin = new AppMasterPermissionsFormAdminLogic();
    IUserAccountAdmin iUserAccountAdmin = new UserAccountAdminLogic();

    List<UserAccountAdmin> userAccountAdmins = new ArrayList<UserAccountAdmin>();

    @Inject
    FormFactory formFactory;

    @Security.Authenticated(SecuredAdmin.class)
    public Result userAccountAdminListController(boolean isLogin, UserAccountAdmin userAccountAdminLogin, UserFotoProfileAdmin userFotoProfileAdminLogin, AppMasterFormAdmin appMasterFormAdmin){
        AppMasterPermissionsFormAdmin appMasterPermissionsFormAdmin = iAppMasterPermissionsFormAdmin.onSelectBy_ampfa_useraccountadmin_and_ampfa_appmasterformadmin(userAccountAdminLogin, appMasterFormAdmin);
        return ok(adminaccountlist.render(appMasterFormAdmin.getAmfa_name(), isLogin, userAccountAdminLogin, userFotoProfileAdminLogin, appMasterFormAdmin, new CreateMenuAdminHtml().getCreateMenuAdminRenderHtml(userAccountAdminLogin),  new CreateMenuFormAdminListHtml().getCreateMenuFormAdminListRenderHtml(appMasterFormAdmin, appMasterPermissionsFormAdmin)));
    }

}
