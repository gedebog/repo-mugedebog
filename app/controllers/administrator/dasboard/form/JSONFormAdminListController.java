package controllers.administrator.dasboard.form;

import controllers.administrator.dasboard.form.security.JSONAdminAccountListController;
import controllers.administrator.dasboard.form.security.JSONUserAccountListController;
import controllers.util.AESCrypto;
import controllers.util.ConstansForm;
import controllers.util.ConstansTable;
import controllers.util.SecuredAdmin;
import models.administrator.app.AppMasterFormAdmin;
import models.administrator.user.UserAccountAdmin;
import models.administrator.user.UserFotoProfileAdmin;
import play.mvc.Controller;
import play.mvc.Result;
import services.administrator.app.AppMasterFormAdminLogic;
import services.administrator.app.IAppMasterFormAdmin;
import views.html.errors.error404admin;

/**
 * Created by tri.jaruto on 6/16/2017.
 */
public class JSONFormAdminListController extends Controller {

    IAppMasterFormAdmin iAppMasterFormAdmin = new AppMasterFormAdminLogic();

    UserAccountAdmin userAccountAdminLogin = new UserAccountAdmin();
    UserFotoProfileAdmin userFotoProfileAdminLogin = new UserFotoProfileAdmin();
    boolean isLogin = false;

    public Result jSonFormAdminListPage(String amfa_name, String amfa_filenamescalahtml, String tablename, String page, String length) {
        userAccountAdminLogin = SecuredAdmin.getUserInfo(ctx());
        amfa_name = AESCrypto.decrypt(amfa_name);
        amfa_filenamescalahtml = AESCrypto.decrypt(amfa_filenamescalahtml);
        tablename = AESCrypto.decrypt(tablename);
        return createJSONFormAdminListPage(userAccountAdminLogin, amfa_name, amfa_filenamescalahtml, tablename, page, length);
    }

    //CREATE JSON ADMINLIST
    public Result createJSONFormAdminListPage(UserAccountAdmin userAccountAdminLogin, String amfa_name, String amfa_filenamescalahtml, String tablename, String page, String length) {
        AppMasterFormAdmin appMasterFormAdmin = iAppMasterFormAdmin.onSelectBy_amfa_filenamescalahtml(amfa_filenamescalahtml);
        if(appMasterFormAdmin!=null){
            if(amfa_filenamescalahtml.equals(ConstansForm.APPMASTERFORMADMIN_AMFA_FILENAMESCALAHTML_ADMINACCOUNT)){
                if(tablename.equals(ConstansTable.TABLE_USER_ACCOUNT_ADMIN)){
                    return new JSONAdminAccountListController().jSONUserAccountAdminListPage(userAccountAdminLogin, appMasterFormAdmin, page, length);
                }
            }else if(amfa_filenamescalahtml.equals(ConstansForm.APPMASTERFORMADMIN_AMFA_FILENAMESCALAHTML_USERACCOUNT)){
                if(tablename.equals(ConstansTable.TABLE_USER_ACCOUNT)){
                    return new JSONUserAccountListController().jSONUserAccountListPage(userAccountAdminLogin, appMasterFormAdmin, page, length);
                }
            }
        }
        return getResultNull(amfa_name, amfa_filenamescalahtml);
    }

    public Result jSonFormAdminListFilterPage(String amfa_name, String amfa_filenamescalahtml, String tablename, String filter, String page, String length){
        userAccountAdminLogin = SecuredAdmin.getUserInfo(ctx());
        amfa_name = AESCrypto.decrypt(amfa_name);
        amfa_filenamescalahtml = AESCrypto.decrypt(amfa_filenamescalahtml);
        return createJSONFormAdminListFilterPage(userAccountAdminLogin, amfa_name, amfa_filenamescalahtml, tablename, filter, page, length);
    }

    //CREATE JSON ADMINLISTFILTER
    public Result createJSONFormAdminListFilterPage(UserAccountAdmin userAccountAdminLogin, String amfa_name, String amfa_filenamescalahtml, String tablename, String filter, String page, String length) {
        AppMasterFormAdmin appMasterFormAdmin = iAppMasterFormAdmin.onSelectBy_amfa_filenamescalahtml(amfa_filenamescalahtml);
        if(appMasterFormAdmin!=null){
            if(amfa_filenamescalahtml.equals(ConstansForm.APPMASTERFORMADMIN_AMFA_FILENAMESCALAHTML_ADMINACCOUNT)){
                if(tablename.equals(ConstansTable.TABLE_USER_ACCOUNT_ADMIN)){
                    return new JSONAdminAccountListController().jSONUserAccountAdminListFilterPage(appMasterFormAdmin, filter, page, length);
                }
            }else if(amfa_filenamescalahtml.equals(ConstansForm.APPMASTERFORMADMIN_AMFA_FILENAMESCALAHTML_USERACCOUNT)){
                if(tablename.equals(ConstansTable.TABLE_USER_ACCOUNT)){
                    return new JSONUserAccountListController().jSONUserAccountListFilterPage(appMasterFormAdmin, filter, page, length);
                }
            }
        }
        return getResultNull(amfa_name, amfa_filenamescalahtml);
    }

    //RESULT NULL
    public Result getResultNull(String amfa_name, String amfa_filenamescalahtml){
        userAccountAdminLogin = SecuredAdmin.getUserInfo(ctx());
        userFotoProfileAdminLogin = SecuredAdmin.getUserFotoProfileAdmin(ctx());
        isLogin = SecuredAdmin.isLoggedIn(ctx());
        return ok(error404admin.render("Error 404", "404", isLogin, userAccountAdminLogin, userFotoProfileAdminLogin));
    }
}
