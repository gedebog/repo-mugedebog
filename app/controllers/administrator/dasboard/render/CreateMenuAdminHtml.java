package controllers.administrator.dasboard.render;

import controllers.util.AESCrypto;
import controllers.util.ConstansForm;
import models.administrator.app.AppMasterAdmin;
import models.administrator.app.AppMasterAdminStructure;
import models.administrator.app.AppMasterFormAdmin;
import models.administrator.app.AppMasterPermissionsFormAdmin;
import models.administrator.user.UserAccountAdmin;
import play.twirl.api.Html;
import services.administrator.app.*;
import controllers.util.Constans;

import java.io.IOException;
import java.net.URLEncoder;
import java.util.List;

import static play.mvc.Http.Context.Implicit.request;

/**
 * Created by tri.jaruto on 5/29/2017.
 */
public class CreateMenuAdminHtml {

    IAppMasterAdmin iAppMasterAdmin = new AppMasterAdminLogic();
    IAppMasterFormAdmin iAppMasterFormAdmin = new AppMasterFormAdminLogic();
    IAppMasterAdminStructure iAppMasterAdminStructure = new AppMasterAdminStructureLogic();
    IAppMasterPermissionsFormAdmin iAppMasterPermissionsFormAdmin = new AppMasterPermissionsFormAdminLogic();

    public Html getCreateMenuAdminRenderHtml(UserAccountAdmin userAccountAdmin){
        String createMenuAdmin = "<div class=\"ui menu\">\n" +
                                     "<a class=\"header item\">" +
                                        "<i class=\"desktop icon\"></i>" +
                                            "Menu Admin" +
                                     "</a>\n" +
                                      getMenuAppMasterAdminList(userAccountAdmin)+
                                     "<div class=\"right menu\">\n" +
                                         "<div class=\"item\">\n" +
                                             "<div class=\"ui action left icon input\">\n" +
                                                 "<i class=\"search icon\"></i>\n" +
                                                 "<input type=\"text\" placeholder=\"Search\">\n" +
                                                 "<button class=\"ui button\">Submit</button>\n" +
                                             "</div>\n" +
                                         "</div>" +
                                     "</div>" +
                                 "</div>";
        return Html.apply(createMenuAdmin);
    }

    public String getMenuAppMasterAdminList(UserAccountAdmin userAccountAdmin){
        String menuAppMaster = "";
        try {
            List<AppMasterAdmin> menuAppMasterAdminList = iAppMasterAdmin.onSelectAllBy_ama_level(Constans.APPMASTERADMIN_AMA_LEVEL_1);
            if(menuAppMasterAdminList!=null){
                if(!menuAppMasterAdminList.isEmpty()){
                    for(AppMasterAdmin appMasterAdmin : menuAppMasterAdminList){
                        if(appMasterAdmin.isAma_isgroup()){
                            menuAppMaster = menuAppMaster + "<div class=\"ui dropdown item\">\n" +
                                                                appMasterAdmin.getAma_name() +
                                                            "<i class=\"dropdown icon\"></i>\n" +
                                                                "<div class=\"menu\">\n" +
                                                                    getSubMenuAppMasterAdminList(userAccountAdmin, appMasterAdmin) +
                                                                "</div>\n" +
                                                            "</div>\n";
                        }else{
                            AppMasterFormAdmin appMasterFormAdmin = iAppMasterFormAdmin.onSelectBy_amfa_appmasteradmin(appMasterAdmin);
                            if(appMasterFormAdmin!=null){
                                AppMasterPermissionsFormAdmin appMasterPermissionsFormAdmin = iAppMasterPermissionsFormAdmin.onSelectBy_ampfa_useraccountadmin_and_ampfa_appmasterformadmin(userAccountAdmin, appMasterFormAdmin);
                                if(appMasterPermissionsFormAdmin!=null){
                                    if(appMasterPermissionsFormAdmin.isAmpfa_isactive()){
                                        String subMenuAHref = controllers.administrator.dasboard.form.routes.FormAdminListController.formAdminList(URLEncoder.encode(AESCrypto.encrypt(appMasterFormAdmin.getAmfa_name()),"UTF-8"),URLEncoder.encode(AESCrypto.encrypt(appMasterFormAdmin.getAmfa_filenamescalahtml()),"UTF-8")).absoluteURL(request());
                                        menuAppMaster = menuAppMaster + "<a href="+ subMenuAHref +" class=\"item\"><i class=\"file text outline icon\"></i>" + appMasterAdmin.getAma_name()+"</a>\n";
                                    }else{
                                        String subMenuAHref = controllers.administrator.dasboard.form.routes.FormAdminListController.formAdminList(URLEncoder.encode(AESCrypto.encrypt(appMasterFormAdmin.getAmfa_name()),"UTF-8"), URLEncoder.encode(AESCrypto.encrypt(ConstansForm.APPMASTERFORMADMIN_AMFA_FILENAMESCALAHTML_FORMADMINLOCK),"UTF-8")).absoluteURL(request());
                                        menuAppMaster = menuAppMaster + "<a href="+ subMenuAHref +" class=\"item\"><i class=\"lock icon\"></i>" + appMasterAdmin.getAma_name()+"</a>\n";
                                    }
                                }else{
                                    String subMenuAHref = controllers.administrator.dasboard.form.routes.FormAdminListController.formAdminList(URLEncoder.encode(AESCrypto.encrypt(appMasterFormAdmin.getAmfa_name()),"UTF-8"), URLEncoder.encode(AESCrypto.encrypt(ConstansForm.APPMASTERFORMADMIN_AMFA_FILENAMESCALAHTML_FORMADMINNULL),"UTF-8")).absoluteURL(request());
                                    menuAppMaster = menuAppMaster + "<a href="+ subMenuAHref +"class=\"item\"><i class=\"ban icon\"></i>" + appMasterAdmin.getAma_name()+"</a>\n";
                                }
                            }
                        }
                    }
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return menuAppMaster;
    }

    public String getSubMenuAppMasterAdminList(UserAccountAdmin userAccountAdmin, AppMasterAdmin amas_appmasteradmin_parent){
        String subMenuAppMaster = "";
        try {
            List<AppMasterAdminStructure> appMasterAdminStructureList = iAppMasterAdminStructure.onSelectAllBy_amas_appmasteradmin_parent(amas_appmasteradmin_parent);
            if(appMasterAdminStructureList!=null) {
                if (!appMasterAdminStructureList.isEmpty()) {
                    for (AppMasterAdminStructure appMasterAdminStructure : appMasterAdminStructureList) {
                        if(appMasterAdminStructure.getAmas_appmasteradmin_child().isAma_isgroup()){
                            subMenuAppMaster = subMenuAppMaster + "<div class=\"ui dropdown item\">\n" +
                                                                    "<i class=\"folder outline  icon\"></i>\n" +
                                                                        appMasterAdminStructure.getAmas_appmasteradmin_child().getAma_name() +
                                                                        "<div class=\"menu\">\n" +
                                                                            getSubMenuAppMasterAdminList(userAccountAdmin, appMasterAdminStructure.getAmas_appmasteradmin_child()) +
                                                                        "</div>\n" +
                                                                    "</div>\n";
                        }else{
                            AppMasterFormAdmin appMasterFormAdmin = iAppMasterFormAdmin.onSelectBy_amfa_appmasteradmin(appMasterAdminStructure.getAmas_appmasteradmin_child());
                            if(appMasterFormAdmin!=null){
                                AppMasterPermissionsFormAdmin appMasterPermissionsFormAdmin = iAppMasterPermissionsFormAdmin.onSelectBy_ampfa_useraccountadmin_and_ampfa_appmasterformadmin(userAccountAdmin, appMasterFormAdmin);
                                if(appMasterPermissionsFormAdmin!=null){
                                    if(appMasterPermissionsFormAdmin.isAmpfa_isactive()){
                                        String subMenuAHref = controllers.administrator.dasboard.form.routes.FormAdminListController.formAdminList(URLEncoder.encode(AESCrypto.encrypt(appMasterFormAdmin.getAmfa_name()),"UTF-8"),URLEncoder.encode(AESCrypto.encrypt(appMasterFormAdmin.getAmfa_filenamescalahtml()),"UTF-8")).absoluteURL(request());
                                        subMenuAppMaster = subMenuAppMaster + "<a href="+ subMenuAHref +" class=\"item\"><i class=\"file text outline icon\"></i>" + appMasterAdminStructure.getAmas_appmasteradmin_child().getAma_name()+"</a>\n";
                                    }else{
                                        String subMenuAHref = controllers.administrator.dasboard.form.routes.FormAdminListController.formAdminList(URLEncoder.encode(AESCrypto.encrypt(appMasterFormAdmin.getAmfa_name()),"UTF-8"), URLEncoder.encode(AESCrypto.encrypt(ConstansForm.APPMASTERFORMADMIN_AMFA_FILENAMESCALAHTML_FORMADMINLOCK),"UTF-8")).absoluteURL(request());
                                        subMenuAppMaster = subMenuAppMaster + "<a href="+ subMenuAHref +" class=\"item\"><i class=\"lock icon\"></i>" + appMasterAdminStructure.getAmas_appmasteradmin_child().getAma_name()+"</a>\n";
                                    }
                                }else{
                                    String subMenuAHref = controllers.administrator.dasboard.form.routes.FormAdminListController.formAdminList(URLEncoder.encode(AESCrypto.encrypt(appMasterFormAdmin.getAmfa_name()),"UTF-8"), URLEncoder.encode(AESCrypto.encrypt(ConstansForm.APPMASTERFORMADMIN_AMFA_FILENAMESCALAHTML_FORMADMINNULL),"UTF-8")).absoluteURL(request());
                                    subMenuAppMaster = subMenuAppMaster + "<a href="+ subMenuAHref +" class=\"item\"><i class=\"ban icon\"></i>" + appMasterAdminStructure.getAmas_appmasteradmin_child().getAma_name()+"</a>\n";
                                }
                            }
                        }
                    }
                }
            }

        } catch (IOException e) {
            e.printStackTrace();
        }

        return subMenuAppMaster;
    }

}
