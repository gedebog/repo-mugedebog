package controllers.administrator.dasboard.render;

import controllers.util.AESCrypto;
import controllers.util.Constans;
import controllers.util.URLEncDec;
import models.administrator.app.AppMasterFormAdmin;
import models.administrator.app.AppMasterPermissionsFormAdmin;
import models.administrator.user.UserAccountAdmin;
import play.twirl.api.Html;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

import static play.mvc.Http.Context.Implicit.request;

/**
 * Created by tri.jaruto on 5/29/2017.
 */
public class CreateMenuFormAdminMainHtml {

    public Html getCreateMenuFormAdminMainRenderHtml(String autoindex, AppMasterFormAdmin appMasterFormAdmin,  AppMasterPermissionsFormAdmin appMasterPermissionsFormAdmin, String action){
        String createMenuFormAdminMain = "";
        try {
        String subMenuAHref = controllers.administrator.dasboard.form.routes.FormAdminListController.formAdminList(URLEncoder.encode(AESCrypto.encrypt(appMasterFormAdmin.getAmfa_name()),"UTF-8"),URLEncoder.encode(AESCrypto.encrypt(appMasterFormAdmin.getAmfa_filenamescalahtml()),"UTF-8")).absoluteURL(request());
        createMenuFormAdminMain = "<div class=\"ui top attached menu\">\n" +
                                        "<div class=\"ui dropdown icon item\">\n" +
                                            "<i class=\"wrench icon\"></i>\n" +
                                            "<div class=\"menu\">\n" +
                                                getMenuAppFormAdminMain(autoindex, appMasterFormAdmin, appMasterPermissionsFormAdmin, action) +
                                            "</div>\n" +
                                        "</div>\n" +
                                        "<a href=" + subMenuAHref + " class=\"item\">\n" +
                                            "<i class=\"list layout icon\"></i>" +
                                            "List " + appMasterFormAdmin.getAmfa_name() +
                                        "</a>\n" +
                                        "<div class=\"right menu\">\n" +
                                            "<div class=\"ui right aligned category search item\">\n" +
                                                "<div class=\"ui transparent icon input\">\n" +
                                                    "<input class=\"prompt\" placeholder=\"Search account...\" type=\"text\">\n" +
                                                "<i class=\"search link icon\"></i>\n" +
                                            "</div>\n" +
                                            "<div class=\"results\"></div>\n" +
                                            "</div>\n" +
                                        "</div>\n" +
                                      "</div>";
        } catch (IOException e) {
            e.printStackTrace();
        }

        return Html.apply(createMenuFormAdminMain);
    }

    public String getMenuAppFormAdminMain(String autoindex, AppMasterFormAdmin appMasterFormAdmin, AppMasterPermissionsFormAdmin appMasterPermissionsFormAdmin, String action){
        String menuAppFormAdminMain = "";
            if(action.equals(Constans.ACTION_NEW)){
                menuAppFormAdminMain =
                    getMenuAppItemSaveNew(autoindex, appMasterFormAdmin, appMasterPermissionsFormAdmin) +
                    getMenuAppItemCancel(autoindex, appMasterFormAdmin, appMasterPermissionsFormAdmin);
            }

            if(action.equals(Constans.ACTION_EDIT)){
                menuAppFormAdminMain =
                    getMenuAppItemSaveEdit(autoindex, appMasterFormAdmin, appMasterPermissionsFormAdmin) +
                    getMenuAppItemCancel(autoindex, appMasterFormAdmin, appMasterPermissionsFormAdmin);
            }

            if(action.equals(Constans.ACTION_VIEW)){
                menuAppFormAdminMain =
                    getMenuAppItemNew(autoindex, appMasterFormAdmin, appMasterPermissionsFormAdmin) +
                    getMenuAppItemEdit(autoindex, appMasterFormAdmin, appMasterPermissionsFormAdmin) +
                    getMenuAppItemDelete(autoindex, appMasterFormAdmin, appMasterPermissionsFormAdmin) +
                    getMenuAppItemPrint(autoindex, appMasterFormAdmin, appMasterPermissionsFormAdmin) +
                    getMenuAppItemExport(autoindex, appMasterFormAdmin, appMasterPermissionsFormAdmin) +
                    getMenuAppItemImport(autoindex, appMasterFormAdmin, appMasterPermissionsFormAdmin);
            }
        return menuAppFormAdminMain;
    }


    public String getMenuAppItemNew(String autoindex, AppMasterFormAdmin appMasterFormAdmin, AppMasterPermissionsFormAdmin appMasterPermissionsFormAdmin){
        String menuAppItemNew = "";
        try {
            if(appMasterPermissionsFormAdmin.isAmpfa_isnew()){
                String subMenuAHref = controllers.administrator.dasboard.form.routes.FormAdminMainController.formAdminMain(URLEncoder.encode(AESCrypto.encrypt(appMasterFormAdmin.getAmfa_name()),"UTF-8"), URLEncoder.encode(AESCrypto.encrypt(appMasterFormAdmin.getAmfa_filenamescalahtml()),"UTF-8"), URLEncoder.encode(AESCrypto.encrypt("0"),"UTF-8"), URLEncoder.encode(AESCrypto.encrypt(Constans.ACTION_NEW),"UTF-8")).absoluteURL(request());
                menuAppItemNew = menuAppItemNew + "<a href="+ subMenuAHref +" class=\"item\">\n" +
                                "<i class=\"plus icon\"></i>\n" +
                                    "New\n" +
                                "</a>";
            }
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return menuAppItemNew;
    }

    public String getMenuAppItemEdit(String autoindex, AppMasterFormAdmin appMasterFormAdmin, AppMasterPermissionsFormAdmin appMasterPermissionsFormAdmin){
        String menuAppItemEdit = "";
        if(appMasterPermissionsFormAdmin.isAmpfa_isupdate()){
            String subMenuAHref = controllers.administrator.dasboard.form.routes.FormAdminMainController.formAdminMain(
                    URLEncDec.URLEncode(AESCrypto.encrypt(appMasterFormAdmin.getAmfa_name())),
                    URLEncDec.URLEncode(AESCrypto.encrypt(appMasterFormAdmin.getAmfa_filenamescalahtml())),
                    URLEncDec.URLEncode(AESCrypto.encrypt(autoindex)),
                    URLEncDec.URLEncode(AESCrypto.encrypt(Constans.ACTION_EDIT))).absoluteURL(request());
            menuAppItemEdit = menuAppItemEdit + "<a href="+ subMenuAHref +" class=\"item\">\n" +
                    "<i class=\"pencil icon\"></i>\n" +
                        "Edit\n" +
                    "</a>";
        }
        return menuAppItemEdit;
    }

    public String getMenuAppItemSaveNew(String autoindex, AppMasterFormAdmin appMasterFormAdmin, AppMasterPermissionsFormAdmin appMasterPermissionsFormAdmin){
        String menuAppItemSave = "";
        if(appMasterPermissionsFormAdmin.isAmpfa_isinsert()){
            String scriptmenuAppItemSaveNew =
                    "<script> " +
                            "$(document).ready(function(){ " +
                            " $(\"#"+Constans.ACTION_ID_SAVE+"\").on('click', function() {" +
                            "$(\"#"+Constans.ACTION_ID_FORM_MAIN+"\").submit();" +
                            "});" +
                            "});" +
                            " </script>";
            menuAppItemSave = scriptmenuAppItemSaveNew + "<a id=\""+Constans.ACTION_ID_SAVE+"\" class=\"item\">\n" +
                    "<i class=\"save icon\"></i>\n" +
                    "Save\n" +
                    "</a>";
        }
        return menuAppItemSave;
    }

    public String getMenuAppItemSaveEdit(String autoindex, AppMasterFormAdmin appMasterFormAdmin, AppMasterPermissionsFormAdmin appMasterPermissionsFormAdmin){
        String menuAppItemSave = "";
        if(appMasterPermissionsFormAdmin.isAmpfa_isupdate()){
            String scriptmenuAppItemSaveEdit =
                    "<script> " +
                            "$(document).ready(function(){ " +
                            " $(\"#"+Constans.ACTION_ID_SAVE+"\").on('click', function() {" +
                            "$(\"#"+Constans.ACTION_ID_FORM_MAIN+"\").submit();" +
                            "});" +
                            "});" +
                            " </script>";
            menuAppItemSave = scriptmenuAppItemSaveEdit + "<a id=\""+Constans.ACTION_ID_SAVE+"\" class=\"item\">\n" +
                    "<i class=\"save icon\"></i>\n" +
                    "Save\n" +
                    "</a>";
        }
        return menuAppItemSave;
    }

    public String getMenuAppItemCancel(String autoindex, AppMasterFormAdmin appMasterFormAdmin, AppMasterPermissionsFormAdmin appMasterPermissionsFormAdmin){
        String menuAppItemCancel = "";
        String subMenuAHref = "";
        if(autoindex.equals("0")){
            subMenuAHref = controllers.administrator.dasboard.form.routes.FormAdminListController.formAdminList(
                    URLEncDec.URLEncode(AESCrypto.encrypt(appMasterFormAdmin.getAmfa_name())),
                    URLEncDec.URLEncode(AESCrypto.encrypt(appMasterFormAdmin.getAmfa_filenamescalahtml()))).absoluteURL(request());
        }else{
            subMenuAHref = controllers.administrator.dasboard.form.routes.FormAdminMainController.formAdminMain(
                    URLEncDec.URLEncode(AESCrypto.encrypt(appMasterFormAdmin.getAmfa_name())),
                    URLEncDec.URLEncode(AESCrypto.encrypt(appMasterFormAdmin.getAmfa_filenamescalahtml())),
                    URLEncDec.URLEncode(AESCrypto.encrypt(autoindex)),
                    URLEncDec.URLEncode(AESCrypto.encrypt(Constans.ACTION_VIEW))).absoluteURL(request());
        }
        menuAppItemCancel = menuAppItemCancel + "<a href="+ subMenuAHref +" class=\"item\">\n" +
                    "<i class=\"cancel icon\"></i>\n" +
                    "Cancel\n" +
                    "</a>";
        return menuAppItemCancel;
    }

    public String getMenuAppItemDelete(String autoindex, AppMasterFormAdmin appMasterFormAdmin, AppMasterPermissionsFormAdmin appMasterPermissionsFormAdmin){
        String menuAppItemDelete = "";
        if(appMasterPermissionsFormAdmin.isAmpfa_isdelete()){
            String scriptmenuAppItemDelete =
                    "<script> " +
                            "$(document).ready(function(){ " +
                            " $(\"#"+Constans.ACTION_ID_DELETE+"\").on('click', function() {" +
                            "           alert(\"tes getMenuAppItemDelete\");" +
                            "});" +
                            "});" +
                            " </script>";
            menuAppItemDelete = scriptmenuAppItemDelete + "<a id=\""+Constans.ACTION_ID_DELETE+"\" class=\"item\">\n" +
                    "<i class=\"delete icon\"></i>\n" +
                        "Delete\n" +
                    "</a>";
        }
        return menuAppItemDelete;
    }

    public String getMenuAppItemPrint(String autoindex, AppMasterFormAdmin appMasterFormAdmin, AppMasterPermissionsFormAdmin appMasterPermissionsFormAdmin){
        String menuAppItemPrint = "";
        if(appMasterPermissionsFormAdmin.isAmpfa_isprint()){
            String scriptmenuAppItemPrint =
                    "<script> " +
                            "$(document).ready(function(){ " +
                            " $(\"#"+Constans.ACTION_ID_PRINT+"\").on('click', function() {" +
                            "           alert(\"tes getMenuAppItemPrint\");" +
                            "});" +
                            "});" +
                            " </script>";
            menuAppItemPrint = scriptmenuAppItemPrint + "<a id=\""+Constans.ACTION_ID_PRINT+"\" class=\"item\">\n" +
                    "<i class=\"print icon\"></i>\n" +
                        "Print\n" +
                    "</a>";
        }
        return menuAppItemPrint;
    }

    public String getMenuAppItemExport(String autoindex, AppMasterFormAdmin appMasterFormAdmin, AppMasterPermissionsFormAdmin appMasterPermissionsFormAdmin){
        String menuAppItemExport = "";
        if(appMasterPermissionsFormAdmin.isAmpfa_isexport()){
            String scriptmenuAppItemExport =
                    "<script> " +
                            "$(document).ready(function(){ " +
                            " $(\"#"+Constans.ACTION_ID_EXPORT+"\").on('click', function() {" +
                            "           alert(\"tes getMenuAppItemExport\");" +
                            "});" +
                            "});" +
                            " </script>";
            menuAppItemExport = scriptmenuAppItemExport + "<a id=\""+Constans.ACTION_ID_EXPORT+"\" class=\"item\">\n" +
                    "<i class=\"sign out icon\"></i>\n" +
                        "Export\n" +
                    "</a>";
        }
        return menuAppItemExport;
    }

    public String getMenuAppItemImport(String autoindex, AppMasterFormAdmin appMasterFormAdmin, AppMasterPermissionsFormAdmin appMasterPermissionsFormAdmin){
        String menuAppItemImport = "";
        if(appMasterPermissionsFormAdmin.isAmpfa_isimport()){
            String scriptmenuAppItemImport =
                    "<script> " +
                            "$(document).ready(function(){ " +
                            " $(\"#"+Constans.ACTION_ID_IMPORT+"\").on('click', function() {" +
                            "           alert(\"tes getMenuAppItemImport\");" +
                            "});" +
                            "});" +
                            " </script>";
            menuAppItemImport = scriptmenuAppItemImport + "<a id=\""+Constans.ACTION_ID_IMPORT+"\" class=\"item\">\n" +
                    "<i class=\"sign out icon\"></i>\n" +
                        "Import\n" +
                    "</a>";
        }
        return menuAppItemImport;
    }



}
