package controllers.administrator.dasboard.render;

import controllers.util.AESCrypto;
import controllers.util.Constans;
import models.administrator.app.AppMasterAdmin;
import models.administrator.app.AppMasterAdminStructure;
import models.administrator.app.AppMasterFormAdmin;
import models.administrator.app.AppMasterPermissionsFormAdmin;
import models.administrator.user.UserAccountAdmin;
import play.twirl.api.Html;
import services.administrator.app.*;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.List;

import static play.mvc.Http.Context.Implicit.request;

/**
 * Created by tri.jaruto on 5/29/2017.
 */
public class CreateMenuFormAdminListHtml {

    public Html getCreateMenuFormAdminListRenderHtml(AppMasterFormAdmin appMasterFormAdmin,  AppMasterPermissionsFormAdmin appMasterPermissionsFormAdmin){
        String createMenuFormAdminList = "";
        try {
            String subMenuAHref = controllers.administrator.dasboard.form.routes.FormAdminListController.formAdminList(URLEncoder.encode(AESCrypto.encrypt(appMasterFormAdmin.getAmfa_name()),"UTF-8"),URLEncoder.encode(AESCrypto.encrypt(appMasterFormAdmin.getAmfa_filenamescalahtml()),"UTF-8")).absoluteURL(request());
            String inputSearchTableId = "input_search_"+appMasterFormAdmin.getAmfa_filenamescalahtml()+"_id";
            createMenuFormAdminList = "<div class=\"ui top attached menu\">\n" +
                                            "<div class=\"ui dropdown icon item\">\n" +
                                                "<i class=\"wrench icon\"></i>\n" +
                                                "<div class=\"menu\">\n" +
                                                    getMenuAppFormAdminList(appMasterFormAdmin, appMasterPermissionsFormAdmin) +
                                                "</div>\n" +
                                            "</div>\n" +
                                            "<a href=" + subMenuAHref + " class=\"item\">\n" +
                                                "<i class=\"list layout icon\"></i>" +
                                                "List " + appMasterFormAdmin.getAmfa_name() +
                                            "</a>\n" +
                                            "<div class=\"right menu\">\n" +
                                                "<div class=\"ui right aligned category search item\">\n" +
                                                    "<div class=\"ui transparent icon input\">\n" +
                                                        "<input id=" + inputSearchTableId + " class=\"global_filter prompt\" placeholder=\"Search "+appMasterFormAdmin.getAmfa_name().toLowerCase()+"...\" type=\"text\">\n" +
                                                    "<i class=\"search link icon\"></i>\n" +
                                                "</div>\n" +
                                                "<div class=\"results\"></div>\n" +
                                                "</div>\n" +
                                            "</div>\n" +
                                          "</div>";

        } catch (IOException e) {
            e.printStackTrace();
        }
        return Html.apply(createMenuFormAdminList);
    }

    public String getMenuAppFormAdminList(AppMasterFormAdmin appMasterFormAdmin, AppMasterPermissionsFormAdmin appMasterPermissionsFormAdmin){
        String menuAppFormAdmin = "";
        menuAppFormAdmin =
                getMenuAppItemNew(appMasterFormAdmin, appMasterPermissionsFormAdmin) +
                getMenuAppItemPrint(appMasterFormAdmin, appMasterPermissionsFormAdmin) +
                getMenuAppItemExport(appMasterFormAdmin, appMasterPermissionsFormAdmin) +
                getMenuAppItemImport(appMasterFormAdmin, appMasterPermissionsFormAdmin);
        return menuAppFormAdmin;
    }

    public String getMenuAppItemNew(AppMasterFormAdmin appMasterFormAdmin, AppMasterPermissionsFormAdmin appMasterPermissionsFormAdmin){
        String menuAppItemNew = "";
        try {
            if(appMasterPermissionsFormAdmin.isAmpfa_isnew()){
                String subMenuAHref = controllers.administrator.dasboard.form.routes.FormAdminMainController.formAdminMain(URLEncoder.encode(AESCrypto.encrypt(appMasterFormAdmin.getAmfa_name()),"UTF-8"), URLEncoder.encode(AESCrypto.encrypt(appMasterFormAdmin.getAmfa_filenamescalahtml()),"UTF-8"), URLEncoder.encode(AESCrypto.encrypt("0"),"UTF-8"), URLEncoder.encode(AESCrypto.encrypt(Constans.ACTION_NEW),"UTF-8")).absoluteURL(request());
                menuAppItemNew = menuAppItemNew + "<a href="+ subMenuAHref +" class=\"item\">\n" +
                        "<i class=\"plus icon\"></i>\n" +
                            "New\n" +
                        "</a>";
            }
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return menuAppItemNew;
    }

    public String getMenuAppItemPrint(AppMasterFormAdmin appMasterFormAdmin, AppMasterPermissionsFormAdmin appMasterPermissionsFormAdmin){
        String menuAppItemPrint = "";
        if(appMasterPermissionsFormAdmin.isAmpfa_isprint()){
            String scriptmenuAppItemPrint =
                    "<script> " +
                            "$(document).ready(function(){ " +
                            " $(\"#"+Constans.ACTION_ID_PRINT+"\").on('click', function() {" +
                            "           alert(\"tes getMenuAppItemPrint\");" +
                            "});" +
                            "});" +
                            " </script>";
            menuAppItemPrint = scriptmenuAppItemPrint + "<a id=\""+Constans.ACTION_ID_PRINT+"\" class=\"item\">\n" +
                    "<i class=\"print icon\"></i>\n" +
                    "Print\n" +
                    "</a>";
        }
        return menuAppItemPrint;
    }

    public String getMenuAppItemExport(AppMasterFormAdmin appMasterFormAdmin, AppMasterPermissionsFormAdmin appMasterPermissionsFormAdmin){
        String menuAppItemExport = "";
        if(appMasterPermissionsFormAdmin.isAmpfa_isexport()){
            String scriptmenuAppItemExport =
                    "<script> " +
                            "$(document).ready(function(){ " +
                            " $(\"#"+Constans.ACTION_ID_EXPORT+"\").on('click', function() {" +
                            "           alert(\"tes getMenuAppItemExport\");" +
                            "});" +
                            "});" +
                            " </script>";
            menuAppItemExport = scriptmenuAppItemExport + "<a id=\""+Constans.ACTION_ID_EXPORT+"\" class=\"item\">\n" +
                    "<i class=\"sign out icon\"></i>\n" +
                    "Export\n" +
                    "</a>";
        }
        return menuAppItemExport;
    }

    public String getMenuAppItemImport(AppMasterFormAdmin appMasterFormAdmin, AppMasterPermissionsFormAdmin appMasterPermissionsFormAdmin){
        String menuAppItemImport = "";
        if(appMasterPermissionsFormAdmin.isAmpfa_isimport()){
            String scriptmenuAppItemImport =
                    "<script> " +
                            "$(document).ready(function(){ " +
                            " $(\"#"+Constans.ACTION_ID_IMPORT+"\").on('click', function() {" +
                            "           alert(\"tes getMenuAppItemImport\");" +
                            "});" +
                            "});" +
                            " </script>";
            menuAppItemImport = scriptmenuAppItemImport + "<a id=\""+Constans.ACTION_ID_IMPORT+"\" class=\"item\">\n" +
                    "<i class=\"sign out icon\"></i>\n" +
                    "Import\n" +
                    "</a>";
        }
        return menuAppItemImport;
    }


}
