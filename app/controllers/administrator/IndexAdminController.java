package controllers.administrator;

import controllers.util.SecuredAdmin;
import models.administrator.user.UserAccountAdmin;
import models.administrator.user.UserFotoProfileAdmin;
import play.mvc.Controller;
import play.mvc.Result;
import play.mvc.Security;
import views.html.administrator.indexadmin;

/**
 * Created by tri.jaruto on 18/11/2016.
 */
public class IndexAdminController extends Controller {

    UserAccountAdmin userAccountAdminLogin = new UserAccountAdmin();
    UserFotoProfileAdmin userFotoProfileAdminLogin = new UserFotoProfileAdmin();
    boolean isLogin = false;

    public Result indexAdmin() {
        userAccountAdminLogin = SecuredAdmin.getUserInfo(ctx());
        userFotoProfileAdminLogin = SecuredAdmin.getUserFotoProfileAdmin(ctx());
        isLogin = SecuredAdmin.isLoggedIn(ctx());
        return ok(indexadmin.render("Gedebog Admin", isLogin, userAccountAdminLogin, userFotoProfileAdminLogin));
    }

    @Security.Authenticated(SecuredAdmin.class)
    public Result logoutAdmin() {
        session().clear();
        return redirect(routes.IndexAdminController.indexAdmin());
    }
}
