package controllers.user;

import controllers.util.Secured;
import models.user.user.UserAccount;
import models.user.user.UserFotoProfile;
import play.mvc.Controller;
import play.mvc.Result;
import play.mvc.Security;
import views.html.user.index;

/**
 * Created by tri.jaruto on 18/11/2016.
 */
public class IndexController extends Controller {

    UserAccount userAccountLogin = new UserAccount();
    UserFotoProfile userFotoProfileLogin = new UserFotoProfile();
    boolean isLogin = false;

    public Result index() {
        userAccountLogin = Secured.getUserInfo(ctx());
        userFotoProfileLogin = Secured.getUserFotoProfile(ctx());
        isLogin = Secured.isLoggedIn(ctx());
        return ok(index.render("Gedebog", isLogin, userAccountLogin, userFotoProfileLogin));
    }

    @Security.Authenticated(Secured.class)
    public Result logout() {
        session().clear();
        return redirect(routes.IndexController.index());
    }
}
