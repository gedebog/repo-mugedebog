package controllers.user.dasboard;

import controllers.user.dasboard.render.CreateMenuHtml;
import controllers.util.Secured;
import controllers.util.SendEmail;
import models.administrator.user.UserAccountAdmin;
import models.administrator.user.UserFotoProfileAdmin;
import models.user.user.UserAccount;
import models.user.user.UserFotoProfile;
import play.libs.mailer.MailerClient;
import play.mvc.Controller;
import play.mvc.Result;
import play.mvc.Security;
import services.user.user.IUserAccount;
import services.user.user.UserAccountLogic;
import views.html.user.dasboard.dasboard;

import javax.inject.Inject;


/**
 * Created by tri.jaruto on 18/11/2016.
 */
public class DasboardController extends Controller {

    IUserAccount iUserAccount = new UserAccountLogic();

    UserAccount userAccountLogin = new UserAccount();
    UserFotoProfile userFotoProfileLogin = new UserFotoProfile();
    boolean isLogin = false;

    @Inject
    MailerClient mailerClient;

    @Security.Authenticated(Secured.class)
    public Result dasboard(){
        userAccountLogin = Secured.getUserInfo(ctx());
        userFotoProfileLogin = Secured.getUserFotoProfile(ctx());
        isLogin = Secured.isLoggedIn(ctx());
        return ok(dasboard.render("Dasboard", Secured.isLoggedIn(ctx()), Secured.getUserInfo(ctx()), Secured.getUserFotoProfile(ctx()), new CreateMenuHtml().getCreateMenuRenderHtml(userAccountLogin)));
    }

    public Result sendActivationCode(){
        //SEND EMAIL
        SendEmail sendEmail = new SendEmail(mailerClient);
        sendEmail.onSendEmailSignUpActivation(Secured.getUserInfo(ctx()));
        return redirect(controllers.user.dasboard.routes.DasboardController.dasboard());
    }

}
