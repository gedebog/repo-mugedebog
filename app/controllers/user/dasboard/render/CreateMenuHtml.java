package controllers.user.dasboard.render;

import controllers.util.AESCrypto;
import controllers.util.Constans;
import controllers.util.ConstansForm;
import controllers.util.URLEncDec;
import models.user.app.AppMaster;
import models.user.app.AppMasterForm;
import models.user.app.AppMasterPermissionsForm;
import models.user.app.AppMasterStructure;
import models.user.user.UserAccount;
import play.twirl.api.Html;
import services.user.app.*;

import java.util.List;

import static play.mvc.Http.Context.Implicit.request;

/**
 * Created by tri.jaruto on 5/29/2017.
 */
public class CreateMenuHtml {

    IAppMaster iAppMaster = new AppMasterLogic();
    IAppMasterForm iAppMasterForm = new AppMasterFormLogic();
    IAppMasterStructure iAppMasterStructure = new AppMasterStructureLogic();
    IAppMasterPermissionsForm iAppMasterPermissionsForm = new AppMasterPermissionsFormLogic();

    public Html getCreateMenuRenderHtml(UserAccount userAccount){
        String createMenu = "<div class=\"ui menu\">\n" +
                                     "<a class=\"header item\">" +
                                        "<i class=\"desktop icon\"></i>" +
                                            "Menu " +
                                     "</a>\n" +
                                      getMenuAppMasterList(userAccount)+
                                     "<div class=\"right menu\">\n" +
                                         "<div class=\"item\">\n" +
                                             "<div class=\"ui action left icon input\">\n" +
                                                 "<i class=\"search icon\"></i>\n" +
                                                 "<input type=\"text\" placeholder=\"Search\">\n" +
                                                 "<button class=\"ui button\">Submit</button>\n" +
                                             "</div>\n" +
                                         "</div>" +
                                     "</div>" +
                                 "</div>";
        return Html.apply(createMenu);
    }

    public String getMenuAppMasterList(UserAccount userAccount){
        String menuAppMaster = "";
        List<AppMaster> menuAppMasterList = iAppMaster.onSelectAllBy_am_level(Constans.APPMASTERADMIN_AMA_LEVEL_1);
        if(menuAppMasterList!=null){
            if(!menuAppMasterList.isEmpty()){
                for(AppMaster appMaster : menuAppMasterList){
                    if(appMaster.isAm_isgroup()){
                        menuAppMaster = menuAppMaster + "<div class=\"ui dropdown item\">\n" +
                                                            appMaster.getAm_name() +
                                                        "<i class=\"dropdown icon\"></i>\n" +
                                                            "<div class=\"menu\">\n" +
                                                                getSubMenuAppMasterList(userAccount, appMaster) +
                                                            "</div>\n" +
                                                        "</div>\n";
                    }else{
                        AppMasterForm appMasterForm = iAppMasterForm.onSelectBy_amf_appmaster(appMaster);
                        if(appMasterForm!=null){
                            AppMasterPermissionsForm appMasterPermissionsForm = iAppMasterPermissionsForm.onSelectBy_ampf_useraccount_and_ampf_appmasterform(userAccount, appMasterForm);
                            if(appMasterPermissionsForm!=null){
                                if(appMasterPermissionsForm.isAmpf_isactive()){
                                    String subMenuAHref = controllers.user.dasboard.form.routes.FormListController.formList(URLEncDec.URLEncode(AESCrypto.encrypt(appMasterForm.getAmf_name())),URLEncDec.URLEncode(AESCrypto.encrypt(appMasterForm.getAmf_filenamescalahtml()))).absoluteURL(request());
                                    menuAppMaster = menuAppMaster + "<a href="+ subMenuAHref +" class=\"item\"><i class=\"file text outline icon\"></i>" + appMaster.getAm_name()+"</a>\n";
                                }else{
                                    String subMenuAHref = controllers.user.dasboard.form.routes.FormListController.formList(URLEncDec.URLEncode(AESCrypto.encrypt(appMasterForm.getAmf_name())),URLEncDec.URLEncode(AESCrypto.encrypt(ConstansForm.APPMASTERFORM_AMF_FILENAMESCALAHTML_FORMLOCK))).absoluteURL(request());
                                    menuAppMaster = menuAppMaster + "<a href="+ subMenuAHref +" class=\"item\"><i class=\"lock icon\"></i>" + appMaster.getAm_name()+"</a>\n";
                                }
                            }else{
                                String subMenuAHref = controllers.user.dasboard.form.routes.FormListController.formList(URLEncDec.URLEncode(AESCrypto.encrypt(appMasterForm.getAmf_name())),URLEncDec.URLEncode(AESCrypto.encrypt(ConstansForm.APPMASTERFORM_AMF_FILENAMESCALAHTML_FORMNULL))).absoluteURL(request());
                                menuAppMaster = menuAppMaster + "<a href="+ subMenuAHref +"class=\"item\"><i class=\"ban icon\"></i>" + appMaster.getAm_name()+"</a>\n";
                            }
                        }
                    }
                }
            }
        }
        return menuAppMaster;
    }

    public String getSubMenuAppMasterList(UserAccount userAccount, AppMaster amas_appmasteradmin_parent){
        String subMenuAppMaster = "";
        List<AppMasterStructure> appMasterStructureList = iAppMasterStructure.onSelectAllBy_ams_appmaster_parent(amas_appmasteradmin_parent);
        if(appMasterStructureList!=null) {
            if (!appMasterStructureList.isEmpty()) {
                for (AppMasterStructure appMasterStructure : appMasterStructureList) {
                    if(appMasterStructure.getAms_appmaster_child().isAm_isgroup()){
                        subMenuAppMaster = subMenuAppMaster + "<div class=\"ui dropdown item\">\n" +
                                                                "<i class=\"folder outline  icon\"></i>\n" +
                                                                    appMasterStructure.getAms_appmaster_child().getAm_name() +
                                                                    "<div class=\"menu\">\n" +
                                                                        getSubMenuAppMasterList(userAccount, appMasterStructure.getAms_appmaster_child()) +
                                                                    "</div>\n" +
                                                                "</div>\n";
                    }else{
                        AppMasterForm appMasterForm = iAppMasterForm.onSelectBy_amf_appmaster(appMasterStructure.getAms_appmaster_child());
                        if(appMasterForm!=null){
                            AppMasterPermissionsForm appMasterPermissionsForm = iAppMasterPermissionsForm.onSelectBy_ampf_useraccount_and_ampf_appmasterform(userAccount, appMasterForm);
                            if(appMasterPermissionsForm!=null){
                                if(appMasterPermissionsForm.isAmpf_isactive()){
                                    String subMenuAHref = controllers.user.dasboard.form.routes.FormListController.formList(URLEncDec.URLEncode(AESCrypto.encrypt(appMasterForm.getAmf_name())),URLEncDec.URLEncode(AESCrypto.encrypt(appMasterForm.getAmf_filenamescalahtml()))).absoluteURL(request());
                                    subMenuAppMaster = subMenuAppMaster + "<a href="+ subMenuAHref +" class=\"item\"><i class=\"file text outline icon\"></i>" + appMasterStructure.getAms_appmaster_child().getAm_name()+"</a>\n";
                                }else{
                                    String subMenuAHref = controllers.user.dasboard.form.routes.FormListController.formList(URLEncDec.URLEncode(AESCrypto.encrypt(appMasterForm.getAmf_name())),URLEncDec.URLEncode(AESCrypto.encrypt(ConstansForm.APPMASTERFORM_AMF_FILENAMESCALAHTML_FORMLOCK))).absoluteURL(request());
                                    subMenuAppMaster = subMenuAppMaster + "<a href="+ subMenuAHref +" class=\"item\"><i class=\"lock icon\"></i>" + appMasterStructure.getAms_appmaster_child().getAm_name()+"</a>\n";
                                }
                            }else{
                                String subMenuAHref = controllers.user.dasboard.form.routes.FormListController.formList(URLEncDec.URLEncode(AESCrypto.encrypt(appMasterForm.getAmf_name())),URLEncDec.URLEncode(AESCrypto.encrypt(ConstansForm.APPMASTERFORM_AMF_FILENAMESCALAHTML_FORMNULL))).absoluteURL(request());
                                subMenuAppMaster = subMenuAppMaster + "<a href="+ subMenuAHref +" class=\"item\"><i class=\"ban icon\"></i>" + appMasterStructure.getAms_appmaster_child().getAm_name()+"</a>\n";
                            }
                        }
                    }
                }
            }
        }

        return subMenuAppMaster;
    }

}
