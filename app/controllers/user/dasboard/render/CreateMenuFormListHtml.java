package controllers.user.dasboard.render;

import controllers.util.AESCrypto;
import controllers.util.Constans;
import controllers.util.URLEncDec;
import models.user.app.AppMasterForm;
import models.user.app.AppMasterPermissionsForm;
import play.twirl.api.Html;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

import static play.mvc.Http.Context.Implicit.request;

/**
 * Created by tri.jaruto on 5/29/2017.
 */
public class CreateMenuFormListHtml {

    public Html getCreateMenuFormListRenderHtml(AppMasterForm appMasterForm, AppMasterPermissionsForm appMasterPermissionsForm){
        String createMenuFormList = "";
       // try {
            String subMenuAHref = controllers.user.dasboard.form.routes.FormListController.formList(URLEncDec.URLEncode(AESCrypto.encrypt(appMasterForm.getAmf_name())),URLEncDec.URLEncode(AESCrypto.encrypt(appMasterForm.getAmf_filenamescalahtml()))).absoluteURL(request());
            String inputSearchTableId = "input_search_"+appMasterForm.getAmf_filenamescalahtml()+"_id";
            createMenuFormList = "<div class=\"ui top attached menu\">\n" +
                                            "<div class=\"ui dropdown icon item\">\n" +
                                                "<i class=\"wrench icon\"></i>\n" +
                                                "<div class=\"menu\">\n" +
                                                    getMenuAppFormList(appMasterForm, appMasterPermissionsForm) +
                                                "</div>\n" +
                                            "</div>\n" +
                                            "<a href=" + subMenuAHref + " class=\"item\">\n" +
                                                "<i class=\"list layout icon\"></i>" +
                                                "List " + appMasterForm.getAmf_name() +
                                            "</a>\n" +
                                            "<div class=\"right menu\">\n" +
                                                "<div class=\"ui right aligned category search item\">\n" +
                                                    "<div class=\"ui transparent icon input\">\n" +
                                                        "<input id=" + inputSearchTableId + " class=\"global_filter prompt\" placeholder=\"Search "+appMasterForm.getAmf_name().toLowerCase()+"...\" type=\"text\">\n" +
                                                    "<i class=\"search link icon\"></i>\n" +
                                                "</div>\n" +
                                                "<div class=\"results\"></div>\n" +
                                                "</div>\n" +
                                            "</div>\n" +
                                          "</div>";

        //} catch (IOException e) {
          //  e.printStackTrace();
       // }
        return Html.apply(createMenuFormList);
    }

    public String getMenuAppFormList(AppMasterForm appMasterForm, AppMasterPermissionsForm appMasterPermissionsForm){
        String menuAppForm = "";
        menuAppForm =
                getMenuAppItemNew(appMasterForm, appMasterPermissionsForm) +
                getMenuAppItemPrint(appMasterForm, appMasterPermissionsForm) +
                getMenuAppItemExport(appMasterForm, appMasterPermissionsForm) +
                getMenuAppItemImport(appMasterForm, appMasterPermissionsForm);
        return menuAppForm;
    }

    public String getMenuAppItemNew(AppMasterForm appMasterForm, AppMasterPermissionsForm appMasterPermissionsForm){
        String menuAppItemNew = "";
        //try {
            if(appMasterPermissionsForm.isAmpf_isnew()){
                String subMenuAHref = controllers.user.dasboard.form.routes.FormMainController.formMain(URLEncDec.URLEncode(AESCrypto.encrypt(appMasterForm.getAmf_name())),URLEncDec.URLEncode(AESCrypto.encrypt(appMasterForm.getAmf_filenamescalahtml())), URLEncDec.URLEncode(AESCrypto.encrypt("0")), URLEncDec.URLEncode(AESCrypto.encrypt(Constans.ACTION_NEW))).absoluteURL(request());
                menuAppItemNew = menuAppItemNew + "<a href="+ subMenuAHref +" class=\"item\">\n" +
                        "<i class=\"plus icon\"></i>\n" +
                            "New\n" +
                        "</a>";
            }
        //} catch (UnsupportedEncodingException e) {
        //    e.printStackTrace();
       // }
        return menuAppItemNew;
    }

    public String getMenuAppItemPrint(AppMasterForm appMasterForm, AppMasterPermissionsForm appMasterPermissionsForm){
        String menuAppItemPrint = "";
        if(appMasterPermissionsForm.isAmpf_isprint()){
            String scriptmenuAppItemPrint =
                    "<script> " +
                            "$(document).ready(function(){ " +
                            " $(\"#"+Constans.ACTION_ID_PRINT+"\").on('click', function() {" +
                            "           alert(\"tes getMenuAppItemPrint\");" +
                            "});" +
                            "});" +
                            " </script>";
            menuAppItemPrint = scriptmenuAppItemPrint + "<a id=\""+Constans.ACTION_ID_PRINT+"\" class=\"item\">\n" +
                    "<i class=\"print icon\"></i>\n" +
                    "Print\n" +
                    "</a>";
        }
        return menuAppItemPrint;
    }

    public String getMenuAppItemExport(AppMasterForm appMasterForm, AppMasterPermissionsForm appMasterPermissionsForm){
        String menuAppItemExport = "";
        if(appMasterPermissionsForm.isAmpf_isexport()){
            String scriptmenuAppItemExport =
                    "<script> " +
                            "$(document).ready(function(){ " +
                            " $(\"#"+Constans.ACTION_ID_EXPORT+"\").on('click', function() {" +
                            "           alert(\"tes getMenuAppItemExport\");" +
                            "});" +
                            "});" +
                            " </script>";
            menuAppItemExport = scriptmenuAppItemExport + "<a id=\""+Constans.ACTION_ID_EXPORT+"\" class=\"item\">\n" +
                    "<i class=\"sign out icon\"></i>\n" +
                    "Export\n" +
                    "</a>";
        }
        return menuAppItemExport;
    }

    public String getMenuAppItemImport(AppMasterForm appMasterForm, AppMasterPermissionsForm appMasterPermissionsForm){
        String menuAppItemImport = "";
        if(appMasterPermissionsForm.isAmpf_isimport()){
            String scriptmenuAppItemImport =
                    "<script> " +
                            "$(document).ready(function(){ " +
                            " $(\"#"+Constans.ACTION_ID_IMPORT+"\").on('click', function() {" +
                            "           alert(\"tes getMenuAppItemImport\");" +
                            "});" +
                            "});" +
                            " </script>";
            menuAppItemImport = scriptmenuAppItemImport + "<a id=\""+Constans.ACTION_ID_IMPORT+"\" class=\"item\">\n" +
                    "<i class=\"sign out icon\"></i>\n" +
                    "Import\n" +
                    "</a>";
        }
        return menuAppItemImport;
    }


}
