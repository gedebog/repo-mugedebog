package controllers.user.dasboard.form;

import controllers.user.dasboard.render.CreateMenuHtml;
import controllers.util.*;
import models.user.app.AppMasterForm;
import models.user.user.UserAccount;
import models.user.user.UserFotoProfile;
import play.data.DynamicForm;
import play.data.FormFactory;
import play.libs.mailer.MailerClient;
import play.mvc.Controller;
import play.mvc.Http;
import play.mvc.Result;
import play.mvc.Security;
import services.user.app.AppMasterFormLogic;
import services.user.app.IAppMasterForm;
import views.html.user.dasboard.form.formlock;
import views.html.user.dasboard.form.formnull;
import views.html.errors.error404;

import javax.inject.Inject;
import java.io.File;

/**
 * Created by tri.jaruto on 18/11/2016.
 */
public class FormMainController extends Controller {

    IAppMasterForm iAppMasterForm = new AppMasterFormLogic();

    UserAccount userAccountLogin = new UserAccount();
    UserFotoProfile userFotoProfileLogin = new UserFotoProfile();
    boolean isLogin = false;

    UserAccount userAccount = new UserAccount();

    @Inject
    MailerClient mailerClient;

    @Inject
    FormFactory formFactory;

    //GET
    @Security.Authenticated(Secured.class)
    public Result formMain(String amf_name, String amf_filenamescalahtml, String autoindex, String action){
        userAccountLogin = Secured.getUserInfo(ctx());
        userFotoProfileLogin = Secured.getUserFotoProfile(ctx());
        isLogin = Secured.isLoggedIn(ctx());
        Http.MultipartFormData<File> requestbody = request().body().asMultipartFormData();
        DynamicForm requestData = formFactory.form().bindFromRequest();
        amf_name = AESCrypto.decrypt(URLEncDec.URLDecode(amf_name));
        amf_filenamescalahtml = AESCrypto.decrypt(URLEncDec.URLDecode(amf_filenamescalahtml));
        autoindex = AESCrypto.decrypt(URLEncDec.URLDecode(autoindex));
        action = AESCrypto.decrypt(URLEncDec.URLDecode(action));
        return createFormMainGET(isLogin, userAccountLogin, userFotoProfileLogin, amf_name, amf_filenamescalahtml, autoindex, action, requestData, requestbody, mailerClient);
    }

    public Result createFormMainGET(boolean isLogin, UserAccount userAccountLogin, UserFotoProfile userFotoProfileLogin, String amf_name, String amf_filenamescalahtml, String autoindex, String action, DynamicForm requestData, Http.MultipartFormData<File> requestbody, MailerClient mailerClient){
        AppMasterForm appMasterForm = iAppMasterForm.onSelectBy_amf_filenamescalahtml(amf_filenamescalahtml);
        if(appMasterForm!=null){
            if(amf_filenamescalahtml.equals(ConstansForm.APPMASTERFORM_AMF_FILENAMESCALAHTML_ORGANIZATION)){

            }
        }
        return getResultLockAndNull(isLogin, userAccountLogin, userFotoProfileLogin, amf_name, amf_filenamescalahtml);
    }


    //POST
    public Result onAction(){
        userAccountLogin = Secured.getUserInfo(ctx());
        userFotoProfileLogin = Secured.getUserFotoProfile(ctx());
        isLogin = Secured.isLoggedIn(ctx());
        Http.MultipartFormData<File> requestbody = request().body().asMultipartFormData();
        DynamicForm requestData = formFactory.form().bindFromRequest();
        String amf_name = AESCrypto.decrypt(URLEncDec.URLDecode(requestData.get("amf_name_name")));
        String amf_filenamescalahtml = AESCrypto.decrypt(URLEncDec.URLDecode(requestData.get("amf_filenamescalahtml_name")));
        String tablename_input_name = AESCrypto.decrypt(URLEncDec.URLDecode(requestData.get("tablename_input_name")));
        String autoindex_input_name = AESCrypto.decrypt(URLEncDec.URLDecode(requestData.get("autoindex_input_name")));
        return createFormMainPOST(isLogin, userAccountLogin, userFotoProfileLogin, amf_name, amf_filenamescalahtml, tablename_input_name, autoindex_input_name, Constans.ACTION_SAVE, requestData, requestbody, mailerClient);
    }

    public Result onUpload(){
        userAccountLogin = Secured.getUserInfo(ctx());
        isLogin = Secured.isLoggedIn(ctx());
        Http.MultipartFormData<File> requestbody = request().body().asMultipartFormData();
        DynamicForm requestData = formFactory.form().bindFromRequest();
        String amf_name = AESCrypto.decrypt(URLEncDec.URLDecode(requestData.get("amf_name_name")));
        String amf_filenamescalahtml = AESCrypto.decrypt(URLEncDec.URLDecode(requestData.get("amf_filenamescalahtml_name")));
        String tablename_input_name = AESCrypto.decrypt(URLEncDec.URLDecode(requestData.get("tablename_input_name")));
        String autoindex_input_name = AESCrypto.decrypt(URLEncDec.URLDecode(requestData.get("autoindex_input_name")));
        return createFormMainPOST(isLogin, userAccountLogin, userFotoProfileLogin, amf_name, amf_filenamescalahtml, tablename_input_name, autoindex_input_name, Constans.ACTION_UPLOAD, requestData, requestbody, mailerClient);
    }

    public Result createFormMainPOST(boolean isLogin, UserAccount userAccountLogin, UserFotoProfile userFotoProfileLogin, String amf_name, String amf_filenamescalahtml, String tablename_input_name, String autoindex_input_name, String action, DynamicForm requestData, Http.MultipartFormData<File> requestbody, MailerClient mailerClient){
        AppMasterForm appMasterForm = iAppMasterForm.onSelectBy_amf_filenamescalahtml(amf_filenamescalahtml);
        if(appMasterForm!=null){
            if(amf_filenamescalahtml.equals(ConstansForm.APPMASTERFORM_AMF_FILENAMESCALAHTML_ORGANIZATION)){
                if(tablename_input_name.equals(ConstansTable.TABLE_APP_MASTER_ORGANIZATION)){
                    //return new AccountMainController().userAccountController(isLogin, userAccountLogin, userFotoProfileLogin, appMasterForm, autoindex_input_name, action, requestData, requestbody, mailerClient);
                }
            }
        }
        return getResultLockAndNull(isLogin, userAccountLogin, userFotoProfileLogin, amf_name, amf_filenamescalahtml);
    }

    //RESULT LOCK AND NULL
    public Result getResultLockAndNull(boolean isLogin, UserAccount userAccountLogin, UserFotoProfile userFotoProfileLogin, String amf_name, String amf_filenamescalahtml){
        if(amf_filenamescalahtml.equals(ConstansForm.APPMASTERFORM_AMF_FILENAMESCALAHTML_FORMLOCK)){
            return ok(formlock.render("Form  Lock", isLogin, userAccountLogin, userFotoProfileLogin, new CreateMenuHtml().getCreateMenuRenderHtml(userAccountLogin)));
        }else if(amf_filenamescalahtml.equals(ConstansForm.APPMASTERFORM_AMF_FILENAMESCALAHTML_FORMNULL)){
            return ok(formnull.render("Form  Null", isLogin, userAccountLogin, userFotoProfileLogin, new CreateMenuHtml().getCreateMenuRenderHtml(userAccountLogin)));
        }else{
            return ok(error404.render("Error 404", "404", isLogin, userAccountLogin, userFotoProfileLogin));
        }
    }

}
