package controllers.user.dasboard.form;

import controllers.user.dasboard.form.master.OrganizationListController;
import controllers.user.dasboard.render.CreateMenuHtml;
import controllers.util.AESCrypto;
import controllers.util.ConstansForm;
import controllers.util.Secured;
import controllers.util.URLEncDec;
import models.user.app.AppMasterForm;
import models.user.user.UserAccount;
import models.user.user.UserFotoProfile;
import play.data.FormFactory;
import play.libs.mailer.MailerClient;
import play.mvc.Controller;
import play.mvc.Result;
import play.mvc.Security;
import services.user.app.AppMasterFormLogic;
import services.user.app.IAppMasterForm;
import views.html.user.dasboard.form.formlock;
import views.html.user.dasboard.form.formnull;
import views.html.errors.error404;

import javax.inject.Inject;

/**
 * Created by tri.jaruto on 18/11/2016.
 */
public class FormListController extends Controller {

    IAppMasterForm iAppMasterForm = new AppMasterFormLogic();

    UserAccount userAccountLogin = new UserAccount();
    UserFotoProfile userFotoProfileLogin = new UserFotoProfile();
    boolean isLogin = false;

    @Inject
    MailerClient mailerClient;

    @Inject
    FormFactory formFactory;

    @Security.Authenticated(Secured.class)
    public Result formList(String amf_name, String amf_filenamescalahtml) {
        userAccountLogin = Secured.getUserInfo(ctx());
        userFotoProfileLogin = Secured.getUserFotoProfile(ctx());
        isLogin = Secured.isLoggedIn(ctx());
        amf_name = AESCrypto.decrypt(URLEncDec.URLDecode(amf_name));
        amf_filenamescalahtml = AESCrypto.decrypt(URLEncDec.URLDecode(amf_filenamescalahtml));
        return createFormList(isLogin, userAccountLogin, userFotoProfileLogin, amf_name, amf_filenamescalahtml);
    }

    public Result createFormList(boolean isLogin, UserAccount userAccountLogin, UserFotoProfile userFotoProfileLogin, String amf_name, String amf_filenamescalahtml){
        AppMasterForm appMasterForm = iAppMasterForm.onSelectBy_amf_filenamescalahtml(amf_filenamescalahtml);
        if(appMasterForm!=null){
            if(amf_filenamescalahtml.equals(ConstansForm.APPMASTERFORM_AMF_FILENAMESCALAHTML_ORGANIZATION)){
                return new OrganizationListController().organizationListController(isLogin, userAccountLogin, userFotoProfileLogin, appMasterForm);
               // return ok(amf_filenamescalahtml);
            }else if(amf_filenamescalahtml.equals(ConstansForm.APPMASTERFORM_AMF_FILENAMESCALAHTML_EMPLOYEE)){
                return ok(amf_filenamescalahtml);
            }else if(amf_filenamescalahtml.equals(ConstansForm.APPMASTERFORM_AMF_FILENAMESCALAHTML_POSITION)){
                return ok(amf_filenamescalahtml);
            }
        }
        return getResultLockAndNull(isLogin, userAccountLogin, userFotoProfileLogin, amf_name, amf_filenamescalahtml);
    }


    //RESULT LOCK AND NULL
    public Result getResultLockAndNull(boolean isLogin, UserAccount userAccountLogin,  UserFotoProfile userFotoProfileLogin, String amf_name, String amf_filenamescalahtml){
        if(amf_filenamescalahtml.equals(ConstansForm.APPMASTERFORM_AMF_FILENAMESCALAHTML_FORMLOCK)){
            return ok(formlock.render(amf_name+" Lock", isLogin, userAccountLogin, userFotoProfileLogin, new CreateMenuHtml().getCreateMenuRenderHtml(userAccountLogin)));
        }else if(amf_filenamescalahtml.equals(ConstansForm.APPMASTERFORM_AMF_FILENAMESCALAHTML_FORMNULL)){
            return ok(formnull.render(amf_name+" Null", isLogin, userAccountLogin, userFotoProfileLogin, new CreateMenuHtml().getCreateMenuRenderHtml(userAccountLogin)));
        }else{
            return ok(error404.render("Error 404", "404", isLogin, userAccountLogin, userFotoProfileLogin));
        }
    }


}
