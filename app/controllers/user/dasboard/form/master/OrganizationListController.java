package controllers.user.dasboard.form.master;

import controllers.user.dasboard.render.CreateMenuFormListHtml;
import controllers.user.dasboard.render.CreateMenuHtml;
import controllers.util.Secured;
import models.user.app.AppMasterForm;
import models.user.app.AppMasterPermissionsForm;
import models.user.user.UserAccount;
import models.user.user.UserFotoProfile;
import play.data.FormFactory;
import play.mvc.Controller;
import play.mvc.Result;
import play.mvc.Security;
import services.user.app.AppMasterPermissionsFormLogic;
import services.user.app.IAppMasterPermissionsForm;
import services.user.user.IUserAccount;
import services.user.user.UserAccountLogic;
import views.html.user.dasboard.form.master.organizationlist;

import javax.inject.Inject;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by tri.jaruto on 5/31/2017.
 */
public class OrganizationListController extends Controller {

    IAppMasterPermissionsForm iAppMasterPermissionsForm = new AppMasterPermissionsFormLogic();
    IUserAccount iUserAccount = new UserAccountLogic();

    List<UserAccount> userAccounts = new ArrayList<UserAccount>();

    @Inject
    FormFactory formFactory;

    @Security.Authenticated(Secured.class)
    public Result organizationListController(boolean isLogin, UserAccount userAccountLogin, UserFotoProfile userFotoProfileLogin, AppMasterForm appMasterForm){
        AppMasterPermissionsForm appMasterPermissionsForm = iAppMasterPermissionsForm.onSelectBy_ampf_useraccount_and_ampf_appmasterform(userAccountLogin, appMasterForm);
        return ok(organizationlist.render(appMasterForm.getAmf_name(), isLogin, userAccountLogin, userFotoProfileLogin, appMasterForm, new CreateMenuHtml().getCreateMenuRenderHtml(userAccountLogin),  new CreateMenuFormListHtml().getCreateMenuFormListRenderHtml(appMasterForm, appMasterPermissionsForm)));
    }

}
