package controllers.user.dasboard.profile;

import controllers.user.dasboard.render.CreateMenuHtml;
import controllers.util.Secured;
import models.user.user.UserAccount;
import models.user.user.UserFotoProfile;
import models.user.user.UserProfile;
import play.data.DynamicForm;
import play.data.FormFactory;
import play.mvc.Controller;
import play.mvc.Result;
import play.mvc.Security;
import services.user.user.IUserProfile;
import services.user.user.UserProfileLogic;
import javax.inject.Inject;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import static controllers.util.Constans.ACTION_EDIT;
import static controllers.util.Constans.ACTION_VIEW;
import views.html.user.dasboard.profile.profile;

/**
 * Created by tri.jaruto on 13/12/2016.
 */
public class ProfileController extends Controller {

    IUserProfile iUserProfile = new UserProfileLogic();

    UserAccount userAccountLogin = new UserAccount();
    UserFotoProfile userFotoProfileLogin = new UserFotoProfile();
    boolean isLogin = false;

    SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");

    @Inject
    FormFactory formFactory;

    @Security.Authenticated(Secured.class)
    public Result profile(){
        userAccountLogin = Secured.getUserInfo(ctx());
        userFotoProfileLogin = Secured.getUserFotoProfile(ctx());
        isLogin = Secured.isLoggedIn(ctx());
        return ok(profile.render("Profile", isLogin, userAccountLogin, userFotoProfileLogin, new CreateMenuHtml().getCreateMenuRenderHtml(userAccountLogin), ACTION_VIEW));
    }

    public Result onAction(){
        userAccountLogin = Secured.getUserInfo(ctx());
        userFotoProfileLogin = Secured.getUserFotoProfile(ctx());
        isLogin = Secured.isLoggedIn(ctx());
        //GET DYNAMIC FORM
        DynamicForm requestData = formFactory.form().bindFromRequest();
        if(requestData.get("profile_save_button_name") != null){
            onUpdate(requestData);
            return ok(profile.render("Profile", isLogin, userAccountLogin, userFotoProfileLogin, new CreateMenuHtml().getCreateMenuRenderHtml(userAccountLogin), ACTION_VIEW));
        }else if(requestData.get("profile_edit_button_name") != null){
            return ok(profile.render("Profile", isLogin, userAccountLogin, userFotoProfileLogin, new CreateMenuHtml().getCreateMenuRenderHtml(userAccountLogin), ACTION_EDIT));
        }else if(requestData.get("profile_cancel_button_name") != null){
            return ok(profile.render("Profile", isLogin, userAccountLogin, userFotoProfileLogin, new CreateMenuHtml().getCreateMenuRenderHtml(userAccountLogin), ACTION_VIEW));
        }
        return ok(profile.render("Profile", isLogin, userAccountLogin, userFotoProfileLogin, new CreateMenuHtml().getCreateMenuRenderHtml(userAccountLogin), ACTION_VIEW));
    }

    public UserAccount onUpdate(DynamicForm requestData){
        try {
            UserProfile userProfile = new UserProfile();
            userProfile.setUp_autoindex(userAccountLogin.getUa_userprofile().getUp_autoindex());
            userProfile.setUp_name(requestData.get("up_name_name"));
            userProfile.setUp_nickname(requestData.get("up_nickname_name"));
            userProfile.setUp_birthplace(requestData.get("up_birthplace_name"));
            userProfile.setUp_birthdate(dateFormat.parse(requestData.get("up_birthdate_name")));
            userProfile.setUp_isgender(Boolean.parseBoolean(requestData.get("up_isgender_name")));
            userProfile.setUp_updated(new Date());
            userProfile.setUp_updater(userAccountLogin.getUa_username());
            iUserProfile.onUpdate(userProfile);
            userAccountLogin.setUa_userprofile(userProfile);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return userAccountLogin;
    }



}
