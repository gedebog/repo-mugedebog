package controllers.user.authentication;

import controllers.util.Secured;
import play.mvc.Controller;
import play.mvc.Result;
import play.mvc.Security;

/**
 * Created by tri.jaruto on 22/11/2016.
 */
public class LogoutController extends Controller {

    @Security.Authenticated(Secured.class)
    public Result logout() {
        session().clear();
        return redirect(controllers.user.routes.IndexController.index());
    }

}
