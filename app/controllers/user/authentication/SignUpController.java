package controllers.user.authentication;

import controllers.util.AppException;
import controllers.util.BCrypto;
import controllers.util.Secured;
import controllers.util.SendEmail;
import controllers.user.routes;
import models.user.user.UserAccount;
import models.user.user.UserFotoProfile;
import models.user.user.UserProfile;
import play.Configuration;
import play.data.DynamicForm;
import play.data.FormFactory;
import play.libs.mailer.MailerClient;
import play.libs.ws.WSClient;
import play.libs.ws.WSResponse;
import play.mvc.Controller;
import play.mvc.Result;
import services.user.user.IUserAccount;
import services.user.user.IUserProfile;
import services.user.user.UserAccountLogic;
import javax.inject.Inject;
import java.util.Date;
import java.util.concurrent.ExecutionException;
import services.user.user.UserProfileLogic;
import views.html.user.authentication.signup;

/**
 * Created by tri.jaruto on 21/11/2016.
 */
public class SignUpController  extends Controller {

    IUserAccount iUserAccount = new UserAccountLogic();
    IUserProfile iUserProfile = new UserProfileLogic();

    UserAccount userAccountLogin = new UserAccount();
    UserFotoProfile userFotoProfileLogin = new UserFotoProfile();
    boolean isLogin = false;

    @Inject
    MailerClient mailerClient;

    @Inject
    FormFactory formFactory;

    @Inject
    WSClient ws;

    public Result signUp(){
        userAccountLogin = Secured.getUserInfo(ctx());
        userFotoProfileLogin = Secured.getUserFotoProfile(ctx());
        isLogin = Secured.isLoggedIn(ctx());
        if(isLogin){
            return redirect(routes.IndexController.index());
        }
        return ok(signup.render("Signup", isLogin, userAccountLogin, userFotoProfileLogin));
    }

    public Result onSignUp() {
        flash().clear();
        userAccountLogin = Secured.getUserInfo(ctx());
        userFotoProfileLogin = Secured.getUserFotoProfile(ctx());
        isLogin = Secured.isLoggedIn(ctx());
        //GET RESPONSE RECAPTCHA
        DynamicForm requestData = formFactory.form().bindFromRequest();
        String recaptcha_response = requestData.get("g-recaptcha-response");
        String ua_username_name = requestData.get("ua_username_name");
        String ua_userpassword_name = requestData.get("ua_userpassword_name");

        boolean responseJson = false;
        try {
            responseJson = ws.url(Configuration.root().getString("recaptcha.siteverify.url")).setContentType("application/x-www-form-urlencoded").post("secret=" + Configuration.root().getString("recaptcha.secretkey") + "&response=" + recaptcha_response + "&remoteip=" + request().remoteAddress()).thenApply(WSResponse::asJson).toCompletableFuture().get().findPath("success").asBoolean();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }

        if (iUserAccount.onCheckedBy_ua_username(ua_username_name)) {
            flash("error", "Email already exist");
            return badRequest(signup.render("Signup", isLogin, userAccountLogin, userFotoProfileLogin));
        } else {
            if(responseJson){
                try {
                    UserAccount userAccount = new UserAccount();
                    userAccount.setUa_username(ua_username_name);
                    userAccount.setUa_userpassword(BCrypto.createPassword(ua_userpassword_name));
                    userAccount.setUa_isactive(false);
                    userAccount.setUa_activationcode(BCrypto.createPassword(ua_username_name));
                    userAccount.setUa_isforgotpassword(false);
                    userAccount.setUa_created(new Date());
                    userAccount.setUa_updated(new Date());
                    userAccount = iUserAccount.onSave_Return_UserAccount(userAccount);

                    userAccount.setUa_creater(userAccount.getUa_username());
                    userAccount.setUa_updater(userAccount.getUa_username());
                    iUserAccount.onUpdate(userAccount);

                    UserProfile userProfile = new UserProfile();
                    userProfile.setUp_useraccount(userAccount);
                    userProfile.setUp_created(new Date());
                    userProfile.setUp_creater(userAccount.getUa_username());
                    userProfile.setUp_updated(new Date());
                    userProfile.setUp_updater(userAccount.getUa_username());
                    iUserProfile.onSave(userProfile);

                    //SEND EMAIL
                    SendEmail sendEmail = new SendEmail(mailerClient);
                    sendEmail.onSendEmailSignUpActivation(userAccount);
                } catch (AppException e) {
                    e.printStackTrace();
                }

                flash("error", "Account has been create, please open your email "+ua_username_name+" to activated account.");
                return badRequest(signup.render("Signup", isLogin, userAccountLogin, userFotoProfileLogin));
            }else{
                flash("error", "Please Click reCaptcha");
                return badRequest(signup.render("Signup", isLogin, userAccountLogin, userFotoProfileLogin));
            }
        }
    }

}
