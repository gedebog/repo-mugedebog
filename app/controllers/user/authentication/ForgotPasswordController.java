package controllers.user.authentication;

import controllers.util.Secured;
import controllers.util.SendEmail;
import models.user.user.UserAccount;
import models.user.user.UserFotoProfile;
import play.data.DynamicForm;
import play.data.Form;
import play.data.FormFactory;
import play.libs.mailer.MailerClient;
import play.mvc.Controller;
import play.mvc.Result;
import services.user.user.IUserAccount;
import services.user.user.UserAccountLogic;
import views.html.user.authentication.forgotpassword;
import controllers.user.routes;

import javax.inject.Inject;
import java.util.Date;

/**
 * Created by tri.jaruto on 22/11/2016.
 */
public class ForgotPasswordController extends Controller {

    IUserAccount iUserAccount = new UserAccountLogic();

    UserAccount userAccountLogin = new UserAccount();
    UserFotoProfile userFotoProfileLogin = new UserFotoProfile();
    boolean isLogin = false;

    @Inject
    FormFactory formFactory;

    @Inject
    MailerClient mailerClient;

    public Result forgotPassword() {
        userAccountLogin = Secured.getUserInfo(ctx());
        userFotoProfileLogin = Secured.getUserFotoProfile(ctx());
        isLogin = Secured.isLoggedIn(ctx());
        //Form<UserAccount> formUserAccount = formFactory.form(UserAccount.class).bindFromRequest();
        if(isLogin){
            return redirect(routes.IndexController.index());
        }
        return ok(forgotpassword.render("Forgot Password", isLogin, userAccountLogin, userFotoProfileLogin));
    }

    public Result onForgotPassword() {
        userAccountLogin = Secured.getUserInfo(ctx());
        userFotoProfileLogin = Secured.getUserFotoProfile(ctx());
        isLogin = Secured.isLoggedIn(ctx());
        flash().clear();

        //GET FORM DYNAMIC
        DynamicForm requestData = formFactory.form().bindFromRequest();
        String ua_username_name = requestData.get("ua_username_name");

        if (iUserAccount.onCheckedBy_ua_username(ua_username_name)) {
            UserAccount userAccount = iUserAccount.onSelectBy_ua_username(ua_username_name);
            userAccount.setUa_isforgotpassword(true);
            userAccount.setUa_updated(new Date());
            userAccount.setUa_updater(userAccount.getUa_username());
            iUserAccount.onUpdate(userAccount);

            //SEND EMAIL FORGOT PASSWORD
            SendEmail sendEmail = new SendEmail(mailerClient);
            sendEmail.onSendEmailForgotPassword(userAccount);

            flash("error", "Email has been send, open your email "+ua_username_name+" to reset pasword.");
            return badRequest(forgotpassword.render("Signup", isLogin, userAccountLogin, userFotoProfileLogin));
        } else {
            flash("error", "Email cannot be found!");
            return badRequest(forgotpassword.render("Forgot Password", isLogin, userAccountLogin, userFotoProfileLogin));
        }
    }



}


