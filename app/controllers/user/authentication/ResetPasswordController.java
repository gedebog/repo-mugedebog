package controllers.user.authentication;

import controllers.util.Secured;
import controllers.util.AESCrypto;
import controllers.util.BCrypto;
import controllers.util.AppException;
import models.user.user.UserAccount;
import models.user.user.UserFotoProfile;
import play.Configuration;
import play.data.DynamicForm;
import play.data.FormFactory;
import play.libs.ws.WSClient;
import play.libs.ws.WSResponse;
import play.mvc.Controller;
import play.mvc.Result;
import services.user.user.IUserAccount;
import services.user.user.UserAccountLogic;
import javax.inject.Inject;
import java.util.Date;
import java.util.concurrent.ExecutionException;
import views.html.user.authentication.resetpassword;
import views.html.user.authentication.activation;

/**
 * Created by tri.jaruto on 22/11/2016.
 */
public class ResetPasswordController extends Controller {

    IUserAccount iUserAccount = new UserAccountLogic();

    UserAccount userAccountLogin = new UserAccount();
    UserFotoProfile userFotoProfileLogin = new UserFotoProfile();
    boolean isLogin = false;

    @Inject
    FormFactory formFactory;

    @Inject
    WSClient ws;

    public Result resetPassword(String ua_username, String ua_activationcode) {
        userAccountLogin = Secured.getUserInfo(ctx());
        userFotoProfileLogin = Secured.getUserFotoProfile(ctx());
        isLogin = Secured.isLoggedIn(ctx());
        if(isLogin){
            return redirect(controllers.user.routes.IndexController.index());
        }else{
            ua_username = AESCrypto.decrypt(ua_username);
            if(iUserAccount.onValidateBy_ua_username_and_ua_activationcode(ua_username, ua_activationcode)){
                UserAccount userAccount = iUserAccount.onSelectBy_ua_username(ua_username);
                if(userAccount!=null){
                    if(userAccount.isUa_isforgotpassword()==true){
                        return ok(resetpassword.render("Reset Password", isLogin, userAccountLogin, userFotoProfileLogin));
                    }else{
                        return ok(activation.render("Activation", false, isLogin, userAccountLogin, userFotoProfileLogin));
                    }
                }
            }
            return redirect(controllers.user.routes.IndexController.index());
        }
    }

    public Result onResetPassword() {
        userAccountLogin = Secured.getUserInfo(ctx());
        userFotoProfileLogin = Secured.getUserFotoProfile(ctx());
        isLogin = Secured.isLoggedIn(ctx());
        flash().clear();
        //GET DYNAMIC FORM
        DynamicForm requestData = formFactory.form().bindFromRequest();

        String ua_username_name = requestData.get("ua_username_name");
        String ua_userpassword_name = requestData.get("ua_userpassword_name");
        String recaptcha_response = requestData.get("g-recaptcha-response");

        boolean responseJson = false;
        try {
            responseJson = ws.url(Configuration.root().getString("recaptcha.siteverify.url")).setContentType("application/x-www-form-urlencoded").post("secret=" + Configuration.root().getString("recaptcha.secretkey") + "&response=" + recaptcha_response + "&remoteip=" + request().remoteAddress()).thenApply(WSResponse::asJson).toCompletableFuture().get().findPath("success").asBoolean();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }

        if (iUserAccount.onCheckedBy_ua_username(ua_username_name)) {
            UserAccount userAccount = iUserAccount.onSelectBy_ua_username(ua_username_name);
            if(responseJson){
                try {
                    userAccount.setUa_userpassword(BCrypto.createPassword(ua_userpassword_name));
                    userAccount.setUa_isforgotpassword(false);
                    userAccount.setUa_updated(new Date());
                    userAccount.setUa_updater(userAccount.getUa_username());
                    iUserAccount.onUpdate(userAccount);
                } catch (AppException e) {
                    e.printStackTrace();
                }
                flash("error", "Password has been reset");
                return redirect(controllers.user.authentication.routes.LoginController.login());
            }else{
                flash("error", "Please Click reCaptcha");
                return badRequest(resetpassword.render("Reset Password", isLogin, userAccountLogin, userFotoProfileLogin));
            }
        } else {
            flash("Email cannot be found!");
            return badRequest(resetpassword.render("Reset Password", isLogin, userAccountLogin, userFotoProfileLogin));
        }
    }


}


