package controllers.user.authentication;

import controllers.util.AESCrypto;
import controllers.util.Secured;
import models.user.user.UserAccount;
import models.user.user.UserFotoProfile;
import play.mvc.Controller;
import play.mvc.Result;
import services.user.user.IUserAccount;
import services.user.user.UserAccountLogic;
import views.html.user.authentication.activation;

import java.util.Date;

/**
 * Created by tri.jaruto on 22/11/2016.
 */
public class ActivationController extends Controller {

    IUserAccount iUserAccount = new UserAccountLogic();

    UserAccount userAccountLogin = new UserAccount();
    UserFotoProfile userFotoProfileLogin = new UserFotoProfile();
    boolean isLogin = false;

    UserAccount userAccount = new UserAccount();

    boolean isactive = false;

    public Result activation(String ua_username, String ua_activationcode) {
        userAccountLogin = Secured.getUserInfo(ctx());
        userFotoProfileLogin = Secured.getUserFotoProfile(ctx());
        isLogin = Secured.isLoggedIn(ctx());
        String username = AESCrypto.decrypt(ua_username);
        if(iUserAccount.onValidateBy_ua_username_and_ua_activationcode(username, ua_activationcode)){
            userAccount = iUserAccount.onSelectBy_ua_username(username);
            if(userAccount!=null){
                if(!userAccount.isUa_isactive()){
                    userAccount.setUa_isactive(true);
                    userAccount.setUa_updated(new Date());
                    userAccount.setUa_updater(userAccount.getUa_username());
                    iUserAccount.onUpdate(userAccount);
                    isactive = true;
                }
            }
        }

        return ok(activation.render("Activation", isactive, isLogin, userAccountLogin, userFotoProfileLogin));
    }

}


