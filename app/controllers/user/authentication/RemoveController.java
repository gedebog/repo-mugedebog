package controllers.user.authentication;

import controllers.util.Secured;
import models.user.user.UserAccount;
import models.user.user.UserFotoProfile;
import play.mvc.Controller;
import play.mvc.Result;
import services.user.user.IUserAccount;
import services.user.user.UserAccountLogic;
import views.html.user.authentication.activation;

/**
 * Created by tri.jaruto on 22/11/2016.
 */
public class RemoveController extends Controller {

    UserAccount userAccountLogin = new UserAccount();
    UserFotoProfile userFotoProfileLogin = new UserFotoProfile();
    boolean isLogin = false;
    boolean isactive = false;

    public Result remove(String ua_username, String ua_activationcode) {
        userAccountLogin = Secured.getUserInfo(ctx());
        userFotoProfileLogin = Secured.getUserFotoProfile(ctx());
        isLogin = Secured.isLoggedIn(ctx());
        return ok(activation.render("Remove", isactive, isLogin, userAccountLogin, userFotoProfileLogin));
    }

}


