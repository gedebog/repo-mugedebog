package controllers.user.authentication;

import controllers.util.AESCrypto;
import controllers.util.Secured;
import models.user.user.UserAccount;
import models.user.user.UserFotoProfile;
import play.data.DynamicForm;
import play.data.FormFactory;
import play.mvc.Controller;
import play.mvc.Result;
import services.user.user.IUserAccount;
import services.user.user.UserAccountLogic;
import views.html.user.authentication.login;
import javax.inject.Inject;

/**
 * Created by tri.jaruto on 16/11/2016.
 */
public class LoginController extends Controller {

    IUserAccount iUserAccount = new UserAccountLogic();

    UserAccount userAccountLogin = new UserAccount();
    UserFotoProfile userFotoProfileLogin = new UserFotoProfile();
    boolean isLogin = false;

    @Inject
    FormFactory formFactory;

    public Result login(){
        userAccountLogin = Secured.getUserInfo(ctx());
        userFotoProfileLogin = Secured.getUserFotoProfile(ctx());
        isLogin = Secured.isLoggedIn(ctx());
        if(isLogin){
            return redirect(controllers.user.routes.IndexController.index());
        }
        return ok(login.render("Login", isLogin, userAccountLogin, userFotoProfileLogin));
    }

    public Result onLogin(){
        userAccountLogin = Secured.getUserInfo(ctx());
        isLogin = Secured.isLoggedIn(ctx());
        //GET DYNAMIC FORM
        DynamicForm requestData = formFactory.form().bindFromRequest();
        if (!iUserAccount.onValidateBy_ua_username_and_ua_password(requestData.get("ua_username_name"), requestData.get("ua_userpassword_name"))) {
            flash("error", "Login credentials not valid.");
            return badRequest(login.render("Login", isLogin, userAccountLogin, userFotoProfileLogin));
        }else {
            session().clear();
            UserAccount userAccount = iUserAccount.onSelectBy_ua_username(requestData.get("ua_username_name"));
            session("ua_autoindex", AESCrypto.encrypt(Long.toString(userAccount.getUa_autoindex())));
            session("ua_username", AESCrypto.encrypt(userAccount.getUa_username()));
            return redirect(controllers.user.dasboard.routes.DasboardController.dasboard());
        }
    }


}
