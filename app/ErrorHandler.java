import controllers.util.Secured;
import play.http.HttpErrorHandler;
import play.mvc.*;
import play.mvc.Http.*;
import views.html.errors.error404;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CompletionStage;
import javax.inject.Singleton;

import static play.mvc.Controller.ctx;

/**
 * Created by tri.jaruto on 13/12/2016.
 */

@Singleton
public class ErrorHandler implements HttpErrorHandler {
    public CompletionStage<Result> onClientError(RequestHeader request, int statusCode, String message) {
        if(statusCode==404){
            return CompletableFuture.completedFuture(
                    Results.status(statusCode, error404.render("Error "+statusCode, String.valueOf(statusCode), Secured.isLoggedIn(ctx()), Secured.getUserInfo(ctx()), Secured.getUserFotoProfile(ctx())))
            );
        }else{
            return CompletableFuture.completedFuture(
                    Results.status(statusCode, error404.render("Error "+statusCode, String.valueOf(statusCode), Secured.isLoggedIn(ctx()), Secured.getUserInfo(ctx()), Secured.getUserFotoProfile(ctx())))
            );
        }
    }

    @Override
   public CompletionStage<Result> onServerError(RequestHeader request, Throwable exception) {
        return null;
    }

    /*public CompletionStage<Result> onServerError(RequestHeader request, Throwable exception) {
        return CompletableFuture.completedFuture(
                Results.internalServerError(errorServer.render("internalServerError "+exception.getMessage(), exception.toString(), Secured.isLoggedIn(ctx()), Secured.getUserInfo(ctx())))
        );
    }*/
}
