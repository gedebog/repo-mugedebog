import controllers.util.SecuredAdmin;
import play.http.HttpErrorHandler;
import play.mvc.Http.RequestHeader;
import play.mvc.Result;
import play.mvc.Results;
import views.html.errors.error404admin;

import javax.inject.Singleton;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CompletionStage;

import static play.mvc.Controller.ctx;

/**
 * Created by tri.jaruto on 13/12/2016.
 */

@Singleton
public class ErrorHandlerAdmin implements HttpErrorHandler {
    public CompletionStage<Result> onClientError(RequestHeader request, int statusCode, String message) {
        if(statusCode==404){
            return CompletableFuture.completedFuture(
                    Results.status(statusCode, error404admin.render("Error "+statusCode, String.valueOf(statusCode), SecuredAdmin.isLoggedIn(ctx()), SecuredAdmin.getUserInfo(ctx()), SecuredAdmin.getUserFotoProfileAdmin(ctx())))
            );
        }else{
            return CompletableFuture.completedFuture(
                    Results.status(statusCode, error404admin.render("Error "+statusCode, String.valueOf(statusCode), SecuredAdmin.isLoggedIn(ctx()), SecuredAdmin.getUserInfo(ctx()), SecuredAdmin.getUserFotoProfileAdmin(ctx())))
            );
        }
    }

    @Override
   public CompletionStage<Result> onServerError(RequestHeader request, Throwable exception) {
        return null;
    }

    /*public CompletionStage<Result> onServerError(RequestHeader request, Throwable exception) {
        return CompletableFuture.completedFuture(
                Results.internalServerError(errorServer.render("internalServerError "+exception.getMessage(), exception.toString(), Secured.isLoggedIn(ctx()), Secured.getUserInfo(ctx())))
        );
    }*/
}
