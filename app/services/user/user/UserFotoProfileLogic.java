package services.user.user;

import com.avaje.ebean.Model;
import models.user.user.UserAccount;
import models.user.user.UserFotoProfile;
import play.db.ebean.Transactional;

/**
 * Created by tri.jaruto on 06/12/2016.
 */
public class UserFotoProfileLogic implements IUserFotoProfile{

    public static Model.Finder<Long, UserFotoProfile> find = new Model.Finder<Long,UserFotoProfile>(UserFotoProfile.class);

    @Override
    @Transactional
    public void onSave(UserFotoProfile userFotoProfile) {
        userFotoProfile.save();
    }

    @Override
    @Transactional
    public void onUpdate(UserFotoProfile userFotoProfile) {
        userFotoProfile.update();
    }

    @Override
    public UserFotoProfile onSelectBy_ufp_autoindex(long ufpa_autoindex) {
        UserFotoProfile userFotoProfile = find.where().eq("ufp_autoindex", ufpa_autoindex).findUnique();
        if (userFotoProfile != null) {
            return userFotoProfile;
        }
        return null;
    }

    @Override
    public UserFotoProfile onSelectBy_ufp_useraccount(UserAccount userAccount) {
        UserFotoProfile userFotoProfile = find.where().eq("ufp_useraccount", userAccount).findUnique();
        if (userFotoProfile != null) {
            return userFotoProfile;
        }
        return null;
    }
}
