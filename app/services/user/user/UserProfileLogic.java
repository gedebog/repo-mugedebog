package services.user.user;

import com.avaje.ebean.Model;
import models.user.user.UserProfile;
import play.db.ebean.Transactional;


/**
 * Created by tri.jaruto on 06/12/2016.
 */
public class UserProfileLogic implements IUserProfile{

    public static Model.Finder<Long, UserProfile> find = new Model.Finder<Long,UserProfile>(UserProfile.class);

    //SAVE
    @Override
    @Transactional
    public void onSave(UserProfile userProfile) {
        userProfile.save();
    }

    //UPDATE
    @Override
    @Transactional
    public void onUpdate(UserProfile userProfile) {
        userProfile.update();
    }

    //DELETE
    @Override
    public void onDelete(UserProfile userProfile) {
        userProfile.delete();
    }

    //SELECT
    @Override
    public UserProfile onSelectBy_up_autoindex(long up_autoindex) {
        UserProfile userProfile = find.where().eq("up_autoindex", up_autoindex).findUnique();
        if (userProfile != null) {
            return userProfile;
        }
        return null;
    }

    @Override
    public UserProfile onSelectBy_up_user_account(long up_user_account) {
        UserProfile userProfile = find.where().eq("up_user_account", up_user_account).findUnique();
        if (userProfile != null) {
            return userProfile;
        }
        return null;
    }
}
