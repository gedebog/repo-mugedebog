package services.user.user;

import models.user.user.UserAccount;

import java.util.List;


/**
 * Created by tri.jaruto on 06/12/2016.
 */
public interface IUserAccount {


    //SAVE
    public void onSave(UserAccount userAccount);
    public long onSave_Return_ua_autoindex(UserAccount userAccount);
    public UserAccount onSave_Return_UserAccount(UserAccount userAccount);

    //UPDATE
    public void onUpdate(UserAccount userAccount);
    public UserAccount onUpdate_Return_UserAccount(UserAccount userAccount);

    //DELETE
    public void onDelete(UserAccount userAccount);

    //SELECT
    public int onRowCount();
    public int onRowCountByFilter(String filter);

    public UserAccount onSelectBy_ua_autoindex(long ua_autoindex);
    public UserAccount onSelectBy_ua_username(String ua_username);
    public boolean onValidateBy_ua_username_and_ua_password(String ua_username, String ua_password);
    public boolean onValidateBy_ua_username_and_ua_activationcode(String ua_username, String ua_activationcode);
    public boolean onCheckedBy_ua_username(String ua_username);

    public List<UserAccount> onSelectAll();
    public List<UserAccount> onSelectAllBy_Pagination(int startrow, int endrow);
    public List<UserAccount> onSelectAllBy_Filter_and_Pagination(String filter, int startrow, int endrow);


}
