package services.user.user;

import com.avaje.ebean.Ebean;
import com.avaje.ebean.Model;
import com.avaje.ebean.PagedList;
import com.avaje.ebean.SqlUpdate;
import controllers.util.BCrypto;
import models.user.user.UserAccount;
import models.user.user.UserProfile;
import play.db.ebean.Transactional;

import java.util.List;

/**
 * Created by tri.jaruto on 06/12/2016.
 */
public class UserAccountLogic implements IUserAccount {

    public static Model.Finder<Long, UserAccount> find = new Model.Finder<Long,UserAccount>(UserAccount.class);

    //SAVE
    @Override
    @Transactional
    public void onSave(UserAccount userAccount) {
        userAccount.save();
    }

    @Override
    @Transactional
    public long onSave_Return_ua_autoindex(UserAccount userAccount) {
        userAccount.save();
        return userAccount.getUa_autoindex();
    }

    @Override
    @Transactional
    public UserAccount onSave_Return_UserAccount(UserAccount userAccount) {
        userAccount.save();
        return onSelectBy_ua_username(userAccount.getUa_username());
    }

    //UPDATE
    @Override
    @Transactional
    public void onUpdate(UserAccount userAccount) {
        userAccount.update();
    }

    @Override
    public UserAccount onUpdate_Return_UserAccount(UserAccount userAccount) {
        userAccount.update();
        return onSelectBy_ua_autoindex(userAccount.getUa_autoindex());
    }

    //DELETE
    @Override
    @Transactional
    public void onDelete(UserAccount userAccount) {
        userAccount.delete();
    }

    @Override
    public int onRowCount() {
        return find.findRowCount();
    }

    @Override
    public int onRowCountByFilter(String filter) {
        return find.where().like("ua_username", "%"+filter+"%").findRowCount();
    }

    //SELECT
    @Override
    public UserAccount onSelectBy_ua_autoindex(long ua_autoindex) {
        UserAccount userAccount = find.where().eq("ua_autoindex", ua_autoindex).findUnique();
        if (userAccount != null) {
            return userAccount;
        }
        return null;
    }

    @Override
    public UserAccount onSelectBy_ua_username(String ua_username) {
        UserAccount userAccount = find.where().eq("ua_username", ua_username).findUnique();
        if (userAccount != null) {
                return userAccount;
        }
        return null;
    }

    @Override
    public boolean onValidateBy_ua_username_and_ua_password(String ua_username, String ua_userpassword) {
        boolean isValid = false;
        UserAccount userAccount = find.where().eq("ua_username", ua_username).findUnique();
        if(userAccount!=null){
            isValid = BCrypto.checkPassword(ua_userpassword, userAccount.getUa_userpassword());
        }
        return isValid;
    }

    @Override
    public boolean onValidateBy_ua_username_and_ua_activationcode(String ua_username, String ua_activationcode) {
        boolean isValid = false;
        UserAccount userAccount = find.where().eq("ua_username", ua_username).findUnique();
        if(userAccount!=null){
            isValid = BCrypto.checkPassword(ua_username, userAccount.getUa_activationcode());
        }
        return isValid;
    }

    @Override
    public boolean onCheckedBy_ua_username(String ua_username) {
        boolean isChecked = false;
        UserAccount userAccount = find.where().eq("ua_username", ua_username).findUnique();
        if(userAccount!=null){
            isChecked = true;
        }
        return isChecked;
    }

    @Override
    public List<UserAccount> onSelectAll() {
        List<UserAccount> userAccounts = find.findList();
        if (!userAccounts.isEmpty()) {
            return userAccounts;
        }
        return null;
    }

    @Override
    public List<UserAccount> onSelectAllBy_Pagination(int startrow, int endrow) {
        PagedList<UserAccount> pagedList = find.order().desc("ua_autoindex").findPagedList(startrow, endrow);
        List<UserAccount> userAccounts = pagedList.getList();
        if (!userAccounts.isEmpty()) {
            return userAccounts;
        }
        return null;
    }

    @Override
    public List<UserAccount> onSelectAllBy_Filter_and_Pagination(String filter, int startrow, int endrow) {
        PagedList<UserAccount> pagedList = find.where().like("ua_username", "%"+filter+"%").order().desc("ua_autoindex").findPagedList(startrow, endrow);
        List<UserAccount> userAccounts = pagedList.getList();
        if (!userAccounts.isEmpty()) {
            return userAccounts;
        }
        return null;
    }


    //CONTOH QUERY
    /*public void onUpdate_By_ua_autoindex(long ua_autoindex, UserAccount userAccount) {
        System.out.println("onUpdate_By_ua_autoindex userAccount.ua_isactive : "+userAccount.isUa_isactive());
        System.out.println("onUpdate_By_ua_autoindex ua_username.ua_username : "+userAccount.getUa_username());
        String sql = " update " +
                     " user_account " +
                     " set " +
                     " ua_username = :ua_username " +
                     " where " +
                     " ua_autoindex = :ua_autoindex";
        SqlUpdate update = Ebean.createSqlUpdate(sql)
                .setParameter("ua_username", "123123213")
                *//*.setParameter("ua_userpassword", userAccount.ua_userpassword)
                .setParameter("ua_isactive", true)
                .setParameter("ua_activationcode", userAccount.ua_activationcode)
                .setParameter("ua_isforgotpassword", userAccount.ua_isforgotpassword)
                .setParameter("ua_create", userAccount.ua_create)
                .setParameter("ua_updated", userAccount.ua_updated)*//*
                .setParameter("ua_autoindex", userAccount.getUa_autoindex());
        System.out.println("onUpdate_By_ua_autoindex userAccount.ua_isactive : "+userAccount.isUa_isactive());
        int rows = update.execute();
        System.out.println("onUpdate_By_ua_autoindex userAccount.ua_isactive : "+userAccount.getUa_username());
    }*/


}
