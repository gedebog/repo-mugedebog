package services.user.user;

import models.user.user.UserAccount;
import models.user.user.UserFotoProfile;

/**
 * Created by tri.jaruto on 06/12/2016.
 */
public interface IUserFotoProfile {


    //SAVE
    public void onSave(UserFotoProfile userFotoProfile);

    //UPDATE
    public void onUpdate(UserFotoProfile userFotoProfile);

    //DELETE

    //SELECT
    public UserFotoProfile onSelectBy_ufp_autoindex(long ufp_autoindex);
    public UserFotoProfile onSelectBy_ufp_useraccount(UserAccount userAccount);


}
