package services.user.user;

import models.user.user.UserProfile;


/**
 * Created by tri.jaruto on 06/12/2016.
 */
public interface IUserProfile {


    //SAVE
    public void onSave(UserProfile userProfile);

    //UPDATE
    public void onUpdate(UserProfile userProfile);

    //DELETE
    public void onDelete(UserProfile userProfile);

    //SELECT
    public UserProfile onSelectBy_up_autoindex(long up_autoindex);
    public UserProfile onSelectBy_up_user_account(long up_user_account);


}
