package services.user.app;

import com.avaje.ebean.Model;
import models.user.app.AppMaster;
import models.user.app.AppMasterForm;
import play.db.ebean.Transactional;

import java.util.List;

/**
 * Created by tri.jaruto on 06/12/2016.
 */
public class AppMasterFormLogic implements IAppMasterForm {

    public static Model.Finder<Long, AppMasterForm> find = new Model.Finder<Long,AppMasterForm>(AppMasterForm.class);


    @Override
    @Transactional
    public void onSave(AppMasterForm appMasterForm) {
        appMasterForm.save();
    }

    @Override
    @Transactional
    public long onSave_Return_amf_autoindex(AppMasterForm appMasterForm) {
        appMasterForm.save();
        return appMasterForm.getAmf_autoindex();
    }

    @Override
    @Transactional
    public AppMasterForm onSave_Return_AppMasterForm(AppMasterForm appMasterForm) {
        appMasterForm.save();
        return onSelectBy_amf_autoindex(appMasterForm.getAmf_autoindex());
    }

    @Override
    @Transactional
    public void onUpdate(AppMasterForm appMasterForm) {
        appMasterForm.update();
    }

    @Override
    @Transactional
    public AppMasterForm onUpdate_Return_AppMasterForm(AppMasterForm appMasterForm) {
        appMasterForm.update();
        return onSelectBy_amf_autoindex(appMasterForm.getAmf_autoindex());
    }

    @Override
    @Transactional
    public void onDelete(AppMasterForm appMasterForm) {
        appMasterForm.delete();
    }

    @Override
    @Transactional
    public void onDeleteBy_amf_autoindex(long amf_autoindex) {
        AppMasterForm appMasterForm = find.where().eq("amf_autoindex", amf_autoindex).findUnique();
        if (appMasterForm != null) {
            appMasterForm.delete();
        }
    }

    @Override
    public AppMasterForm onSelectBy_amf_autoindex(long amf_autoindex) {
        AppMasterForm appMasterForm = find.where().eq("amf_autoindex", amf_autoindex).findUnique();
        if (appMasterForm != null) {
            return appMasterForm;
        }
        return null;
    }

    @Override
    public AppMasterForm onSelectBy_amf_appmaster(AppMaster amf_appmaster) {
        AppMasterForm appMasterForm = find.where().eq("amf_appmaster", amf_appmaster).findUnique();
        if (appMasterForm != null) {
            return appMasterForm;
        }
        return null;
    }

    @Override
    public AppMasterForm onSelectBy_amf_filenamescalahtml(String amf_filenamescalahtml) {
        AppMasterForm appMasterForm = find.where().eq("amf_filenamescalahtml", amf_filenamescalahtml).findUnique();
        if (appMasterForm != null) {
            return appMasterForm;
        }
        return null;
    }

    @Override
    public List<AppMasterForm> onSelectAll() {
        List<AppMasterForm> appMasterForms = find.findList();
        if (!appMasterForms.isEmpty()) {
            return appMasterForms;
        }
        return null;
    }

    @Override
    public List<AppMasterForm> onSelectAllBy_amf_appmaster(AppMaster amf_appmaster) {
        List<AppMasterForm> appMasterForms = find.where().eq("amf_appmaster", amf_appmaster).findList();
        if (!appMasterForms.isEmpty()) {
            return appMasterForms;
        }
        return null;
    }

    @Override
    public List<AppMasterForm> onSelectAllBy_amf_autoindex_notin_ampf_appmasterform_by_ampf_useraccount(long ampf_useraccount) {
        String querySql = "find app_master_form WHERE amf_autoindex NOT IN (SELECT ampf_appmasterform FROM app_master_permissions_form WHERE ampf_useraccount = :ampf_useraccount);";
        List<AppMasterForm> appMasterForms = find.setQuery(querySql).setParameter("ampf_useraccount", ampf_useraccount).findList();
        if (!appMasterForms.isEmpty()) {
            return appMasterForms;
        }
        return null;
    }
}
