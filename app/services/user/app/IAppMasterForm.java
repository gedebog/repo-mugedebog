package services.user.app;


import models.user.app.AppMaster;
import models.user.app.AppMasterForm;

import java.util.List;

/**
 * Created by tri.jaruto on 06/12/2016.
 */
public interface IAppMasterForm {

    //SAVE
    public void onSave(AppMasterForm appMasterForm);
    public long onSave_Return_amf_autoindex(AppMasterForm appMasterForm);
    public AppMasterForm onSave_Return_AppMasterForm(AppMasterForm appMasterForm);

    //UPDATE
    public void onUpdate(AppMasterForm appMasterForm);
    public AppMasterForm onUpdate_Return_AppMasterForm(AppMasterForm appMasterForm);

    //DELETE
    public void onDelete(AppMasterForm appMasterForm);
    public void onDeleteBy_amf_autoindex(long amf_autoindex);

    //SELECT
    public AppMasterForm onSelectBy_amf_autoindex(long amf_autoindex);
    public AppMasterForm onSelectBy_amf_appmaster(AppMaster amf_appmaster);
    public AppMasterForm onSelectBy_amf_filenamescalahtml(String amf_filenamescalahtml);

    public List<AppMasterForm> onSelectAll();
    public List<AppMasterForm> onSelectAllBy_amf_appmaster(AppMaster amf_appmaster);

    public List<AppMasterForm> onSelectAllBy_amf_autoindex_notin_ampf_appmasterform_by_ampf_useraccount(long ampf_useraccount);

}
