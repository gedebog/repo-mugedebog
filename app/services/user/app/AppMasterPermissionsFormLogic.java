package services.user.app;

import com.avaje.ebean.Model;
import com.avaje.ebean.PagedList;
import models.user.app.AppMasterForm;
import models.user.app.AppMasterPermissionsForm;
import models.user.user.UserAccount;
import play.db.ebean.Transactional;
import java.util.List;

/**
 * Created by tri.jaruto on 06/12/2016.
 */
public class AppMasterPermissionsFormLogic implements IAppMasterPermissionsForm {

    public static Model.Finder<Long, AppMasterPermissionsForm> find = new Model.Finder<Long,AppMasterPermissionsForm>(AppMasterPermissionsForm.class);

    @Override
    @Transactional
    public void onSave(AppMasterPermissionsForm appMasterPermissionsForm) {
        appMasterPermissionsForm.save();
    }

    @Override
    @Transactional
    public long onSave_Return_ampf_autoindex(AppMasterPermissionsForm appMasterPermissionsForm) {
        appMasterPermissionsForm.save();
        return appMasterPermissionsForm.getAmpf_autoindex();
    }

    @Override
    @Transactional
    public AppMasterPermissionsForm onSave_Return_AppMasterPermissionsForm(AppMasterPermissionsForm appMasterPermissionsForm) {
        appMasterPermissionsForm.save();
        return onSelectBy_ampf_autoindex(appMasterPermissionsForm.getAmpf_autoindex());
    }

    @Override
    @Transactional
    public void onUpdate(AppMasterPermissionsForm appMasterPermissionsForm) {
        appMasterPermissionsForm.update();
    }

    @Override
    @Transactional
    public AppMasterPermissionsForm onUpdate_Return_AppMasterPermissionsForm(AppMasterPermissionsForm appMasterPermissionsForm) {
        appMasterPermissionsForm.update();
        return onSelectBy_ampf_autoindex(appMasterPermissionsForm.getAmpf_autoindex());
    }

    @Override
    @Transactional
    public void onDelete(AppMasterPermissionsForm appMasterPermissionsForm) {
        appMasterPermissionsForm.delete();
    }

    @Override
    @Transactional
    public void onDeleteBy_ampf_autoindex(long ampf_autoindex) {
        AppMasterPermissionsForm appMasterPermissionsForm = find.where().eq("ampf_autoindex", ampf_autoindex).findUnique();
        if (appMasterPermissionsForm != null) {
            appMasterPermissionsForm.delete();
        }
    }

    @Override
    public int onRowCount() {
        return find.findRowCount();
    }

    @Override
    public int onRowCountBy_ampf_useraccount(UserAccount ampf_useraccount) {
        return find.where().eq("ampf_useraccount", ampf_useraccount).findRowCount();
    }

    @Override
    public int onRowCountByFilter(String filter) {
        return find.where().like("amfa_name", "%"+filter+"%").findRowCount();
    }

    @Override
    public int onRowCountBy_ampf_useraccount_and_filter(UserAccount ampf_useraccount, String filter) {
        return find.where().eq("ampf_useraccount", ampf_useraccount).like("amfa_name", "%"+filter+"%").findRowCount();
    }

    @Override
    public AppMasterPermissionsForm onSelectBy_ampf_autoindex(long ampf_autoindex) {
        AppMasterPermissionsForm appMasterPermissionsForm = find.where().eq("ampf_autoindex", ampf_autoindex).findUnique();
        if (appMasterPermissionsForm != null) {
            return appMasterPermissionsForm;
        }
        return null;
    }

    @Override
    public AppMasterPermissionsForm onSelectBy_ampf_useraccount_and_ampf_appmasterform(UserAccount ampf_useraccount, AppMasterForm ampf_appmasterform) {
        AppMasterPermissionsForm appMasterPermissionsForm = find.where().eq("ampf_useraccount", ampf_useraccount).eq("ampf_appmasterform", ampf_appmasterform).findUnique();
        if (appMasterPermissionsForm != null) {
            return appMasterPermissionsForm;
        }
        return null;
    }

    @Override
    public AppMasterPermissionsForm onSelectBy_ampf_useraccount_and_ampf_appmasterform_and_ampf_isactive(UserAccount ampf_useraccount, AppMasterForm ampf_appmasterform, boolean ampf_isactive) {
        AppMasterPermissionsForm appMasterPermissionsForm = find.where().eq("ampf_useraccount", ampf_useraccount).eq("ampf_appmasterform", ampf_appmasterform).eq("ampf_isactive", ampf_isactive).findUnique();
        if (appMasterPermissionsForm != null) {
            return appMasterPermissionsForm;
        }
        return null;
    }

    @Override
    public List<AppMasterPermissionsForm> onSelectAll() {
        List<AppMasterPermissionsForm> appMasterPermissionsForms = find.findList();
        if (!appMasterPermissionsForms.isEmpty()) {
            return appMasterPermissionsForms;
        }
        return null;
    }

    @Override
    public List<AppMasterPermissionsForm> onSelectAllBy_ampf_useraccount(UserAccount ampf_useraccount) {
        List<AppMasterPermissionsForm> appMasterPermissionsForms = find.where().eq("ampf_useraccount", ampf_useraccount).findList();
        if (!appMasterPermissionsForms.isEmpty()) {
            return appMasterPermissionsForms;
        }
        return null;
    }

    @Override
    public List<AppMasterPermissionsForm> onSelectAllBy_ampf_appmasterform(AppMasterForm ampf_appmasterform) {
        List<AppMasterPermissionsForm> appMasterPermissionsForms = find.where().eq("ampf_appmasterform", ampf_appmasterform).findList();
        if (!appMasterPermissionsForms.isEmpty()) {
            return appMasterPermissionsForms;
        }
        return null;
    }

    @Override
    public List<AppMasterPermissionsForm> onSelectAllInnerAppMasterFormBy_ampf_useraccount_and_pagination(UserAccount ampf_useraccount, int startrow, int endrow) {
        PagedList<AppMasterPermissionsForm> pagedList = find.
                where().eq("ampf_useraccount", ampf_useraccount).
                order().desc("ampf_autoindex").
                findPagedList(startrow, endrow);
        List<AppMasterPermissionsForm> appMasterPermissionsForms = pagedList.getList();
        if (!appMasterPermissionsForms.isEmpty()) {
            return appMasterPermissionsForms;
        }
        return null;
    }

    @Override
    public List<AppMasterPermissionsForm> onSelectAllInnerAppMasterFormBy_ampf_useraccount_and_filter_and_pagination(UserAccount ampf_useraccount, String filter, int startrow, int endrow) {
        PagedList<AppMasterPermissionsForm> pagedList = find
                .where().eq("ampf_useraccount", ampf_useraccount)
                .like("amfa_name", "%"+filter+"%")
                .order().desc("ampf_autoindex")
                .findPagedList(startrow, endrow);
        List<AppMasterPermissionsForm> appMasterPermissionsForms = pagedList.getList();
        if (!appMasterPermissionsForms.isEmpty()) {
            return appMasterPermissionsForms;
        }
        return null;
    }
}
