package services.user.app;

import com.avaje.ebean.Model;
import models.user.app.AppMaster;

import java.util.List;

/**
 * Created by tri.jaruto on 06/12/2016.
 */
public class AppMasterLogic implements IAppMaster {

    public static Model.Finder<Long, AppMaster> find = new Model.Finder<Long,AppMaster>(AppMaster.class);

    @Override
    public void onSave(AppMaster appMaster) {
        appMaster.save();
    }

    @Override
    public long onSave_Return_am_autoindex(AppMaster appMaster) {
        appMaster.save();
        return appMaster.getAm_autoindex();
    }

    @Override
    public AppMaster onSave_Return_AppMaster(AppMaster appMaster) {
        appMaster.save();
        return onSelectBy_am_autoindex(appMaster.getAm_autoindex());
    }

    @Override
    public void onUpdate(AppMaster appMaster) {
        appMaster.update();
    }

    @Override
    public AppMaster onUpdate_Return_AppMaster(AppMaster appMaster) {
        appMaster.update();
        return onSelectBy_am_autoindex(appMaster.getAm_autoindex());
    }

    @Override
    public void onDelete(AppMaster appMaster) {
        appMaster.delete();
    }

    @Override
    public void onDeleteBy_am_autoindex(long am_autoindex) {
        AppMaster appMaster = find.where().eq("am_autoindex", am_autoindex).findUnique();
        if (appMaster != null) {
            appMaster.delete();
        }
    }

    @Override
    public AppMaster onSelectBy_am_autoindex(long am_autoindex) {
        AppMaster appMaster = find.where().eq("am_autoindex", am_autoindex).findUnique();
        if (appMaster != null) {
            return appMaster;
        }
        return null;
    }

    @Override
    public List<AppMaster> onSelectAll() {
        List<AppMaster> appMasters = find.findList();
        if (!appMasters.isEmpty()) {
            return appMasters;
        }
        return null;
    }

    @Override
    public List<AppMaster> onSelectAllBy_am_type(long am_type) {
        List<AppMaster> appMasters = find.where().eq("am_type", am_type).orderBy("am_autoindex").findList();
        if (!appMasters.isEmpty()) {
            return appMasters;
        }
        return null;
    }

    @Override
    public List<AppMaster> onSelectAllBy_am_level(long am_level) {
        List<AppMaster> appMasters = find.where().eq("am_level", am_level).orderBy("am_autoindex").findList();
        if (!appMasters.isEmpty()) {
            return appMasters;
        }
        return null;
    }

    @Override
    public List<AppMaster> onSelectAllBy_am_type_and_am_level(long am_type, long am_level) {
        List<AppMaster> appMasters = find.where().eq("am_type", am_type).eq("am_level", am_level).orderBy("am_autoindex").findList();
        if (!appMasters.isEmpty()) {
            return appMasters;
        }
        return null;
    }

    /*public void onUpdate_By_ua_autoindex(long ua_autoindex, UserAccount userAccount) {
        System.out.println("onUpdate_By_ua_autoindex userAccount.ua_isactive : "+userAccount.isUa_isactive());
        System.out.println("onUpdate_By_ua_autoindex ua_username.ua_username : "+userAccount.getUa_username());
        String sql = " update " +
                     " user_account " +
                     " set " +
                     " ua_username = :ua_username " +
                     " where " +
                     " ua_autoindex = :ua_autoindex";
        SqlUpdate update = Ebean.createSqlUpdate(sql)
                .setParameter("ua_username", "123123213")
                *//*.setParameter("ua_userpassword", userAccount.ua_userpassword)
                .setParameter("ua_isactive", true)
                .setParameter("ua_activationcode", userAccount.ua_activationcode)
                .setParameter("ua_isforgotpassword", userAccount.ua_isforgotpassword)
                .setParameter("ua_create", userAccount.ua_create)
                .setParameter("ua_updated", userAccount.ua_updated)*//*
                .setParameter("ua_autoindex", userAccount.getUa_autoindex());
        System.out.println("onUpdate_By_ua_autoindex userAccount.ua_isactive : "+userAccount.isUa_isactive());
        int rows = update.execute();
        System.out.println("onUpdate_By_ua_autoindex userAccount.ua_isactive : "+userAccount.getUa_username());
    }*/



}
