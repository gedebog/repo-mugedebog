package services.user.app;

import models.user.app.AppMaster;
import models.user.app.AppMasterStructure;

import java.util.List;

/**
 * Created by tri.jaruto on 06/12/2016.
 */
public interface IAppMasterStructure {

    //SAVE
    public void onSave(AppMasterStructure appMasterStructure);
    public long onSave_Return_ams_autoindex(AppMasterStructure appMasterStructure);
    public AppMasterStructure onSave_Return_AppMasterStructure(AppMasterStructure appMasterStructure);

    //UPDATE
    public void onUpdate(AppMasterStructure appMasterStructure);
    public AppMasterStructure onUpdate_Return_AppMasterStructure(AppMasterStructure appMasterStructure);

    //DELETE
    public void onDelete(AppMasterStructure appMasterStructure);
    public void onDeleteBy_ams_autoindex(long ams_autoindex);

    //SELECT
    public AppMasterStructure onSelectBy_ams_autoindex(long ams_autoindex);
    public List<AppMasterStructure> onSelectAllBy_ams_appmaster_parent(AppMaster ams_appmaster_parent);
    public List<AppMasterStructure> onSelectAllBy_ams_appmaster_chilld(AppMaster ams_appmaster_child);

}
