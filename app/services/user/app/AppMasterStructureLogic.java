package services.user.app;

import com.avaje.ebean.Model;
import models.user.app.AppMaster;
import models.user.app.AppMasterStructure;

import java.util.List;

/**
 * Created by tri.jaruto on 06/12/2016.
 */
public class AppMasterStructureLogic implements IAppMasterStructure {

    public static Model.Finder<Long, AppMasterStructure> find = new Model.Finder<Long,AppMasterStructure>(AppMasterStructure.class);

    @Override
    public void onSave(AppMasterStructure appMasterStructure) {
        appMasterStructure.save();
    }

    @Override
    public long onSave_Return_ams_autoindex(AppMasterStructure appMasterStructure) {
        appMasterStructure.save();
        return appMasterStructure.getAms_autoindex();
    }

    @Override
    public AppMasterStructure onSave_Return_AppMasterStructure(AppMasterStructure appMasterStructure) {
        appMasterStructure.save();
        return onSelectBy_ams_autoindex(appMasterStructure.getAms_autoindex());
    }

    @Override
    public void onUpdate(AppMasterStructure appMasterStructure) {
        appMasterStructure.update();
    }

    @Override
    public AppMasterStructure onUpdate_Return_AppMasterStructure(AppMasterStructure appMasterStructure) {
        appMasterStructure.update();
        return onSelectBy_ams_autoindex(appMasterStructure.getAms_autoindex());
    }

    @Override
    public void onDelete(AppMasterStructure appMasterStructure) {
        appMasterStructure.delete();
    }

    @Override
    public void onDeleteBy_ams_autoindex(long ama_autoindex) {
        AppMasterStructure appMasterStructure = find.where().eq("ams_autoindex", ama_autoindex).findUnique();
        if (appMasterStructure != null) {
            appMasterStructure.delete();
        }
    }

    @Override
    public AppMasterStructure onSelectBy_ams_autoindex(long ama_autoindex) {
        AppMasterStructure appMasterStructure = find.where().eq("ams_autoindex", ama_autoindex).findUnique();
        if (appMasterStructure != null) {
            return appMasterStructure;
        }
        return null;
    }

    @Override
    public List<AppMasterStructure> onSelectAllBy_ams_appmaster_parent(AppMaster ams_appmaster_parent) {
        List<AppMasterStructure> appMasterStructures = find.where().eq("ams_appmaster_parent", ams_appmaster_parent).findList();
        if (!appMasterStructures.isEmpty()) {
            return appMasterStructures;
        }
        return null;
    }

    @Override
    public List<AppMasterStructure> onSelectAllBy_ams_appmaster_chilld(AppMaster ams_appmaster_child) {
        List<AppMasterStructure> appMasterStructures = find.where().eq("ams_appmaster_child", ams_appmaster_child).findList();
        if (!appMasterStructures.isEmpty()) {
            return appMasterStructures;
        }
        return null;
    }
}
