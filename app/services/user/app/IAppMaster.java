package services.user.app;


import models.user.app.AppMaster;
import java.util.List;

/**
 * Created by tri.jaruto on 06/12/2016.
 */
public interface IAppMaster {

    //SAVE
    public void onSave(AppMaster appMaster);
    public long onSave_Return_am_autoindex(AppMaster appMaster);
    public AppMaster onSave_Return_AppMaster(AppMaster appMaster);

    //UPDATE
    public void onUpdate(AppMaster appMaster);
    public AppMaster onUpdate_Return_AppMaster(AppMaster appMaster);

    //DELETE
    public void onDelete(AppMaster appMaster);
    public void onDeleteBy_am_autoindex(long am_autoindex);

    //SELECT
    public AppMaster onSelectBy_am_autoindex(long am_autoindex);
    public List<AppMaster> onSelectAll();
    public List<AppMaster> onSelectAllBy_am_type(long am_type);
    public List<AppMaster> onSelectAllBy_am_level(long am_level);
    public List<AppMaster> onSelectAllBy_am_type_and_am_level(long am_type, long am_level);

}
