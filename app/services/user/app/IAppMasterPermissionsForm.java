package services.user.app;


import models.user.app.AppMasterForm;
import models.user.app.AppMasterPermissionsForm;
import models.user.user.UserAccount;

import java.util.List;

/**
 * Created by tri.jaruto on 06/12/2016.
 */
public interface IAppMasterPermissionsForm {

    //SAVE
    public void onSave(AppMasterPermissionsForm appMasterPermissionsForm);
    public long onSave_Return_ampf_autoindex(AppMasterPermissionsForm appMasterPermissionsForm);
    public AppMasterPermissionsForm onSave_Return_AppMasterPermissionsForm(AppMasterPermissionsForm appMasterPermissionsForm);

    //UPDATE
    public void onUpdate(AppMasterPermissionsForm appMasterPermissionsForm);
    public AppMasterPermissionsForm onUpdate_Return_AppMasterPermissionsForm(AppMasterPermissionsForm appMasterPermissionsForm);

    //DELETE
    public void onDelete(AppMasterPermissionsForm appMasterPermissionsForm);
    public void onDeleteBy_ampf_autoindex(long amfa_autoindex);

    //SELECT
    public int onRowCount();
    public int onRowCountBy_ampf_useraccount(UserAccount ampf_useraccount);
    public int onRowCountByFilter(String filter);
    public int onRowCountBy_ampf_useraccount_and_filter(UserAccount ampf_useraccount, String filter);

    public AppMasterPermissionsForm onSelectBy_ampf_autoindex(long ampf_autoindex);
    public AppMasterPermissionsForm onSelectBy_ampf_useraccount_and_ampf_appmasterform(UserAccount ampf_useraccount, AppMasterForm ampf_appmasterform);
    public AppMasterPermissionsForm onSelectBy_ampf_useraccount_and_ampf_appmasterform_and_ampf_isactive(UserAccount ampf_useraccount, AppMasterForm ampf_appmasterform, boolean ampf_isactive);
    public List<AppMasterPermissionsForm> onSelectAll();
    public List<AppMasterPermissionsForm> onSelectAllBy_ampf_useraccount(UserAccount ampf_useraccount);
    public List<AppMasterPermissionsForm> onSelectAllBy_ampf_appmasterform(AppMasterForm ampf_appmasterform);

    public List<AppMasterPermissionsForm> onSelectAllInnerAppMasterFormBy_ampf_useraccount_and_pagination(UserAccount ampf_useraccount, int startrow, int endrow);
    public List<AppMasterPermissionsForm> onSelectAllInnerAppMasterFormBy_ampf_useraccount_and_filter_and_pagination(UserAccount ampf_useraccount, String filter, int startrow, int endrow);

}
