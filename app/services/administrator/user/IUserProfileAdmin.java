package services.administrator.user;

import models.administrator.user.UserProfileAdmin;


/**
 * Created by tri.jaruto on 06/12/2016.
 */
public interface IUserProfileAdmin {


    //SAVE
    public void onSave(UserProfileAdmin userProfileAdmin);

    //UPDATE
    public void onUpdate(UserProfileAdmin userProfileAdmin);

    //DELETE

    //SELECT
    public UserProfileAdmin onSelectBy_upa_autoindex(long upa_autoindex);
    public UserProfileAdmin onSelectBy_upa_useraccountadmin(long upa_useraccountadmin);


}
