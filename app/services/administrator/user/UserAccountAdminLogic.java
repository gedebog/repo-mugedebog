package services.administrator.user;

import com.avaje.ebean.Model;
import com.avaje.ebean.PagedList;
import controllers.util.BCrypto;
import models.administrator.user.UserAccountAdmin;
import play.db.ebean.Transactional;

import java.util.List;

/**
 * Created by tri.jaruto on 06/12/2016.
 */
public class UserAccountAdminLogic implements IUserAccountAdmin {

    public static Model.Finder<Long, UserAccountAdmin> find = new Model.Finder<Long,UserAccountAdmin>(UserAccountAdmin.class);

    //SAVE
    @Override
    @Transactional
    public void onSave(UserAccountAdmin userAccountAdmin) {
        userAccountAdmin.save();
    }

    @Override
    @Transactional
    public long onSave_Return_uaa_autoindex(UserAccountAdmin userAccountAdmin) {
        userAccountAdmin.save();
        return userAccountAdmin.getUaa_autoindex();
    }

    @Override
    @Transactional
    public UserAccountAdmin onSave_Return_UserAccountAdmin(UserAccountAdmin userAccountAdmin) {
        userAccountAdmin.save();
        return onSelectBy_uaa_autoindex(userAccountAdmin.getUaa_autoindex());
    }

    //UPDATE
    @Override
    @Transactional
    public void onUpdate(UserAccountAdmin userAccountAdmin) {
        userAccountAdmin.update();
    }

    @Override
    public UserAccountAdmin onUpdate_Return_UserAccountAdmin(UserAccountAdmin userAccountAdmin) {
        userAccountAdmin.update();
        return onSelectBy_uaa_autoindex(userAccountAdmin.getUaa_autoindex());
    }

    @Override
    public void onDelete(UserAccountAdmin userAccountAdmin) {
        userAccountAdmin.delete();
    }

    //DELETE

    //SELECT
    @Override
    public int onRowCount(){
        return find.findRowCount();
    }

    @Override
    public int onRowCountByFilter(String filter) {
        return find.where().like("uaa_username", "%"+filter+"%").findRowCount();
    }

    @Override
    public UserAccountAdmin onSelectBy_uaa_autoindex(long uaa_autoindex) {
        UserAccountAdmin userAccountAdmin = find.where().eq("uaa_autoindex", uaa_autoindex).findUnique();
        if (userAccountAdmin != null) {
            return userAccountAdmin;
        }
        return null;
    }

    @Override
    public UserAccountAdmin onSelectBy_uaa_username(String uaa_username) {
        UserAccountAdmin userAccountAdmin = find.where().eq("uaa_username", uaa_username).findUnique();
        if (userAccountAdmin != null) {
                return userAccountAdmin;
        }
        return null;
    }

    @Override
    public boolean onValidateBy_uaa_username_and_uaa_password(String uaa_username, String uaa_userpassword) {
        boolean isValid = false;
        UserAccountAdmin userAccountAdmin = find.where().eq("uaa_username", uaa_username).findUnique();
        if(userAccountAdmin!=null){
            isValid = BCrypto.checkPassword(uaa_userpassword, userAccountAdmin.getUaa_userpassword());
        }
        return isValid;
    }

    @Override
    public boolean onValidateBy_uaa_username_and_uaa_activationcode(String uaa_username, String uaa_activationcode) {
        boolean isValid = false;
        UserAccountAdmin userAccountAdmin = find.where().eq("uaa_username", uaa_username).findUnique();
        if(userAccountAdmin!=null){
            isValid = BCrypto.checkPassword(uaa_username, userAccountAdmin.getUaa_activationcode());
        }
        return isValid;
    }

    @Override
    public boolean onCheckedBy_uaa_username(String uaa_username) {
        boolean isChecked = false;
        UserAccountAdmin userAccountAdmin = find.where().eq("uaa_username", uaa_username).findUnique();
        if(userAccountAdmin!=null){
            isChecked = true;
        }
        return isChecked;
    }

    @Override
    public List<UserAccountAdmin> onSelectAll() {
        List<UserAccountAdmin> userAccountAdmins = find.findList();
        if (!userAccountAdmins.isEmpty()) {
            return userAccountAdmins;
        }
        return null;
    }

    @Override
    public List<UserAccountAdmin> onSelectAllBy_Pagination(int startrow, int endrow) {
        PagedList<UserAccountAdmin> pagedList = find.order().desc("uaa_autoindex").findPagedList(startrow, endrow);
        List<UserAccountAdmin> userAccountAdmins = pagedList.getList();
        if (!userAccountAdmins.isEmpty()) {
            return userAccountAdmins;
        }
        return null;
    }

    @Override
    public List<UserAccountAdmin> onSelectAllBy_Filter_and_Pagination(String filter, int startrow, int endrow) {
        PagedList<UserAccountAdmin> pagedList = find.where().like("uaa_username", "%"+filter+"%").order().desc("uaa_autoindex").findPagedList(startrow, endrow);
        List<UserAccountAdmin> userAccountAdmins = pagedList.getList();
        if (!userAccountAdmins.isEmpty()) {
            return userAccountAdmins;
        }
        return null;
    }


}
