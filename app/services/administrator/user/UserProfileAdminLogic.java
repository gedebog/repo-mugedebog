package services.administrator.user;

import com.avaje.ebean.Model;
import models.administrator.user.UserProfileAdmin;
import play.db.ebean.Transactional;

/**
 * Created by tri.jaruto on 06/12/2016.
 */
public class UserProfileAdminLogic implements IUserProfileAdmin{

    public static Model.Finder<Long, UserProfileAdmin> find = new Model.Finder<Long,UserProfileAdmin>(UserProfileAdmin.class);

    @Override
    @Transactional
    public void onSave(UserProfileAdmin userProfileAdmin) {
        userProfileAdmin.save();
    }

    @Override
    @Transactional
    public void onUpdate(UserProfileAdmin userProfileAdmin) {
        userProfileAdmin.update();
    }

    @Override
    public UserProfileAdmin onSelectBy_upa_autoindex(long upa_autoindex) {
        UserProfileAdmin userProfileAdmin = find.where().eq("upa_autoindex", upa_autoindex).findUnique();
        if (userProfileAdmin != null) {
            return userProfileAdmin;
        }
        return null;
    }

    @Override
    public UserProfileAdmin onSelectBy_upa_useraccountadmin(long upa_useraccountadmin) {
        UserProfileAdmin userProfileAdmin = find.where().eq("upa_useraccountadmin", upa_useraccountadmin).findUnique();
        if (userProfileAdmin != null) {
            return userProfileAdmin;
        }
        return null;
    }
}
