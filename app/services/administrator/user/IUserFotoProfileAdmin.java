package services.administrator.user;

import models.administrator.user.UserAccountAdmin;
import models.administrator.user.UserFotoProfileAdmin;
import models.administrator.user.UserProfileAdmin;


/**
 * Created by tri.jaruto on 06/12/2016.
 */
public interface IUserFotoProfileAdmin {


    //SAVE
    public void onSave(UserFotoProfileAdmin userFotoProfileAdmin);

    //UPDATE
    public void onUpdate(UserFotoProfileAdmin userFotoProfileAdmin);

    //DELETE

    //SELECT
    public UserFotoProfileAdmin onSelectBy_ufpa_autoindex(long ufpa_autoindex);
    public UserFotoProfileAdmin onSelectBy_ufpa_useraccountadmin(UserAccountAdmin userAccountAdmin);


}
