package services.administrator.user;

import models.administrator.user.UserAccountAdmin;

import java.util.List;


/**
 * Created by tri.jaruto on 06/12/2016.
 */
public interface IUserAccountAdmin {

    //SAVE
    public void onSave(UserAccountAdmin userAccountAdmin);
    public long onSave_Return_uaa_autoindex(UserAccountAdmin userAccountAdmin);
    public UserAccountAdmin onSave_Return_UserAccountAdmin(UserAccountAdmin userAccountAdmin);

    //UPDATE
    public void onUpdate(UserAccountAdmin userAccountAdmin);
    public UserAccountAdmin onUpdate_Return_UserAccountAdmin(UserAccountAdmin userAccountAdmin);

    //DELETE
    public void onDelete(UserAccountAdmin userAccountAdmin);

    //SELECT
    public int onRowCount();
    public int onRowCountByFilter(String filter);

    public UserAccountAdmin onSelectBy_uaa_autoindex(long uaa_autoindex);
    public UserAccountAdmin onSelectBy_uaa_username(String uaa_username);
    public boolean onValidateBy_uaa_username_and_uaa_password(String uaa_username, String uaa_password);
    public boolean onValidateBy_uaa_username_and_uaa_activationcode(String uaa_username, String uaa_activationcode);
    public boolean onCheckedBy_uaa_username(String uaa_username);

    public List<UserAccountAdmin> onSelectAll();
    public List<UserAccountAdmin> onSelectAllBy_Pagination(int startrow, int endrow);
    public List<UserAccountAdmin> onSelectAllBy_Filter_and_Pagination(String filter, int startrow, int endrow);

}
