package services.administrator.user;

import com.avaje.ebean.Model;
import models.administrator.user.UserAccountAdmin;
import models.administrator.user.UserFotoProfileAdmin;
import models.administrator.user.UserProfileAdmin;
import play.db.ebean.Transactional;

/**
 * Created by tri.jaruto on 06/12/2016.
 */
public class UserFotoProfileAdminLogic implements IUserFotoProfileAdmin{

    public static Model.Finder<Long, UserFotoProfileAdmin> find = new Model.Finder<Long,UserFotoProfileAdmin>(UserFotoProfileAdmin.class);

    @Override
    @Transactional
    public void onSave(UserFotoProfileAdmin userFotoProfileAdmin) {
        userFotoProfileAdmin.save();
    }

    @Override
    @Transactional
    public void onUpdate(UserFotoProfileAdmin userFotoProfileAdmin) {
        userFotoProfileAdmin.update();
    }

    @Override
    public UserFotoProfileAdmin onSelectBy_ufpa_autoindex(long ufpa_autoindex) {
        UserFotoProfileAdmin userFotoProfileAdmin = find.where().eq("ufpa_autoindex", ufpa_autoindex).findUnique();
        if (userFotoProfileAdmin != null) {
            return userFotoProfileAdmin;
        }
        return null;
    }

    @Override
    public UserFotoProfileAdmin onSelectBy_ufpa_useraccountadmin(UserAccountAdmin userAccountAdmin) {
        UserFotoProfileAdmin userFotoProfileAdmin = find.where().eq("ufpa_useraccountadmin", userAccountAdmin).findUnique();
        if (userFotoProfileAdmin != null) {
            return userFotoProfileAdmin;
        }
        return null;
    }
}
