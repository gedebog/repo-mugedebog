package services.administrator.app;

import com.avaje.ebean.Model;
import models.administrator.app.AppMasterAdmin;
import models.administrator.app.AppMasterFormAdmin;
import models.administrator.user.UserAccountAdmin;
import play.db.ebean.Transactional;

import java.util.List;

/**
 * Created by tri.jaruto on 06/12/2016.
 */
public class AppMasterFormAdminLogic implements IAppMasterFormAdmin {

    public static Model.Finder<Long, AppMasterFormAdmin> find = new Model.Finder<Long,AppMasterFormAdmin>(AppMasterFormAdmin.class);


    @Override
    @Transactional
    public void onSave(AppMasterFormAdmin appMasterFormAdmin) {
        appMasterFormAdmin.save();
    }

    @Override
    @Transactional
    public long onSave_Return_amfa_autoindex(AppMasterFormAdmin appMasterFormAdmin) {
        appMasterFormAdmin.save();
        return appMasterFormAdmin.getAmfa_autoindex();
    }

    @Override
    @Transactional
    public AppMasterFormAdmin onSave_Return_AppMasterFormAdmin(AppMasterFormAdmin appMasterFormAdmin) {
        appMasterFormAdmin.save();
        return onSelectBy_amfa_autoindex(appMasterFormAdmin.getAmfa_autoindex());
    }

    @Override
    @Transactional
    public void onUpdate(AppMasterFormAdmin appMasterFormAdmin) {
        appMasterFormAdmin.update();
    }

    @Override
    @Transactional
    public AppMasterFormAdmin onUpdate_Return_AppMasterFormAdmin(AppMasterFormAdmin appMasterFormAdmin) {
        appMasterFormAdmin.update();
        return onSelectBy_amfa_autoindex(appMasterFormAdmin.getAmfa_autoindex());
    }

    @Override
    @Transactional
    public void onDelete(AppMasterFormAdmin appMasterFormAdmin) {
        appMasterFormAdmin.delete();
    }

    @Override
    @Transactional
    public void onDeleteBy_amfa_autoindex(long amfa_autoindex) {
        AppMasterFormAdmin appMasterFormAdmin = find.where().eq("amfa_autoindex", amfa_autoindex).findUnique();
        if (appMasterFormAdmin != null) {
            appMasterFormAdmin.delete();
        }
    }

    @Override
    public AppMasterFormAdmin onSelectBy_amfa_autoindex(long amfa_autoindex) {
        AppMasterFormAdmin appMasterFormAdmin = find.where().eq("amfa_autoindex", amfa_autoindex).findUnique();
        if (appMasterFormAdmin != null) {
            return appMasterFormAdmin;
        }
        return null;
    }

    @Override
    public AppMasterFormAdmin onSelectBy_amfa_appmasteradmin(AppMasterAdmin amfa_appmasteradmin) {
        AppMasterFormAdmin appMasterFormAdmin = find.where().eq("amfa_appmasteradmin", amfa_appmasteradmin).findUnique();
        if (appMasterFormAdmin != null) {
            return appMasterFormAdmin;
        }
        return null;
    }

    @Override
    public AppMasterFormAdmin onSelectBy_amfa_filenamescalahtml(String amfa_filenamescalahtml) {
        AppMasterFormAdmin appMasterFormAdmin = find.where().eq("amfa_filenamescalahtml", amfa_filenamescalahtml).findUnique();
        if (appMasterFormAdmin != null) {
            return appMasterFormAdmin;
        }
        return null;
    }

    @Override
    public List<AppMasterFormAdmin> onSelectAll() {
        List<AppMasterFormAdmin> appMasterFormAdmins = find.findList();
        if (!appMasterFormAdmins.isEmpty()) {
            return appMasterFormAdmins;
        }
        return null;
    }

    @Override
    public List<AppMasterFormAdmin> onSelectAllBy_amfa_appmasteradmin(AppMasterAdmin amfa_appmasteradmin) {
        List<AppMasterFormAdmin> appMasterFormAdmins = find.where().eq("amfa_appmasteradmin", amfa_appmasteradmin).findList();
        if (!appMasterFormAdmins.isEmpty()) {
            return appMasterFormAdmins;
        }
        return null;
    }

    @Override
    public List<AppMasterFormAdmin> onSelectAllBy_amfa_autoindex_notin_ampfa_appmasterformadmin_by_ampfa_useraccountadmin(long ampfa_useraccountadmin) {
        System.out.println("ampfa_useraccountadmin : "+ampfa_useraccountadmin);
        String querySql = "find app_master_form_admin WHERE amfa_autoindex NOT IN (SELECT ampfa_appmasterformadmin FROM app_master_permissions_form_admin WHERE ampfa_useraccountadmin = :ampfa_useraccountadmin);";
        List<AppMasterFormAdmin> appMasterFormAdmins = find.setQuery(querySql).setParameter("ampfa_useraccountadmin", ampfa_useraccountadmin).findList();
        if (!appMasterFormAdmins.isEmpty()) {
            return appMasterFormAdmins;
        }
        return null;
    }
}
