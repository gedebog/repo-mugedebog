package services.administrator.app;

import models.administrator.app.AppMasterAdmin;
import models.administrator.app.AppMasterFormAdmin;
import models.administrator.app.AppMasterPermissionsFormAdmin;
import models.administrator.user.UserAccountAdmin;

import java.util.List;

/**
 * Created by tri.jaruto on 06/12/2016.
 */
public interface IAppMasterPermissionsFormAdmin {

    //SAVE
    public void onSave(AppMasterPermissionsFormAdmin appMasterPermissionsFormAdmin);
    public long onSave_Return_ampfa_autoindex(AppMasterPermissionsFormAdmin appMasterPermissionsFormAdmin);
    public AppMasterPermissionsFormAdmin onSave_Return_AppMasterPermissionsFormAdmin(AppMasterPermissionsFormAdmin appMasterPermissionsFormAdmin);

    //UPDATE
    public void onUpdate(AppMasterPermissionsFormAdmin appMasterPermissionsFormAdmin);
    public AppMasterPermissionsFormAdmin onUpdate_Return_AppMasterPermissionsFormAdmin(AppMasterPermissionsFormAdmin appMasterPermissionsFormAdmin);

    //DELETE
    public void onDelete(AppMasterPermissionsFormAdmin appMasterPermissionsFormAdmin);
    public void onDeleteBy_ampfa_autoindex(long amfa_autoindex);

    //SELECT
    public int onRowCount();
    public int onRowCountBy_ampfa_useraccountadmin(UserAccountAdmin ampfa_useraccountadmin);
    public int onRowCountByFilter(String filter);
    public int onRowCountBy_ampfa_useraccountadmin_and_filter(UserAccountAdmin ampfa_useraccountadmin, String filter);

    public AppMasterPermissionsFormAdmin onSelectBy_ampfa_autoindex(long ampfa_autoindex);
    public AppMasterPermissionsFormAdmin onSelectBy_ampfa_useraccountadmin_and_ampfa_appmasterformadmin(UserAccountAdmin ampfa_useraccountadmin, AppMasterFormAdmin ampfa_appmasterformadmin);
    public AppMasterPermissionsFormAdmin onSelectBy_ampfa_useraccountadmin_and_ampfa_appmasterformadmin_and_ampfa_isactive(UserAccountAdmin ampfa_useraccountadmin, AppMasterFormAdmin ampfa_appmasterformadmin, boolean ampfa_isactive);
    public List<AppMasterPermissionsFormAdmin> onSelectAll();
    public List<AppMasterPermissionsFormAdmin> onSelectAllBy_ampfa_useraccountadmin(UserAccountAdmin ampfa_useraccountadmin);
    public List<AppMasterPermissionsFormAdmin> onSelectAllBy_ampfa_appmasterformadmin(AppMasterFormAdmin ampfa_appmasterformadmin);

    public List<AppMasterPermissionsFormAdmin> onSelectAllInnerAppMasterFormAdminBy_ampfa_useraccountadmin_and_pagination(UserAccountAdmin ampfa_useraccountadmin, int startrow, int endrow);
    public List<AppMasterPermissionsFormAdmin> onSelectAllInnerAppMasterFormAdminBy_ampfa_useraccountadmin_and_filter_and_pagination(UserAccountAdmin ampfa_useraccountadmin, String filter, int startrow, int endrow);

}
