package services.administrator.app;

import models.administrator.app.AppMasterAdmin;
import models.administrator.app.AppMasterFormAdmin;
import models.administrator.user.UserAccountAdmin;

import java.util.List;

/**
 * Created by tri.jaruto on 06/12/2016.
 */
public interface IAppMasterFormAdmin {

    //SAVE
    public void onSave(AppMasterFormAdmin appMasterFormAdmin);
    public long onSave_Return_amfa_autoindex(AppMasterFormAdmin appMasterFormAdmin);
    public AppMasterFormAdmin onSave_Return_AppMasterFormAdmin(AppMasterFormAdmin appMasterFormAdmin);

    //UPDATE
    public void onUpdate(AppMasterFormAdmin appMasterFormAdmin);
    public AppMasterFormAdmin onUpdate_Return_AppMasterFormAdmin(AppMasterFormAdmin appMasterFormAdmin);

    //DELETE
    public void onDelete(AppMasterFormAdmin appMasterFormAdmin);
    public void onDeleteBy_amfa_autoindex(long amfa_autoindex);

    //SELECT
    public AppMasterFormAdmin onSelectBy_amfa_autoindex(long amfa_autoindex);
    public AppMasterFormAdmin onSelectBy_amfa_appmasteradmin(AppMasterAdmin amfa_appmasteradmin);
    public AppMasterFormAdmin onSelectBy_amfa_filenamescalahtml(String amfa_filenamescalahtml);

    public List<AppMasterFormAdmin> onSelectAll();
    public List<AppMasterFormAdmin> onSelectAllBy_amfa_appmasteradmin(AppMasterAdmin amfa_appmasteradmin);

    public List<AppMasterFormAdmin> onSelectAllBy_amfa_autoindex_notin_ampfa_appmasterformadmin_by_ampfa_useraccountadmin(long ampfa_useraccountadmin);

}
