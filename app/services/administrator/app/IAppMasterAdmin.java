package services.administrator.app;

import models.administrator.app.AppMasterAdmin;

import java.util.List;

/**
 * Created by tri.jaruto on 06/12/2016.
 */
public interface IAppMasterAdmin {

    //SAVE
    public void onSave(AppMasterAdmin appMasterAdmin);
    public long onSave_Return_ama_autoindex(AppMasterAdmin appMasterAdmin);
    public AppMasterAdmin onSave_Return_AppMasterAdmin(AppMasterAdmin appMasterAdmin);

    //UPDATE
    public void onUpdate(AppMasterAdmin appMasterAdmin);
    public AppMasterAdmin onUpdate_Return_AppMasterAdmin(AppMasterAdmin appMasterAdmin);

    //DELETE
    public void onDelete(AppMasterAdmin appMasterAdmin);
    public void onDeleteBy_ama_autoindex(long ama_autoindex);

    //SELECT
    public AppMasterAdmin onSelectBy_ama_autoindex(long ama_autoindex);
    public List<AppMasterAdmin> onSelectAll();
    public List<AppMasterAdmin> onSelectAllBy_ama_type(long ama_type);
    public List<AppMasterAdmin> onSelectAllBy_ama_level(long ama_level);
    public List<AppMasterAdmin> onSelectAllBy_ama_type_and_ama_level(long ama_type, long ama_level);

}
