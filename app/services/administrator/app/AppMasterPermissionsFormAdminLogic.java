package services.administrator.app;

import com.avaje.ebean.Model;
import com.avaje.ebean.PagedList;
import models.administrator.app.AppMasterAdmin;
import models.administrator.app.AppMasterFormAdmin;
import models.administrator.app.AppMasterPermissionsFormAdmin;
import models.administrator.user.UserAccountAdmin;
import play.db.ebean.Transactional;

import java.util.List;

/**
 * Created by tri.jaruto on 06/12/2016.
 */
public class AppMasterPermissionsFormAdminLogic implements IAppMasterPermissionsFormAdmin {

    public static Model.Finder<Long, AppMasterPermissionsFormAdmin> find = new Model.Finder<Long,AppMasterPermissionsFormAdmin>(AppMasterPermissionsFormAdmin.class);

    @Override
    @Transactional
    public void onSave(AppMasterPermissionsFormAdmin appMasterPermissionsFormAdmin) {
        appMasterPermissionsFormAdmin.save();
    }

    @Override
    @Transactional
    public long onSave_Return_ampfa_autoindex(AppMasterPermissionsFormAdmin appMasterPermissionsFormAdmin) {
        appMasterPermissionsFormAdmin.save();
        return appMasterPermissionsFormAdmin.getAmpfa_autoindex();
    }

    @Override
    @Transactional
    public AppMasterPermissionsFormAdmin onSave_Return_AppMasterPermissionsFormAdmin(AppMasterPermissionsFormAdmin appMasterPermissionsFormAdmin) {
        appMasterPermissionsFormAdmin.save();
        return onSelectBy_ampfa_autoindex(appMasterPermissionsFormAdmin.getAmpfa_autoindex());
    }

    @Override
    @Transactional
    public void onUpdate(AppMasterPermissionsFormAdmin appMasterPermissionsFormAdmin) {
        appMasterPermissionsFormAdmin.update();
    }

    @Override
    @Transactional
    public AppMasterPermissionsFormAdmin onUpdate_Return_AppMasterPermissionsFormAdmin(AppMasterPermissionsFormAdmin appMasterPermissionsFormAdmin) {
        appMasterPermissionsFormAdmin.update();
        return onSelectBy_ampfa_autoindex(appMasterPermissionsFormAdmin.getAmpfa_autoindex());
    }

    @Override
    @Transactional
    public void onDelete(AppMasterPermissionsFormAdmin appMasterPermissionsFormAdmin) {
        appMasterPermissionsFormAdmin.delete();
    }

    @Override
    @Transactional
    public void onDeleteBy_ampfa_autoindex(long ampfa_autoindex) {
        AppMasterPermissionsFormAdmin appMasterPermissionsFormAdmin = find.where().eq("ampfa_autoindex", ampfa_autoindex).findUnique();
        if (appMasterPermissionsFormAdmin != null) {
            appMasterPermissionsFormAdmin.delete();
        }
    }

    @Override
    public int onRowCount() {
        return find.findRowCount();
    }

    @Override
    public int onRowCountBy_ampfa_useraccountadmin(UserAccountAdmin ampfa_useraccountadmin) {
        return find.where().eq("ampfa_useraccountadmin", ampfa_useraccountadmin).findRowCount();
    }

    @Override
    public int onRowCountByFilter(String filter) {
        return find.where().like("amfa_name", "%"+filter+"%").findRowCount();
    }

    @Override
    public int onRowCountBy_ampfa_useraccountadmin_and_filter(UserAccountAdmin ampfa_useraccountadmin, String filter) {
        return find.where().eq("ampfa_useraccountadmin", ampfa_useraccountadmin).like("amfa_name", "%"+filter+"%").findRowCount();
    }

    @Override
    public AppMasterPermissionsFormAdmin onSelectBy_ampfa_autoindex(long ampfa_autoindex) {
        AppMasterPermissionsFormAdmin appMasterPermissionsFormAdmin = find.where().eq("ampfa_autoindex", ampfa_autoindex).findUnique();
        if (appMasterPermissionsFormAdmin != null) {
            return appMasterPermissionsFormAdmin;
        }
        return null;
    }

    @Override
    public AppMasterPermissionsFormAdmin onSelectBy_ampfa_useraccountadmin_and_ampfa_appmasterformadmin(UserAccountAdmin ampfa_useraccountadmin, AppMasterFormAdmin ampfa_appmasterformadmin) {
        AppMasterPermissionsFormAdmin appMasterPermissionsFormAdmin = find.where().eq("ampfa_useraccountadmin", ampfa_useraccountadmin).eq("ampfa_appmasterformadmin", ampfa_appmasterformadmin).findUnique();
        if (appMasterPermissionsFormAdmin != null) {
            return appMasterPermissionsFormAdmin;
        }
        return null;
    }

    @Override
    public AppMasterPermissionsFormAdmin onSelectBy_ampfa_useraccountadmin_and_ampfa_appmasterformadmin_and_ampfa_isactive(UserAccountAdmin ampfa_useraccountadmin, AppMasterFormAdmin ampfa_appmasterformadmin, boolean ampfa_isactive) {
        AppMasterPermissionsFormAdmin appMasterPermissionsFormAdmin = find.where().eq("ampfa_useraccountadmin", ampfa_useraccountadmin).eq("ampfa_appmasterformadmin", ampfa_appmasterformadmin).eq("ampfa_isactive", ampfa_isactive).findUnique();
        if (appMasterPermissionsFormAdmin != null) {
            return appMasterPermissionsFormAdmin;
        }
        return null;
    }

    @Override
    public List<AppMasterPermissionsFormAdmin> onSelectAll() {
        List<AppMasterPermissionsFormAdmin> appMasterPermissionsFormAdmins = find.findList();
        if (!appMasterPermissionsFormAdmins.isEmpty()) {
            return appMasterPermissionsFormAdmins;
        }
        return null;
    }

    @Override
    public List<AppMasterPermissionsFormAdmin> onSelectAllBy_ampfa_useraccountadmin(UserAccountAdmin ampfa_useraccountadmin) {
        List<AppMasterPermissionsFormAdmin> appMasterPermissionsFormAdmins = find.where().eq("ampfa_useraccountadmin", ampfa_useraccountadmin).findList();
        if (!appMasterPermissionsFormAdmins.isEmpty()) {
            return appMasterPermissionsFormAdmins;
        }
        return null;
    }

    @Override
    public List<AppMasterPermissionsFormAdmin> onSelectAllBy_ampfa_appmasterformadmin(AppMasterFormAdmin ampfa_appmasterformadmin) {
        List<AppMasterPermissionsFormAdmin> appMasterPermissionsFormAdmins = find.where().eq("ampfa_appmasterformadmin", ampfa_appmasterformadmin).findList();
        if (!appMasterPermissionsFormAdmins.isEmpty()) {
            return appMasterPermissionsFormAdmins;
        }
        return null;
    }

    @Override
    public List<AppMasterPermissionsFormAdmin> onSelectAllInnerAppMasterFormAdminBy_ampfa_useraccountadmin_and_pagination(UserAccountAdmin ampfa_useraccountadmin, int startrow, int endrow) {
        PagedList<AppMasterPermissionsFormAdmin> pagedList = find.
                where().eq("ampfa_useraccountadmin", ampfa_useraccountadmin).
                order().desc("ampfa_autoindex").
                findPagedList(startrow, endrow);
        List<AppMasterPermissionsFormAdmin> appMasterPermissionsFormAdmins = pagedList.getList();
        if (!appMasterPermissionsFormAdmins.isEmpty()) {
            return appMasterPermissionsFormAdmins;
        }
        return null;
    }

    @Override
    public List<AppMasterPermissionsFormAdmin> onSelectAllInnerAppMasterFormAdminBy_ampfa_useraccountadmin_and_filter_and_pagination(UserAccountAdmin ampfa_useraccountadmin, String filter, int startrow, int endrow) {
        PagedList<AppMasterPermissionsFormAdmin> pagedList = find
                .where().eq("ampfa_useraccountadmin", ampfa_useraccountadmin)
                .like("amfa_name", "%"+filter+"%")
                .order().desc("ampfa_autoindex")
                .findPagedList(startrow, endrow);
        List<AppMasterPermissionsFormAdmin> appMasterPermissionsFormAdmins = pagedList.getList();
        if (!appMasterPermissionsFormAdmins.isEmpty()) {
            return appMasterPermissionsFormAdmins;
        }
        return null;
    }
}
