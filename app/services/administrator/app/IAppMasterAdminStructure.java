package services.administrator.app;

import models.administrator.app.AppMasterAdmin;
import models.administrator.app.AppMasterAdminStructure;

import java.util.List;

/**
 * Created by tri.jaruto on 06/12/2016.
 */
public interface IAppMasterAdminStructure {

    //SAVE
    public void onSave(AppMasterAdminStructure appMasterAdminStructure);
    public long onSave_Return_amas_autoindex(AppMasterAdminStructure appMasterAdminStructure);
    public AppMasterAdminStructure onSave_Return_AppMasterAdminStructure(AppMasterAdminStructure appMasterAdminStructure);

    //UPDATE
    public void onUpdate(AppMasterAdminStructure appMasterAdminStructure);
    public AppMasterAdminStructure onUpdate_Return_AppMasterAdminStructure(AppMasterAdminStructure appMasterAdminStructure);

    //DELETE
    public void onDelete(AppMasterAdminStructure appMasterAdminStructure);
    public void onDeleteBy_amas_autoindex(long amas_autoindex);

    //SELECT
    public AppMasterAdminStructure onSelectBy_amas_autoindex(long amas_autoindex);
    public List<AppMasterAdminStructure> onSelectAllBy_amas_appmasteradmin_parent(AppMasterAdmin amas_appmasteradmin_parent);
    public List<AppMasterAdminStructure> onSelectAllBy_amas_appmasteradmin_chilld(AppMasterAdmin amas_appmasteradmin_child);

}
