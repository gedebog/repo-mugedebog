package services.administrator.app;

import com.avaje.ebean.Ebean;
import com.avaje.ebean.Model;
import com.avaje.ebean.PagedList;
import com.avaje.ebean.SqlRow;
import controllers.util.BCrypto;
import models.administrator.app.AppMasterAdmin;
import models.administrator.user.UserAccountAdmin;
import play.db.ebean.Transactional;
import services.administrator.user.IUserAccountAdmin;

import java.util.List;

/**
 * Created by tri.jaruto on 06/12/2016.
 */
public class AppMasterAdminLogic implements IAppMasterAdmin {

    public static Model.Finder<Long, AppMasterAdmin> find = new Model.Finder<Long,AppMasterAdmin>(AppMasterAdmin.class);

    @Override
    public void onSave(AppMasterAdmin appMasterAdmin) {
        appMasterAdmin.save();
    }

    @Override
    public long onSave_Return_ama_autoindex(AppMasterAdmin appMasterAdmin) {
        appMasterAdmin.save();
        return appMasterAdmin.getAma_autoindex();
    }

    @Override
    public AppMasterAdmin onSave_Return_AppMasterAdmin(AppMasterAdmin appMasterAdmin) {
        appMasterAdmin.save();
        return onSelectBy_ama_autoindex(appMasterAdmin.getAma_autoindex());
    }

    @Override
    public void onUpdate(AppMasterAdmin appMasterAdmin) {
        appMasterAdmin.update();
    }

    @Override
    public AppMasterAdmin onUpdate_Return_AppMasterAdmin(AppMasterAdmin appMasterAdmin) {
        appMasterAdmin.update();
        return onSelectBy_ama_autoindex(appMasterAdmin.getAma_autoindex());
    }

    @Override
    public void onDelete(AppMasterAdmin appMasterAdmin) {
        appMasterAdmin.delete();
    }

    @Override
    public void onDeleteBy_ama_autoindex(long ama_autoindex) {
        AppMasterAdmin appMasterAdmin = find.where().eq("ama_autoindex", ama_autoindex).findUnique();
        if (appMasterAdmin != null) {
            appMasterAdmin.delete();
        }
    }

    @Override
    public AppMasterAdmin onSelectBy_ama_autoindex(long ama_autoindex) {
        AppMasterAdmin appMasterAdmin = find.where().eq("ama_autoindex", ama_autoindex).findUnique();
        if (appMasterAdmin != null) {
            return appMasterAdmin;
        }
        return null;
    }

    @Override
    public List<AppMasterAdmin> onSelectAll() {
        List<AppMasterAdmin> appMasterAdmins = find.findList();
        if (!appMasterAdmins.isEmpty()) {
            return appMasterAdmins;
        }
        return null;
    }

    @Override
    public List<AppMasterAdmin> onSelectAllBy_ama_type(long ama_type) {
        List<AppMasterAdmin> appMasterAdmins = find.where().eq("ama_type", ama_type).orderBy("ama_autoindex").findList();
        if (!appMasterAdmins.isEmpty()) {
            return appMasterAdmins;
        }
        return null;
    }

    @Override
    public List<AppMasterAdmin> onSelectAllBy_ama_level(long ama_level) {
        List<AppMasterAdmin> appMasterAdmins = find.where().eq("ama_level", ama_level).orderBy("ama_autoindex").findList();
        if (!appMasterAdmins.isEmpty()) {
            return appMasterAdmins;
        }
        return null;
    }

    @Override
    public List<AppMasterAdmin> onSelectAllBy_ama_type_and_ama_level(long ama_type, long ama_level) {
        List<AppMasterAdmin> appMasterAdmins = find.where().eq("ama_type", ama_type).eq("ama_level", ama_level).orderBy("ama_autoindex").findList();
        if (!appMasterAdmins.isEmpty()) {
            return appMasterAdmins;
        }
        return null;
    }

    /*public void onUpdate_By_ua_autoindex(long ua_autoindex, UserAccount userAccount) {
        System.out.println("onUpdate_By_ua_autoindex userAccount.ua_isactive : "+userAccount.isUa_isactive());
        System.out.println("onUpdate_By_ua_autoindex ua_username.ua_username : "+userAccount.getUa_username());
        String sql = " update " +
                     " user_account " +
                     " set " +
                     " ua_username = :ua_username " +
                     " where " +
                     " ua_autoindex = :ua_autoindex";
        SqlUpdate update = Ebean.createSqlUpdate(sql)
                .setParameter("ua_username", "123123213")
                *//*.setParameter("ua_userpassword", userAccount.ua_userpassword)
                .setParameter("ua_isactive", true)
                .setParameter("ua_activationcode", userAccount.ua_activationcode)
                .setParameter("ua_isforgotpassword", userAccount.ua_isforgotpassword)
                .setParameter("ua_create", userAccount.ua_create)
                .setParameter("ua_updated", userAccount.ua_updated)*//*
                .setParameter("ua_autoindex", userAccount.getUa_autoindex());
        System.out.println("onUpdate_By_ua_autoindex userAccount.ua_isactive : "+userAccount.isUa_isactive());
        int rows = update.execute();
        System.out.println("onUpdate_By_ua_autoindex userAccount.ua_isactive : "+userAccount.getUa_username());
    }*/



}
