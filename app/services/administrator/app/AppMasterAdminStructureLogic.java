package services.administrator.app;

import com.avaje.ebean.Model;
import models.administrator.app.AppMasterAdmin;
import models.administrator.app.AppMasterAdminStructure;

import java.util.List;

/**
 * Created by tri.jaruto on 06/12/2016.
 */
public class AppMasterAdminStructureLogic implements IAppMasterAdminStructure {

    public static Model.Finder<Long, AppMasterAdminStructure> find = new Model.Finder<Long,AppMasterAdminStructure>(AppMasterAdminStructure.class);

    @Override
    public void onSave(AppMasterAdminStructure appMasterAdminStructure) {
        appMasterAdminStructure.save();
    }

    @Override
    public long onSave_Return_amas_autoindex(AppMasterAdminStructure appMasterAdminStructure) {
        appMasterAdminStructure.save();
        return appMasterAdminStructure.getAmas_autoindex();
    }

    @Override
    public AppMasterAdminStructure onSave_Return_AppMasterAdminStructure(AppMasterAdminStructure appMasterAdminStructure) {
        appMasterAdminStructure.save();
        return onSelectBy_amas_autoindex(appMasterAdminStructure.getAmas_autoindex());
    }

    @Override
    public void onUpdate(AppMasterAdminStructure appMasterAdminStructure) {
        appMasterAdminStructure.update();
    }

    @Override
    public AppMasterAdminStructure onUpdate_Return_AppMasterAdminStructure(AppMasterAdminStructure appMasterAdminStructure) {
        appMasterAdminStructure.update();
        return onSelectBy_amas_autoindex(appMasterAdminStructure.getAmas_autoindex());
    }

    @Override
    public void onDelete(AppMasterAdminStructure appMasterAdminStructure) {
        appMasterAdminStructure.delete();
    }

    @Override
    public void onDeleteBy_amas_autoindex(long ama_autoindex) {
        AppMasterAdminStructure appMasterAdminStructure = find.where().eq("amas_autoindex", ama_autoindex).findUnique();
        if (appMasterAdminStructure != null) {
            appMasterAdminStructure.delete();
        }
    }

    @Override
    public AppMasterAdminStructure onSelectBy_amas_autoindex(long ama_autoindex) {
        AppMasterAdminStructure appMasterAdminStructure = find.where().eq("amas_autoindex", ama_autoindex).findUnique();
        if (appMasterAdminStructure != null) {
            return appMasterAdminStructure;
        }
        return null;
    }

    @Override
    public List<AppMasterAdminStructure> onSelectAllBy_amas_appmasteradmin_parent(AppMasterAdmin amas_appmasteradmin_parent) {
        List<AppMasterAdminStructure> appMasterAdminStructures = find.where().eq("amas_appmasteradmin_parent", amas_appmasteradmin_parent).findList();
        if (!appMasterAdminStructures.isEmpty()) {
            return appMasterAdminStructures;
        }
        return null;
    }

    @Override
    public List<AppMasterAdminStructure> onSelectAllBy_amas_appmasteradmin_chilld(AppMasterAdmin amas_appmasteradmin_child) {
        List<AppMasterAdminStructure> appMasterAdminStructures = find.where().eq("amas_appmasteradmin_child", amas_appmasteradmin_child).findList();
        if (!appMasterAdminStructures.isEmpty()) {
            return appMasterAdminStructures;
        }
        return null;
    }
}
