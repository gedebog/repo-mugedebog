# --- Created by Ebean DDL
# To stop Ebean DDL generation, remove this comment and start using Evolutions

# --- !Ups

create table app_master (
  am_autoindex                  bigserial not null,
  am_name                       varchar(255),
  am_type                       smallint,
  am_level                      smallint,
  am_isgroup                    boolean,
  am_isactive                   boolean,
  am_created                    timestamp,
  am_creater                    varchar(255),
  am_updated                    timestamp,
  am_updater                    varchar(255),
  constraint pk_app_master primary key (am_autoindex)
);
comment on table app_master is 'Table AppMaster';

create table app_master_admin (
  ama_autoindex                 bigserial not null,
  ama_name                      varchar(255),
  ama_type                      smallint,
  ama_level                     smallint,
  ama_isgroup                   boolean,
  ama_isactive                  boolean,
  ama_created                   timestamp,
  ama_creater                   varchar(255),
  ama_updated                   timestamp,
  ama_updater                   varchar(255),
  constraint pk_app_master_admin primary key (ama_autoindex)
);
comment on table app_master_admin is 'Table AppMasterAdmin';

create table app_master_admin_structure (
  amas_autoindex                bigserial not null,
  amas_appmasteradmin_parent    bigint,
  amas_appmasteradmin_child     bigint,
  constraint pk_app_master_admin_structure primary key (amas_autoindex)
);
comment on table app_master_admin_structure is 'Table AppMasterAdminStructure';

create table app_master_form (
  amf_autoindex                 bigserial not null,
  amf_appmaster                 bigint,
  amf_name                      varchar(255),
  amf_filenamescalahtml         varchar(100) not null,
  amf_isactive                  boolean,
  amf_created                   timestamp,
  amf_creater                   varchar(255),
  amf_updated                   timestamp,
  amf_updater                   varchar(255),
  constraint uq_app_master_form_amf_appmaster unique (amf_appmaster),
  constraint uq_app_master_form_amf_filenamescalahtml unique (amf_filenamescalahtml),
  constraint pk_app_master_form primary key (amf_autoindex)
);
comment on table app_master_form is 'Table AppMasterForm';

create table app_master_form_admin (
  amfa_autoindex                bigserial not null,
  amfa_appmasteradmin           bigint,
  amfa_name                     varchar(255),
  amfa_filenamescalahtml        varchar(100) not null,
  amfa_isactive                 boolean,
  amfa_created                  timestamp,
  amfa_creater                  varchar(255),
  amfa_updated                  timestamp,
  amfa_updater                  varchar(255),
  constraint uq_app_master_form_admin_amfa_appmasteradmin unique (amfa_appmasteradmin),
  constraint uq_app_master_form_admin_amfa_filenamescalahtml unique (amfa_filenamescalahtml),
  constraint pk_app_master_form_admin primary key (amfa_autoindex)
);
comment on table app_master_form_admin is 'Table AppMasterFormAdmin';

create table app_master_organization (
  amo_autoindex                 bigserial not null,
  amo_code                      varchar(255),
  amo_name                      varchar(255),
  amo_isactive                  boolean,
  amo_created                   timestamp,
  amo_creater                   varchar(255),
  amo_updated                   timestamp,
  amo_updater                   varchar(255),
  constraint pk_app_master_organization primary key (amo_autoindex)
);
comment on table app_master_organization is 'Table AppMasterOrganization';

create table app_master_organization_structure (
  amos_autoindex                bigserial not null,
  amos_appmasterorganization_parent bigint,
  amos_appmasterorganization_child bigint,
  constraint pk_app_master_organization_structure primary key (amos_autoindex)
);
comment on table app_master_organization_structure is 'Table AppMasterOrganizationStructure';

create table app_master_permissions_form (
  ampf_autoindex                bigserial not null,
  ampf_useraccount              bigint,
  ampf_appmasterform            bigint,
  ampf_isnew                    boolean,
  ampf_isselect                 boolean,
  ampf_isinsert                 boolean,
  ampf_isupdate                 boolean,
  ampf_isdelete                 boolean,
  ampf_isprint                  boolean,
  ampf_isexport                 boolean,
  ampf_isimport                 boolean,
  ampf_isactive                 boolean,
  ampf_created                  timestamp,
  ampf_creater                  varchar(255),
  ampf_updated                  timestamp,
  ampf_updater                  varchar(255),
  constraint pk_app_master_permissions_form primary key (ampf_autoindex)
);
comment on table app_master_permissions_form is 'Table AppMasterPermissionsForm';

create table app_master_permissions_form_admin (
  ampfa_autoindex               bigserial not null,
  ampfa_useraccountadmin        bigint,
  ampfa_appmasterformadmin      bigint,
  ampfa_isnew                   boolean,
  ampfa_isselect                boolean,
  ampfa_isinsert                boolean,
  ampfa_isupdate                boolean,
  ampfa_isdelete                boolean,
  ampfa_isprint                 boolean,
  ampfa_isexport                boolean,
  ampfa_isimport                boolean,
  ampfa_isactive                boolean,
  ampfa_created                 timestamp,
  ampfa_creater                 varchar(255),
  ampfa_updated                 timestamp,
  ampfa_updater                 varchar(255),
  constraint pk_app_master_permissions_form_admin primary key (ampfa_autoindex)
);
comment on table app_master_permissions_form_admin is 'Table AppMasterPermissionsFormAdmin';

create table app_master_structure (
  ams_autoindex                 bigserial not null,
  ams_appmaster_parent          bigint,
  ams_appmaster_child           bigint,
  constraint pk_app_master_structure primary key (ams_autoindex)
);
comment on table app_master_structure is 'Table AppMasterStructure';

create table user_account (
  ua_autoindex                  bigserial not null,
  ua_username                   varchar(100) not null,
  ua_userpassword               varchar(100) not null,
  ua_isactive                   boolean,
  ua_activationcode             varchar(255),
  ua_isforgotpassword           boolean,
  ua_created                    timestamp,
  ua_creater                    varchar(255),
  ua_updated                    timestamp,
  ua_updater                    varchar(255),
  constraint uq_user_account_ua_username unique (ua_username),
  constraint pk_user_account primary key (ua_autoindex)
);
comment on table user_account is 'Table UserAccount';

create table user_account_admin (
  uaa_autoindex                 bigserial not null,
  uaa_username                  varchar(100) not null,
  uaa_userpassword              varchar(100) not null,
  uaa_usertype                  smallint,
  uaa_isactive                  boolean,
  uaa_activationcode            varchar(255),
  uaa_isforgotpassword          boolean,
  uaa_created                   timestamp,
  uaa_creater                   varchar(255),
  uaa_updated                   timestamp,
  uaa_updater                   varchar(255),
  constraint uq_user_account_admin_uaa_username unique (uaa_username),
  constraint pk_user_account_admin primary key (uaa_autoindex)
);
comment on table user_account_admin is 'Table UserAccountAdmin';

create table user_foto_profile (
  ufp_autoindex                 bigserial not null,
  ufp_useraccount               bigint,
  ufp_name                      varchar(255),
  ufp_status                    varchar(255),
  ufp_note                      varchar(255),
  ufp_filename                  varchar(255),
  ufp_created                   timestamp,
  ufp_creater                   varchar(255),
  ufp_updated                   timestamp,
  ufp_updater                   varchar(255),
  constraint uq_user_foto_profile_ufp_useraccount unique (ufp_useraccount),
  constraint pk_user_foto_profile primary key (ufp_autoindex)
);
comment on table user_foto_profile is 'Table UserFotoProfile';

create table user_foto_profile_admin (
  ufpa_autoindex                bigserial not null,
  ufpa_useraccountadmin         bigint,
  ufpa_name                     varchar(255),
  ufpa_status                   varchar(255),
  ufpa_note                     varchar(255),
  ufpa_filename                 varchar(255),
  ufpa_created                  timestamp,
  ufpa_creater                  varchar(255),
  ufpa_updated                  timestamp,
  ufpa_updater                  varchar(255),
  constraint uq_user_foto_profile_admin_ufpa_useraccountadmin unique (ufpa_useraccountadmin),
  constraint pk_user_foto_profile_admin primary key (ufpa_autoindex)
);
comment on table user_foto_profile_admin is 'Table UserFotoProfileAdmin';

create table user_profile (
  up_autoindex                  bigserial not null,
  up_useraccount                bigint,
  up_name                       varchar(255),
  up_nickname                   varchar(255),
  up_birthdate                  date,
  up_birthplace                 varchar(255),
  up_isgender                   boolean,
  up_created                    timestamp,
  up_creater                    varchar(255),
  up_updated                    timestamp,
  up_updater                    varchar(255),
  constraint uq_user_profile_up_useraccount unique (up_useraccount),
  constraint pk_user_profile primary key (up_autoindex)
);
comment on table user_profile is 'Table UserProfile';

create table user_profile_admin (
  upa_autoindex                 bigserial not null,
  upa_useraccountadmin          bigint,
  upa_name                      varchar(255),
  upa_nickname                  varchar(255),
  upa_birthdate                 date,
  upa_birthplace                varchar(255),
  upa_isgender                  boolean,
  upa_created                   timestamp,
  upa_creater                   varchar(255),
  upa_updated                   timestamp,
  upa_updater                   varchar(255),
  constraint uq_user_profile_admin_upa_useraccountadmin unique (upa_useraccountadmin),
  constraint pk_user_profile_admin primary key (upa_autoindex)
);
comment on table user_profile_admin is 'Table UserProfileAdmin';

alter table app_master_admin_structure add constraint fk_app_master_admin_structure_amas_appmasteradmin_parent foreign key (amas_appmasteradmin_parent) references app_master_admin (ama_autoindex) on delete restrict on update restrict;
create index ix_app_master_admin_structure_amas_appmasteradmin_parent on app_master_admin_structure (amas_appmasteradmin_parent);

alter table app_master_admin_structure add constraint fk_app_master_admin_structure_amas_appmasteradmin_child foreign key (amas_appmasteradmin_child) references app_master_admin (ama_autoindex) on delete restrict on update restrict;
create index ix_app_master_admin_structure_amas_appmasteradmin_child on app_master_admin_structure (amas_appmasteradmin_child);

alter table app_master_form add constraint fk_app_master_form_amf_appmaster foreign key (amf_appmaster) references app_master (am_autoindex) on delete restrict on update restrict;

alter table app_master_form_admin add constraint fk_app_master_form_admin_amfa_appmasteradmin foreign key (amfa_appmasteradmin) references app_master_admin (ama_autoindex) on delete restrict on update restrict;

alter table app_master_organization_structure add constraint fk_app_master_organization_structure_amos_appmasterorgani_1 foreign key (amos_appmasterorganization_parent) references app_master_organization (amo_autoindex) on delete restrict on update restrict;
create index ix_app_master_organization_structure_amos_appmasterorgani_1 on app_master_organization_structure (amos_appmasterorganization_parent);

alter table app_master_organization_structure add constraint fk_app_master_organization_structure_amos_appmasterorgani_2 foreign key (amos_appmasterorganization_child) references app_master_organization (amo_autoindex) on delete restrict on update restrict;
create index ix_app_master_organization_structure_amos_appmasterorgani_2 on app_master_organization_structure (amos_appmasterorganization_child);

alter table app_master_permissions_form add constraint fk_app_master_permissions_form_ampf_useraccount foreign key (ampf_useraccount) references user_account (ua_autoindex) on delete restrict on update restrict;
create index ix_app_master_permissions_form_ampf_useraccount on app_master_permissions_form (ampf_useraccount);

alter table app_master_permissions_form add constraint fk_app_master_permissions_form_ampf_appmasterform foreign key (ampf_appmasterform) references app_master_form (amf_autoindex) on delete restrict on update restrict;
create index ix_app_master_permissions_form_ampf_appmasterform on app_master_permissions_form (ampf_appmasterform);

alter table app_master_permissions_form_admin add constraint fk_app_master_permissions_form_admin_ampfa_useraccountadmin foreign key (ampfa_useraccountadmin) references user_account_admin (uaa_autoindex) on delete restrict on update restrict;
create index ix_app_master_permissions_form_admin_ampfa_useraccountadmin on app_master_permissions_form_admin (ampfa_useraccountadmin);

alter table app_master_permissions_form_admin add constraint fk_app_master_permissions_form_admin_ampfa_appmasterforma_2 foreign key (ampfa_appmasterformadmin) references app_master_form_admin (amfa_autoindex) on delete restrict on update restrict;
create index ix_app_master_permissions_form_admin_ampfa_appmasterforma_2 on app_master_permissions_form_admin (ampfa_appmasterformadmin);

alter table app_master_structure add constraint fk_app_master_structure_ams_appmaster_parent foreign key (ams_appmaster_parent) references app_master (am_autoindex) on delete restrict on update restrict;
create index ix_app_master_structure_ams_appmaster_parent on app_master_structure (ams_appmaster_parent);

alter table app_master_structure add constraint fk_app_master_structure_ams_appmaster_child foreign key (ams_appmaster_child) references app_master (am_autoindex) on delete restrict on update restrict;
create index ix_app_master_structure_ams_appmaster_child on app_master_structure (ams_appmaster_child);

alter table user_foto_profile add constraint fk_user_foto_profile_ufp_useraccount foreign key (ufp_useraccount) references user_account (ua_autoindex) on delete restrict on update restrict;

alter table user_foto_profile_admin add constraint fk_user_foto_profile_admin_ufpa_useraccountadmin foreign key (ufpa_useraccountadmin) references user_account_admin (uaa_autoindex) on delete restrict on update restrict;

alter table user_profile add constraint fk_user_profile_up_useraccount foreign key (up_useraccount) references user_account (ua_autoindex) on delete restrict on update restrict;

alter table user_profile_admin add constraint fk_user_profile_admin_upa_useraccountadmin foreign key (upa_useraccountadmin) references user_account_admin (uaa_autoindex) on delete restrict on update restrict;


# --- !Downs

alter table if exists app_master_admin_structure drop constraint if exists fk_app_master_admin_structure_amas_appmasteradmin_parent;
drop index if exists ix_app_master_admin_structure_amas_appmasteradmin_parent;

alter table if exists app_master_admin_structure drop constraint if exists fk_app_master_admin_structure_amas_appmasteradmin_child;
drop index if exists ix_app_master_admin_structure_amas_appmasteradmin_child;

alter table if exists app_master_form drop constraint if exists fk_app_master_form_amf_appmaster;

alter table if exists app_master_form_admin drop constraint if exists fk_app_master_form_admin_amfa_appmasteradmin;

alter table if exists app_master_organization_structure drop constraint if exists fk_app_master_organization_structure_amos_appmasterorgani_1;
drop index if exists ix_app_master_organization_structure_amos_appmasterorgani_1;

alter table if exists app_master_organization_structure drop constraint if exists fk_app_master_organization_structure_amos_appmasterorgani_2;
drop index if exists ix_app_master_organization_structure_amos_appmasterorgani_2;

alter table if exists app_master_permissions_form drop constraint if exists fk_app_master_permissions_form_ampf_useraccount;
drop index if exists ix_app_master_permissions_form_ampf_useraccount;

alter table if exists app_master_permissions_form drop constraint if exists fk_app_master_permissions_form_ampf_appmasterform;
drop index if exists ix_app_master_permissions_form_ampf_appmasterform;

alter table if exists app_master_permissions_form_admin drop constraint if exists fk_app_master_permissions_form_admin_ampfa_useraccountadmin;
drop index if exists ix_app_master_permissions_form_admin_ampfa_useraccountadmin;

alter table if exists app_master_permissions_form_admin drop constraint if exists fk_app_master_permissions_form_admin_ampfa_appmasterforma_2;
drop index if exists ix_app_master_permissions_form_admin_ampfa_appmasterforma_2;

alter table if exists app_master_structure drop constraint if exists fk_app_master_structure_ams_appmaster_parent;
drop index if exists ix_app_master_structure_ams_appmaster_parent;

alter table if exists app_master_structure drop constraint if exists fk_app_master_structure_ams_appmaster_child;
drop index if exists ix_app_master_structure_ams_appmaster_child;

alter table if exists user_foto_profile drop constraint if exists fk_user_foto_profile_ufp_useraccount;

alter table if exists user_foto_profile_admin drop constraint if exists fk_user_foto_profile_admin_ufpa_useraccountadmin;

alter table if exists user_profile drop constraint if exists fk_user_profile_up_useraccount;

alter table if exists user_profile_admin drop constraint if exists fk_user_profile_admin_upa_useraccountadmin;

drop table if exists app_master cascade;

drop table if exists app_master_admin cascade;

drop table if exists app_master_admin_structure cascade;

drop table if exists app_master_form cascade;

drop table if exists app_master_form_admin cascade;

drop table if exists app_master_organization cascade;

drop table if exists app_master_organization_structure cascade;

drop table if exists app_master_permissions_form cascade;

drop table if exists app_master_permissions_form_admin cascade;

drop table if exists app_master_structure cascade;

drop table if exists user_account cascade;

drop table if exists user_account_admin cascade;

drop table if exists user_foto_profile cascade;

drop table if exists user_foto_profile_admin cascade;

drop table if exists user_profile cascade;

drop table if exists user_profile_admin cascade;

